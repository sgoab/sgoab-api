#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

import requests
import json
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image


def make_xywh_from_ratio(bbx: list, im_width, im_height):
    x_pixel = bbx[1] * im_width
    y_pixel = bbx[0] * im_height
    w_pixel = int(bbx[3]*im_width)-int(bbx[1]*im_width)
    h_pixel = int(bbx[2]*im_height)-int(bbx[0]*im_height)
    return x_pixel, y_pixel, w_pixel, h_pixel


def is_ratio(bbx: list):
    for i in bbx:
        if float(i) > 1:
            return False
    return True


def output_plt_bbx_full(img_path: str, bbxes: list, classnames: list, img_is_URL: bool = False):
    """[Creates an image (matplotlib.pyplot) with the bounding boxes from the object-detection.]

    Args:
        img_path (str): [image path. It can be URL or local path.]
        TF_serving_query_output (dict): [output from TF_serving_object_detection()]
        img_is_URL (bool, optional): [True if img_path is a URL. Otherwise, False.]. Defaults to False.

    Returns:
        [None]: [Returns nothing.]
    """

    fig = plt.figure(figsize=(30, 40), dpi=180)
    ax = fig.subplots()

    # load the image
    image = None
    if img_is_URL:
        # URL image

        resp = requests.get(img_path, stream=True)
        try:
            import io
            image = Image.open(io.BytesIO(resp.content))
        except Exception:
            from urllib.request import urlopen
            image = Image.open(urlopen(img_path))

    else:
        # localpath image
        image = Image.open(img_path)

    im_width, im_height = image.size
    ax.imshow(image)

    for i in range(0, len(classnames)):
        bbx = bbxes[i]
        classname = classnames[i]

        # for a bounding box
        if is_ratio(bbx):
            x_pixel, y_pixel, w_pixel, h_pixel = make_xywh_from_ratio(
                bbx, im_width, im_height)
        else:
            # TBD
            print("Better to use RATIO as an input. Not pixels for the bounding boxes!!")
            x_pixel = bbx[1]
            y_pixel = bbx[0]
            w_pixel = bbx[3] - bbx[1]
            h_pixel = bbx[2] - bbx[0]
            pass

        xy = (float(x_pixel), float(y_pixel))

        rect = patches.Rectangle(xy, w_pixel, h_pixel,
                                 linewidth=3, edgecolor="g", facecolor='none')
        ax.annotate(classname, xy, color="w", size=20)
        ax.add_patch(rect)
    return plt


def output_plt_save(figure_plt: plt, img_path: str, fname: str = '', outdir: str = '/tmp/') -> str:
    """[Saves locally the image (matplotlib.pyplot) with the bounding boxes.]

    Args:
        figure_plt (plt): [description]
        img_path (str): [description]
        outdir (str, optional): [description]. Defaults to SGOAB_API_ENRICHMENTS_OUTPUT_DIR.

    Returns:
        str: [public URL of the saved image with the bounding boxes. Accessible URL in the web-services server.]
    """
    # savefig(filename, dpi=None, format='png', bbox_inches='tight', pad_inches=0.2, bbox=None, pad=None, dashes=None, loc='upper left', rot=0, vmax='I', vmin='I', hmax='I', hmin='I')
    import base64
    #filename = base64.b64encode(img_path.encode('utf-8'))
    import re
    fname = re.sub(r'[^A-Za-z]', '', img_path)
    if fname == '':
        filepath_local = "{dir}{fname}.jpg".format(
            dir=outdir, fname=fname)
    else:
        filepath_local = "{dir}{fname}.jpg".format(
            dir=outdir, fname=fname)

    figure_plt.axis('off')
    figure_plt.savefig(filepath_local, dpi=60, pil_kwargs={
                       'quality': 40}, bbox_inches='tight')
    figure_plt.close()

    return filepath_local


def test_generate_images_one_bbx(enrich_json: dict, outdir: str) -> tuple:

    imgpath = enrich_json["img_path"]
    out_bbxs = enrich_json["output"]["object_detection"]["bbx"]
    classnames_all = enrich_json["output"]["object_detection"]["classes-labels"]

    fig = output_plt_bbx_full(
        imgpath, out_bbxs, classnames_all, img_is_URL=True)
    outfile_path = output_plt_save(
        fig, imgpath, outdir=outdir)

    return imgpath, outfile_path


def json_n_outdir():

    import argparse

    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Optional app description')
    # Required positional argument
    parser.add_argument('json_filepath', type=str,
                        help='A required integer positional argument')
    parser.add_argument('outdir', type=str,
                        help='A required integer positional argument')
    args = parser.parse_args()

    input_file = args.json_filepath
    outdir = args.outdir

    # start generating
    enrich_json = None
    with open(input_file) as fp:
        enrich_json = json.load(fp)
    imgpath, outfile_path = test_generate_images_one_bbx(enrich_json, outdir)
    return imgpath, outfile_path


if __name__ == '__main__':

    imgpath, outfile_path = json_n_outdir()

    out_text = """
    Results:
    Img URL:\t{}
    Img with bounding boxes:\t{}
    """

    print(out_text.format(imgpath, outfile_path))
