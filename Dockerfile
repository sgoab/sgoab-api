# Use the following if your tool runs on Python 3
FROM python:3.8

RUN mkdir -p /tmp/object-detection/output/

COPY ./src/ /super/
COPY ./requirements.txt /super/requirements.txt
# generate required libraries in 'requirements.txt' file
#RUN pip install pipreqs
#RUN pipreqs /super/

# install python libraries defined by 'requirements.txt' file
RUN pip --no-cache-dir install -r /super/requirements.txt

#define work directory
WORKDIR /super

CMD ["python", "-u", "server_api.py"]
