# Saint George on a Bike API - SGoaB-API (BSC)

## Description of the tool
This project implements the SGoaB API. 
It allows querying the Web Services:
The Web Services currently are:
- Object Detection
- Visual Relation (TO-BE-UPDATED)
- NLP (TO-DO?)

## Authorization

Ask the project responsible for a Token.

## API Requests

```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
'{
  "item": "http://data.europeana.eu/item/15501/003841",
  "enrichments":["object-detection"],
  "output_profile":"EF-ANNOTATION-SIMPLE"
}' \
 'http://localhost:5050/api/enrichment'
```

## Tool status

Release Candidate: v0.1-beta

Approved: -

NEW features: 

- WS (object-detection)
- HTTP Auhtentication
- Object-detection model updated.
- Output with Collections of Web annotation

## API Documentation

### Object detection 

Input: 

- Image URL (image formats: png/jpg/jpeg)

Output: 

- JSON file with:
  - Bounding boxes (pixels)
  - Classes recognized 
    - ID
    - Label
  - Score for each class

### Visual Relation

Input: 

- Image URL (image formats: png/jpg/jpeg)

Output: 

- [ JSON | JSON-LD ] output with:
  - Bounding boxes (pixels)
  - Classes recognized 
    - ID
    - Label
  - Score for each class

## Documentation

We have split the documentation into two separated READMEs:

* **[Technical README](README_technical_part.md)** about how to set up the tool
* **[User README](README_user_part.md)** about how to use the tool


If we have omitted any relevant information that prevents you from understanding or running the tool following this documentation, you can contact us using the maintainers list below.

## Maintainers

| Email address                                     |
|---------------------------------------------------|
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es)  |


## License

[BSC License]()
