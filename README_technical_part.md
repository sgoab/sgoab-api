# Saint George on a Bike (BSC)

**This README explains how to set up the tools: build containers, etc.**

These services offer generate metadata enrichments for paintings between s.XII and s.XVIII.
The offered enrichments that can be generated are:
- object detection
- visual relations

## Table of Contents
- [Saint George on a Bike (BSC)](#saint-george-on-a-bike-bsc)
  - [Table of Contents](#table-of-contents)
  - [How to set up](#how-to-set-up)
    - [Prerequisites](#prerequisites)
    - [Create a base directory](#create-a-base-directory)
    - [SgoaB-API docker container](#sgoab-api-docker-container)
    - [SgoaB object-detection docker container](#sgoab-object-detection-docker-container)
    - [Configure environment](#configure-environment)
    - [Run the environment](#run-the-environment)
    - [Execution](#execution)
    - [Run Test Cases](#run-test-cases)
  - [Maintainers](#maintainers)
  - [License](#license)

## How to set up

### Prerequisites

- Having docker and docker-compose installed. (tested with Docker version 20.10.21)
  
- Having access to repositories:
  - https://gitlab.bsc.es/sgoab/sgoab-api.git
  - https://gitlab.bsc.es/sgoab/TF-Serving.git

### Create a base directory

 * Create a directory ```sgoab``` (e.g. ```/absolute/path/sgoab```).
 * Create a directory ```output``` (e.g. ```/absolute/path/sgoab/output```)


### SgoaB-API docker container

1. Enter the directory ```sgoab```
2. Clone the respository
```bash
git clone -b development https://gitlab.bsc.es/sgoab/sgoab-api.git
```
3. Enter sgoab-api directory
```bash
cd sgoab-api
```
5. Build the docker image
```bash
cd sgoab-api
docker build --tag sgoab-api:v0.1b .
```

### SgoaB object-detection docker container

1. Enter the directory ```sgoab```
2. Clone the respository
```bash
git clone https://gitlab.bsc.es/sgoab/TF-Serving.git
```
3. Get the Tensorflow server
```bash
docker pull tensorflow/serving:2.5.1
```

### Configure environment

Set the environment variables in the file ```.env``` (e.g. /absolute/path/sgoab/sgoab-api/.env).
Find more information about the .env syntax [here](https://docs.docker.com/compose/env-file/#syntax).

- SGoaB server 
  - ```SGOAB_API_HOME_DIR``` : Absolute path where you cloned the sgoab-api (e.g. /absolute/path/sgoab/sgoab-api)
  - ```SGOAB_API_PORT``` : [ Optional ] PORT of the SGoaB API endpoint address (default: 5050)
  - ```SGOAB_API_ENRICHMENTS_OUTPUT_DIR```: Output directory.  Absolute path to the directory where the enrichments output files will be stored. (e.g./absolute/path/sgoab/output) .
- Tensorflow server
  - ```TF_SERVER_HOME_DIR``` : Absolute path where you cloned the Tensorflow server (e.g. /absolute/path/sgoab/TF-Serving)
- Europeana Foundation Record API URL
  - ```EF_API_URL``` : [ Optional ] URL of the EUROPEANA API with this format (default: ```https://api.europeana.eu/record/v2```)
  - ```EF_API_KEY``` : KEY of the EUROPEANA API . Contact Europeana [here](https://pro.europeana.eu/pages/get-api') if you don't already have one (it may be freely).

An example of .env file content would be:

```bash
# SGOAB API
SGOAB_API_HOME_DIR=/absolute/path/sgoab/sgoab-api
SGOAB_API_PORT=5050
SGOAB_API_ENRICHMENTS_OUTPUT_DIR=/absolute/path/sgoab/output
# TF-SERVING
TF_SERVER_HOME_DIR=/absolute/path/sgoab/TF-Serving
TF_SERVER_PORT=8501
# Europeana Foundation Search API
EF_API_URL=https://api.europeana.eu/record/v2
EF_API_KEY=YOUR_EF_API_KEY
```

### Run the environment

1. Enter the ```sgoab-api``` directory
```bash 
cd sgoab/sgoab-api
```
2. Start the docker containers
   
```bash 
docker-compose up -d --remove-orphans
```

### Execution

Check that environment configuration runs properly.

1. Execute this command in terminal.

```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
'{
  "item": "http://data.europeana.eu/item/15501/003841",
  "enrichments":["object-detection"],
  "output_profile":"EF-ANNOTATION-COMPLETE"
}' \
 'http://localhost:5050/api/enrichment'
```

2. Verify that the response from the SGoaB API has the following format:

```bash
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 7748
Server: Werkzeug/2.0.2 Python/3.8.13
Date: Wed, 01 Nov 2022 10:15:53 GMT

{
  "@context": "http://www.w3.org/ns/anno.jsonld", 
  "first": {
    "id": "http://saintgeorgeonabike.eu/page/1", 
    "items": [
      {
        "@context": {
          "@base": "http://www.w3.org/ns/anno.jsonld", 
          "confidenceLevel": "http://www.europeana.eu/schemas/edm/confidenceLevel"
        }, 
        "body": {
          "confidenceLevel": "0.99714762", 
          "language": "en", 
          "type": "TextualBody", 
          "value": "person"
        }, 
        "created": "2022-07-10T14:08:07Z", 
        "creator": {
          "homepage": "https://saintgeorgeonabike.eu/", 
          "name": "SGoaB", 
          "type": "Software"
        }, 
        "id": "http://saintgeorgeonabike.eu/annotations/20221109101553419577/1", 
        "motivation": "tagging", 
        "target": {
          "scope": "http://data.europeana.eu/item/15501/003841", 
          "source": "https://realonline.imareal.sbg.ac.at/imageservice/kupo/DA005093#xywh=174,554,311,261"
          },
        }, 
        "type": "Annotation"
      }, 
      ...
      ...
    ], 
    "startIndex": 0, 
    "type": "AnnotationPage"
  }, 
  "id": "http://saintgeorgeonabike.eu/collection/20221109101553420616/1", 
  "total": 8, 
  "type": "AnnotationCollection"
}
```

### Run Test Cases

See [User Guide](./README_user_part.md).

## Maintainers

| Email address                                    |
| ------------------------------------------------ |
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es) |
|                                                  |

## License

[SGOAB License](LICENSE.md)
