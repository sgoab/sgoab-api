#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################



from asyncore import write
import unittest
import numpy as np
import json

# tested modules
from vis_rel import img_data_model as vr_idm
#from vis_rel import bbx_visrel_prod as bbx_visrel
from enrichment import object_detection as od

# utils libraries
import libaux

from service_interface import ANNOTATION_SIMPLE, ANNOTATION_COMPLETE


def create_input(item, output_default: str = ANNOTATION_SIMPLE):
    return {
        "item": item,
        "enrichments": ['object-detection'],
        "output-format": ANNOTATION_SIMPLE
    }


class TestServiceInterface(unittest.TestCase):

    @unittest.skip("skipping test_enrichments_europeana")
    # test vis-rel model generation
    def test_enrichments_europeana_single(self):
        import service_interface
        si = service_interface.ServiceInterface()

        europeana_id = "/07101/O_156"

        input = create_input(europeana_id)
        output = si.enrichments_europeana(input)
        print("{}\n{}\n{}\n".format(20*"*", libaux.jsonify_visrel(output), 20*"*"))

        pass

    # @unittest.skip("skipping test_enrichments_europeana")
    # test vis-rel model generation
    def test_enrichments_europeana_large(self):

        import service_interface
        si = service_interface.ServiceInterface()

        europeana_id_list_path = "/home/smendoza/local/sgoab/sgoab-api/data/test/europeana_id_list.json"
        enrichments_output_filepath = "/home/smendoza/local/sgoab/sgoab-api/data/test/test-service_interface.out"
        europeana_id_list_ = []
        with open(europeana_id_list_path, 'r') as eidfile:
            europeana_id_list = json.load(eidfile)

        for europeana_id in europeana_id_list:
            input = create_input(europeana_id)
            output = si.unique_endpoint_enrichment(
                input["item"], input["enrichments"])
            outline = "{}\n{}\n{}\n".format(
                20*"*", libaux.jsonify_visrel(output), 20*"*")
            enrichments_output_filepath = "/home/smendoza/local/sgoab/sgoab-api/data/test/output/enrichments-output/enrichments-{}.out".format(
                europeana_id.replace('/', '_'))
            with open(enrichments_output_filepath, 'w') as outfile:
                outfile.write(outline)
            # print(outline)
            # outfile.writelines([outline])
        pass

    # @unittest.skip("skipping test_enrichments_europeana")
    # test vis-rel model generation

    def test_enrichments_europeana_large(self):

        import service_interface
        si = service_interface.ServiceInterface()

        europeana_id_list_path = "/home/smendoza/local/sgoab/sgoab-api/data/test/ele_in_1.csv"
        enrichments_output_filepath = "/home/smendoza/local/sgoab/sgoab-api/data/test/ele_1.1.out"
        europeana_id_list_ = []
        with open(europeana_id_list_path, 'r') as eidfile:
            europeana_id_list = json.load(eidfile)
            with open(enrichments_output_filepath, 'w+') as outfile:
                for europeana_id in europeana_id_list:
                    input = create_input(europeana_id)
                    output = si.unique_endpoint_enrichment(
                        input["item"], input["enrichments"])
                    outline = "{}".format(libaux.jsonify_visrel(output))

                    outfile.write(outline)
                    # print(outline)
                    # outfile.writelines([outline])
        pass


if __name__ == '__main__':
    unittest.main()
