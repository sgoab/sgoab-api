#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


import unittest
import numpy as np
import json

# tested modules
from vis_rel import img_data_model as vr_idm
#from vis_rel import bbx_visrel_prod as bbx_visrel
from vis_rel import bbx_visrel as bbx_visrel
from enrichment import object_detection as od

# utils libraries
import libaux


def ratio_to_px(input_bbxs: list, im_width: int, im_height: int) -> list:
    # convert ratio:float to px:int (bounding boxes)
    # conversion round down
    for bbx in input_bbxs:
        bbx[0] = int(bbx[0]*im_width)  # 'xmin'
        bbx[1] = int(bbx[1]*im_height)  # 'ymin'
        bbx[2] = int(bbx[2]*im_width)  # 'xmax'
        bbx[3] = int(bbx[3]*im_height)  # 'ymax'
    return input_bbxs


class TestBbxVisRel(unittest.TestCase):

    # library to be testes

    # @unittest.skip('skip this')
    # test vis-rel model generation
    def test_img_data_model(self):
        """
        @param      detection_output: json file obtained from R-CNN processing
        @type       dict

        @param      width: image width in pixels
        @type       int

        @param      height: image height in pixel
        @type       int

        @param     img_dict: image metadata and computed metrics
                {
                    'size': {'width':width,'height':height},
                    'bbx': {ulabel1: {'xmin':xmin1,
                                        'ymin':ymin1,
                                        'xmax':xmax1,
                                        'ymax':ymax1,
                                        'cpt':{'xc':xc1,'yc':yc1},
                                        'oloc':oloc1,
                                        'rsa':rsa1,
                                        'off':off1,
                                        'score':score1
                                        },
                            ulabel2: {'xmin':xmin2,
                                        'ymin':ymin2,
                                        'xmax':xmax2,
                                        'ymax':ymax2,
                                        'cpt':{'xc':xc2,'yc':yc2},
                                        'oloc':oloc2,
                                        'rsa':rsa2,
                                        'off':off2,
                                        'score':score2
                                        },
                            ulabel3: {...}
                            },
                    'bbx_cnt': bbx_cnt
                }
        @type      dict

        @param     vra, visual relationship array between bbxes pairwise
                Non diagonal elts are lists of length 4, diagonal elts are lists of length 5.
        """

        # img_path = "/home/smendoza/local/sgoab/sgoab-api/data/test_vis-rel/0000001.jpg"
        json_path = "/home/smendoza/local/sgoab/sgoab-api/data/test_vis-rel/0000001.json"
        img_path = "/home/smendoza/local/sgoab/sgoab-api/data/test_vis-rel/00004730.jpg"
        test_vra_path = "/home/smendoza/local/sgoab/sgoab-api/data/test_vis-rel/00004730.vra"

        im_width, im_height = libaux.image_width_height(img_path)

        """
        output_object_detection = None
        with open(json_path) as fjson:
            output_object_detection = json.load(fjson)
        """
        output_object_detection = output_object_detection = od.TF_serving_object_detection(
            img_path, img_is_URL=False, save_bbx_img=False)

        output_object_detection['bbx'] = ratio_to_px(
            output_object_detection['bbx'], im_width, im_height)

        print("OBJECT_DETECTION: {}".format(
            json.dumps(output_object_detection)))

        img_dict, vra = vr_idm.img_data_model(
            output_object_detection, im_width, im_height)
        print("VIS-REL ## IMG_DICT: {}".format(json.dumps(img_dict)))
        print("VIS-REL ## VRA: {}".format(json.dumps(vra.tolist())))

        with open(test_vra_path, 'rb') as file_from:
            # hyperparametric array
            vra = np.load(file_from, allow_pickle=True)
            class_lst = [vra[idx][idx][0]
                         for idx in range(len(vra))]  # class labels
            print(" >> VRA-TEST: {}".format(vra))
            # assert(class_lst==vr_idm.LABEL_MAP)

    #@unittest.skip('skip this')
    # test vis-rel model generation
    def test_img_data_model(self):

        def bbx_vis_rel(width: int, height: int, img_dict: dict, vra: np.ndarray) -> dict:
            '''
            bbx_vis_rel():
            Computes caption seeds and compositional data.

            Input:
            @param      width: image width in pixels
            @type       int

            @param      height: image height in pixels
            @type       int

            @param      img_dict: processed image metadata and computed metrics
            @type:      json formatted dict

            @param      vra: visual relationship array
            @type       np.ndarray

            Output:
            @param      vrd: visual relationships dictionary
            @type       dict formated as json
            '''
        #img_path = "/home/smendoza/local/sgoab/sgoab-api/data/test_vis-rel/00004730.jpg"
        #img_path = "/home/smendoza/local/sgoab/sgoab-api/data/21knight.jpg"
        img_path = "/home/smendoza/local/sgoab/sgoab-api/data/00071501.jpeg"

        # get image width and height
        im_width, im_height = libaux.image_width_height(img_path)

        output_object_detection = output_object_detection = od.TF_serving_object_detection(
            img_path, img_is_URL=False, save_bbx_img=False, score_threshold=0.5)

        output_object_detection['bbx'] = ratio_to_px(
            output_object_detection['bbx'], im_width, im_height)

        # get img_dict: image metadata and computed metrics
        # get vra, visual relationship array
        img_dict, vra = vr_idm.img_data_model(
            output_object_detection, im_width, im_height)

        output_bbx_vis_rel = bbx_visrel.bbx_vis_rel(
            im_width, im_height, img_dict, vra)

        assert (isinstance(output_bbx_vis_rel, dict))
        print(type(output_bbx_vis_rel))
        print(output_bbx_vis_rel)

        print("bbx_visrel.bbx_vis_rel() >> {}".format(
            libaux.jsonify_visrel(output_bbx_vis_rel)))

        pass


if __name__ == '__main__':
    unittest.main()
