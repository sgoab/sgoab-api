#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


from apiflask.validators import Length, OneOf, URL
from apiflask.validators import URL as URLValidator

from apiflask.fields import Integer, String, List, Boolean, Dict
from apiflask.fields import URL
from apiflask import Schema, abort

class EnrichmentHeaders(Schema):
    bbx = List()
    img_path = String()
    object_detection = Boolean,
    visual_relations = Boolean,
    output = Dict()

class EnrichmentObjectDetection(Schema):
    bbx = List()
    img_path = String()
    object_detection = Boolean,
    visual_relations = Boolean,
    output = Dict()

class EnrichmentInSchema(Schema):
    items = List(URL(required=True, validate=URLValidator(
        relative=False, schemes=["http", "https"])))
    object_detection = Boolean(required=True, validate=OneOf([True, False]))
    visual_relations = Boolean(required=True, validate=OneOf([True, False]))

class EnrichmentOutSchema(Schema):
    queryID = String()
    img_path = String()
    object_detection = Boolean,
    visual_relations = Boolean,
    output = Dict()

example_enrichment_input = {
    "items": [
        "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
        "https://i0.wp.com/www.hisour.com/wp-content/uploads/2018/06/Venetian-Renaissance-in-15th-century.jpg"
    ],
    "object_detection": True,
    "visual_relations": False
}

example_enrichment_output = {
    "queryID": "e91110d3f12b515152fabad660c66af7",
    "img_path": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
    "object_detection": True,
    "visual_relations": False,
    "output": {
        "object_detection": {
                "bbx": [
                    [
                        0.601267517,
                        0.392477065,
                        0.68912,
                        0.806616366
                    ],
                    [
                        0.0504028499,
                        0.704973638,
                        0.323536694,
                        0.947903037
                    ],
                    [
                        0.289663911,
                        0.011395189,
                        0.665526867,
                        0.738713
                    ],
                    [
                        0.24941963,
                        0.0630826429,
                        0.369912684,
                        0.394429296
                    ]
                ],
            "scores": [
                    0.995772302,
                    0.994411647,
                    0.934053242,
                    0.736239
                ],
            "classes": [
                    11.0,
                    1.0,
                    3.0,
                    54.0
                ],
            "classes-labels": [
                    "book",
                    "crucifixion",
                    "person",
                    "halo"
                ],
            "img-bbx-path": "/static//object-detection/output/b%27aHR0cHM6Ly93d3cubXVzZW9kZWxwcmFkby5lcy9pbWFnZW5lcy9Eb2N1bWVudG9zL2ltZ3NlbS9jNC9jNGNhL2M0Y2FlM2I3LTA1MTgtNGNiYy05ODVhLTViNDNlZDNmYzYwMS85NTBmYzY2Yy1kZDVhLTRjODItYjIxYi1iM2Y1MTg2YmMxMDAuanBn%27.jpg"
        }
    },
    "status": 0,
    "outdir": "/home/smendoza/tmp/sgoab/enrichments/None",
    "label": 500
}

