#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################



# python libraries
import json

# sgoab libraries
from api_interface import APIInterface
from enrichment import enrichment_query_manager as eqm

class APISgoab (APIInterface):

    eqm = eqm.EnrichmentQueryManager()


    # ENRICHMENTS INTERFACE
    def enrichment_query_service_interface(self, imagepath: str, object_detection: bool = True, visual_relations: bool = False) -> dict:
        """[Returns the enrichments for the given images.
        Available enrichments are:
        1. object-detection
        2. visual-relations
        ]

        Args:
            imagepaths (list): [list with the [URL|localpath] of the images to be queried for enrichment]
            object_detection (bool, optional): [True if querying for object-detection enrichments. False otherwise.]. Defaults to True.
            visual_relations (bool, optional): [True if querying for visual-relations enrichments. False otherwise.]. Defaults to False.

        Returns:
            dict: [has the specified enrichments for the given images]
        """

        eqm.add_query(imagepath, object_detection, visual_relations)
        out = eqm.exec_next()
        return out


    # ENRICHMENTS INTERFACE
    def enrichment_query_json(self, query_json: str) -> dict:
        """[Returns the enrichments for the given images.
        Available enrichments are:
        1. object-detection
        2. visual-relations
        ]

        Args:
            imagepaths (list): [list with the [URL|localpath] of the images to be queried for enrichment]
            object_detection (bool, optional): [True if querying for object-detection enrichments. False otherwise.]. Defaults to True.
            visual_relations (bool, optional): [True if querying for visual-relations enrichments. False otherwise.]. Defaults to False.

        Returns:
            dict: [has the specified enrichments for the given images]
        """
        query1 = """{
        "items": [
        "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
        "http://data.europeana.eu/item/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj20822887"
        ],
        "object_detection": "True",
        "visual_relations": "True"
        }"""
        
        eqm.add_query_json(query_json)
        eqm.exec_next()
        pass

    # SEARCH INTERFACE

