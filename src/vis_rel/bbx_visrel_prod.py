# %%
"""
### Problem statement

- Load images and their VRA files.
- Apply heuristics to produce caption seeds.
  Rules are segmented according in 3 sets:
  - get_composition()
  - get_main_topics()
  - get_annot_text()
"""

# %%
"""
### Licensing terms and copyright
This code is protected by the terms and conditions of the GNU_GPL-v3 copyleft license.

Copyright (C) 2021 Cedric Bhihe

This code is free software and was developed by its author while at the Barcelona Supercomputing Center (https://www.bsc.es). You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR ANY PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>, or consult the License.md file in the Git repo.

To contact the author of the code, use cedric.bhihe@gmail.com.
"""

# %%
"""
### Guidelines

Run this notebook with Python versions 3.7.0 or 3.7.1.
"""

# %%
import os, sys
from typing import Union, Any, List, Optional,  Tuple, cast
import re, json

import random
import math as m
import numpy as np

from PIL import Image, ImageShow, ImageDraw, ImageFont, ImageFile
import colorsys

# %%
"""
### Methods
"""

# %%
def json_parse(data: dict,key_stem='bbx',find='first',key_suffix=('_basic',)) -> Tuple[List[Union[str,int]],List[Union[dict, str, float]]]:
    '''
    json_parse()::
      - Parse previosuly loaded json file.
      - Obtain 2 lists by recursively parsing bbx labels in arbitrary json trees.
        
    Input:
    - 1st arg: positional parameter, arbitrarily nested json string as dictionary.
    - 2nd arg: kw-parameter (default is 'bbx'), json sought key_stem as string.
    - 3rd arg: kw-parameter (default is 'first')
      Other value is 'all'. When set default, the json file is parsed until the
      first occurence of the sought 'key_stem' is found. When set to 'all', the whole 
      json file is parsed no matter how many occurences occurences of sought 'key_stem'
      are found.
    - 4th arg: (default is a tuple with one empty string '')
      The default applies when no 'key_stem' suffix are needed or considered.
      When keys are provided as 'key_stem' concatenated with suffix(es), then all or only
      part of those keys = 'key stem' + 'key_suffix' can be specified in a tuple of 
      'key_suffix' string(s). 
      For example if "key_stem='product'" and the sought after keys are 'product_basic', 
      'product_midrange' and/or 'product_topline', then specifying "key_suffix=('',)" will
      produce zero pertinent result. 
      On the other hand "key_suffix=('_basic','_topline')" would only retrieve key values 
      for the two suffixes '_basic' and '_topline'.
    
    Output: a tuple containing two lists: 
    - 1st list: each bbx' label, label_id and xmin, ymin, xmax, ymax
    - 2nd list: each bbx' label, label_id, and 4 computed attributes:'cpt','oloc','rsa','off'
    '''
    if find not in ['first','all']:
        raise ValueError('Bad argument specification for argument find=...\nOnly "first" or "all" are allowed.')

    
    def build_obj_locmtd_list(sought_keys, find, k, v) -> None:
        '''
        Function defined for convenience of not repeating code block
        '''
        nonlocal stack, visited_keys, obj_loc, obj_mtd
        
        if ((k not in sought_keys) and isinstance(v, __builtins__.dict)):
                if k not in visited_keys:
                    stack.extend(v.items())
                
        elif ((k in sought_keys) and isinstance(v, __builtins__.dict) and v != {}):
            for label, coord in list(v.items()):
                
                try:
                    label_cmpnts = label.split('_')
                    label = '_'.join(label_cmpnts[:-1])
                    label_id = label_cmpnts[-1]
                    
                    try:
                        xmin, ymin, xmax, ymax = [int(xy) for xy in list(coord.values())[:4]]
                        cpt, oloc, rsa, off = [coro for coro in list(coord.values())[4:8]]
                    except (AttributeError, ValueError) as ave:
                        print(ave + '\n ==> Missing or non integer bbx coordinates.')
                    except Exception as ex:
                        print(ex)
                        print('Unsure about what happened. Probably unsafe to continue; parsing interrupted.')
                        raise ex
                    
                    obj_loc.append((label, label_id, xmin, ymin, xmax, ymax))  # bbx coordinates list 
                    obj_mtd.append((label, label_id, cpt, oloc, rsa, off))     # bbx metadata list
                    
                except (NameError,):
                    pass 
            
            if find == 'first':
                stack = []  
        else:
            pass
        
        if find == 'first':
            visited_keys.add(k)
        elif find == 'all':
            pass
    
    
    visited_keys = set()
    obj_loc = []
    obj_mtd = []    
    sought_keys = list(map(lambda x: key_stem + x,list(key_suffix)))
    stack = list(data.items())
        
    while stack: 
        k, v = stack.pop()
        build_obj_locmtd_list(sought_keys, find, k, v)
            

    return (obj_loc, obj_mtd)


def get_composition(basename: str,width: int,height: int,bbx_loc: list,vis_rel: 'np.ndarray', ref_objs: list) -> dict:
    '''
    get_composition()::
      - Compute which bbx are in background / foreground 
      - Compute their relative position wrt main topic bbx
    
    Input:
      - 1st arg. (str) image basename
      - 2nd arg. (int) image height in pixels
      - 3rd arg. (int) image width in pixels
      - 4th arg. (list of tuples): bbx_loc = (label, label_id, xmin, ymin, xmax, ymax)
      - 5th arg. (np.ndarray): 'vra', image visual relationship array
              - on diagonal:  (obj_uniq_label,(x_cpt,y_cpt),oloc,rsa,off)
              - off diagonal: (dcpt,cpix,bro_1_2,rpos_1_2)
      - 6th arg. (list of tuples): 'ref_objs' = [(uniq_label, (x_cpt, y_cpt), oloc. rsa), (...)] 
                 as main topic candidates bbx(es)
    
    Output:
      - 1 object (dict): structure as follow ...
        {'clusters':{}, 'foreback':{},'omt':{},'amt':{},'bmt':{}, 'mylomt':{},'myromt':{}}
        where values are dictionaries.
           'clusters' = dict 
                   key: uniq_label of one of bbx pertaining to set.
                   value: set of 'ref_objs' uniq_label that overlap.      
           'foreback' = dict of sets of ('uniq_label','base_criterion','size_criterion') tuples, 
                   situated in the foreground (dist-factor >0) or in the background (dist_factor <0) 
                   with respect to the plane of representation of the 'main_topic'
                   keys are clusters labels.
           'omt' = dict of sets of 'uniq_label' fully situated within bbx of "main topic"
                   i.e. fully overlapped by it.
                   keys are cluster labels.
           'amt' = dict of sets of 'uniq_label' situated "above main topic".
                   keys are cluster labels.
           'bmt' = dict of sets of 'uniq_label' situated "beneath main topic".
                   keys are cluster labels.
           'mylomt' = dict of sets of 'uniq_label' situated "left of main topic".
                    keys are cluster labels.
           'myromt' = dict of sets of 'uniq_label' situated "right of main topic".
                    keys are cluster labels.
     
    '''
    vra = np.array(vis_rel)
    composition = dict()
    clusters = dict()
    foreback = dict()
    omt = dict()
    amt = dict()
    bmt = dict()
    mylomt = dict()
    myromt = dict()
    oloc_val_map = {'tl':60,'tc':90,'tr':40,
                    'cl':75,'cc':100,'cr':55,
                    'bl':45,'bc':80,'br':32}
    
    # Build array of tuples (uniq_label, (x_cpt, y_cpt), oloc, rsa)
    img_objs_lst = [(vra[idx,idx][0],vra[idx,idx][1],vra[idx,idx][2],vra[idx,idx][3]) for idx in range(len(vra))]
    img_objs_labels = [x[0] for x in img_objs_lst]
    ref_objs_labels = [x[0] for x in ref_objs]
    
    if len(ref_objs) > 1:
        for label_1 in ref_objs_labels:
            idx_1 = [vra[idx,idx][0] for idx in range(len(vra))].index(label_1)
            try:
                if isinstance(len(clusters[label_1]),__builtins__.int):
                    clusters[label_1].append(label_1)
            except (KeyError,):
                clusters[label_1] = [label_1,]
            
            #for label_2 in ref_objs_labels:
            for label_2 in img_objs_labels:
                idx_2 = [vra[idx,idx][0] for idx in range(len(vra))].index(label_2)
                try:
                    if isinstance(len(clusters[label_2]),__builtins__.int):
                        clusters[label_2].append(label_2)
                except (KeyError,):
                    clusters[label_2] = [label_2,]
                
                if (label_1 == label_2 or 
                    (label_1 != label_2 and (vra[idx_1,idx_2][2] > 0.0 or vra[idx_1,idx_2][1] == 0))
                   ):
                        clusters[label_1].append(label_2)
                        clusters[label_2].append(label_1)
        
        keys_to_delete = list()
        for k,v in clusters.items():
            v = list(set(clusters[k]))
            if len(v) == 1:
                keys_to_delete.append(k)
            else:
                clusters[k] = v
        
        for k in keys_to_delete:
            del(clusters[k])
        
        clusters_enum = list(enumerate(clusters))
        
        flag = True
        cnt = 0
        
        while flag:
            new_clusters = dict()
            cnt += 1
            for idx_1 in range(len(clusters_enum)):
                set_1 = set(clusters[str(clusters_enum[idx_1][1])])
                for idx_2 in range(idx_1+1,len(clusters_enum)):
                    set_2 = set(clusters[str(clusters_enum[idx_2][1])])
                    set_intersection = set_1.intersection(set_2)
                    
                    if set_intersection:
                        cnt = 0
                        vra_idx = [vra[idx,idx][0] for idx in range(len(vra))].index(clusters_enum[idx_1][1])
                        rsa_1 = vra[vra_idx,vra_idx][3]
                        vra_idx = [vra[idx,idx][0] for idx in range(len(vra))].index(clusters_enum[idx_2][1])
                        rsa_2 = vra[vra_idx,vra_idx][3]
                        
                        union_sets_1_2 = set_1.union(set_2)
                        idx_to_keep = idx_1 if (oloc_val_map[str(vra[idx_1,idx_1][2])]*rsa_1 > oloc_val_map[str(vra[idx_2,idx_2][2])]*rsa_2) else idx_2
                        
                        try:
                            if isinstance(len(new_clusters[clusters_enum[idx_to_keep][1]]),__builtins__.int):
                                new_clusters[clusters_enum[idx_to_keep][1]] = new_clusters[clusters_enum[idx_to_keep][1]].union(union_sets_1_2)
                        except (KeyError,):
                            new_clusters[clusters_enum[idx_to_keep][1]] = union_sets_1_2
            
            if cnt == 0:
                clusters = new_clustersclusters = new_clusters 
                clusters_enum = list(enumerate(clusters))
            
            if (len(clusters_enum) == 1 or cnt != 0):
                flag = False
    
    elif len(ref_objs) == 1:
        clusters[list(ref_objs)[0][0]] = [ref_objs[0][0],]
    
    else:
        pass
    
    
    for img_main_label in clusters:
        img_main_label_idx = img_objs_labels.index(img_main_label)
        main_baseline = bbx_loc[img_main_label_idx][5]
        main_height = bbx_loc[img_main_label_idx][5] - bbx_loc[img_main_label_idx][3]
        main_rsa = vra[img_main_label_idx,img_main_label_idx][3]
        img_other_objs_idx = list(range(len(vra)))
        del img_other_objs_idx[img_main_label_idx]
        
        ppa_main_label = '_'.join(img_main_label.split(sep='_')[:-1])
        ppa_main_label_idx = class_lst.index(ppa_main_label)
        
        for idx in img_other_objs_idx:
            obj_uniq_label = vra[idx,idx][0]
            ppa_obj_label = '_'.join(obj_uniq_label.split(sep='_')[:-1])
            ppa_obj_label_idx = class_lst.index(ppa_obj_label)
            obj_baseline = bbx_loc[idx][5]
            obj_height = bbx_loc[idx][5]-bbx_loc[idx][3]
            obj_rsa = vra[idx,idx][3]
            if ppa_main_label == ppa_obj_label:
                obj_rel_prop_to_main = 1
            else:
                obj_rel_prop_to_main = ppa[ppa_obj_label_idx][ppa_main_label_idx] # rel prop of obj to 'main_topic' bbx size

            obj_rel_pos_to_main = vra[idx,img_main_label_idx][3]
            obj_bro_wrt_self = vra[idx,img_main_label_idx][2]
            obj_bro_wrt_main = vra[img_main_label_idx,idx][2]
            
            if (ppa_main_label in {'book','scroll','banner',
                                   'apple','banana','lily','palm','tree',
                                   'angel','crucifixion','construction','rock',
                                   'bird','devil','butterfly','dove','swan','eagle','fish','cat','monkey','mouse',
                                   'rooster','serpent','pegasus','horn','sea shell',
                                   'saturno','camauro','zucchetto','mitre','tiara','crown','halo','stole',
                                   'crown of thorn','helmet',
                                   'hand','severed head','skull','wing',
                                   'lance','shield','arrow','sword',
                                   'jug','chalice','key of heaven',}
                or ppa_obj_label in {'book','scroll','banner',
                                     'apple','banana','lily','palm','tree',
                                     'angel','devil','crucifixion','construction','rock',
                                     'bird','butterfly','dove','swan','eagle','fish','cat','monkey','mouse',
                                     'rooster','serpent','pegasus','horn','sea shell',
                                     'saturno','camauro','zucchetto','mitre','tiara','crown','halo','stole',
                                     'crown of thorn','helmet',
                                     'hand','severed head','skull','wing',
                                     'lance','shield','arrow','sword',
                                     'jug','chalice','key of heaven',}):
                base_criterion = None
            else:
                base_criterion = round((obj_baseline - main_baseline) / height,2)
            
            size_criterion = round((obj_rsa - obj_rel_prop_to_main * main_rsa)/100,2)

            try:
                if isinstance(len(foreback[img_main_label]),__builtins__.int):
                    foreback[img_main_label].append((obj_uniq_label,base_criterion,size_criterion))
            except(KeyError,):
                foreback[img_main_label] = [(obj_uniq_label,base_criterion,size_criterion),]
                
            ## Total overlap of img_obj_label in img_main_label's bbx
            criterion = obj_bro_wrt_self / 100                                    # bounded
            if criterion >= bro_th:
                try:
                    if isinstance(len(omt[img_main_label]),__builtins__.int):
                        omt[img_main_label].append(obj_uniq_label)
                except(KeyError,):
                    omt[img_main_label] = [obj_uniq_label,]
            
            ## Objs positioned either:
            #      - above main_label's bbx: amt       {'NN', 'NW', 'NE'}
            #      - beneath main_label's bbx: bmt     {'SS', 'SW', 'SE'}
            #      - to my left of main topic: mylomt  {'WW'}
            #      - to my right of main topic: myromt {'EE'}
            criterion = obj_rel_pos_to_main
            if criterion in {'NN', 'NW', 'NE'}:
                try:
                    if isinstance(len(amt[img_main_label]),__builtins__.int):
                        amt[img_main_label].append(obj_uniq_label)
                except(KeyError,):
                    amt[img_main_label] = [obj_uniq_label,]
                    
                if criterion in {'NW'}:
                    try:
                        if isinstance(len(mylomt[img_main_label]),__builtins__.int):
                            mylomt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        mylomt[img_main_label] = [obj_uniq_label,]
                elif criterion in {'NE'}:
                    try:
                        if isinstance(len(myromt[img_main_label]),__builtins__.int):
                            myromt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        myromt[img_main_label] = [obj_uniq_label,]

                        
            elif criterion in {'SS', 'SW', 'SE'}:
                try:
                    if isinstance(len(bmt[img_main_label]),__builtins__.int):
                        bmt[img_main_label].append(obj_uniq_label)
                except(KeyError,):
                    bmt[img_main_label] = [obj_uniq_label,]
                    
                if criterion in {'SW'}:
                    try:
                        if isinstance(len(mylomt[img_main_label]),__builtins__.int):
                            mylomt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        mylomt[img_main_label] = [obj_uniq_label,]
                        
                elif criterion in {'SE'}:
                    try:
                        if isinstance(len(myromt[img_main_label]),__builtins__.int):
                            myromt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        myromt[img_main_label] = [obj_uniq_label,]
                        
            elif criterion in {'WW'}:
                try:
                    if isinstance(len(mylomt[img_main_label]),__builtins__.int):
                        mylomt[img_main_label].append(obj_uniq_label)
                except(KeyError,):
                    mylomt[img_main_label] = [obj_uniq_label,]
                    
            else:
                # if criterion in {'EE'}:
                try:
                    if isinstance(len(myromt[img_main_label]),__builtins__.int):
                        myromt[img_main_label].append(obj_uniq_label)
                except(KeyError,):
                    myromt[img_main_label] = [obj_uniq_label,]
    
    composition['clusters']= clusters
    composition['foreback'] = foreback
    composition['omt'] = omt
    composition['amt'] = amt
    composition['bmt'] = bmt
    composition['mylomt'] = mylomt
    composition['myromt']= myromt
    
    return composition


def get_main_topics(basename: str, vis_rel: 'np.ndarray') -> dict:
    '''
    get_main_topics()::
      - Detect candidates objects to promote them to values under the key 'main_topic(s)'.
    
    Input: 
      - 1st arg. (numpy.ndarray): visual relationships array (vra)
               
    Output:
      - 1 object of type list; it consists of tuples with structure:
        (uniq_label, oloc, rsa, off, rank). 
        - oloc: object's bbx' general location in painting; sought items usually have 
          attribute values in {'tc','cc'}
        - rsa: object's bbx' relative surface area wrt complete image; sought items are 
          the 3 objects with largest such attributes in an image within the "pertinent"
          list of vetted "main_topic" candidates.       
    '''
    rel_sizes = np.array(ppa)
    vra = np.array(vis_rel)
    img_main_objs = dict()
    
    # Build array of tuples (uniq_label, (x_ctp, y_ctp), oloc, rsa)
    if len(vra[0,:]) == 0:
        pass
    else: 
        img_class_lst = [(vra[idx,idx][0],vra[idx,idx][1],vra[idx,idx][2],vra[idx,idx][3]) for idx in range(len(vra[0,:]))]
        main_topic_objs = [ppa[idx][idx][0] for idx in range(len(ppa)) if ppa[idx][idx][1] == 'main_topic']

        notable_center_objs = [x for x in img_class_lst 
                               if x[2] in {'tc','cc','bc'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
        notable_left_objs = [x for x in img_class_lst 
                             if x[2] in {'tl','cl','bl'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
        notable_right_objs = [x for x in img_class_lst 
                              if x[2] in {'tr','cr','br'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
        notable_sides_objs = notable_left_objs + notable_right_objs

        if (len(notable_center_objs) != 0 and len(notable_sides_objs) != 0):
            center_objs_to_remove = []
            sides_objs_to_remove = []

            for center_obj in notable_center_objs:
                center_obj_idx = img_class_lst.index(center_obj)
                center_obj_ppa_idx = class_lst.index('_'.join(center_obj[0].split(sep='_')[:-1]))
                for sides_obj in notable_sides_objs:
                    sides_obj_idx = img_class_lst.index(sides_obj)
                    sides_obj_ppa_idx = class_lst.index('_'.join(sides_obj[0].split(sep='_')[:-1]))
                    if center_obj_ppa_idx != sides_obj_ppa_idx:
                        rel_pp_center2side = rel_sizes[center_obj_ppa_idx,sides_obj_ppa_idx]
                    else:
                        rel_pp_center2side = 1

                    if vra[center_obj_idx,center_obj_idx][3] >= 1.3 * rel_pp_center2side *vra[sides_obj_idx,sides_obj_idx][3]:
                        sides_objs_to_remove.append(sides_obj)
                    elif vra[center_obj_idx,center_obj_idx][3] <= 0.7 * rel_pp_center2side *vra[sides_obj_idx,sides_obj_idx][3]:
                        center_objs_to_remove.append(center_obj)
                    else:
                        pass

            notable_center_objs = [x for x in notable_center_objs if x not in center_objs_to_remove]
            notable_sides_objs = [x for x in notable_sides_objs if x not in sides_objs_to_remove]

        if (len(notable_center_objs) == 0 and len(notable_sides_objs) != 0):
            notable_objs = notable_sides_objs
        elif (len(notable_center_objs) != 0 and len(notable_sides_objs) != 0):
            notable_objs = notable_center_objs + notable_sides_objs
        else:
            notable_objs = notable_center_objs

        notable_objs = sorted(notable_objs,
                              key = lambda x: (x[2],x[3]),
                              reverse=True)
    
        # Define 'bigger_objs' as any object whose bbx' surface area is at least 10% of that of the whole image. 
        bigger_objs = sorted([x for x in img_class_lst if x[3] > 10 and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs], 
                             key=lambda x:x[3],
                             reverse=True)

        if set(notable_objs) & set(bigger_objs):
            ref_objs = list(set(notable_objs) & set(bigger_objs))
        elif notable_objs:
            ref_objs = notable_objs
        elif bigger_objs:
            ref_objs = bigger_objs
        else:
            ref_objs = []   # apply other criteria ?

        img_main_objs = {'notable_objs':notable_objs,'bigger_objs':bigger_objs,'ref_objs':ref_objs}
    
    return img_main_objs


def get_annot_txt(basename: str,width: int,height: int,img_bbx_loc: list,img_vis_rel: 'np.ndarray',size_th: float,base_th: float,bro_th: float) -> dict:
    '''
    annot_txt()::
      - Compute 
          1/  annotation text among bbxes pairwise wherever physical overlap is meaningful,
          2/ solitary bbx' object's attributes. 

    Input:
      - 1st arg. (str) image basename
      - 2nd arg. (int) image height in pixels
      - 3rd arg. (list of tuples) bbx_loc = (label, label_id, xmin, ymin, xmax, ymax)
      - 4th arg. (np.ndarray) 'vra', image visual relationship array
                          - on diagonal:  (obj_uniq_label,(x_cpt,y_cpt),oloc,rsa,off)
                          - off diagonal: (dcpt,cpix,bro_1_2,rpos_1_2)
    
    Output:
        Annotations for pairwise visual relationships and for single or isolated objects attributes
    '''    
    rel_sizes = np.array(ppa)
    vra = np.array(img_vis_rel)
    annot_txt = dict()
    soa_1_key = soa_2_key = None
    
    # Build array of tuples (uniq_label, (x_cpt, y_cpt), oloc, rsa, off)
    img_objs_lst = [tuple(vra[idx,idx]) for idx in range(len(vra))]
    img_objs_labels = [x[0] for x in img_objs_lst]
    
    if len(img_objs_labels) == 1:
        annot_soa_1 = str()
        label_1 = img_objs_labels[0]
        label_1_ns = '_'.join(label_1.split(sep=' '))
        ppa_label_1 = ' '.join(label_1.split(sep='_')[:-1])        
        
        if (ppa_label_1 in flying_birds and vra[0,0][4] <= 0): 
            annot_soa_1 += label_1_ns + " is_flying\n"
            print("Pipeline info: 'get_annot_txt()' section for exactly one detected object and bbx")
        elif ppa_label_1 in winged_things - flying_birds:
            # not included in previous conditional clause because representations mostly show those
            #+  objects ground-bound, either crawling, lying or standing w/ or w/o spread wings
            pass
        else:
            pass
        
        # Build single_object_attribute (soa) label as key for lone bbx attribute(s) in textual annotations
        if annot_soa_1:
            soa_key_1 = label_1_ns + '++' + 'attr'
            if soa_1_key in annot_txt.keys():
                annot_txt[soa_1_key] += list(annot_soa_1.split('\n')[:-1])
            else:
                annot_txt[soa_1_key] = list(annot_soa_1.split('\n')[:-1])
                        
            annot_txt[soa_1_key] = list(set(annot_txt[soa_1_key]))
            
    elif len(img_objs_labels) >= 2:
        img_rsa_list = [vra[idx,idx][3] for idx in range(len(img_objs_labels))]
        img_rsa_list.sort()
        img_rsa_mid_idx = len(img_rsa_list)//2
        img_median_rsa = (img_rsa_list[img_rsa_mid_idx]+img_rsa_list[~img_rsa_mid_idx])/2
        len_img_max_rsa = len(str(max(img_rsa_list)))
        
        # Test for at least 2 bbxes touching or overlapping
        list_bro_1_2 = [(vra[idx_1,idx_2][1],vra[idx_1,idx_2][2])
                        for idx_1 in range(len(img_objs_labels))
                        for idx_2 in range(idx_1+1,len(img_objs_labels))]
        
        for idx_1 in range(len(img_objs_labels)):
            label_1 = img_objs_labels[idx_1]
            label_1_ns = '_'.join(label_1.split(sep=' '))
            ppa_label_1 = '_'.join(label_1.split(sep='_')[:-1])
            
            
            # caution: list class labels 'class_lst' made of strings with spaces
            ppa_idx_1 = class_lst.index(ppa_label_1)       # 'class_lst' is global
            label_1_rsa = vra[idx_1,idx_1][3]
            baseline_1 = img_bbx_loc[idx_1][5]
            annot_soa_1 = str()
            
            idx_lst = list(range(0,idx_1))
            idx_lst.extend(list(range(idx_1+1,len(img_objs_labels))))
            obj_overlaps = [img_objs_labels[idx_2] for idx_2 in idx_lst if vra[idx_1,idx_2][1] <= 0]
            
            if (ppa_label_1 in flying_birds and not obj_overlaps):
                if vra[idx_1,idx_1][4] <= -0.02:
                    annot_soa_1 += label_1_ns + " is_flying\n"
                else:
                    if ppa_label_1 not in {"swan"}:
                        annot_soa_1 += label_1_ns + " stands\n"
                    else:
                        annot_soa_1 += label_1_ns + " floats/stands\n"
                    
                soa_1_key = label_1_ns + '++' + 'attr'
                
                if soa_1_key in annot_txt:
                    annot_txt[soa_1_key] += list(annot_soa_1.split('\n')[:-1])
                else:
                    annot_txt[soa_1_key] = list(annot_soa_1.split('\n')[:-1])
                    
                annot_txt[soa_1_key] = list(set(annot_txt[soa_1_key]))
                annot_soa_1 = str()

            for idx_2 in range(idx_1+1,len(img_objs_labels)):
                label_2 = img_objs_labels[idx_2]
                label_2_ns = '_'.join(label_2.split(sep=' '))
                ppa_label_2 = '_'.join(label_2.split(sep='_')[:-1])
                
                ppa_idx_2 = class_lst.index(ppa_label_2)
                label_2_rsa = vra[idx_2,idx_2][3]
                baseline_2 = img_bbx_loc[idx_2][5]
                annot_soa_2 = str()
                
                # Compute size criterion relative to avg_reduced_rsa of 2 objects being compared
                prop_obj_1_2 = ppa[ppa_idx_1][ppa_idx_2]   # multiplier to go from rsa(bbx_2) to rsa(bbx_1)
                prop_obj_2_1 = ppa[ppa_idx_2][ppa_idx_1]
                
                if prop_obj_2_1 > prop_obj_1_2:
                    avg_reduced_rsa = (label_1_rsa + prop_obj_1_2 * label_2_rsa)/2
                    size_criterion = round((label_1_rsa - prop_obj_1_2 * label_2_rsa)/avg_reduced_rsa, 2)
                
                elif prop_obj_2_1 < prop_obj_1_2:
                    avg_reduced_rsa = (label_2_rsa + prop_obj_2_1 * label_1_rsa)/2
                    size_criterion = round((label_2_rsa - prop_obj_2_1 * label_1_rsa)/avg_reduced_rsa, 2)
                
                else:
                    prop_obj_1_2 = 1
                    avg_reduced_rsa = (label_1_rsa + prop_obj_1_2 * label_2_rsa)/2
                    size_criterion = round((label_1_rsa - prop_obj_1_2 * label_2_rsa)/avg_reduced_rsa, 2)
                
                size_th -= (m.tan( (avg_reduced_rsa - img_median_rsa) * m.pi/10**len_img_max_rsa) / 2)**3

                if (ppa_label_1 == 'crucifixion' or ppa_label_2 == 'crucifixion'):
                    base_criterion = None
                else:
                    base_criterion = round((baseline_1 - baseline_2) / height,2)
                
                rpos_1_2 = vra[idx_1,idx_2][3]
                rpos_2_1 = vra[idx_2,idx_1][3]
                bro_1_2 = vra[idx_1,idx_2][2]
                bro_2_1 = vra[idx_2,idx_1][2]
                cpix = vra[idx_1,idx_2][1]
                
                xmin_1, ymin_1, xmax_1, ymax_1 = img_bbx_loc[idx_1][2:]
                xmin_2, ymin_2, xmax_2, ymax_2 = img_bbx_loc[idx_2][2:]
                
                annot_pvr = str()
                
                if (bro_1_2 > 0. or bro_2_1 > 0. or cpix == 0):
                    
                    # ####################
                    ## PAIRWISE RULES
                    # ####################

                    if (ppa_label_1 in person_like_instances and 
                        ppa_label_2 in person_like_instances):

                        if vra[idx_2,idx_2][4] >= 0.1:
                            annot_soa_2 += label_2_ns + " stands\n"
                        elif vra[idx_2,idx_2][4] <= -0.1:
                            annot_soa_2 += label_2_ns + " lies/reclines\n"
                        else:
                            annot_soa_2 += label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                        if vra[idx_1,idx_1][4] >= 0.1:
                            annot_soa_1 += label_1_ns + " stands\n"
                        elif vra[idx_1,idx_1][4] <= -0.1:
                            annot_soa_1 += label_1_ns + " lies/reclines\n"
                        else:
                            annot_soa_1 += label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                        if abs(base_criterion) > base_th:
                            
                            if (abs(size_criterion) > size_th and
                                (abs(ymax_2-ymin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height > 2* base_th or
                                 abs(xmax_2-xmin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height > 2* base_th or
                                 abs(xmax_2-xmin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height > 2* base_th or
                                 abs(ymax_2-ymin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height > 2* base_th)):
                                
                                if (bro_1_2 >= 10 and label_1_rsa < 0.7 * label_2_rsa):
                                    if (ppa_label_1 in person_like_instances - {'devil','pope','knight','judith'}):
                                        
                                        if ((4*ymin_2 + ymax_2)/5 <= vra[idx_1,idx_1][1][1] <= (ymax_2 + ymin_2)/2 and
                                            (2*xmin_2 + xmax_2)/3 <= vra[idx_1,idx_1][1][0] <= (2*xmax_2 + xmin_2)/3
                                           ):  
                                            annot_soa_1 += label_1_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                            annot_pvr += label_2_ns + " holds " + label_1_ns + "\n"
                                        else:
                                            #annot_pvr += label_2_ns + " is_next_to " + label_1_ns + "\n"
                                            pass
                                    else:
                                        annot_soa_1 += label_1_ns + " is partial\n"
                                
                                elif (bro_2_1 >= 10 and label_2_rsa < 0.7 * label_1_rsa):
                                    if (ppa_label_2 in person_like_instances - {'devil','pope','knight','judith'}):
                                        
                                        if ((4*ymin_1 + ymax_1)/5 <= vra[idx_2,idx_2][1][1] <= (ymax_1 + ymin_1)/2 and
                                            (2*xmin_1 + xmax_1)/3 <= vra[idx_2,idx_2][1][0] <= (2*xmax_1 + xmin_1)/3
                                           ):
                                            annot_soa_2 += label_2_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                            annot_pvr += label_1_ns + " holds " + label_2_ns + "\n"
                                        else:
                                            #annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                                            pass
                                    else:
                                        annot_soa_2 += label_2_ns + " is partial\n"
                                
                                else:
                                    pass
                            
                            elif (abs(size_criterion) > size_th and
                                  (abs(ymax_2-ymin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                   abs(xmax_2-xmin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                   abs(xmax_2-xmin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                   abs(ymax_2-ymin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height <= 2*base_th)):

                                if (label_1_rsa < label_2_rsa
                                    and round(baseline_2/10,0) >= round(baseline_1/10,0)):  
                                    annot_pvr += label_2_ns + " is_before " + label_1_ns + "\n"    
                                elif (label_2_rsa < label_1_rsa
                                      and round(baseline_1/10,0) >= round(baseline_2/10,0)):
                                    annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"   
                                else:
                                    annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                            
                            else:
                                if round(baseline_2/10,0) >= round(baseline_1/10,0):  
                                    annot_pvr += label_2_ns + " is_before " + label_1_ns + "\n"
                                elif round(baseline_1/10,0) >= round(baseline_2/10,0):
                                    annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                                else:
                                    annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                        
                        else:
                        
                            if (round(baseline_1/10,0) > round(baseline_2/10,0)):
                                annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                            elif (round(baseline_1/10,0) < round(baseline_2/10,0)):
                                annot_pvr += label_1_ns + " is_behind " + label_2_ns + "\n"
                            else:
                                annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"

                            if abs(size_criterion) >  size_th:
                                diff_max_dim = abs(max(xmax_1-xmin_1,ymax_1-ymin_1) - max(xmax_2-xmin_2,ymax_2-ymin_2))
                                avg_max_dim = (max(xmax_1-xmin_1,ymax_1-ymin_1) + max(xmax_2-xmin_2,ymax_2-ymin_2))/2
                                
                                if diff_max_dim/avg_max_dim > 20*size_th:
                                    if label_1_rsa < label_2_rsa:
                                        if ppa_label_1 in person_like_instances - {'devil','pope','knight','judith'}:
                                            annot_soa_1 += label_1_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                        else:
                                            annot_soa_1 += label_1_ns + " is_partial\n"
                                    
                                    elif label_2_rsa < label_1_rsa:
                                        if ppa_label_2 in person_like_instances - {'devil','pope','knight','judith'}:
                                            annot_soa_2 += label_2_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                        else:
                                            annot_soa_2 += label_2_ns + " is_partial\n"
                                    
                                    else:
                                        pass
                            
                            else:
                                pass
                            
                        if annot_soa_1:
                            soa_1_key = label_1_ns + '++' + 'attr'
                            
                        if annot_soa_2:
                            soa_2_key = label_2_ns + '++' + 'attr'
                        
                    
                    
                    if ((ppa_label_1 in person_like_instances and  
                         ppa_label_2 in {'arrow','book','lily','palm',} and 
                         bro_2_1 >= bro_th/2) or 
                        (ppa_label_2 in person_like_instances and 
                         ppa_label_1 in {'arrow','book','lily','palm',} and 
                         bro_1_2 >= bro_th/2)):
                        
                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if PH_ppa_label_2 in {'book','lily','palm',}:
                            if (vra[PH_idx_1,PH_idx_1][4] > 0.1):
                                annot_soa_1 += PH_label_1_ns + " stands\n"

                                if vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                    annot_pvr += PH_label_1_ns + " is_with/holds " + PH_label_2_ns + "\n"
                                elif vra[PH_idx_2,PH_idx_2][1][1] >= (4*PH_ymax_1 + PH_ymin_1)/5:
                                    annot_pvr += PH_label_2_ns + " is_at_feet_of " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            elif (vra[PH_idx_1,PH_idx_1][4] < -0.1):
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                                if (PH_ppa_label_2 in {'lily', 'palm'} and vra[PH_idx_2,PH_idx_2][4] <= -0.1):
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"

                                    if vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4:
                                        annot_pvr += PH_label_2_ns + " lies_left_of " + PH_label_1_ns + "\n"

                                    elif vra[PH_idx_2,PH_idx_2][1][0] >= (3*PH_xmax_1 + PH_xmin_1)/4:
                                        annot_pvr += PH_label_2_ns + " lies_right_of " + PH_label_1_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"

                                elif (PH_ppa_label_2 in {'lily', 'palm'} and vra[PH_idx_2,PH_idx_2][4] > -0.1):
                                    if (vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 3 * PH_xmin_1)/4):
                                        annot_pvr += PH_label_2_ns + " is_at_left_of " + PH_label_1_ns + "\n"
                                    elif (vra[PH_idx_2,PH_idx_2][1][0] >= (3 * PH_xmax_1 + PH_xmin_1)/4):
                                        annot_pvr += PH_label_2_ns + " is_at_right_of " + PH_label_1_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_at_mid-section_of " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            else:
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                                if vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                    annot_pvr += PH_label_1_ns + " is_with/holds " + PH_label_2_ns + "\n"   
                                else:
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                                    
                        elif (PH_ppa_label_2 in {'arrow',} and PH_bro_2_1 > 8):
                            annot_pvr += PH_label_1_ns + " is_pierced_by " + PH_label_2_ns + "\n"
                            
                            if (vra[PH_idx_1,PH_idx_1][4] > 0.1):
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                            elif (vra[PH_idx_1,PH_idx_1][4] < -0.1):
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                        else:
                            pass
                                                    
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                            
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'


                    if ((ppa_label_1 == 'crucifixion' and
                         ppa_label_2 in {'crown of thorns','halo','crown'} and
                         bro_2_1 >= bro_th) or
                        (ppa_label_2 == 'crucifixion' and
                         ppa_label_1 in {'crown of thorns','halo','crown'} and
                         bro_1_2 >= bro_th)):
                        
                        if ppa_label_1 == 'crucifixion':
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if (vra[PH_idx_2,PH_idx_2][1][1] >= PH_ymin_1 and
                            vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4 and 
                            vra[PH_idx_2,PH_idx_2][1][0] >= (2*PH_xmin_1 + PH_xmax_1)/3 and 
                            vra[PH_idx_2,PH_idx_2][1][0] <= (2*PH_xmax_1 + PH_xmin_1)/3):
                            annot_soa_1 += PH_label_1_ns + " ((person)) is_vertical\n"
                            annot_pvr += PH_label_1_ns + " ((person)) is_on ((cross))\n"
                            if PH_ppa_label_2 in {'crown of thorns','crown'}:
                                annot_pvr += PH_label_1_ns + " ((person)) wears " + PH_label_2_ns + "\n"
                            else:
                                annot_pvr += PH_label_1_ns + " ((person)) is_with " + PH_label_2_ns + "\n"
                            
                            # Deductive process for demonstration purposes only
                            # Task belongs to graph-bound logic, not to vis-rel.
                            if PH_ppa_label_2 in {'crown of thorns','crown'}:
                                annot_soa_1 += PH_label_1_ns + " ((person)) is ((jesus_christ))\n"
                            elif PH_ppa_label_2 == 'halo':
                                annot_soa_1 += PH_label_1_ns + " ((person)) is ((saint))/((martyr))/((good_thief)\n"
                            else:
                                pass
                        elif (((vra[PH_idx_2,PH_idx_2][1][0] >= PH_xmin_1 
                                and vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4)
                              or (vra[PH_idx_2,PH_idx_2][1][0] >= (xmin_1 + 3*PH_xmax_1)/4 
                                  and vra[PH_idx_2,PH_idx_2][1][0] <= PH_xmax_1))
                              and vra[PH_idx_2,PH_idx_2][1][1] >= (2*PH_ymin_1 + PH_ymax_1)/3
                              and vra[PH_idx_2,PH_idx_2][1][1] <= (2*PH_ymax_1 + PH_ymin_1)/3):
                            
                            annot_soa_1 += PH_label_1_ns + " ((person)) is_horizontal/is_inclined\n"
                            annot_soa_1 += PH_label_1_ns + " ((person)) is_on ((cross))\n"
                            if PH_ppa_label_2 in {'crown of thorns','crown'}:
                                annot_pvr += PH_label_1_ns + " ((person)) wears " + PH_label_2_ns + "\n"
                            else:
                                annot_pvr += PH_label_1_ns + " ((person)) is_with " + PH_label_2_ns + "\n"
                                    
                            if PH_ppa_label_2 in {'crown of thorns','crown'}:
                                annot_soa_1 += PH_label_1_ns + " ((person)) is ((jesus_christ))\n"
                            elif PH_ppa_label_2 == 'halo':
                                annot_soa_1 += PH_label_1_ns + " ((person)) is ((saint))/((martyr))/((good_thief)\n"
                            else:
                                pass 
                        else:
                            pass
                        
                                                    
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                            
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'


                    if (((ppa_label_1 == 'crucifixion' and ppa_label_2 in person_like_instances)
                         or (ppa_label_2 == 'crucifixion' and ppa_label_1 in person_like_instances))
                        and (bro_2_1 > bro_th / 3 or bro_1_2 > bro_th / 3)):
                        
                        if ppa_label_1 == 'crucifixion':
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            
                        annot_soa_1 += PH_label_1_ns + ' ((person)) is_on ((cross))\n'

                        if (vra[PH_idx_2,PH_idx_2][4] >= 0.1):
                            annot_soa_2 += PH_label_2_ns + " stands\n"
                        elif (vra[PH_idx_2,PH_idx_2][4] <= -0.1):
                            annot_soa_2 += PH_label_2 + " lies/reclines\n"
                        else:
                            annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                        if (vra[PH_idx_1,PH_idx_1][4] >= 0.1):
                            annot_soa_1 += PH_label_1_ns + " ((person))/((cross)) is_vertical\n"

                            if (vra[PH_idx_2,PH_idx_2][4] >= 0.1):
                                diff_height = PH_ymax_1 - PH_ymin_1 - PH_ymax_2 + PH_ymin_2
                                avg_height = (PH_ymax_1 - PH_ymin_1 + PH_ymax_2 - PH_ymin_2) / 2
                            elif (vra[PH_idx_2,PH_idx_2][4] <= -0.1):
                                diff_height = PH_ymax_1 - PH_ymin_1 - PH_xmax_2 + PH_xmin_2
                                avg_height = (PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2) / 2
                            else:
                                diff_height = PH_ymax_1 - PH_ymin_1 - (PH_xmax_2 - PH_xmin_2 + PH_ymax_2 - PH_ymin_2)/2
                                avg_height = (PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2) / 2

                            if (PH_rpos_2_1 in {"SS"}): 
                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                                else:
                                    pass

                            if (PH_rpos_2_1 in {"SW"}):
                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1 + " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += label_2 + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                else:
                                    pass

                            if (PH_rpos_2_1 in {"SE"}):
                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                else:
                                    pass

                            if (PH_rpos_2_1 in {"EE"}):
                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                else:
                                    pass

                            if (PH_rpos_2_1 in {"WW"}):
                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                                else:
                                    pass
                        elif (vra[PH_idx_1,PH_idx_1][4] <= -0.1):
                            annot_soa_1 += PH_label_1_ns + " ((person))/((cross)) is_horizontal\n"

                            if (vra[PH_idx_2,PH_idx_2][4] >= 0.1):
                                diff_height = PH_xmax_1 - PH_xmin_1 - PH_ymax_2 + PH_ymin_2
                                avg_height = (PH_xmax_1 - PH_xmin_1 + PH_ymax_2 - PH_ymin_2) / 2
                            elif (vra[PH_idx_2,PH_idx_2][4] <= -0.1):
                                diff_height = PH_xmax_1 - PH_xmin_1 - PH_xmax_2 + PH_xmin_2
                                avg_height = (PH_xmax_1 - PH_xmin_1 + PH_xmax_2 - PH_xmin_2) / 2
                            else:
                                pass

                            if (abs(diff_height)/avg_height < 0.45):
                                annot_pvr += PH_label_2_ns + " is_near " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (diff_height > 0):
                                annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (diff_height <= 0):
                                annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                            else:
                                pass

                            if (PH_rpos_2_1 in {"SS"}):
                                annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"SW"}):
                                annot_pvr += PH_label_2_ns + " is_below " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"SE"}):
                                annot_pvr += PH_label_2_ns + " is_below " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"WW"}):
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"EE"}):
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"NW"}):
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"NE"}):
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            else:
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                        else:
                            if (vra[PH_idx_2,PH_idx_2][4] >= 0.1):
                                diff_height = max(PH_ymax_1 - PH_ymin_1 - PH_ymax_2 + PH_ymin_2,PH_xmax_1 - PH_xmin_1 - PH_ymax_2 + PH_ymin_2)
                                avg_height = max(PH_ymax_1 - PH_ymin_1 + PH_ymax_2 - PH_ymin_2, PH_xmax_1 - PH_xmin_1 + PH_ymax_2 - PH_ymin_2) / 2
                            elif (vra[PH_idx_2,PH_idx_2][4] <= -0.1):
                                diff_height = max(PH_ymax_1 - PH_ymin_1 - PH_xmax_2 + PH_xmin_2, PH_xmax_1 - PH_xmin_1 - PH_xmax_2 + PH_xmin_2)
                                avg_height = max(PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2, PH_xmax_1 - PH_xmin_1 + PH_xmax_2 - PH_xmin_2) / 2
                            else:
                                pass

                            if (abs(diff_height)/avg_height < 0.45):
                                annot_pvr += PH_label_2_ns + " is_near " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (diff_height > 0):
                                annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (diff_height <= 0):
                                annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + " ((person))/((cross))\n"
                            else:
                                pass

                            if (PH_rpos_2_1 in {"SS"}):
                                annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"SW"}):
                                annot_pvr += PH_label_2_ns + " is_below " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"SE"}):
                                annot_pvr += PH_label_2_ns + " is_below " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"WW"}):
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"EE"}):
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"NW"}):
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            elif (PH_rpos_2_1 in {"NE"}):
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + " ((person))/((cross))\n"
                            else:
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + " ((person))/((cross))\n"
                        
                         
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                            
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    
                    if ((ppa_label_1 in {'crucifixion','angel'}
                         and ppa_label_2 in {'bird', 'dove'}
                         and rpos_2_1 in {'NN','ǸW','NE'})
                        or (ppa_label_2 in {'crucifixion','angel'}
                            and ppa_label_1 in {'bird','dove'}
                            and rpos_1_2 in {'NN','ǸW','NE'})):

                        if ppa_label_1 in {'crucifixion','angel'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if PH_ppa_label_1 == 'crucifixion':
                            annot_soa_1 += PH_label_1_ns + ' ((person)) is ((jesus_christ))\n'
                        
                        if PH_ppa_label_2 == 'dove':
                            annot_soa_2 += PH_label_2_ns + ' is ((holy_spirit))\n'
                        
                        annot_pvr += PH_label_2_ns + ' hovers_over ' + PH_label_1_ns + '\n'
                                                
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    
                    if ((ppa_label_1 in {'person','prayer','knight'}
                         and ppa_label_2 in {'halo','helmet','crown of thorns','tiara','crown'}
                         and bro_2_1 >= bro_th)
                        or (ppa_label_2 in {'person','prayer','knight'}
                         and ppa_label_1 in {'halo','helmet','crown of thorns','tiara','crown'}
                         and bro_1_2 >= bro_th)):
                        
                        if ppa_label_1 in {'person','prayer','knight'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if 0.1 < vra[PH_idx_1,PH_idx_1][4]:
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                            
                            if PH_ymin_1 <= vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 4*PH_ymin_1)/5:
                            
                                if PH_ppa_label_2 == 'crown of thorns':
                                    # Below commented out to allow for Saint Catalina of Sienna's corner case 
                                    #annot_soa_1 += PH_label_1_ns + " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                elif PH_ppa_label_2 in {'helmet','crown', 'tiara'}:
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'knight':
                                        annot_soa_1 += PH_label_1_ns + " is ((man_at_arms))\n"

                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                
                                else:
                                    pass
                            
                            elif (2*PH_ymin_1 + PH_ymax_1)/3 <= vra[PH_idx_2,PH_idx_2][1][1] <= (2*PH_ymax_1 + PH_ymin_1)/3:
                                
                                if PH_ppa_label_2 in {'helmet','crown', 'tiara'}:
                                    annot_pvr += PH_label_1_ns + " holds " + PH_label_2_ns + "\n"
                                else:
                                    pass
                            
                            else:
                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                
                        
                        elif vra[PH_idx_1,PH_idx_1][4] < -0.1:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                            if ((PH_xmin_1 <= vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4) or
                                ((PH_xmin_1 + 3*PH_xmax_1)/4 <= vra[PH_idx_2,PH_idx_2][1][0] <= PH_xmax_1)
                               ):
                                
                                if PH_ppa_label_2 == 'crown of thorns':
                                    # Below NOT commented out because lying Saint Catalina of Sienna highly unusual
                                    annot_soa_1 += PH_label_1_ns + " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                elif PH_ppa_label_2 in {'helmet','crown', 'tiara'}:
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'knight' and PH_ppa_label_2 == 'helmet':
                                        annot_soa_1 += PH_label_1_ns + " is ((man_at_arms))\n"

                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                else:
                                    pass
                            else:
                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                        
                        else:
                            # (-0.1 <= vra[PH_idx_1,PH_idx_1][4] <= 0.1):
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                            if ((PH_ymin_1 <= vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 4*PH_ymin_1)/5) or
                                (PH_xmin_1 <= vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 4*PH_xmin_1)/5) or 
                                ((PH_xmin_1 + 4*PH_xmax_1)/5 <= vra[PH_idx_2,PH_idx_2][1][0] <= PH_xmax_1)
                               ):
                                
                                if PH_ppa_label_2 == 'crown of thorns':
                                    # Below commented out to allow for Saint Catalina of Sienna's corner case 
                                    #annot_soa_1 += PH_label_1_ns + " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                
                                elif PH_ppa_label_2 in {'helmet','crown', 'tiara'}:
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                    
                                    if ppa_label_1 != 'knight' and PH_ppa_label_2 == 'helmet':
                                        annot_soa_1 += PH_label_1_ns + " is ((man_at_arms))\n"
                                
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                
                                else:
                                    pass
                            
                            elif ((3*PH_ymin_1+PH_ymax_1)/4 <= vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymin_1+3*PH_ymax_1)/4
                                 ):
                                
                                if PH_ppa_label_2 in {'helmet','tiara','crown'}:
                                    # Below commented to allow for Saint Catalina of Sienna's corner case 
                                    #annot_soa_1 += PH_label_1_ns + " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " holds " + PH_label_2_ns + "\n"
                                    
                                    if ppa_label_1 != 'knight' and PH_ppa_label_2 == 'helmet':
                                        annot_soa_1 += PH_label_1_ns + " is ((man_at_arms))\n"
                                
                            else:
                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                        
                    
                    if ((ppa_label_1 in person_like_instances
                         and ppa_label_2 in {'sword','lance', 'shield'}
                         and bro_2_1 >= 4.)
                        or (ppa_label_2 in person_like_instances
                            and ppa_label_1 in {'sword','lance', 'shield'}
                            and bro_1_2 >= 4.)):
                        
                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                            
                        
                        if  0.1 < vra[PH_idx_1,PH_idx_1][4]:
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                            
                            if abs(PH_baseline_2 - PH_baseline_1)/height <= base_th:   
                                annot_pvr += PH_label_2_ns + " is_at_feet_of " + PH_label_1_ns + "\n"
                            
                            if 0 < vra[PH_idx_2,PH_idx_2][4]:
                                annot_soa_2 += PH_label_2_ns + " is_vertical\n"
                                
                                if (PH_bro_2_1 >= bro_th and
                                    (PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4):
                                    annot_pvr += PH_label_1_ns + " carries " + PH_label_2_ns + "\n"
                            
                            elif -0.1 < vra[PH_idx_2,PH_idx_2][4] <= 0: 
                                if PH_ppa_label_2  in {'sword','lance',}:
                                    annot_soa_2 += PH_label_2_ns + " is_oblique\n"
                                    
                                if vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                    if PH_ppa_label_2 in {'sword','lance',}:
                                        annot_pvr += PH_label_1_ns + " wields/points " + PH_label_2_ns + "\n"
                                    elif PH_ppa_label_2 in {'shield',}:
                                        if PH_bro_2_1 >= bro_th:
                                            annot_pvr += PH_label_1_ns + " holds " + PH_label_2_ns + '\n'
                                        else:
                                            annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                    else:
                                        pass
                                
                                else:
                                    if PH_ppa_label_2 in {'sword','lance',}:
                                        annot_pvr += PH_label_1_ns + " carries " + PH_label_2_ns + "\n"
                                    
                                    
                            elif vra[PH_idx_2,PH_idx_2][4] <= -0.1:
                                annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                                
                                if (PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4:
                                    if PH_ppa_label_2 in {'sword','lance',}:
                                        annot_pvr += PH_label_1_ns + " wields/points " + PH_label_2_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                                        
                            else:
                                pass
                                
                            if vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4:
                                annot_pvr += PH_label_1_ns + " holds_high " + PH_label_2_ns + "\n"
                                
                        elif (vra[PH_idx_1,PH_idx_1][4] <= -0.1):
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                            
                            if -0.1 < vra[PH_idx_2,PH_idx_2][4] <= 0:
                                if PH_ppa_label_2 in {'sword','lance','shield',}:
                                    annot_soa_2 += PH_label_2_ns + " is_oblique\n"
                                    
                            elif 0 < vra[PH_idx_2,PH_idx_2][4]:
                                annot_soa_2 += PH_label_2_ns + " is_vertical\n"
                                
                            else:
                                annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                                annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                        
                        elif (-0.1 < vra[PH_idx_1,PH_idx_1][4] <= 0.1):
                            if PH_ppa_label_1 in person_like_instances:
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                if PH_bro_2_1 >= bro_th:
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"

                            if -0.1 < vra[PH_idx_2,PH_idx_2][4] <= 0.1:
                                if PH_ppa_label_2 in {'sword','lance',}:
                                    annot_soa_2 += PH_label_2_ns + " is_oblique\n"    
                                
                            elif 0.1 < vra[PH_idx_2,PH_idx_2][4]:
                                annot_soa_2 += PH_label_2_ns + " is_vertical\n"
                            
                            else:
                                annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                        else:
                            pass

                        if (PH_ppa_label_1 in {'person', 'man',} and
                            vra[PH_idx_2,PH_idx_2][1][1] < (3*PH_ymax_1 + PH_ymin_1)/4
                           ):
                            annot_soa_1 += PH_label_1_ns + " is ((man_at_arms))\n"
                        
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    
                    if ((ppa_label_1 in person_like_instances
                         and ppa_label_2 in {'skull','scroll',}
                         and bro_2_1 >= 4.)
                        or (ppa_label_2 in person_like_instances
                            and ppa_label_1 in {'skull','scroll',}
                            and bro_1_2 >= 4.)):
                        
                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if  0.1 < vra[PH_idx_1,PH_idx_1][4]:
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                        
                        elif vra[PH_idx_1,PH_idx_1][4] <= -0.1:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                        
                        else:
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                        
                        
                        if ((PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4 or
                            vra[PH_idx_1,PH_idx_1][4] <= 0.1
                           ):
                            annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    
                    if ((ppa_label_1 in {'person','nude','prayer','pope','knight'} 
                         and (ppa_label_2 in {'crown','camauro','saturno','zucchetto','mitre','tiara'}) 
                         and bro_2_1 >= bro_th/3)
                        or (ppa_label_2 in {'person','nude','prayer','pope','knight'} 
                         and (ppa_label_1 in {'crown','camauro','saturno','zucchetto','mitre','tiara'}) 
                         and bro_1_2 >= bro_th/3)):
                                                
                        if ppa_label_1 in {'person','nude','prayer','pope','knight'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if (vra[PH_idx_1,PH_idx_1][4] > 0.1 
                            and vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4):
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                            annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                            
                            if PH_ppa_label_2 == 'crown':
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 in {'saturno','zucchetto'} and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            else:
                                pass
                        elif (-0.1 <= vra[PH_idx_1,PH_idx_1][4] <= 0.1 
                              and vra[PH_idx_2,PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4):
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                            annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                
                            if PH_ppa_label_2 == 'crown' and PH_ppa_label_1 != 'knight':
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'crown' and PH_ppa_label_1 == 'knight':
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 in {'saturno','zucchetto'} and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            else:
                                pass

                        elif (vra[PH_idx_1,PH_idx_1][4] <= -0.1 
                              and ((vra[PH_idx_2,PH_idx_2][1][0] >= PH_xmin_1 
                                    and vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4) 
                                   or (vra[PH_idx_2,PH_idx_2][1][0] >= (PH_xmin_1 + 3*PH_xmax_1)/4 
                                       and vra[PH_idx_2,PH_idx_2][1][0] <= PH_xmax_1))):
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            
                            annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                
                            if PH_ppa_label_2 == 'crown':
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 in {'saturno','zucchetto'} and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                            else:
                                pass

                        else:
                            pass
                                                                           
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    if ((ppa_label_1 in {"person",'prayer','pope'} and ppa_label_2 == "stole") 
                        or (ppa_label_2 in {"person",'prayer','pope'} and ppa_label_1 == "stole")):
                        
                        if ppa_label_1 in {'person','prayer','pope'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_rpos_1_2 = rpos_1_2
                            PH_rpos_2_1 = rpos_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_rpos_1_2 = rpos_2_1
                            PH_rpos_2_1 = rpos_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                            
                            
                        if vra[PH_idx_1,PH_idx_1][4] >= 0.1:
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                        elif -0.1 < vra[PH_idx_1,PH_idx_1][4] < 0.1:
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                        else:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            
                        if PH_bro_2_1 >= bro_th:
                            if (vra[PH_idx_1,PH_idx_1][4] >= -0.1 
                                and vra[PH_idx_2,PH_idx_2][4] > -0.1 
                                and ((PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4
                                     or
                                     (PH_xmax_1 + 3*PH_xmin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][0] <= (3*PH_xmax_1 + PH_xmin_1)/4)):
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                if PH_ppa_label_1 != 'pope':
                                    annot_soa_1 += PH_label_1_ns + " is ((pope))/((cleric))\n"
                            elif (vra[PH_idx_1,PH_idx_1][4] <= -0.1 
                                  and vra[PH_idx_2,PH_idx_2][4] <= -0.1
                                  and (PH_xmax_1 + 3*PH_xmin_1)/4 <= vra[PH_idx_2,PH_idx_2][1][0] <= (PH_xmin_1 + 3*PH_xmax_1)/4):
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                    
                                if PH_ppa_label_1 != 'pope':
                                    annot_soa_1 += PH_label_1_ns + " is ((pope))/((cleric))\n"
                            else:
                                pass
                                                                           
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'



                    if ((ppa_label_1 in {"person",'prayer','pope'} and ppa_label_2 == "crozier") 
                        or (ppa_label_2 in {"person",'prayer','pope'} and ppa_label_1 == "crozier")):

                        if (ppa_label_1 in {"person",'prayer','pope'} and bro_2_1 >= bro_th):
                            if (vra[idx_1,idx_1][4] >= 0.1 and vra[idx_2,idx_2][4] >= 0.1):
                                annot_soa_1 += label_1_ns + " stands\n"
                                annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                if ppa_label_1 != 'pope':
                                    annot_soa_1 += label_1_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            elif (vra[idx_1,idx_1][4] <= -0.1 and vra[idx_2,idx_2][4] <= -0.1):
                                annot_soa_1 += label_1_ns + " lies/reclines\n"
                                annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                if ppa_label_1 != 'pope':
                                    annot_soa_1 += label_1_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            elif (-0.1 < vra[idx_1,idx_1][4] < 0.1):
                                annot_soa_1 += label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                if ppa_label_1 != 'pope':
                                    annot_soa_1 += label_1_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            else:
                                pass

                        if (ppa_label_2 in {"person",'prayer','pope'} and bro_1_2 >= bro_th):
                            if (vra[idx_2,idx_2][4] >= 0.1 and vra[idx_1,idx_1][4] >= 0.1):
                                annot_soa_2 += label_2_ns + " stands\n"
                                annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                if ppa_label_2 != 'pope':
                                    annot_soa_2 += label_2_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            elif (vra[idx_2,idx_2][4] <= -0.1 and vra[idx_1,idx_1][4] <= -0.1):
                                annot_soa_2 += label_2_ns + " lies/reclines\n"
                                annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                if ppa_label_2 != 'pope':
                                    annot_soa_2 += label_2_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            elif (-0.1 < vra[idx_2,idx_2][4] < 0.1):
                                annot_soa_2 += label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                if ppa_label_2 != 'pope':
                                    annot_soa_2 += label_2_ns + " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                            else:
                                pass
                        
                        if annot_soa_1:
                            soa_1_key = label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = label_2_ns + '++' + 'attr'
                        
                    
                    
                    if (ppa_label_1 in quadrupeds and ppa_label_2 in quadrupeds):
                        if (abs(baseline_2 - baseline_1)/height <= base_th):
                            annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                            
                            if ppa_label_1 == ppa_label_2:
                                if label_1_rsa >= 2* label_2_rsa:
                                    annot_soa_2 += label_2_ns + " is " + animal_youngs[ppa_label_2] + "\n"
                                elif label_2_rsa >= 2* label_1_rsa:
                                    annot_soa_1 += label_1_ns + " is " + animal_youngs[ppa_label_1] + "\n"
                                
                        else:
                            if baseline_2 > baseline_1:
                                annot_pvr += label_1_ns + " is_behind " + label_2_ns + "\n"
                            else:
                                annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                            
                        if (vra[idx_1,idx_1][4] >= 0.1 and ppa_label_1 in {'bear'}):
                            annot_soa_1 += label_1_ns + " stands_upright\n"
                                
                        if ppa_label_1 in {'cow','sheep'}:
                            if vra[idx_1,idx_1][4] < -0.2:
                                annot_soa_1 += label_1_ns + " lies\n"
                            elif -0.2 <= vra[idx_1,idx_1][4] < -0.01:
                                annot_soa_1 += label_1_ns + " stands/pastures\n"
                            elif -0.01 <= vra[idx_1,idx_1][4] < 0.1:
                                annot_soa_1 += label_1_ns + " is_partial\n"
                            else:
                                pass
                        if (rear_parameter <= vra[idx_1,idx_1][4] < 0.05 and
                            ppa_label_1 in mountworthy_animals - {'bear','cow','sheep','lion'}):
                            if ppa_label_2 not in {'cow','sheep','horse','donkey','cat','mouse'}:
                                annot_soa_1 += label_1_ns + " rears\n"
                            elif (ppa_label_2 in {'horse'} and
                                  rear_parameter <= vra[idx_2,idx_2][4] < 0.05):
                                annot_soa_1 += label_1_ns + " rears\n"
                            else:
                                annot_soa_1 += label_1_ns + " stands/pastures\n"
                                
                        if (vra[idx_2,idx_2][4] >= 0.1 and ppa_label_2 in {'bear'}):
                            annot_soa_2 += label_2_ns + " stands_upright\n"
                                
                        if ppa_label_2 in {'cow','sheep'}:
                            if vra[idx_2,idx_2][4] < -0.2:
                                annot_soa_2 += label_2_ns + " lies\n"
                            elif -0.2 <= vra[idx_2,idx_2][4] < -0.01:
                                annot_soa_2 = label_2_ns + " stands/pastures\n"
                            elif -0.01 <= vra[idx_2,idx_2][4] < 0.1:
                                annot_soa_2 = label_2_ns + " is_partial\n"
                            else:
                                pass

                        if (rear_parameter <= vra[idx_2,idx_2][4] < 0.05 and
                            ppa_label_2 in mountworthy_animals - {'bear','cow','sheep','lion'}):
                            if ppa_label_1 not in {'cow','sheep','horse','donkey','cat','mouse'}:
                                annot_soa_2 += label_2_ns + " rears\n"
                            elif (ppa_label_1 in {'horse'} and
                                  rear_parameter <= vra[idx_1,idx_1][4] < 0.05):
                                annot_soa_2 += label_2_ns + " rears\n"
                            else:  
                                annot_soa_2 += label_2_ns + " stands/pastures\n"
                                                
                        if annot_soa_1:
                            soa_1_key = label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = label_2_ns + '++' + 'attr'                        
                    
                    
                    
                    if ((ppa_label_1 in person_like_instances and ppa_label_2 in {'boat',})
                        or 
                        (ppa_label_2 in person_like_instances and ppa_label_1 in {'boat',})):
                        
                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                                            
                        if (vra[PH_idx_1,PH_idx_1][4] >= 0.1):
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                        elif (-0.1 <= vra[PH_idx_1,PH_idx_1][4] < 0.1):
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                        else:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            
                        if vra[PH_idx_1,PH_idx_2][3] in {'NN','NE','NW'}:
                            # test on 'PH_rpos_1_2'
                            annot_pvr += PH_label_1_ns + " rides/is_on " + PH_label_2_ns + "\n"
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'                     
                    
                    
                    
                    if ((ppa_label_1 in person_like_instances and ppa_label_2 in mountworthy_animals)
                        or 
                        (ppa_label_2 in person_like_instances and ppa_label_1 in mountworthy_animals)):
                        
                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_prop_obj_2_1 = prop_obj_2_1
                            PH_prop_obj_1_2 = prop_obj_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_prop_obj_2_1 = prop_obj_1_2
                            PH_prop_obj_1_2 = prop_obj_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if (vra[PH_idx_1,PH_idx_1][4] >= 0.1):
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                        elif (-0.1 <= vra[PH_idx_1,PH_idx_1][4] < 0.1):
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                        else:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                                
                        if ((PH_bro_2_1 >= bro_th/2 or PH_bro_1_2 >= bro_th/2)
                            and abs(PH_baseline_2 - PH_baseline_1)/height > base_th
                            and vra[PH_idx_1,PH_idx_1][4] >= -0.1):
                            
                            if vra[PH_idx_1,PH_idx_2][3]== 'NN':
                                annot_pvr += PH_label_1_ns + " rides/is_on " + PH_label_2_ns + "\n"
                            elif (vra[PH_idx_1,PH_idx_2][3] in {'SE','SW','SS'} 
                                  and PH_ppa_label_2 in {'pegasus'}):
                                annot_pvr += PH_label_2_ns + " hovers_over " + PH_label_1_ns + "\n"
                                annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                            else:
                                if abs(size_criterion) <= size_th:
                                    annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                elif ((size_criterion < 0 and prop_obj_2_1 > prop_obj_1_2) or
                                      (size_criterion > 0 and prop_obj_2_1 < prop_obj_1_2)):
                                    if PH_label_1 == label_1:
                                        annot_pvr += PH_label_1_ns + " is_behind " + PH_label_2_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + "\n"
                                elif ((size_criterion < 0 and prop_obj_2_1 < prop_obj_1_2) or
                                      (size_criterion > 0 and prop_obj_2_1 > prop_obj_1_2)):
                                    if PH_label_1 == label_1:
                                        annot_pvr += PH_label_1_ns + " is_before " + PH_label_2_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + "\n"
                                else:
                                    pass
                        elif (abs(PH_baseline_2 - PH_baseline_1)/height <= base_th and
                              vra[PH_idx_1,PH_idx_1][4] >= -0.1):
                            
                            if PH_baseline_1 < PH_baseline_2:
                                annot_pvr += PH_label_1_ns + " is_behind " + PH_label_2_ns + "\n"
                            elif PH_baseline_1 > PH_baseline_2:
                                annot_pvr += PH_label_1_ns + " is_before " + PH_label_2_ns + "\n"
                            else:
                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                        elif (abs(PH_baseline_2 - PH_baseline_1)/height <= base_th
                              and vra[PH_idx_1,PH_idx_2][3] in {'SE','SW','SS'}
                              and vra[PH_idx_1,PH_idx_1][4] <= 0.1):
                            annot_pvr += PH_label_1_ns + " is_at_feet_of " + PH_label_2_ns + "\n"
                        else:
                            pass
                        
                        if (vra[PH_idx_2,PH_idx_2][4] >= 0.1 and PH_label_2 in {'bear'}):
                            annot_soa_2 += PH_label_2_ns + " stands_upright\n"
                                
                        if (vra[PH_idx_2,PH_idx_2][4] <= 0.1 and PH_label_2 in {'cow','sheep'}):
                            annot_soa_2 += PH_label_2_ns + " stands/pastures\n"
                        

                        if (rear_parameter <= vra[PH_idx_2,PH_idx_2][4] < 0.05 
                            and PH_label_2 in mountworthy_animals - {'bear','cow','sheep','lion'}):
                            annot_soa_2 += PH_label_2_ns + " rears\n"
                                
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                        
                    
                    
                    if ((ppa_label_1 in person_like_instances and ppa_label_2 == 'angel') 
                        or (ppa_label_2 in person_like_instances and ppa_label_1 == 'angel')):

                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if vra[PH_idx_1,PH_idx_1][4] >= 0.1:
                            annot_soa_1 += PH_label_1_ns + " stands\n"
                        elif vra[PH_idx_1,PH_idx_1][4] <= -0.1:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines/is_horizontal\n"
                        else:
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                        if vra[PH_idx_2,PH_idx_2][4] >= 0.1:
                            annot_soa_2 += PH_label_2_ns + " stands\n"
                            if PH_baseline_2 <= (PH_ymin_1 + PH_ymax_1)/2:    
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                            elif abs(base_criterion) <= base_th:
                                annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                            else:
                                pass
                        elif vra[PH_idx_2,PH_idx_2][4] <= -0.1:
                            if abs(base_criterion) <= base_th:
                                annot_soa_2 += PH_label_2_ns + " lies/reclines\n"
                                annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                            elif PH_baseline_2 <= (PH_ymin_1 + PH_ymax_1)/2:
                                annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                            else:
                                pass
                        else:
                            annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                        if vra[PH_idx_2,PH_idx_1][3] == 'WW':
                            annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + "\n"
                        elif vra[PH_idx_2,PH_idx_1][3] == 'EE':
                            annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + "\n"
                        elif vra[PH_idx_2,PH_idx_1][3] in {'NN','NW','NE'}:
                            annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                        elif vra[PH_idx_2,PH_idx_1][3] in {'SS','SW','SE'}:
                            annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + "\n"
                        else:
                            pass
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    if ({ppa_label_1, ppa_label_2} == {"lily","angel"}):

                        if ppa_label_1 == 'angel':
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if vra[PH_idx_1,PH_idx_1][4] >= 0.1:
                            annot_soa_1 += PH_label_1_ns + " stands/is_vertical\n"
                        elif vra[PH_idx_1,PH_idx_1][4] <= -0.1:
                            annot_soa_1 += PH_label_1_ns + " lies/reclines/is_horizontal\n"
                        else:
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                        annot_soa_1 += PH_label_1_ns + " is ((saint_gabriel))\n"

                        if vra[PH_idx_2,PH_idx_2][4] >= 0.05:
                            annot_soa_2 += PH_label_2_ns + " is_vertical\n"
                        elif vra[PH_idx_2,PH_idx_2][4] <= -0.1:
                            annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                        else:
                            annot_soa_2 += PH_label_2_ns + " is_at_a_slant/is_oblique\n"

                        if vra[PH_idx_2,PH_idx_1][3] == 'WW':
                            annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + "\n"
                        elif vra[PH_idx_2,PH_idx_1][3] == 'EE':
                            annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + "\n"
                        elif (vra[PH_idx_2,PH_idx_1][3] in {'NN','NW','NE'} and
                              PH_baseline_2 <= (5*PH_ymin_1+PH_ymax_1)/6):
                            annot_pvr += PH_label_2_ns + " is_atop " + PH_label_1_ns + "\n"
                        elif vra[PH_idx_2,PH_idx_1][3] in {'SS','SW','SE'}:
                            annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + "\n"
                        else:
                            pass

                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    if ((ppa_label_1 in {'banana','apple','orange','serpent'} and ppa_label_2 == 'tree') 
                        or (ppa_label_2 in {'banana','apple','orange','serpent'} and ppa_label_1 == 'tree')):

                        if ppa_label_1 in {'banana','apple','orange', 'serpent'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if PH_baseline_1 <= vra[PH_idx_2,PH_idx_2][1][1]:
                            annot_pvr += PH_label_1_ns + " hangs_from " + PH_label_2_ns + "\n"
                        else: 
                            pass
                    
                    
                    if ((ppa_label_1 in person_like_instances
                         and ppa_label_2 in {'table', 'seat','bedding','rock'}) 
                        or (ppa_label_2 in person_like_instances
                            and ppa_label_1 in {'table','seat','bedding','rock'})):

                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if (abs(base_criterion) <= 1.2*base_th and 0 <= vra[PH_idx_1,PH_idx_1][4] <= 0.1):
                            annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            
                            if PH_ppa_label_2 == 'table':
                                annot_pvr += PH_label_1_ns + " sits_at " + PH_label_2_ns + "\n"
                            elif PH_ppa_label_2 in {'seat','bedding','rock'}:
                                annot_pvr += PH_label_1_ns + " sits_on " + PH_label_2_ns + "\n"
                            else:
                                pass
                        elif (PH_ppa_label_2 != 'seat' 
                              and abs(base_criterion) > 1.2 * base_th 
                              and vra[PH_idx_1,PH_idx_1][4] <= -0.1 
                              and vra[PH_idx_1,PH_idx_2][3] == 'NN'):
                            annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                            annot_pvr += PH_label_1_ns + " lies_on " + PH_label_2_ns + "\n"
                        else: 
                            pass

                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    if ((ppa_label_1 in small_objects 
                         and ppa_label_2 in {'table','seat','bedding','rock','shield'}) 
                        or (ppa_label_2 in small_objects 
                            and ppa_label_1 in {'table','seat','bedding','rock','shield'})):

                        if ppa_label_1 in small_objects:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if (vra[PH_idx_1,PH_idx_2][3] == 'NN' 
                            and PH_baseline_1 >= PH_baseline_2 
                            and PH_bro_1_2 >= min(1.7 * bro_th, 70)): # ALTERNATIVE: use absolute threshold value
                            #and vra[PH_idx_1,PH_idx_1][3] <= vra[PH_idx_2,PH_idx_2][3] * 0.4
                            annot_pvr += PH_label_1_ns + " rests_on/lays_on " + PH_label_2_ns + "\n"
                    
                    
                    if ((ppa_label_1 in {'severed head',} and ppa_label_2 in {'sword',}) 
                        or (ppa_label_2 in {'severed head',} and ppa_label_1 in {'sword'})
                       ):

                        if ppa_label_1 in {'severed head',}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                        if (vra[PH_idx_1,PH_idx_1][4] <= 0 and 
                            (PH_bro_1_2 >= 20 or PH_bro_2_1 >= 20)
                           ):
                            annot_pvr += PH_label_1_ns + " lies_next_to " + PH_label_2_ns + "\n"
                    
                    
                    if ((ppa_label_1 in person_like_instances
                         and ppa_label_2 in {'banana','apple','orange'}) 
                        or (ppa_label_2 in person_like_instances
                            and ppa_label_1 in {'banana','apple','orange'})):

                        if ppa_label_1 in person_like_instances:
                            PH_label_1 = label_1
                            PH_label_2 = label_2   
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns                             
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1


                        PH_ppa_label_1 = '_'.join(PH_label_1.split(sep='_')[:-1])
                        PH_ppa_idx_1 = class_lst.index(PH_ppa_label_1)
                        PH_ppa_label_2 = '_'.join(PH_label_2.split(sep='_')[:-1])
                        PH_ppa_idx_2 = class_lst.index(PH_ppa_label_2)
                        prop_obj_2_1 = ppa[PH_ppa_idx_2][PH_ppa_idx_1]

                        if (PH_ymin_1 < vra[PH_idx_2,PH_idx_2][1][1] <= (3*PH_ymin_1 + PH_ymax_1)/4 
                            and vra[PH_idx_1,PH_idx_1][4] >= 0 
                            and PH_bro_2_1 >= min(2 * bro_th,0.85)
                            and abs(vra[PH_idx_2,PH_idx_2][3] - prop_obj_2_1 * vra[PH_idx_1,PH_idx_1][3]) <= size_criterion):
                            
                            annot_pvr += PH_label_1_ns + " eats/holds " + PH_label_2_ns + "\n"
                    
                    
                    if (ppa_label_1 in flying_birds or ppa_label_2 in flying_birds):
                        
                        if ppa_label_1 in flying_birds:
                            PH_label_1 = label_1
                            PH_label_2 = label_2         
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_prop_obj_1_2 = prop_obj_1_2
                            PH_prop_obj_2_1 = prop_obj_2_1   
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1   
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_prop_obj_1_2 = prop_obj_2_1
                            PH_prop_obj_2_1 = prop_obj_1_2 
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                        
                        if (PH_ppa_label_2 not in person_like_instances.union({'hand'}) and
                            not (PH_ppa_label_2 in flying_birds and vra[PH_idx_2,PH_idx_2][4] >= -0.1) and
                            vra[PH_idx_1,PH_idx_1][4] <= -0.1):
                            annot_soa_1 += PH_label_1_ns + " is_flying\n"
                            
                        if PH_ppa_label_2 in person_like_instances:
                            if vra[PH_idx_2,PH_idx_2][4] >= 0.1:
                                annot_soa_2 += PH_label_2_ns + " stands\n"
                            elif -0.1 <= vra[PH_idx_2,PH_idx_2][4] < 0.1:
                                annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            else:
                                annot_soa_2 += PH_label_2_ns + " lies/reclines\n"
                            
                            if vra[PH_idx_1,PH_idx_1][4] <= -0.05:
                                annot_soa_1 += PH_label_1_ns + " has_deployed_wings\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                                    
                            if (vra[PH_idx_2,PH_idx_2][4] >= -0.1 and
                                PH_ymin_2 < PH_baseline_1 <= (4*PH_ymin_2 + PH_ymax_2)/5 and
                                vra[PH_idx_1,PH_idx_1][4] > -0.1):
                                annot_pvr += PH_label_1_ns + " is_on_shoulder_of/is_at_shoulder_level_of " + PH_label_2_ns + "\n"
                            elif (vra[PH_idx_2,PH_idx_2][4] < -0.1 and
                                  PH_ymin_2 < PH_baseline_1 <= (4*PH_ymin_2 + PH_ymax_2)/5):
                                annot_pvr += PH_label_1_ns + " is_on_top_of " + PH_label_2_ns + "\n"
                            elif (vra[PH_idx_2,PH_idx_2][4] < -0.1 and
                                  (4*PH_xmin_2 + PH_xmax_2)/5 <= vra[PH_idx_1,PH_idx_1][1][0] <= (PH_xmin_2 + 4*PH_xmax_2)/5 and
                                  (4*PH_ymin_2 + PH_ymax_2)/5 <= vra[PH_idx_1,PH_idx_1][1][1] <= (PH_ymin_2 + 4*PH_ymax_2)/5 and
                                    PH_bro_1_2 >= bro_th/2):
                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                annot_pvr += PH_label_1_ns + " is_at_mid-section_of " + PH_label_2_ns + "\n"
                            elif (vra[PH_idx_2,PH_idx_2][4] >= -0.05 and
                                (PH_ymin_2 + 4*PH_ymax_2)/5 < PH_baseline_1 <= 1.1*PH_ymax_2):
                                annot_pvr += PH_label_1_ns + " is_at_feet_of " + PH_label_2_ns + "\n"
                            else:
                                pass
                        
                        elif (PH_ppa_label_2 in {'hand'} and
                              vra[PH_idx_1,PH_idx_2][3] in {'NN','NE','NW'}):
                            annot_pvr += PH_label_1_ns + " sits_on " + PH_label_2_ns + "\n"
                            
                            if vra[PH_idx_1,PH_idx_1][4] <= 0:
                                annot_soa_1 += PH_label_1_ns + " has_deployed_wings\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " has_folded_wings\n"
                        
                        elif ((PH_ppa_label_2 in flying_birds and
                               (vra[PH_idx_2,PH_idx_2][4] >= -0.1 or vra[PH_idx_1,PH_idx_1][4] >= -0.1)) or
                              (PH_ppa_label_2 in quadrupeds and vra[PH_idx_1,PH_idx_1][4] < 0.1)
                             ):
                            #and  abs(size_criterion) < size_th):
                            annot_pvr += PH_label_1_ns + " rests_close_to " + PH_label_2_ns + "\n"
                        
                        else:
                            pass
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
                    
                    
                    if ((ppa_label_1 in person_like_instances and ppa_label_2 in {'hand'})
                        or
                        (ppa_label_2 in person_like_instances and ppa_label_1 in {'hand'})):
                        
                        if ppa_label_1 in {'hand'}:
                            PH_label_1 = label_1
                            PH_label_2 = label_2         
                            PH_label_1_ns = label_1_ns
                            PH_label_2_ns = label_2_ns
                            PH_ppa_label_1 = ppa_label_1
                            PH_ppa_label_2 = ppa_label_2
                            PH_idx_1 = idx_1
                            PH_idx_2 = idx_2
                            PH_ppa_idx_1 = ppa_idx_1
                            PH_ppa_idx_2 = ppa_idx_2
                            PH_baseline_1 = baseline_1
                            PH_baseline_2 = baseline_2
                            PH_bro_1_2 = bro_1_2
                            PH_bro_2_1 = bro_2_1
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                        else:
                            PH_label_1 = label_2
                            PH_label_2 = label_1   
                            PH_label_1_ns = label_2_ns
                            PH_label_2_ns = label_1_ns
                            PH_ppa_label_1 = ppa_label_2
                            PH_ppa_label_2 = ppa_label_1
                            PH_idx_1 = idx_2
                            PH_idx_2 = idx_1
                            PH_ppa_idx_1 = ppa_idx_2
                            PH_ppa_idx_2 = ppa_idx_1
                            PH_baseline_1 = baseline_2
                            PH_baseline_2 = baseline_1
                            PH_bro_1_2 = bro_2_1
                            PH_bro_2_1 = bro_1_2
                            PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                            PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1
                            
                        if vra[PH_idx_2,PH_idx_2][4] >= 0.1:
                            annot_soa_2 += PH_label_2_ns + " stands\n"
                            
                            if (size_criterion <= 1.5 * size_th and PH_bro_1_2 >= 90):
                                annot_pvr += PH_label_2_ns + " has " + PH_label_1_ns + "\n"
                                
                            if (3*PH_ymin_2 + PH_ymax_2)/4 <= vra[PH_idx_1,PH_idx_1][1][1] <= (PH_ymin_2 + 3*PH_ymax_2)/4:
                                annot_pvr += PH_label_1_ns + " is_at_mid-section_of " + PH_label_2_ns + "\n"
                            elif vra[PH_idx_1,PH_idx_1][1][1] <= (2*PH_ymin_2 + PH_ymax_2)/3:
                                annot_pvr += PH_label_2_ns + " lifts_up " + PH_label_1_ns + "\n"
                            else:
                                pass
                        elif -0.1 <= vra[PH_idx_2,PH_idx_2][4] <= 0.1:
                            annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                        else:
                            annot_soa_2 += PH_label_2_ns + " lies/reclines\n"      
                        
                        if annot_soa_1:
                            soa_1_key = PH_label_1_ns + '++' + 'attr'
                        
                        if annot_soa_2:
                            soa_2_key = PH_label_2_ns + '++' + 'attr'
                    
            
                    # ####################
                    if annot_pvr:
                        pol_key_1_2 = label_1_ns + '++' + label_2_ns
                        pol_key_2_1 = label_2_ns + '++' + label_1_ns
                        
                        if pol_key_1_2 in annot_txt.keys():
                            annot_txt[pol_key_1_2] += list(annot_pvr.split('\n')[:-1])
                            annot_txt[pol_key_1_2] = list(set(annot_txt[pol_key_1_2])) # suppress dupes
                        elif pol_key_2_1 in annot_txt.keys():
                            annot_txt[pol_key_2_1] += list(annot_pvr.split('\n')[:-1])
                            annot_txt[pol_key_2_1] = list(set(annot_txt[pol_key_2_1])) # suppress dupes
                        else:
                            annot_txt[pol_key_1_2] = list(set(annot_pvr.split('\n')[:-1])) # suppress dupes
                        
                        
                        if soa_1_key != None:
                            if soa_1_key in annot_txt.keys():
                                annot_txt[soa_1_key] += list(annot_soa_1.split('\n')[:-1])
                            else:
                                annot_txt[soa_1_key] = list(annot_soa_1.split('\n')[:-1])
                            
                            annot_txt[soa_1_key] = list(set(annot_txt[soa_1_key])) # suppress dupes
                        
                        if soa_2_key != None:
                            if soa_2_key in annot_txt.keys():
                                annot_txt[soa_2_key] += list(annot_soa_2.split('\n')[:-1])
                            else:
                                annot_txt[soa_2_key] = list(annot_soa_2.split('\n')[:-1])
                            
                            annot_txt[soa_2_key] = list(set(annot_txt[soa_2_key])) # suppress dupes
    else:
        pass
    
    return annot_txt


def get_descriptoids(basename: str, 
                     img_vis_rel: 'np.ndarray', 
                     size_th: float, 
                     base_th: float, 
                     bro_th: float, 
                     img_width: int, 
                     img_height: int, 
                     img_bbx_loc: List[Tuple[str, str, int, int, int, int]]) -> dict:
    '''
    get_descriptoids()::
      - Compute basic description elements from visual relationships between bbxes in an image.
    
    Input:
        @type      basename: str
        @type   img_vis_rel: np.ndarray
        @type       size_th: float
        @type       base_th: float
        @type        bro_th: float
        @type     img_width: int
        @type    img_height: int
        @type   img_bbx_loc: List[Tuple[str, str, int, int, int, int]]

        @param     basename: image filename with no suffix
        @param  img_vis_rel: image visual relationships' array
        @param      size_th: bbx' size criterion's threshold
        @param      base_th: bbx' baseline criterion's threshold
        @param       bro_th: bbx' percent surface overlap criterion's threshold
        @param    img_width: image width in pixels
        @param   img_height: image height in pixels
    
    
    Output:       
        @type           vrd: dict 
        @param          vrd: return structure:
            {'filename':basename,
            'artist':{'name':{'firstname':None, 'lastname':None},
                       'life_dates':{'birth_year':None, 'death_year':None}},
            'artwork':{'title':{'en':None,},
                        'work_dates':{'year':[],'century':None},
                        'dimensions':{'width':None,'height':None}},
            'main_topic(s)':{'notable_objs':[],'bigger_objs':[], 'ref_objs':[]},
            'composition':{'foreback':{}, 'omt':{},'amt':{},'bmt':{}, 'mylomt':{},'myromt':{}},
            'annot_txt':{}}

            Detailed structure:
                'main_topics': elements are lists of tuples (uniq_label, (x_ctp, y_ctp), oloc, rsa)
                'composition': values are sets of 'uniq_label':
                    'cluster(s)': dict 
                        key: uniq_label of one of bbx pertaining to set.
                        value: set of 'ref_objs' uniq_label that overlap.
                    'foreback': dict of sets of ('uniq_label','base_criterion','size_criterion') tuples, 
                        situated in the foreground (dist-factor >0) or in the background (dist_factor <0) 
                        with respect to the plane of representation of the 'main_topic'
                        keys are clusters labels.
                    'omt': dict of sets of 'uniq_label' situated "over main topic"
                        i.e. near or on top of it, behind or in front of it.
                        keys are cluster labels.
                    'amt': dict of sets of 'uniq_label' situated "above main topic".
                        keys are cluster labels.
                    'bmt': dict of sets of 'uniq_label' situated "beneath main topic".
                        keys are cluster labels.
                    'mylomt': dict of sets of 'uniq_label' situated to "my left of main topic".
                        keys are cluster labels.
                    'myromt': dict of sets of 'uniq_label' situated to "my right of main topic".
                        keys are cluster labels.
                'annot_txt': dict
                    key = 'uniq_label_1+uniq_label_2'  for pairwise visual relationships between objects
                    key = 'uniq_label_1'+'attr'        for single/isolated objects' attributes
    '''
    
    rel_sizes = np.array(ppa)
    vra = np.array(img_vis_rel)
    
    vrd = dict()
    annot_txt = dict()
    
    vrd = {'filename':basename,
           'artist':{'name':{'firstname':None, 'lastname':None},
                     'life_dates':{'birth_year':None, 'death_year':None}},
           'artwork':{'title':{'en':None,},
                      'work_dates':{'year':[],'century':None},
                      'dims_cm':{'width':None,'height':None},
                      'dims_pixel':{'width':img_width,'height':img_height}},
           'main_topics':{'notable_objs':[],'bigger_objs':[], 'ref_objs':[]},
           'composition':{'clusters':{},'foreback':{},'omt':{},'amt':{},'bmt':{},'mylomt':{},'myromt':{}},
           'annot_txt': annot_txt
          }

    if len(vra) == 0:
        pass
    else:
        vrd['main_topics'].update(get_main_topics(basename,img_vis_rel))
        ref_objs = vrd['main_topics']['ref_objs']
        print(f' ref_objs:\n{ref_objs}')                        # TESTING
        vrd['composition'] = get_composition(basename,img_width,img_height,img_bbx_loc,img_vis_rel,ref_objs)
        print(f' composition:\n{vrd["composition"]}')          # TESTING
        vrd['annot_txt'] = get_annot_txt(basename,img_width,img_height,img_bbx_loc,img_vis_rel,size_th,base_th,bro_th)
        print(f' annotations:\n{vrd["annot_txt"]}\n')    # TESTING
    
    return vrd


def show_img_bbx(f_jpg: str, f_json: str, display_only: float =1.0) -> None:
    '''
    show_img_bbx()::
        Open selected image file, draw bounding boxes, and display image using default 
        external display utility.
    
      Input:
        @type               f_jpg: str
        @param              f_jpg: fully qualified jpeg or png image filename
    
        @type              f_json: str
        @param             f_json: fully qualified json filename;
                                    contains annotations extracted from xml file
    
        @type        display_only: float
        @param       display_only: values between 0. and 1. (default)
    
      Output:
        @return:             None
    '''
    global width, height
    
    if not ( 0. <= display_only <= 1. ):
        print(f'Usage: kwarg \'display_only\' accepts values between 0.0 and 1.0. \nExecution defaults to 1.0 and proceeds.')
        display_only = 1.   
    
    ## Open image descriptor
    img = Image.open(f_jpg, mode='r', formats=None)
    img.LOAD_TRUNCATED_IMAGES = True
    
    draw = ImageDraw.Draw(img)        
    width, height = img.size
    
    ## Prepare image display with bbx decorations of detected objects
    path_to_font = '/usr/share/fonts/TTF/times.ttf'    # system specific
    if os.path.exists(path_to_font):
        try:
            font_size = max(28 * int(1.5*max(width/1000, height/1500)),28)
            label_font = ImageFont.truetype(path_to_font,font_size)
        except (NameError) as ne:
            print(ne + '\n ==> Missing \'from PIL import ImageFont\'\n If necessary install module PIL/Pillow. Abort.')
            raise ne
        except Exception as ex:
            print(ex)
            print('Unsure about what happened. Probably unsafe to continue; ttf loading interrupted. Abort.')
            raise ex
    else:
        print(' ==> Missing font or erroneous path to font file. Continue with system default font.')
        label_font = ''
    
    ## Load relevant bbx data
    try:
        with open(f_json, "r") as infile:
            data = json.load(infile)
    except (IOError) as ioe:
        print(' ==> json file cannot be opened or loaded properly. Abort.')
        raise ioe
    except (JSONDecodeError) as jse:
        print(' ==> deserialized data from infile is not JSON. Abort.')
        raise jse
        
    bbx_locs, _ = json_parse(data, key_stem='bbx', find='first', key_suffix=('_basic',))
    
    ## Adjust label_cx to prevent overlap of bbxes' labels for bbxes w/ similar baselines
    label_locs = list()         # list of label positions on bbx' baselines
    label_posit_dic = dict()    # dictionary: keys are 'label', values are '(label_cx, label_cy)'
    label_cymin = height
    label_cymax = 0
    
    if len(bbx_locs) != 0:
        for label, label_id, xmin, ymin, xmax, ymax in bbx_locs:
            label = label+"_"+label_id
            label_length = int(label_font.getsize(label)[0])
            #x_offset = label_length/2
            #label_cx = random.uniform(xmin+x_offset,xmax-x_offset)
            if int((xmin + xmax)/2 + label_font.getsize(label)[0]) < width-8:
                label_cx = int((xmin + xmax)/2)
            else:
                label_cx = width - label_length - 2

            label_cy = ymax-round(font_size*1.1,0)

            label_locs.append([label,label_cx, label_cy,label_length, xmin, xmax])
    
    if len(bbx_locs) > 1:
        for idx1 in range(0,len(label_locs)-1):
            label1, label1_cx, label1_cy, label1_length, xmin1, xmax1 = label_locs[idx1]
            for idx2 in range(idx1+1,len(label_locs)):
                label_locs[idx2][1]
                label2, label2_cx, label2_cy, label2_length, xmin2, xmax2 = label_locs[idx2]

                eps = 1.0

                while (abs(label1_cy - label2_cy) <= font_size*1.1 and 
                       abs(label1_cx - label2_cx) <= 1.1*max(label1_length,label2_length) and
                       eps > 1e-3):
                    label1_cx_old = label1_cx
                    label2_cx_old = label2_cx

                    if label1_cx <= label2_cx:
                        if label1_cx >= max(xmin1 - label1_length + 8, 8):
                            label1_cx -= 4
                        if label2_cx <= min(xmax2 + label2_length - 8, width - label2_length - 8):
                            label2_cx += 4
                    else:
                        if label1_cx <= min(xmax1 + label1_length - 8, width - label1_length - 8):
                            label1_cx += 4
                        if label2_cx >= max(xmin2 - label2_length + 8, 8):
                            label2_cx -= 4

                    try:
                        eps = abs(label1_cx - label2_cx)/abs(label1_cx_old - label2_cx_old)-1
                    except ZeroDivisionError:
                        eps = abs(label1_cx - label2_cx + 1)/abs(label1_cx_old - label2_cx_old + 1)-1
                        
                label_locs[idx1][1] = label1_cx
                label_locs[idx2][1] = label2_cx
                label_posit_dic[label1] = (label1_cx, label1_cy)
                label_posit_dic[label2] = (label2_cx, label2_cy)
    
    elif len(bbx_locs) == 1:
        label_posit_dic[label] = (label_cx, label_cy)
    
    else:
        if random.random() <= display_only:
            img.show()
            return
    
    # Compute optimal bbx' line width    
    for label, label_id, xmin, ymin, xmax, ymax in bbx_locs:
        label += "_"+label_id
        label_posit = label_posit_dic[label]
        
        # draw bbx and text
        lw = int(max(1, 2*width/1000+0.5, 2*height/1500+0.5))     # line width
        draw.line([(xmin, ymin), (xmax,ymin), (xmax,ymax), (xmin,ymax),(xmin, ymin)],
                  fill='red',
                  width=lw)
        
        if label_font:
            draw.text(label_posit,
                      label, 
                      fill='white',
                      font=label_font)
        else:
            draw.text(label_posit,
                      label, 
                      fill='white')
            
        
    if random.random() <= display_only:
        ImageShow.show(img,title=f_jpg)
    
    img.close()
    
    return

if __name__ == '__main__':
    # %%
    """
    ### Hyperparameters
    """

    # %%
    ## Global variables & hyperparameters
    #  ################   ###############                        

    base_th = 0.08    # applies to linear dimensions (e.g. baseline, height) of bbxes
    size_th = 0.10    # applies to surface areas (e.g. 'rsa') of bbxes
    bro_th = 40       # values [0, 100], applies to 'bro_1_2' parameter values
    rear_parameter = -0.03  
                    # characterizes attitude of mountworthy_animals capable of rearing
                    #+ (horse, donkey, zebra, elephant, pegasus, dragon...)

    # %%
    ## Prepare destination directory for imported images
    #  ####################

    path_lst = os.getcwd().split('/')
    try:
        path_list_idx = [idx for idx,val in enumerate(path_lst) if val == 'Sgoab'][-1]
    except (ValueError, IndexError) as err:
        path_list_idx = len(path_lst)-1   # -1 to align to index count
        
    path_lst = path_lst[:path_list_idx+1]

    # %%
    ## Read/Select source directory and basenames of files to process
    #  ####################

    # Select image basename trailing suffix if any
    ser_suffix = '' # '_train' # '_test' #
    """@smendoza
    data_dir = '/'.join(path_lst) + '/Data'
    img_dir = '/'.join(path_lst) + '/Data/Prado_train_dataset'
    """

    data_dir = '/'.join(path_lst)
    img_dir = '/'.join(path_lst) + '/Data/Prado_train_dataset'


    print('Location to keep images to process:\n    img_dir: {0}\n'.format(img_dir))

    # List files basenames manually
    #files = [...,...,]
    #print('\n',*files,sep='\n')

    # or
    # Obtain list of all files in `img_dir` programatically
    files = list()
    for p, _, files_lst in os.walk(img_dir,topdown=True):
        if p == img_dir:
            for f in files_lst:
                if ser_suffix != '':
                    f = '.'.join(f.split('.')[:-1]).split(ser_suffix)[0] 
                else:
                    f = '.'.join(f.split('.')[:-1])

                if (os.path.isfile(img_dir + '/' + f + '.jpg') and
                    os.path.isfile(img_dir + '/' + f + ser_suffix + '.xml')
                ):
                    files.append(f)

    exclude_files = ['00c008ca6-03ce-0241-7ea7-bd1002eb8802',]
                # b&w image (depth=1) cannot be processed by Vis-Rel; nothing detected by R-CNN.
    test_set_files = []

    files = list(set(files)) 
    files = [file for file in files if file not in exclude_files + test_set_files]
    files.sort()

    # %%
    ## Read in classes & define hyperclasses
    #  ####################

    infile = data_dir + '/vis_rel/' + 'class.ppa'
    with open(infile, 'rb') as file_from:
        ppa = np.load(file_from, allow_pickle=True)

    # Class labels
    class_lst = [ppa[idx][idx][0] for idx in range(len(ppa))]

    ## Define hyperclasses as class groups' labels
    person_like_instances = {"person","child","devil","angel","prayer","man","woman",
                            "monk","nun","pope","knight","king","queen","judith",
                            "peasant","shepherd","fisherman","hermit","hunter","nude"}

    small_objects = {'banana','apple','orange','skull','sword','helmet','bird','dove',
                    'rooster','mouse','sea shell','book','scroll','cross','chalice','arrow',
                    'jug','hand','severed head','sculpture','crown','crown of thorns',
                    'mitre','saturno','tiara','zucchetto'}

    winged_things = {'angel','devil','dragon','pegasus',
                    'bird','rooster','butterfly','dove','eagle','swan'}

    flying_birds = winged_things - {'angel','devil','dragon','pegasus','rooster'}

    quadrupeds = {'dragon','pegasus','centaur',
                'horse','donkey','sheep','cow','cat','dog',
                'mouse',
                'elephant','lion','monkey','zebra','bear','deer',}

    mountworthy_animals = {'horse','donkey','zebra','deer','pegasus','centaur','bear','lion','elephant',
                        'cow','sheep'}

    animal_youngs = {'horse':'foal','zebra':'colt','bear':'cub','lion':'cub','cow':'calf','elephant':'calf',
                    'dog':'brat','cat':'kitten','person':'infant','monkey':'infant','deer':'fawn',
                    'sheep':'lamb','rooster':'chick','mouse':'pup','swan':'flapper','snake':'snakelet'}

    # %%
    ## Select test switch (... continued)
    # ##############

    is_test = True # False #

    if is_test:
        zeiganteil = 1.0   # percentage of processed images being displayed
        loc_files = [(img_dir + '/' + file + '.jpg',
                    img_dir + '/' + file + ser_suffix + '.vra', 
                    img_dir + '/' + file + ser_suffix + '.json') 
                    for file in ['Cabeza_de_San_Pablo_2C_copia_de_un_original_de_Murillo__28Museo_del_Prado_29',]]
    else:
        zeiganteil = 0.07  # percentage of processed images being displayed
        loc_files = [(img_dir + '/' + file + '.jpg',
                    img_dir + '/' + file + ser_suffix + '.vra', 
                    img_dir + '/' + file + ser_suffix + '.json') 
                    for file in files]

    # %%
    ## Display sample of collected images with bbxes
    # #####################

    sample_idx = random.sample(range(len(loc_files)),int(zeiganteil * len(loc_files)))

    print(f'Processing {ser_suffix} images:')
    for idx in sample_idx:
        f_jpg, _, f_json = loc_files[idx]
        if ser_suffix != '':
            print(f' - {f_jpg.split("/")[-1].split("_")[:-1]}')
        else:
            print(f' - {f_jpg.split("/")[-1]}')
        show_img_bbx(f_jpg, f_json)

    # %%
    ## Load image metadata & apply VISREL rules
    #  ###################

    print('\n\nProcessing images:\n-----------------')

    for f_jpg, f_vra, f_json in loc_files:
        filename = f_jpg.split('/')[-1]
        basename = '.'.join(filename.split('.')[:-1])
        print(f'\n ** {filename}')

        with open(f_vra, 'rb') as file_from:
            vra = np.load(file_from, allow_pickle=True)
            # bbx visual relationship array (numpy array 'vra') consisting of:
            #  - off-diagonal elements (i,j) representing relations of every pair of bbxes in image 
            #    as: [dcpt, cpix, bro_1_2, rpos_1_2])
            #  - diagonal elements (i,i) with metadata about the reference bbx itself:
            #    [uniq_label, (x_ctp, y_ctp), oloc, rsa, off]
            
        print('Detected objects:')
        for idx in range(len(vra)):
            print('   - {:<17}'.format(vra[idx][idx][0]), end=(' ',' ',' ','\n')[idx%4])
        
        print("\n")
        
        try:
            with open(f_json, "r") as infile:
                data = json.load(infile)               # dict
                height = int(data['size']['height'])
                width = int(data['size']['width'])
                bbx_loc, _ = json_parse(data, key_stem='bbx', find='first', key_suffix=('_basic','_added'))
                infile.close()
        except (IOError) as ioe:
            print(' ==> json file cannot be opened or loaded properly. Abort.')
            raise ioe
            
        vrd = get_descriptoids(basename,vra,size_th,base_th,bro_th,img_width=width,img_height=height,img_bbx_loc=bbx_loc)

        img_vrd_json_file = img_dir + '/' + basename + ser_suffix + '_vrd.json'
        vrd = str(vrd).replace("\'", '"')
        json_str = json.dumps(vrd, indent=4)

        with open(img_vrd_json_file, "w") as write_file:
            json.dump(vrd, write_file, indent=4)
        

    # %%
    vra

    # %%
