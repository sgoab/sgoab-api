""" 
============================
Licensing terms and copyright
This code is protected by the terms and conditions of the GNU_GPL-v3 copyleft license.

Copyright (C) 2021 Cedric Bhihe

This code is free software and was developed by its author while at the Barcelona Supercomputing Center (https://www.bsc.es). You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>, or consult the License.md file in this repo.

To contact the author of the code, use cedric.bhihe@gmail.com.

============================
"""

import builtins
import numpy as np


# Global Labels from used by the object-detection
LABEL_MAP = ["BG", "crucifixion", "angel", "person", "crown of thorns", "horse", "dragon", "bird", "dog", "boat", "cat", "book",
             "sheep", "shepherd", "elephant", "zebra", "crown", "tiara", "camauro", "zucchetto", "mitre", "saturno", "skull",
             "orange", "apple", "banana", "nude", "monk", "lance", "key of heaven", "banner", "chalice", "palm", "sword", "rooster",
             "knight", "scroll", "lily", "horn", "prayer", "tree", "arrow", "crozier", "deer", "devil", "dove", "eagle", "hands",
             "head", "lion", "serpent", "stole", "trumpet", "judith", "halo", "helmet", "shield", "jug", "holy shroud", "god the father",
             "swan", "butterfly", "bear", "centaur", "pegasus", "donkey", "mouse", "monkey", "cow", "unicorn"]
label_map = LABEL_MAP.copy()  # rename for debugging


def bbx_vis_rel(width: int,
                height: int,
                img_dict: dict,
                vra: np.ndarray
                ) -> dict:
    '''
    bbx_vis_rel():
    Computes caption seeds and compositional data.

    Input:
    @param      width: image width in pixels
    @type       int

    @param      height: image height in pixels
    @type       int

    @param      img_dict: processed image metadata and computed metrics
    @type:      json formatted dict

    @param      vra: visual relationship array
    @type       np.ndarray

    Output:
    @param      vrd: visual relationships dictionary
    @type       dict formated as json
    '''

    import math as m
    import numpy as np
    from typing import Union, Any, List, Optional, Tuple, cast

    # ===============================
    # Define or load hyperparameters
    # ===============================
    # <<< Hyperparameter  # applies to linear dimensions (e.g. baseline, height) of bbxes
    base_th = 0.08
    # <<< Hyperparameter  # applies to surface areas (e.g. 'rsa') of bbxes
    size_th = 0.10
    bro_th = 40       # <<< Hyperparameter [0,100]
    # applies to 'bro_1_2' parameter values (relative overlap of bbx_1 and bbx_2 wrt to bbx_1)
    rear_parameter = -0.03
    # Parameter useful in characterizing attitude of mountworthy_animals capable of rearing
    # + (horse, donkey, zebra, elephant, pegasus, dragon...). Modifies 'off' based criterion


    # local environment
    CLASS_PPA_PATH = '/home/smendoza/local/sgoab/sgoab-api/src/vis_rel'

    """
    # production environment
    CLASS_PPA_PATH = '/super/vis_rel'
    """
    infile = '{}/class.ppa'.format(CLASS_PPA_PATH)

    # 'ppa': pair-wise proportions array (np.ndarray)
    # - non-diagonal elts (i,j): i-th label object size proportions wrt to j-th label object
    # - diagonal elts (i,i): Tuple[class' label,optional main topic tag]

    # ===============================
    # Sanity checks
    # ===============================
    try:
        assert('label_map' in globals())
    except AssertionError as ae:
        print(f'Error: variable \'label_map\' must be defined globally. Interrupt.')
        raise ae

    with open(infile, 'rb') as file_from:
        # hyperparametric array
        ppa = np.load(file_from, allow_pickle=True)
        class_lst = [ppa[idx][idx][0]
                     for idx in range(len(ppa))]  # class labels

    """ 
    @sergiom commented: object-detection label_map is a list and may not have the same size
    try:
        assert(list(label_map.keys()) == class_lst)
    except AssertionError as ae:
        print(f'Discrepancy between global class list and hyperparametric settings. Interrupt.')
        raise ae
    """
    #  ===============================
    # Local methods
    #  ===============================

    # Function to infer advanced visual relationsship from overlapping bbxes and detected object locations

    def get_annot_txt(width: int,
                      height: int,
                      bbx_loc: list,
                      vra: 'np.ndarray',
                      size_th: float,
                      base_th: float,
                      bro_th: float
                      ) -> dict:
        '''
        get_annot_txt():
        Computes:
              (i)  annotation text wherever physical overlap occurs between any 2 bbxes,
              (ii) single bbx' object's attributes.
        Processing is based on bbx' locations, as well as on 'vra' and 'ppa' arrays.

        Input:
          - 1st arg. (int) image width in pixels
          - 2nd arg. (int) image height in pixels
          - 3rd arg. (list of tuples) bbx_loc = (label, label_id, xmin, ymin, xmax, ymax)
          - 4th arg. (np.ndarray) 'vra', visual relationship array
                              - on diagonal:  (uniq_label,(x_cpt,y_cpt),oloc,rsa,off)
                              - off diagonal: (dcpt,cpix,bro_1_2,rpos_1_2)
          - 5th arg. (float) size_th, size criterion threshold
          - 6th arg. (float) base_th, baseline criterion threshold
          - 7th arg. (float) bro_th, bbx relative overlap threshold

        Output:
            Annotations for pairwise visual relationships and for single or isolated objects attributes

          # 1 object (dict): 'annot_txt' with structure as follow
          # {'uniq_label_1_ns'+'++'+uniq_label_2_ns':("","",...);'uniq_label_1_ns'+'++attr':("","",...); ...}
          # where 'uniq_label_1_ns' identifies object 1  w/ unique identifier and no space in string.

        '''

        rel_sizes = np.array(ppa)
        annot_txt = dict()
        soa_1_key = soa_2_key = None

        # Build array of tuples (uniq_label, (x_cpt, y_cpt), oloc, rsa, off)
        img_objs_lst = [tuple(vra[idx, idx]) for idx in range(len(vra))]
        # list of unique labels made from space-separated
        img_objs_labels = [x[0] for x in img_objs_lst]
        # + words with '_#' at the end

        if len(img_objs_labels) == 1:
            # soa: "single object attribute", textual annot for sole object
            annot_soa_1 = str()
            # detected object's uniq_label, w/ spaces
            label_1 = img_objs_labels[0]
            # detected object's uniq_label, no space
            label_1_ns = '_'.join(label_1.split(sep=' '))
            # element of class list, w/ spaces
            ppa_label_1 = ' '.join(label_1.split(sep='_')[:-1])

            if (ppa_label_1 in flying_birds and vra[0, 0][4] <= 0):
                # Unique object has horizontal orientation
                annot_soa_1 += label_1_ns + " is_flying\n"
            elif ppa_label_1 in winged_things - flying_birds:
                # not included in previous conditional clause because representations mostly show those
                # +  objects ground-bound either crawling, lying or standing w/ or w/o spread wings
                pass
            else:
                pass

            # Build single_object_attribute (soa) label as key for lone bbx attribute(s) in textual annotations
            if annot_soa_1:
                soa_key_1 = label_1_ns + '++' + 'attr'
                if soa_1_key in annot_txt.keys():
                    annot_txt[soa_1_key] += list(annot_soa_1.split('\n')[:-1])
                else:
                    annot_txt[soa_1_key] = list(annot_soa_1.split('\n')[:-1])

                annot_txt[soa_1_key] = list(
                    set(annot_txt[soa_1_key]))      # suppress dupes

        elif len(img_objs_labels) >= 2:
            # Intermediate variables for 'size_th' calculation
            img_rsa_list = [vra[idx, idx][3]
                            for idx in range(len(img_objs_labels))]
            img_rsa_list.sort()
            img_rsa_mid_idx = len(img_rsa_list)//2
            img_median_rsa = (
                img_rsa_list[img_rsa_mid_idx]+img_rsa_list[~img_rsa_mid_idx])/2
            len_img_max_rsa = len(str(max(img_rsa_list)))

            # Test for at least 2 bbxes touching or overlapping
            list_bro_1_2 = [(vra[idx_1, idx_2][1], vra[idx_1, idx_2][2])
                            for idx_1 in range(len(img_objs_labels))
                            for idx_2 in range(idx_1+1, len(img_objs_labels))]

            # if any(y > 0 or x == 0 for x, y in list_bro_1_2):
            for idx_1 in range(len(img_objs_labels)):
                # detected object's uniq_label, w/ spaces
                label_1 = img_objs_labels[idx_1]
                # detected object's uniq_label, no space
                label_1_ns = '_'.join(label_1.split(sep=' '))
                # detected object's class label (non-unique)
                ppa_label_1 = '_'.join(label_1.split(sep='_')[:-1])
                # caution: list class labels 'class_lst', strings w/ spaces
                # variable 'class_lst' is global
                ppa_idx_1 = class_lst.index(ppa_label_1)
                label_1_rsa = vra[idx_1, idx_1][3]
                baseline_1 = bbx_loc[idx_1][5]
                annot_soa_1 = str()

                idx_lst = list(range(0, idx_1))
                idx_lst.extend(list(range(idx_1+1, len(img_objs_labels))))
                obj_overlaps = [img_objs_labels[idx_2]
                                for idx_2 in idx_lst if vra[idx_1, idx_2][1] <= 0]

                if (ppa_label_1 in flying_birds and not obj_overlaps):
                    if vra[idx_1, idx_1][4] <= -0.08:
                        # Identify unique objects w/ class labels in 'flying_birds', w/ no overlap
                        annot_soa_1 += label_1_ns + " is_flying\n"
                    else:
                        if ppa_label_1 not in {"swan"}:
                            annot_soa_1 += label_1_ns + " stands\n"
                        else:
                            annot_soa_1 += label_1_ns + " floats/stands\n"

                    # "single object attribute" (soa) label as key
                    soa_1_key = label_1_ns + '++' + 'attr'

                    if soa_1_key in annot_txt:
                        annot_txt[soa_1_key] += list(
                            annot_soa_1.split('\n')[:-1])
                    else:
                        annot_txt[soa_1_key] = list(
                            annot_soa_1.split('\n')[:-1])

                    annot_txt[soa_1_key] = list(set(annot_txt[soa_1_key]))
                    annot_soa_1 = str()

                for idx_2 in range(idx_1+1, len(img_objs_labels)):
                    # Index range ensures idx_1 != idx_2
                    # 'uniq_label'
                    label_2 = img_objs_labels[idx_2]
                    # detected object's uniq_label, no space
                    label_2_ns = '_'.join(label_2.split(sep=' '))
                    # class label (non-unique)
                    ppa_label_2 = '_'.join(label_2.split(sep='_')[:-1])
                    # caution: list class labels 'class_lst' made of strings with spaces
                    # use global variable 'class_lst'
                    ppa_idx_2 = class_lst.index(ppa_label_2)
                    label_2_rsa = vra[idx_2, idx_2][3]
                    baseline_2 = bbx_loc[idx_2][5]
                    annot_soa_2 = str()

                    # Compute size criterion relative to avg_reduced_rsa of 2 objects being compared
                    # + in order to free size_criterion from painting size effect.
                    # Calculate avg_reduced_size as arithmetic mean to relax 'size_th' threshold contraint
                    # + or as geometric means to enforce it more strictly. If perspective relies on linear
                    # + proportionality, pick arithmetic mean.

                    # multiplier to go from rsa(bbx_2) to rsa(bbx_1)
                    prop_obj_1_2 = ppa[ppa_idx_1][ppa_idx_2]
                    # multiplier to go from rsa(bbx_1) to rsa(bbx_2)
                    prop_obj_2_1 = ppa[ppa_idx_2][ppa_idx_1]

                    if prop_obj_2_1 > prop_obj_1_2:
                        avg_reduced_rsa = (
                            label_1_rsa + prop_obj_1_2 * label_2_rsa)/2
                        size_criterion = round(
                            (label_1_rsa - prop_obj_1_2 * label_2_rsa)/avg_reduced_rsa, 2)
                    elif prop_obj_2_1 < prop_obj_1_2:
                        avg_reduced_rsa = (
                            label_2_rsa + prop_obj_2_1 * label_1_rsa)/2
                        size_criterion = round(
                            (label_2_rsa - prop_obj_2_1 * label_1_rsa)/avg_reduced_rsa, 2)
                    else:
                        # ppa_label_1 == ppa_label_2:
                        prop_obj_1_2 = 1
                        avg_reduced_rsa = (
                            label_1_rsa + prop_obj_1_2 * label_2_rsa)/2
                        size_criterion = round(
                            (label_1_rsa - prop_obj_1_2 * label_2_rsa)/avg_reduced_rsa, 2)

                    # Tweak naive size threshold, 'size_th', so instead of being constant it is bigger
                    # + for smaller avg_reduced_size values and smaller for larger size values; reflects
                    # + added complexity of painting exactly proportioned objects when those are small.
                    size_th -= (m.tan((avg_reduced_rsa - img_median_rsa)
                                * m.pi/10**len_img_max_rsa) / 2)**3

                    if (ppa_label_1 == 'crucifixion' or ppa_label_2 == 'crucifixion'):
                        base_criterion = None
                        # If either ppa_label_1 or _2 is of type "crucifixion",
                        # do not compare bbx' baselines, but rather bbx' RSA values
                    else:
                        base_criterion = round(
                            (baseline_1 - baseline_2) / height, 2)

                    # 'rpos_1_2' (relative position of 1 wrt 2) in 'vra'
                    rpos_1_2 = vra[idx_1, idx_2][3]
                    rpos_2_1 = vra[idx_2, idx_1][3]
                    # 'bro_1_2' (bbxes relative overlap) relative to 'label_1'
                    bro_1_2 = vra[idx_1, idx_2][2]
                    bro_2_1 = vra[idx_2, idx_1][2]
                    # 'cpix' (distance between 2 bbxes' closest pixels)'
                    cpix = vra[idx_1, idx_2][1]

                    xmin_1, ymin_1, xmax_1, ymax_1 = bbx_loc[idx_1][2:]

                    xmin_2, ymin_2, xmax_2, ymax_2 = bbx_loc[idx_2][2:]

                    annot_pvr = str()                # initialize "pairwise visual relationship" variable

                    # Bbxes must either overlap or touch and objects must be either proportioned according
                    #    to ppa or both correspond to person labels to be recognized as possibly valid cases
                    #    of  overlapping bbxes.
                    if ((bro_1_2 > 0. or cpix == 0)):
                        # and (abs(size_criterion) <= 6*size_th or (ppa_label_1 == ppa_label_2 == "person"))):

                        # ####################
                        # PAIRWISE RULES
                        # ####################

                        if (ppa_label_1 in person_like_instances and
                                ppa_label_2 in person_like_instances):
                            # Detected objects are both 'person' class instances

                            if vra[idx_2, idx_2][4] >= 0.1:
                                # Object_2 has vertical orientation
                                annot_soa_2 += label_2_ns + " stands\n"
                            elif vra[idx_2, idx_2][4] <= -0.1:
                                # Object_2 has horizontal orientation
                                annot_soa_2 += label_2_ns + " lies/reclines\n"
                            else:
                                # Object_2 has indeterminate orientation
                                annot_soa_2 += label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            if vra[idx_1, idx_1][4] >= 0.1:
                                # Object_1 has vertical orientation
                                annot_soa_1 += label_1_ns + " stands\n"
                            elif vra[idx_1, idx_1][4] <= -0.1:
                                # Object_1 has horizontal orientation
                                annot_soa_1 += label_1_ns + " lies/reclines\n"
                            else:
                                # Object_1 has indeterminate orientation
                                annot_soa_1 += label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            if abs(base_criterion) > base_th:
                                # The two objects DO NOT share similar baselines

                                if (abs(size_criterion) > size_th and
                                    (abs(ymax_2-ymin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height > 2 * base_th or
                                     abs(xmax_2-xmin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height > 2 * base_th or
                                     abs(xmax_2-xmin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height > 2 * base_th or
                                     abs(ymax_2-ymin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height > 2 * base_th)):
                                    # Bbxes have arbitrary form factors, different RSA and baseline values.
                                    # Their size difference CANNOT be explained by perspective.

                                    # In all likelihood, they belong to different planes of representation,
                                    # each to its own depth-plane in the analyzed image.

                                    # An exception is treated below if the smaller person-like instances largely
                                    # overlaps with the larger person-like instance and situated at
                                    # its mid-heigth or higher, identify as possible Madonna, i.e. Infant Christ held
                                    # by Virgin Mary on her lap.

                                    if (bro_1_2 >= 80 and label_1_rsa < 0.5 * label_2_rsa):
                                        if (ppa_label_1 in person_like_instances - {'devil', 'pope', 'knight', 'judith'}):
                                            # INFANT/CHILD/BABY BEFORE BIGGER PERSON
                                            # Object_2 is bigger than object_1
                                            annot_soa_1 += label_1_ns + " is_child/is_infant/is_dwarf/is_partial\n"

                                            if (9*ymin_2 + ymax_2)/10 <= vra[idx_1, idx_1][1][1] <= (ymax_2 + ymin_2)/2:
                                                # INFANT/CHILD ON BIGGER PERSON's LAP
                                                # pairwise visual relationship (pvr)
                                                annot_pvr += label_2_ns + " holds " + label_1_ns + "\n"
                                            else:
                                                annot_pvr += label_2_ns + " is_next_to " + label_1_ns + "\n"
                                        else:
                                            annot_soa_1 += label_1_ns + " is partial\n"
                                    elif (bro_2_1 >= 80 and label_2_rsa < 0.5 * label_1_rsa):
                                        if (ppa_label_2 in person_like_instances - {'devil', 'pope', 'knight', 'judith'}):
                                            # INFANT/CHILD/BABY BEFORE BIGGER PERSON
                                            # Object_1 is bigger than object_2
                                            annot_soa_2 += label_2_ns + " is_child/is_infant/is_dwarf/is_partial\n"

                                            if (9*ymin_1 + ymax_1)/10 <= vra[idx_2, idx_2][1][1] <= (ymax_1 + ymin_1)/2:
                                                # INFANT/CHILD ON BIGGER PERSON's LAP
                                                annot_pvr += label_1_ns + " holds " + label_2_ns + "\n"
                                            else:
                                                annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                                        else:
                                            annot_soa_2 += label_2_ns + " is partial\n"
                                    else:
                                        pass
                                elif (abs(size_criterion) > size_th and
                                      (abs(ymax_2-ymin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                       abs(xmax_2-xmin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                       abs(xmax_2-xmin_2-ymax_1+ymin_1-abs(baseline_1-baseline_2))/height <= 2*base_th or
                                       abs(ymax_2-ymin_2-xmax_1+xmin_1-abs(baseline_1-baseline_2))/height <= 2*base_th)):
                                    # Bbxes have arbitrary form factors, different RSAs and baselines.
                                    # Their size difference CAN be explained by perspective.

                                    if (label_1_rsa < label_2_rsa
                                            and round(baseline_2/10, 0) >= round(baseline_1/10, 0)):
                                        # Object_2 is bigger than object_1 and its baseline is
                                        # BELOW that of object_1
                                        annot_pvr += label_2_ns + " is_before " + label_1_ns + "\n"
                                    elif (label_2_rsa < label_1_rsa
                                          and round(baseline_1/10, 0) >= round(baseline_2/10, 0)):
                                        # Object_1 is bigger than object_2 and its baseline is
                                        # BELOW that of object_2
                                        annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                                    else:
                                        annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                                else:
                                    # Condition      "abs(size_criterion) < size_th"
                                    # ####################
                                    # Bbxes have arbitrary form factors, commensurate RSAs and distinct baselines,
                                    # They overlap but nothing else can be said about them.
                                    # pass

                                    if round(baseline_2/10, 0) >= round(baseline_1/10, 0):
                                        # Object_2's baseline is BELOW that of object_1
                                        annot_pvr += label_2_ns + " is_before " + label_1_ns + "\n"
                                    elif round(baseline_1/10, 0) >= round(baseline_2/10, 0):
                                        # Object_1's baseline is BELOW that of object_2
                                        annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                                    else:
                                        annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"
                            else:
                                # abs(base_criterion) <= base_th:
                                # The two person-instance objects share a similar baseline

                                if (round(baseline_1/10, 0) > round(baseline_2/10, 0)):
                                    annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"
                                elif (round(baseline_1/10, 0) < round(baseline_2/10, 0)):
                                    annot_pvr += label_1_ns + " is_behind " + label_2_ns + "\n"
                                else:
                                    annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"

                                if abs(size_criterion) > size_th:
                                    # One object's RSA is significantly smaller than the other,
                                    # yet the 2 objects share a baseline.

                                    diff_max_dim = abs(
                                        max(xmax_1-xmin_1, ymax_1-ymin_1) - max(xmax_2-xmin_2, ymax_2-ymin_2))
                                    avg_max_dim = (
                                        max(xmax_1-xmin_1, ymax_1-ymin_1) + max(xmax_2-xmin_2, ymax_2-ymin_2))/2

                                    if diff_max_dim/avg_max_dim > 20*size_th:
                                        # test whether relative difference between max dimensions of the 2 bbxes veryfies size criterion
                                        if label_1_rsa < label_2_rsa:
                                            if ppa_label_1 in person_like_instances - {'devil', 'pope', 'knight', 'judith'}:
                                                annot_soa_1 += label_1_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                            else:
                                                annot_soa_1 += label_1_ns + " is_partial\n"
                                        elif label_2_rsa < label_1_rsa:
                                            if ppa_label_2 in person_like_instances - {'devil', 'pope', 'knight', 'judith'}:
                                                annot_soa_2 += label_2_ns + " is_child/is_infant/is_dwarf/is_partial\n"
                                            else:
                                                annot_soa_2 += label_2_ns + " is_partial\n"
                                        else:
                                            pass
                                else:
                                    # The 2 objects have similar RSAs
                                    pass

                            if annot_soa_1:
                                soa_1_key = label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances and
                             ppa_label_2 in {'book', 'lily', 'palm'} and
                             bro_2_1 >= bro_th/2) or
                            (ppa_label_2 in person_like_instances and
                             ppa_label_1 in {'book', 'lily', 'palm'} and
                             bro_1_2 >= bro_th/2)):
                            # relaxed overlap criterion

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_1, PH_idx_1][4] > 0.1):
                                # vertical orientation of 'person'
                                # no condition is forced on orientation of PH_ppa_label_2 instance
                                # + because books may have horizontal orientation, while palm and lilies generally do not
                                annot_soa_1 += PH_label_1_ns + " stands\n"

                                if vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                    # 'book'|'lily'|'palm' are vertically positionned in upper
                                    # half region of 'person'
                                    annot_pvr += PH_label_1_ns + " is_with/holds " + PH_label_2_ns + "\n"
                                elif vra[PH_idx_2, PH_idx_2][1][1] >= (4*PH_ymax_1 + PH_ymin_1)/5:
                                    # 'book'|'lily'|'palm' is vertically positionned in
                                    # at feet of 'person'
                                    annot_pvr += PH_label_2_ns + " is_at_feet_of " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            elif (vra[PH_idx_1, PH_idx_1][4] < -0.1):
                                # horizontal orientation of 'person'; other object can be
                                # + centered at any height of 'person'
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                                if (PH_ppa_label_2 in {'lily', 'palm'} and vra[PH_idx_2, PH_idx_2][4] <= -0.1):
                                    # 'lily' or 'palm' is laid along 'person'
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"

                                    if vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4:
                                        annot_pvr += PH_label_2_ns + " lies_left_of " + PH_label_1_ns + "\n"

                                    elif vra[PH_idx_2, PH_idx_2][1][0] >= (3*PH_xmax_1 + PH_xmin_1)/4:
                                        annot_pvr += PH_label_2_ns + " lies_right_of " + PH_label_1_ns + "\n"
                                    else:
                                        # locator "is_next_to" used below is a copout; needs refining
                                        annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"

                                elif (PH_ppa_label_2 in {'lily', 'palm'} and vra[PH_idx_2, PH_idx_2][4] > -0.1):
                                    # object_2 is either 'book' or is 'lily' or 'palm'
                                    # with non-horizontal orientation
                                    if (vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3 * PH_xmin_1)/4):
                                        annot_pvr += PH_label_2_ns + " is_at_left_of " + PH_label_1_ns + "\n"
                                    elif (vra[PH_idx_2, PH_idx_2][1][0] >= (3 * PH_xmax_1 + PH_xmin_1)/4):
                                        annot_pvr += PH_label_2_ns + " is_at_right_of " + PH_label_1_ns + "\n"
                                    else:
                                        annot_pvr += PH_label_2_ns + " is_at_mid-section_of " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            else:
                                # vra[idx_1,idx_1][4] >= -0.1 and vra[idx_1,idx_1][4] <= 0.1:
                                # 'person' sits/bows/bends/kneels/crouches/squats
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                                if vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                    annot_pvr += PH_label_1_ns + " is_with/holds " + PH_label_2_ns + "\n"
                                else:
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 == 'crucifixion' and
                             ppa_label_2 in {'crown of thorns', 'halo', 'crown'} and
                             bro_2_1 >= bro_th) or
                            (ppa_label_2 == 'crucifixion' and
                             ppa_label_1 in {'crown of thorns', 'halo', 'crown'} and
                             bro_1_2 >= bro_th)):

                            # CRUCIFIXION + CROWN_OF_THORNS | HALO + bro_th >> JESUS_CHRIST ON CROSS|GOOD_THIEF ON CROSS|SAINT ON CROSS

                            if ppa_label_1 == 'crucifixion':
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_2, PH_idx_2][1][1] >= PH_ymin_1 and
                                vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4 and
                                vra[PH_idx_2, PH_idx_2][1][0] >= (2*PH_xmin_1 + PH_xmax_1)/3 and
                                    vra[PH_idx_2, PH_idx_2][1][0] <= (2*PH_xmax_1 + PH_xmin_1)/3):
                                # test bbx_2's center point's y coordinate
                                # 'person' object is vertically oriented and 'halo' or 'crown_of_thorns'
                                # object is centered in upper 4th section of 'person'
                                annot_soa_1 += PH_label_1_ns + \
                                    " ((person)) is_vertical\n"
                                annot_pvr += PH_label_1_ns + \
                                    " ((person)) is_on ((cross))\n"
                                if PH_ppa_label_2 in {'crown of thorns', 'crown'}:
                                    annot_pvr += PH_label_1_ns + \
                                        " ((person)) wears " + \
                                        PH_label_2_ns + "\n"
                                else:
                                    annot_pvr += PH_label_1_ns + \
                                        " ((person)) is_with " + \
                                        PH_label_2_ns + "\n"

                                # Deductive process for demonstration purposes only
                                # Task belongs to KG, not to vis-rel.
                                if PH_ppa_label_2 in {'crown of thorns', 'crown'}:
                                    annot_soa_1 += PH_label_1_ns + \
                                        " ((person)) is ((jesus_christ))\n"
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " ((person)) is ((saint))/((martyr))/((good_thief)\n"
                                else:
                                    # add any other attributes apart from 'halo', 'crown', crown of thorns'
                                    pass
                            elif (((vra[PH_idx_2, PH_idx_2][1][0] >= PH_xmin_1
                                    and vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4)
                                  or (vra[PH_idx_2, PH_idx_2][1][0] >= (xmin_1 + 3*PH_xmax_1)/4
                                      and vra[PH_idx_2, PH_idx_2][1][0] <= PH_xmax_1))
                                  and vra[PH_idx_2, PH_idx_2][1][1] >= (2*PH_ymin_1 + PH_ymax_1)/3
                                  and vra[PH_idx_2, PH_idx_2][1][1] <= (2*PH_ymax_1 + PH_ymin_1)/3):

                                annot_soa_1 += PH_label_1_ns + \
                                    " ((person)) is_horizontal/is_inclined\n"
                                annot_soa_1 += PH_label_1_ns + \
                                    " ((person)) is_on ((cross))\n"
                                if PH_ppa_label_2 in {'crown of thorns', 'crown'}:
                                    annot_pvr += PH_label_1_ns + \
                                        " ((person)) wears " + \
                                        PH_label_2_ns + "\n"
                                else:
                                    annot_pvr += PH_label_1_ns + \
                                        " ((person)) is_with " + \
                                        PH_label_2_ns + "\n"

                                if PH_ppa_label_2 in {'crown of thorns', 'crown'}:
                                    annot_soa_1 += PH_label_1_ns + \
                                        " ((person)) is ((jesus_christ))\n"
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " ((person)) is ((saint))/((martyr))/((good_thief)\n"
                                else:
                                    # add any other attributes apart from 'halo' or 'crown of thorns'
                                    pass
                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if (((ppa_label_1 == 'crucifixion' and ppa_label_2 in person_like_instances)
                             or (ppa_label_2 == 'crucifixion' and ppa_label_1 in person_like_instances))
                                and (bro_2_1 > bro_th / 3 or bro_1_2 > bro_th / 3)):
                            #  relaxed overlap threshold condition

                            # CRUCIFIXION + PERSON INSTANCE >> SITUATION UNDER / NEXT_TO CROSS

                            if ppa_label_1 == 'crucifixion':
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            annot_soa_1 += PH_label_1_ns + \
                                ' ((person)) is_on ((cross))\n'

                            if (vra[PH_idx_2, PH_idx_2][4] >= 0.1):
                                annot_soa_2 += PH_label_2_ns + " stands\n"
                            elif (vra[PH_idx_2, PH_idx_2][4] <= -0.1):
                                annot_soa_2 += PH_label_2 + " lies/reclines\n"
                            else:
                                annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            if (vra[PH_idx_1, PH_idx_1][4] >= 0.1):
                                # 'crucifixion((person))' object is vertically oriented
                                annot_soa_1 += PH_label_1_ns + \
                                    " ((person))/((cross)) is_vertical\n"

                                if (vra[PH_idx_2, PH_idx_2][4] >= 0.1):
                                    diff_height = PH_ymax_1 - PH_ymin_1 - PH_ymax_2 + PH_ymin_2
                                    avg_height = (
                                        PH_ymax_1 - PH_ymin_1 + PH_ymax_2 - PH_ymin_2) / 2
                                elif (vra[PH_idx_2, PH_idx_2][4] <= -0.1):
                                    diff_height = PH_ymax_1 - PH_ymin_1 - PH_xmax_2 + PH_xmin_2
                                    avg_height = (
                                        PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2) / 2
                                else:
                                    # 'person' like object_2 either sit/crouches/... or
                                    # is 'child/infant/dwarf'  ==> case not treated
                                    # pass
                                    diff_height = PH_ymax_1 - PH_ymin_1 - \
                                        (PH_xmax_2 - PH_xmin_2 +
                                         PH_ymax_2 - PH_ymin_2)/2
                                    avg_height = (
                                        PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2) / 2

                                if (PH_rpos_2_1 in {"SS"}):
                                    if (abs(diff_height)/avg_height < 0.45):
                                        annot_pvr += PH_label_2_ns + " is_beneath " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height > 0):
                                        annot_pvr += PH_label_2_ns + " is_behind " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height <= 0):
                                        annot_pvr += PH_label_2_ns + " is_before " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    else:
                                        pass

                                if (PH_rpos_2_1 in {"SW"}):
                                    if (abs(diff_height)/avg_height < 0.45):
                                        annot_pvr += PH_label_2_ns + " is_beneath " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_left_of " + \
                                            PH_label_1 + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height > 0):
                                        annot_pvr += PH_label_2_ns + " is_behind " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += label_2 + " is_left_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height <= 0):
                                        annot_pvr += PH_label_2_ns + " is_before " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_left_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    else:
                                        pass

                                if (PH_rpos_2_1 in {"SE"}):
                                    if (abs(diff_height)/avg_height < 0.45):
                                        annot_pvr += PH_label_2_ns + " is_beneath " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height > 0):
                                        annot_pvr += PH_label_2_ns + " is_behind " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height <= 0):
                                        annot_pvr += PH_label_2_ns + " is_before " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    else:
                                        pass

                                if (PH_rpos_2_1 in {"EE"}):
                                    if (abs(diff_height)/avg_height < 0.45):
                                        annot_pvr += PH_label_2_ns + " is_next_to " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height > 0):
                                        annot_pvr += PH_label_2_ns + " is_behind " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height <= 0):
                                        annot_pvr += PH_label_2_ns + " is_before " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_right_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    else:
                                        pass

                                if (PH_rpos_2_1 in {"WW"}):
                                    if (abs(diff_height)/avg_height < 0.45):
                                        annot_pvr += PH_label_2_ns + " is_next_to " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_left_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height > 0):
                                        annot_pvr += PH_label_2_ns + " is_behind " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_left_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    elif (diff_height <= 0):
                                        annot_pvr += PH_label_2_ns + " is_before " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                        annot_pvr += PH_label_2_ns + " is_left_of " + \
                                            PH_label_1_ns + \
                                            " ((person))/((cross))\n"
                                    else:
                                        pass
                            elif (vra[PH_idx_1, PH_idx_1][4] <= -0.1):
                                # object 'crucifixion' is horizontal
                                annot_soa_1 += PH_label_1_ns + \
                                    " ((person))/((cross)) is_horizontal\n"

                                if (vra[PH_idx_2, PH_idx_2][4] >= 0.1):
                                    diff_height = PH_xmax_1 - PH_xmin_1 - PH_ymax_2 + PH_ymin_2
                                    avg_height = (
                                        PH_xmax_1 - PH_xmin_1 + PH_ymax_2 - PH_ymin_2) / 2
                                elif (vra[PH_idx_2, PH_idx_2][4] <= -0.1):
                                    diff_height = PH_xmax_1 - PH_xmin_1 - PH_xmax_2 + PH_xmin_2
                                    avg_height = (
                                        PH_xmax_1 - PH_xmin_1 + PH_xmax_2 - PH_xmin_2) / 2
                                else:
                                    # 'person' like object_2 either sit/crouches/... or
                                    # is 'child/infant/dwarf'  ==> case not treated
                                    pass

                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_near " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                else:
                                    pass

                                if (PH_rpos_2_1 in {"SS"}):
                                    annot_pvr += PH_label_2_ns + " is_beneath " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"SW"}):
                                    annot_pvr += PH_label_2_ns + " is_below " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"SE"}):
                                    annot_pvr += PH_label_2_ns + " is_below " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"WW"}):
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"EE"}):
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"NW"}):
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"NE"}):
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                else:
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                            else:
                                # orientation of object 'crucifixion' cannot be determined
                                if (vra[PH_idx_2, PH_idx_2][4] >= 0.1):
                                    diff_height = max(
                                        PH_ymax_1 - PH_ymin_1 - PH_ymax_2 + PH_ymin_2, PH_xmax_1 - PH_xmin_1 - PH_ymax_2 + PH_ymin_2)
                                    avg_height = max(
                                        PH_ymax_1 - PH_ymin_1 + PH_ymax_2 - PH_ymin_2, PH_xmax_1 - PH_xmin_1 + PH_ymax_2 - PH_ymin_2) / 2
                                elif (vra[PH_idx_2, PH_idx_2][4] <= -0.1):
                                    diff_height = max(
                                        PH_ymax_1 - PH_ymin_1 - PH_xmax_2 + PH_xmin_2, PH_xmax_1 - PH_xmin_1 - PH_xmax_2 + PH_xmin_2)
                                    avg_height = max(
                                        PH_ymax_1 - PH_ymin_1 + PH_xmax_2 - PH_xmin_2, PH_xmax_1 - PH_xmin_1 + PH_xmax_2 - PH_xmin_2) / 2
                                else:
                                    # 'person' like object_2 either sit/crouches/... or
                                    # is 'child/infant/dwarf'  ==> case not treated
                                    pass

                                if (abs(diff_height)/avg_height < 0.45):
                                    annot_pvr += PH_label_2_ns + " is_near " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (diff_height > 0):
                                    annot_pvr += PH_label_2_ns + " is_behind " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (diff_height <= 0):
                                    annot_pvr += PH_label_2_ns + " is_before " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                else:
                                    pass

                                if (PH_rpos_2_1 in {"SS"}):
                                    annot_pvr += PH_label_2_ns + " is_beneath " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"SW"}):
                                    annot_pvr += PH_label_2_ns + " is_below " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"SE"}):
                                    annot_pvr += PH_label_2_ns + " is_below " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"WW"}):
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"EE"}):
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"NW"}):
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_left_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                elif (PH_rpos_2_1 in {"NE"}):
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                    annot_pvr += PH_label_2_ns + " is_right_of " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"
                                else:
                                    annot_pvr += PH_label_2_ns + " is_above " + \
                                        PH_label_1_ns + \
                                        " ((person))/((cross))\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        # if ((ppa_label_1 in {'crucifixion','angel'}
                        if ((ppa_label_1 in {'crucifixion', 'angel'}
                             and ppa_label_2 in {'bird', 'dove'}
                             and rpos_2_1 in {'NN', 'ǸW', 'NE'})
                            # or (ppa_label_2 in {'crucifixion','angel'}
                            or (ppa_label_2 in {'crucifixion', 'angel'}
                                and ppa_label_1 in {'bird', 'dove'}
                                and rpos_1_2 in {'NN', 'ǸW', 'NE'})):

                            # CRUCIFIXION + BIRD + rpos  >> JESUS_CHRIST ON CROSS

                            # if ppa_label_1 in {'crucifixion','person'}:
                            if ppa_label_1 in {'crucifixion', 'angel'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if PH_ppa_label_1 == 'crucifixion':
                                annot_soa_1 += PH_label_1_ns + \
                                    ' ((person)) is ((jesus_christ))\n'

                            if PH_ppa_label_2 == 'dove':
                                annot_soa_2 += PH_label_2_ns + \
                                    ' is ((holy_spirit))\n'

                            annot_pvr += PH_label_2_ns + ' hovers_over ' + PH_label_1_ns + '\n'

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in {'person', 'prayer', 'knight'}
                             and ppa_label_2 in {'halo', 'helmet', 'crown of thorns'}
                             and bro_2_1 >= bro_th)
                            or (ppa_label_2 in {'person', 'prayer', 'knight'}
                                and ppa_label_1 in {'halo', 'helmet', 'crown of thorns'}
                                and bro_1_2 >= bro_th)):

                            # PERSON | PRAYER + CROWN_OF_THORNS | HALO | HELMET + bro_th >> JESUS_CHRIST|SAINT/MARTYR|MAN_AT_ARMS

                            if ppa_label_1 in {'person', 'prayer', 'knight'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_1, PH_idx_1][4] > 0.1
                                and vra[PH_idx_2, PH_idx_2][1][1] >= PH_ymin_1
                                    and (vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 4*PH_ymin_1)/5)):
                                # 'person' object is vertically oriented and other
                                # object is centered in upper 5th of 'person|prayer' bbx
                                annot_soa_1 += PH_label_1_ns + " stands\n"

                                if PH_ppa_label_2 == 'crown of thorns':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 == 'helmet':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'knight':
                                        annot_soa_1 += PH_label_1_ns + \
                                            " is ((soldier))/((man_at_arms))\n"
                                        # can be otherwise in case of allegorical representations
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                else:
                                    pass

                            elif (vra[PH_idx_1, PH_idx_1][4] < -0.1
                                  and ((vra[PH_idx_2, PH_idx_2][1][0] >= PH_xmin_1
                                       and vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4)
                                       or (vra[PH_idx_2, PH_idx_2][1][0] >= (PH_xmin_1 + 3*PH_xmax_1)/4
                                           and vra[PH_idx_2, PH_idx_2][1][0] <= PH_xmax_1))):
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                                if PH_ppa_label_2 == 'crown of thorns':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 == 'helmet':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'knight':
                                        annot_soa_1 += PH_label_1_ns + \
                                            " is ((soldier))/((man_at_arms))\n"
                                        # can be otherwise in case of allegorical representations
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                else:
                                    pass

                            elif (vra[PH_idx_1, PH_idx_1][4] >= -0.1 and vra[PH_idx_1, PH_idx_1][4] <= 0.1
                                  and ((vra[PH_idx_2, PH_idx_2][1][1] >= PH_ymin_1
                                        and vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4)
                                       or (vra[PH_idx_2, PH_idx_2][1][0] >= PH_xmin_1
                                           and vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4)
                                       or (vra[PH_idx_2, PH_idx_2][1][0] >= (PH_xmin_1 + 3*PH_xmax_1)/4
                                           and vra[PH_idx_2, PH_idx_2][1][0] <= PH_xmax_1))):
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                                if PH_ppa_label_2 == 'crown of thorns':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((jesus_christ))\n"
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 == 'helmet':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if ppa_label_1 != 'knight':
                                        annot_soa_1 += PH_label_1_ns + \
                                            " is ((soldier))/((man_at_arms))\n"
                                        # can be otherwise in case of allegorical representations
                                elif PH_ppa_label_2 == 'halo':
                                    annot_soa_1 += PH_label_1_ns + \
                                        " is ((saint))/((martyr))\n"
                                    annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"
                                else:
                                    pass
                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances
                             and ppa_label_2 in {'sword', 'lance', 'shield'}
                             and bro_2_1 >= 4.)
                            or (ppa_label_2 in person_like_instances
                                and ppa_label_1 in {'sword', 'lance', 'shield'}
                                and bro_1_2 >= 4.)):
                            # relax bbx relative overlap criterion (bro_th -> bro_th/2 -> 4.%)
                            # PERSON + SHIELD | LANCE | SWORD + size_th + bro_th >> MAN_AT_ARMS/SOLDIER_WITH_{...}

                            if ppa_label_1 in person_like_instances:
                                # {'person','prayer','knight','monk','nude','devil',...}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if 0.1 < vra[PH_idx_1, PH_idx_1][4]:
                                # vertical orientation of 'person_like_instances'
                                # other object can be centered at any height of 'person_like_instances'
                                annot_soa_1 += PH_label_1_ns + " stands\n"

                                if 0.1 < vra[PH_idx_2, PH_idx_2][4]:
                                    # 'sword' or 'lance' is vertical, shield is also vertical but seen sideways
                                    # with bbx centered on mid-section of 'person_like_instances'
                                    annot_soa_2 += PH_label_2_ns + " is_vertical\n"

                                    if (PH_bro_2_1 >= bro_th and
                                            (PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2, PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4):
                                        annot_pvr += PH_label_1_ns + " carries " + PH_label_2_ns + "\n"

                                elif -0.1 < vra[PH_idx_2, PH_idx_2][4] <= 0.1:
                                    # 'sword' or 'lance' is oblique, i.e. not pointing up or down
                                    # and 'shield' is either vertical or seen sideways and at a slant.
                                    if PH_ppa_label_2 in {'sword', 'lance', }:
                                        annot_soa_2 += PH_label_2_ns + " is_oblique\n"

                                    if vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + PH_ymin_1)/2:
                                        # 'sword', 'shield' or 'lance' is oblique and
                                        # above mid-height of 'person_like_instances'
                                        if PH_ppa_label_2 in {'sword', 'lance', }:
                                            annot_pvr += PH_label_1_ns + " wields/points " + PH_label_2_ns + "\n"
                                        elif PH_ppa_label_2 in {'shield', }:
                                            if PH_bro_2_1 >= bro_th:
                                                annot_pvr += PH_label_1_ns + " holds " + PH_label_2_ns + '\n'
                                            else:
                                                annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                        else:
                                            pass

                                elif vra[PH_idx_2, PH_idx_2][4] <= -0.1:
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"

                                    if abs(PH_baseline_2 - PH_baseline_1)/height <= base_th:
                                        annot_pvr += PH_label_2_ns + " is_at_feet_of " + PH_label_1_ns + "\n"

                                    if (PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2, PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4:
                                        if PH_ppa_label_2 in {'sword', 'lance', }:
                                            annot_pvr += PH_label_1_ns + " wields/points " + PH_label_2_ns + "\n"
                                        else:
                                            annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"

                                else:
                                    pass

                                if vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4:
                                    # if 'sword' or 'lance' or 'shield' bbx' vertical center situated above
                                    # upper quarter section of 'person_like_instances'
                                    annot_pvr += PH_label_1_ns + " holds_high " + PH_label_2_ns + "\n"

                            elif (vra[PH_idx_1, PH_idx_1][4] <= -0.1):
                                # horizontal orientation of 'person_like_instances'
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                                if -0.1 < vra[PH_idx_2, PH_idx_2][4] <= 0.1:
                                    # 'sword' or 'lance' is laid at angle wrt 'person_like_instances'
                                    if PH_ppa_label_2 in {'sword', 'lance', }:
                                        annot_soa_2 += PH_label_2_ns + " is_oblique\n"

                                elif 0.1 < vra[PH_idx_2, PH_idx_2][4]:
                                    # 'shield','sword' or 'lance' sticks vertically, i.e. pointing up or down
                                    annot_soa_2 += PH_label_2_ns + " is_vertical\n"

                                else:
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"

                            elif (-0.1 < vra[PH_idx_1, PH_idx_1][4] <= 0.1):
                                if PH_ppa_label_1 in person_like_instances:
                                    annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                    if PH_bro_2_1 >= bro_th:
                                        annot_pvr += PH_label_1_ns + " is_with " + PH_label_2_ns + "\n"

                                if -0.1 < vra[PH_idx_2, PH_idx_2][4] <= 0.1:
                                    if PH_ppa_label_2 in {'sword', 'lance', }:
                                        annot_soa_2 += PH_label_2_ns + " is_oblique\n"

                                elif 0.1 < vra[PH_idx_2, PH_idx_2][4]:
                                    annot_soa_2 += PH_label_2_ns + " is_vertical\n"

                                else:
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                            else:
                                pass

                            if (PH_ppa_label_1 in person_like_instances - {'knight', 'angel', 'monk'} and
                                    vra[PH_idx_2, PH_idx_2][1][1] < (3*PH_ymax_1 + PH_ymin_1)/4):
                                annot_soa_1 += PH_label_1_ns + \
                                    " is ((man_at_arms))/((soldier))\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in {'person', 'nude', 'prayer', 'pope', 'knight'}
                             and (ppa_label_2 in {'crown', 'camauro', 'saturno', 'zuccheto', 'mitre', 'tiara'})
                             and bro_2_1 >= bro_th/3)
                            or (ppa_label_2 in {'person', 'nude', 'prayer', 'pope', 'knight'}
                                and (ppa_label_1 in {'crown', 'camauro', 'saturno', 'zuccheto', 'mitre', 'tiara'})
                                and bro_1_2 >= bro_th/3)):   # relax rsa-based criterion
                            # and (abs(size_criterion) <= 3*size_th)
                            # PERSON + CROWN|CAMAURO|SATURNO|ZUCCHETO + size_th + bro_th >> KING/QUEEN|POPE|HIGH_CLERIC)

                            if ppa_label_1 in {'person', 'nude', 'prayer', 'pope', 'knight'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_1, PH_idx_1][4] > 0.1
                                    and vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4):
                                # 'person' object is vertically oriented and 'camauro'
                                # object is centered in upper 4th section of 'person'
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                if PH_ppa_label_2 == 'crown':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))
                                elif PH_ppa_label_2 in {'saturno', 'zuccheto'} and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((high_cleric))
                                elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((bishop))/((abbot))/((cardinal))
                                elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))/((high_cleric))
                                else:
                                    pass
                            elif (-0.1 <= vra[PH_idx_1, PH_idx_1][4] <= 0.1
                                  and vra[PH_idx_2, PH_idx_2][1][1] <= (PH_ymax_1 + 3*PH_ymin_1)/4):
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                if PH_ppa_label_2 == 'crown' and PH_ppa_label_1 != 'knight':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                    # is ((king))/((queen))/((saint_mary))
                                elif PH_ppa_label_2 == 'crown' and PH_ppa_label_1 == 'knight':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                    # is ((king))
                                elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))
                                elif PH_ppa_label_2 in {'saturno', 'zuccheto'} and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((high_cleric))
                                elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((bishop))/((abbot))/((cardinal))
                                elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))/((high_cleric))
                                else:
                                    pass

                            elif (vra[PH_idx_1, PH_idx_1][4] <= -0.1
                                  and ((vra[PH_idx_2, PH_idx_2][1][0] >= PH_xmin_1
                                        and vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmax_1 + 3*PH_xmin_1)/4)
                                       or (vra[PH_idx_2, PH_idx_2][1][0] >= (PH_xmin_1 + 3*PH_xmax_1)/4
                                           and vra[PH_idx_2, PH_idx_2][1][0] <= PH_xmax_1))):
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                                annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                if PH_ppa_label_2 == 'crown':
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 == 'camauro' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))
                                elif PH_ppa_label_2 in {'saturno', 'zuccheto'} and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((high_cleric))
                                elif PH_ppa_label_2 == 'mitre' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((bishop))/((abbot))/((cardinal))
                                elif PH_ppa_label_2 == 'tiara' and PH_ppa_label_1 != 'pope':
                                    annot_pvr += PH_label_1_ns + " is_coiffed_with " + PH_label_2_ns + "\n"
                                    # is ((pope))/((high_cleric))
                                else:
                                    pass

                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in {"person", 'prayer', 'pope'} and ppa_label_2 == "stole")
                                or (ppa_label_2 in {"person", 'prayer', 'pope'} and ppa_label_1 == "stole")):
                            # and abs(size_criterion) <= size_th):

                            # PERSON + STOLE + size_th >> POPE/CLERIC

                            if ppa_label_1 in {'person', 'prayer', 'pope'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_rpos_1_2 = rpos_1_2
                                PH_rpos_2_1 = rpos_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_rpos_1_2 = rpos_2_1
                                PH_rpos_2_1 = rpos_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if vra[PH_idx_1, PH_idx_1][4] >= 0.1:
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                            elif -0.1 < vra[PH_idx_1, PH_idx_1][4] < 0.1:
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                            if PH_bro_2_1 >= bro_th:
                                if (vra[PH_idx_1, PH_idx_1][4] >= -0.1
                                    and vra[PH_idx_2, PH_idx_2][4] > -0.1
                                    and ((PH_ymax_1 + 3*PH_ymin_1)/4 <= vra[PH_idx_2, PH_idx_2][1][1] <= (3*PH_ymax_1 + PH_ymin_1)/4
                                         or
                                         (PH_xmax_1 + 3*PH_xmin_1)/4 <= vra[PH_idx_2, PH_idx_2][1][0] <= (3*PH_xmax_1 + PH_xmin_1)/4)):
                                    # 'person' either stands or crouches and 'stole' is centered in middle half section of 'person'
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'pope':
                                        annot_soa_1 += PH_label_1_ns + \
                                            " is ((pope))/((cleric))\n"
                                elif (vra[PH_idx_1, PH_idx_1][4] <= -0.1
                                      and vra[PH_idx_2, PH_idx_2][4] <= -0.1
                                      and (PH_xmax_1 + 3*PH_xmin_1)/4 <= vra[PH_idx_2, PH_idx_2][1][0] <= (PH_xmin_1 + 3*PH_xmax_1)/4):
                                    annot_pvr += PH_label_1_ns + " wears " + PH_label_2_ns + "\n"

                                    if PH_ppa_label_1 != 'pope':
                                        annot_soa_1 += PH_label_1_ns + \
                                            " is ((pope))/((cleric))\n"
                                else:
                                    pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in {"person", 'prayer', 'pope'} and ppa_label_2 == "crozier")
                                or (ppa_label_2 in {"person", 'prayer', 'pope'} and ppa_label_1 == "crozier")):
                            # and abs(size_criterion) <= size_th):
                            # PERSON + CROZIER + size_th >> POPE/BISHOP/APOSTLE/HIGH_CHURCH_PRELATE/HIGH_CLERIC

                            if (ppa_label_1 in {"person", 'prayer', 'pope'} and bro_2_1 >= bro_th):
                                if (vra[idx_1, idx_1][4] >= 0.1 and vra[idx_2, idx_2][4] >= 0.1):
                                    # 'person' object and 'crozier' objects are vertically oriented
                                    annot_soa_1 += label_1_ns + " stands\n"
                                    annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                    if ppa_label_1 != 'pope':
                                        annot_soa_1 += label_1_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                elif (vra[idx_1, idx_1][4] <= -0.1 and vra[idx_2, idx_2][4] <= -0.1):
                                    # 'person' object and 'crozier' object are horizontally oriented
                                    annot_soa_1 += label_1_ns + " lies/reclines\n"
                                    annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                    if ppa_label_1 != 'pope':
                                        annot_soa_1 += label_1_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                elif (-0.1 < vra[idx_1, idx_1][4] < 0.1):
                                    annot_soa_1 += label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                    annot_pvr += label_1_ns + " with " + label_2_ns + "\n"
                                    if ppa_label_1 != 'pope':
                                        annot_soa_1 += label_1_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                else:
                                    pass

                            if (ppa_label_2 in {"person", 'prayer', 'pope'} and bro_1_2 >= bro_th):
                                if (vra[idx_2, idx_2][4] >= 0.1 and vra[idx_1, idx_1][4] >= 0.1):
                                    # 'person' object and 'crozier' objects are vertically oriented
                                    annot_soa_2 += label_2_ns + " stands\n"
                                    annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                    if ppa_label_2 != 'pope':
                                        annot_soa_2 += label_2_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                elif (vra[idx_2, idx_2][4] <= -0.1 and vra[idx_1, idx_1][4] <= -0.1):
                                    # 'person' object and 'crozier' object are horizontally oriented
                                    annot_soa_2 += label_2_ns + " lies/reclines\n"
                                    annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                    if ppa_label_2 != 'pope':
                                        annot_soa_2 += label_2_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                elif (-0.1 < vra[idx_2, idx_2][4] < 0.1):
                                    annot_soa_2 += label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                    annot_pvr += label_2_ns + " with " + label_1_ns + "\n"
                                    if ppa_label_2 != 'pope':
                                        annot_soa_2 += label_2_ns + \
                                            " is ((pope))/((high_cleric))/((bishop))/((apostle))\n"
                                else:
                                    pass

                            if annot_soa_1:
                                soa_1_key = label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = label_2_ns + '++' + 'attr'

                        if (ppa_label_1 in quadrupeds and ppa_label_2 in quadrupeds):
                            if (abs(baseline_2 - baseline_1)/height <= base_th):
                                annot_pvr += label_1_ns + " is_next_to " + label_2_ns + "\n"

                                if ppa_label_1 == ppa_label_2:
                                    if label_1_rsa >= 2 * label_2_rsa:
                                        annot_soa_2 += label_2_ns + " is " + \
                                            animal_youngs[ppa_label_2] + "\n"
                                    elif label_2_rsa >= 2 * label_1_rsa:
                                        annot_soa_1 += label_1_ns + " is " + \
                                            animal_youngs[ppa_label_1] + "\n"

                            else:
                                if baseline_2 > baseline_1:
                                    annot_pvr += label_1_ns + " is_behind " + label_2_ns + "\n"
                                else:
                                    # equivalent to baseline_2 < baseline_1:
                                    annot_pvr += label_1_ns + " is_before " + label_2_ns + "\n"

                            if (vra[idx_1, idx_1][4] >= 0.1 and ppa_label_1 in {'bear'}):
                                annot_soa_1 += label_1_ns + " stands_upright\n"

                            if ppa_label_1 in {'cow', 'sheep'}:
                                if vra[idx_1, idx_1][4] < -0.2:
                                    annot_soa_1 += label_1_ns + " lies\n"
                                elif -0.2 <= vra[idx_1, idx_1][4] < -0.01:
                                    annot_soa_1 += label_1_ns + " stands/pastures\n"
                                elif -0.01 <= vra[idx_1, idx_1][4] < 0.1:
                                    annot_soa_1 += label_1_ns + " is_partial\n"
                                else:
                                    # annot_soa_1 += label_1_ns + " is_vertical\n"
                                    pass
                            if (rear_parameter <= vra[idx_1, idx_1][4] < 0.05 and
                                    ppa_label_1 in mountworthy_animals - {'bear', 'cow', 'sheep', 'lion'}):
                                if ppa_label_2 not in {'cow', 'sheep', 'horse', 'donkey', 'cat', 'mouse'}:
                                    annot_soa_1 += label_1_ns + " rears\n"
                                elif (ppa_label_2 in {'horse'} and
                                      rear_parameter <= vra[idx_2, idx_2][4] < 0.05):
                                    annot_soa_1 += label_1_ns + " rears\n"
                                else:
                                    annot_soa_1 += label_1_ns + " stands/pastures\n"

                            if (vra[idx_2, idx_2][4] >= 0.1 and ppa_label_2 in {'bear'}):
                                annot_soa_2 += label_2_ns + " stands_upright\n"

                            if ppa_label_2 in {'cow', 'sheep'}:
                                if vra[idx_2, idx_2][4] < -0.2:
                                    annot_soa_2 += label_2_ns + " lies\n"
                                elif -0.2 <= vra[idx_2, idx_2][4] < -0.01:
                                    annot_soa_2 = label_2_ns + " stands/pastures\n"
                                elif -0.01 <= vra[idx_2, idx_2][4] < 0.1:
                                    annot_soa_2 = label_2_ns + " is_partial\n"
                                else:
                                    pass

                            if (rear_parameter <= vra[idx_2, idx_2][4] < 0.05 and
                                    ppa_label_2 in mountworthy_animals - {'bear', 'cow', 'sheep', 'lion'}):
                                if ppa_label_1 not in {'cow', 'sheep', 'horse', 'donkey', 'cat', 'mouse'}:
                                    annot_soa_2 += label_2_ns + " rears\n"
                                elif (ppa_label_1 in {'horse'} and
                                      rear_parameter <= vra[idx_1, idx_1][4] < 0.05):
                                    annot_soa_2 += label_2_ns + " rears\n"
                                else:
                                    annot_soa_2 += label_2_ns + " stands/pastures\n"

                            if annot_soa_1:
                                soa_1_key = label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances and ppa_label_2 in {'boat', })
                            or
                                (ppa_label_2 in person_like_instances and ppa_label_1 in {'boat', })):

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_1, PH_idx_1][4] >= 0.1):
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                            elif (-0.1 <= vra[PH_idx_1, PH_idx_1][4] < 0.1):
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                            if vra[PH_idx_1, PH_idx_2][3] in {'NN', 'NE', 'NW'}:
                                # test on 'PH_rpos_1_2'
                                annot_pvr += PH_label_1_ns + " rides/is_on " + PH_label_2_ns + "\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances and ppa_label_2 in mountworthy_animals)
                            or
                                (ppa_label_2 in person_like_instances and ppa_label_1 in mountworthy_animals)):
                            # and abs(size_criterion) <= 3 * size_th):   # relax size_criterion
                            # PERSON + HORSE + base_th criterion >> MOUNTED_PERSON/HORSE_RIDER

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_prop_obj_2_1 = prop_obj_2_1
                                PH_prop_obj_1_2 = prop_obj_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_prop_obj_2_1 = prop_obj_1_2
                                PH_prop_obj_1_2 = prop_obj_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            # prop_obj_2_1  # how many of 1 fit in 2
                            # prop_obj_1_2  # how many of 2 fit in 1

                            if (vra[PH_idx_1, PH_idx_1][4] >= 0.1):
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                            elif (-0.1 <= vra[PH_idx_1, PH_idx_1][4] < 0.1):
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"

                            if ((PH_bro_2_1 >= bro_th/2 or PH_bro_1_2 >= bro_th/2)
                                and abs(PH_baseline_2 - PH_baseline_1)/height > base_th
                                    and vra[PH_idx_1, PH_idx_1][4] >= -0.1):

                                if vra[PH_idx_1, PH_idx_2][3] == 'NN':
                                    # test on 'PH_rpos_1_2'
                                    annot_pvr += PH_label_1_ns + " rides/is_on " + PH_label_2_ns + "\n"
                                elif (vra[PH_idx_1, PH_idx_2][3] in {'SE', 'SW', 'SS'}
                                      and PH_ppa_label_2 in {'pegasus'}):
                                    annot_pvr += PH_label_2_ns + " hovers_over " + PH_label_1_ns + "\n"
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                                else:
                                    if abs(size_criterion) <= size_th:
                                        annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                    elif ((size_criterion < 0 and prop_obj_2_1 > prop_obj_1_2) or
                                          (size_criterion > 0 and prop_obj_2_1 < prop_obj_1_2)):
                                        # object bbx_2 is bigger than expected wrt object bbx_1, or
                                        # object bbx_1 is smaller than expected wrt object bbx_2
                                        if PH_label_1 == label_1:
                                            annot_pvr += PH_label_1_ns + " is_behind " + PH_label_2_ns + "\n"
                                        else:
                                            annot_pvr += PH_label_2_ns + " is_behind " + PH_label_1_ns + "\n"
                                    elif ((size_criterion < 0 and prop_obj_2_1 < prop_obj_1_2) or
                                          (size_criterion > 0 and prop_obj_2_1 > prop_obj_1_2)):
                                        # object bbx_1 is bigger than expected wrt object bbx_1, or
                                        # object bbx_2 is smaller than expected wrt object bbx_2
                                        if PH_label_1 == label_1:
                                            annot_pvr += PH_label_1_ns + " is_before " + PH_label_2_ns + "\n"
                                        else:
                                            annot_pvr += PH_label_2_ns + " is_before " + PH_label_1_ns + "\n"
                                    else:
                                        pass
                            elif (abs(PH_baseline_2 - PH_baseline_1)/height <= base_th and
                                  vra[PH_idx_1, PH_idx_1][4] >= -0.1):

                                if PH_baseline_1 < PH_baseline_2:
                                    annot_pvr += PH_label_1_ns + " is_behind " + PH_label_2_ns + "\n"
                                elif PH_baseline_1 > PH_baseline_2:
                                    annot_pvr += PH_label_1_ns + " is_before " + PH_label_2_ns + "\n"
                                else:
                                    annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                            elif (abs(PH_baseline_2 - PH_baseline_1)/height <= base_th
                                  and vra[PH_idx_1, PH_idx_2][3] in {'SE', 'SW', 'SS'}
                                  and vra[PH_idx_1, PH_idx_1][4] <= 0.1):
                                annot_pvr += PH_label_1_ns + " is_at_feet_of " + PH_label_2_ns + "\n"
                            else:
                                # refine conditional block with new clauses here
                                pass

                            if (vra[PH_idx_2, PH_idx_2][4] >= 0.1 and PH_label_2 in {'bear'}):
                                annot_soa_2 += PH_label_2_ns + " stands_upright\n"

                            if (vra[PH_idx_2, PH_idx_2][4] <= 0.1 and PH_label_2 in {'cow', 'sheep'}):
                                annot_soa_2 += PH_label_2_ns + " stands/pastures\n"

                            if (rear_parameter <= vra[PH_idx_2, PH_idx_2][4] < 0.05
                                    and PH_label_2 in mountworthy_animals - {'bear', 'cow', 'sheep', 'lion'}):
                                annot_soa_2 += PH_label_2_ns + " rears\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances and ppa_label_2 == 'angel')
                                or (ppa_label_2 in person_like_instances and ppa_label_1 == 'angel')):
                            # and abs(size_criterion) <= 4 * size_th):   # relax size_criterion
                            # PERSON + ANGEL + size_th >> DIVINE_INTERVENTION

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if vra[PH_idx_1, PH_idx_1][4] >= 0.1:
                                # test on orientation of labeled object PH_idx_1
                                annot_soa_1 += PH_label_1_ns + " stands\n"
                            elif vra[PH_idx_1, PH_idx_1][4] <= -0.1:
                                # test on orientation of labeled object PH_idx_1
                                annot_soa_1 += PH_label_1_ns + " lies/reclines/is_horizontal\n"
                            else:
                                # test on orientation of labeled object PH_idx_1
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            if vra[PH_idx_2, PH_idx_2][4] >= 0.1:
                                # test on orientation of labeled object PH_idx_2
                                annot_soa_2 += PH_label_2_ns + " stands\n"
                                if PH_baseline_2 <= (PH_ymin_1 + PH_ymax_1)/2:
                                    annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                                elif abs(base_criterion) <= base_th:
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            elif vra[PH_idx_2, PH_idx_2][4] <= -0.1:
                                # test on orientation of labeled object PH_idx_2
                                if abs(base_criterion) <= base_th:
                                    annot_soa_2 += PH_label_2_ns + " lies/reclines\n"
                                    annot_pvr += PH_label_2_ns + " is_next_to " + PH_label_1_ns + "\n"
                                elif PH_baseline_2 <= (PH_ymin_1 + PH_ymax_1)/2:
                                    annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                                    annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            else:
                                # test on orientation of labeled object PH_idx_2
                                annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            if vra[PH_idx_2, PH_idx_1][3] == 'WW':
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + "\n"
                            elif vra[PH_idx_2, PH_idx_1][3] == 'EE':
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + "\n"
                            elif vra[PH_idx_2, PH_idx_1][3] in {'NN', 'NW', 'NE'}:
                                annot_pvr += PH_label_2_ns + " is_above " + PH_label_1_ns + "\n"
                            elif vra[PH_idx_2, PH_idx_1][3] in {'SS', 'SW', 'SE'}:
                                annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + "\n"
                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ({ppa_label_1, ppa_label_2} == {"lily", "angel"}):
                            # and abs(size_criterion) <= 4 * size_th):   # relax size_criterion
                            # LILY + ANGEL + size_th >> DIVINE_INTERVENTION

                            if ppa_label_1 == 'angel':
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if vra[PH_idx_1, PH_idx_1][4] >= 0.1:
                                annot_soa_1 += PH_label_1_ns + " stands/is_vertical\n"
                            elif vra[PH_idx_1, PH_idx_1][4] <= -0.1:
                                annot_soa_1 += PH_label_1_ns + " lies/reclines/is_horizontal\n"
                            else:
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                            annot_soa_1 += PH_label_1_ns + \
                                " is ((saint_gabriel))\n"

                            if vra[PH_idx_2, PH_idx_2][4] >= 0.1:
                                annot_soa_2 += PH_label_2_ns + " is_vertical\n"
                            elif vra[PH_idx_2, PH_idx_2][4] <= -0.1:
                                annot_soa_2 += PH_label_2_ns + " is_horizontal\n"
                            else:
                                annot_soa_2 += PH_label_2_ns + " is_at_a_slant/is_oblique\n"

                            if vra[PH_idx_2, PH_idx_1][3] == 'WW':
                                annot_pvr += PH_label_2_ns + " is_left_of " + PH_label_1_ns + "\n"
                            elif vra[PH_idx_2, PH_idx_1][3] == 'EE':
                                annot_pvr += PH_label_2_ns + " is_right_of " + PH_label_1_ns + "\n"
                            elif (vra[PH_idx_2, PH_idx_1][3] in {'NN', 'NW', 'NE'} and
                                  PH_baseline_2 <= (5*PH_ymin_1+PH_ymax_1)/6):
                                annot_pvr += PH_label_2_ns + " is_atop " + PH_label_1_ns + "\n"
                            elif vra[PH_idx_2, PH_idx_1][3] in {'SS', 'SW', 'SE'}:
                                annot_pvr += PH_label_2_ns + " is_beneath " + PH_label_1_ns + "\n"
                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in {'banana', 'apple', 'orange', 'serpent'} and ppa_label_2 == 'tree')
                                or (ppa_label_2 in {'banana', 'apple', 'orange', 'serpent'} and ppa_label_1 == 'tree')):
                            # and abs(size_criterion) <= 4 * size_th):   # relax size_criterion
                            # PERSON + ANGEL + size_th >> DIVINE_INTERVENTION

                            if ppa_label_1 in {'banana', 'apple', 'orange', 'serpent'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if PH_baseline_1 <= vra[PH_idx_2, PH_idx_2][1][1]:
                                # if baseline of object labeled PH_idx_1 ('banana','apple','orange', 'serpent')
                                # + is above center point of tree
                                annot_pvr += PH_label_1_ns + " hangs_from " + PH_label_2_ns + "\n"
                            else:
                                # indeterminate case, e.g. if canopy is very high above ground or
                                # + equivalently if trunk is disproportionately long wrt canopy height
                                pass

                        if ((ppa_label_1 in person_like_instances
                             and ppa_label_2 in {'table', 'seat', 'bedding', 'rock'})
                            or (ppa_label_2 in person_like_instances
                                and ppa_label_1 in {'table', 'seat', 'bedding', 'rock'})):

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (abs(base_criterion) <= 1.2*base_th and 0 <= vra[PH_idx_1, PH_idx_1][4] <= 0.1):
                                # if baseline of two bbxes verify (relaxed) criterion, and human-like
                                # + figure's bbx' proportions are consistent with potentially seated position
                                annot_soa_1 += PH_label_1_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"

                                if PH_ppa_label_2 == 'table':
                                    annot_pvr += PH_label_1_ns + " sits_at " + PH_label_2_ns + "\n"
                                elif PH_ppa_label_2 in {'seat', 'bedding', 'rock'}:
                                    annot_pvr += PH_label_1_ns + " sits_on " + PH_label_2_ns + "\n"
                                else:
                                    # indeterminate case
                                    pass
                            elif (PH_ppa_label_2 != 'seat'
                                  and abs(base_criterion) > 1.2 * base_th
                                  and vra[PH_idx_1, PH_idx_1][4] <= -0.1
                                  and vra[PH_idx_1, PH_idx_2][3] == 'NN'):
                                annot_soa_1 += PH_label_1_ns + " lies/reclines\n"
                                annot_pvr += PH_label_1_ns + " lies_on " + PH_label_2_ns + "\n"
                            else:
                                # indeterminate case
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in small_objects
                             and ppa_label_2 in {'table', 'seat', 'bedding', 'rock', 'shield'})
                            or (ppa_label_2 in small_objects
                                and ppa_label_1 in {'table', 'seat', 'bedding', 'rock', 'shield'})):

                            if ppa_label_1 in small_objects:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (vra[PH_idx_1, PH_idx_2][3] == 'NN'
                                and PH_baseline_1 >= PH_baseline_2
                                    and PH_bro_1_2 >= min(1.7 * bro_th, 70)):  # ALTERNATIVE: use absolute threshold value
                                # and vra[PH_idx_1,PH_idx_1][3] <= vra[PH_idx_2,PH_idx_2][3] * 0.4
                                annot_pvr += PH_label_1_ns + " rests_on/lays_on " + PH_label_2_ns + "\n"

                        if ((ppa_label_1 in person_like_instances
                             and ppa_label_2 in {'banana', 'apple', 'orange'})
                            or (ppa_label_2 in person_like_instances
                                and ppa_label_1 in {'banana', 'apple', 'orange'})):

                            if ppa_label_1 in person_like_instances:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            PH_ppa_label_1 = '_'.join(
                                PH_label_1.split(sep='_')[:-1])
                            PH_ppa_idx_1 = class_lst.index(PH_ppa_label_1)
                            PH_ppa_label_2 = '_'.join(
                                PH_label_2.split(sep='_')[:-1])
                            PH_ppa_idx_2 = class_lst.index(PH_ppa_label_2)
                            prop_obj_2_1 = ppa[PH_ppa_idx_2][PH_ppa_idx_1]

                            if (PH_ymin_1 < vra[PH_idx_2, PH_idx_2][1][1] <= (3*PH_ymin_1 + PH_ymax_1)/4
                                and vra[PH_idx_1, PH_idx_1][4] >= 0
                                and PH_bro_2_1 >= min(2 * bro_th, 0.85)
                                    and abs(vra[PH_idx_2, PH_idx_2][3] - prop_obj_2_1 * vra[PH_idx_1, PH_idx_1][3]) <= size_criterion):
                                # y coordinate of {fruit} is within upper quarter of human-like figure's
                                # + bbx' height, and
                                # human-like figure is either sitting/crouching/... or standing, and
                                # overlap surf-area of the two bbxes is large relative to the surf-area
                                # + of {fruit}'s bbx, and
                                # rsa of bbxes verify size criterion

                                # Predicate's ambiguity due to inability to detect face or head of
                                # + human-like figure.
                                annot_pvr += PH_label_1_ns + " eats/holds " + PH_label_2_ns + "\n"

                        if (ppa_label_1 in flying_birds or ppa_label_2 in flying_birds):

                            if ppa_label_1 in flying_birds:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_prop_obj_1_2 = prop_obj_1_2
                                PH_prop_obj_2_1 = prop_obj_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_prop_obj_1_2 = prop_obj_2_1
                                PH_prop_obj_2_1 = prop_obj_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if (PH_ppa_label_2 not in person_like_instances.union({'hand'})
                                    and vra[PH_idx_1, PH_idx_1][4] <= -0.1):
                                annot_soa_1 += PH_label_1_ns + " is_flying\n"

                            if PH_ppa_label_2 in person_like_instances:
                                if vra[PH_idx_2, PH_idx_2][4] >= 0.1:
                                    annot_soa_2 += PH_label_2_ns + " stands\n"
                                elif -0.1 <= vra[PH_idx_2, PH_idx_2][4] < 0.1:
                                    annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                                else:
                                    annot_soa_2 += PH_label_2_ns + " lies/reclines\n"

                                if vra[PH_idx_1, PH_idx_1][4] <= -0.05:
                                    annot_soa_1 += PH_label_1_ns + " has_deployed_wings\n"
                                else:
                                    annot_soa_1 += PH_label_1_ns + " stands\n"

                                if (vra[PH_idx_2, PH_idx_2][4] >= -0.1 and
                                    PH_ymin_2 < PH_baseline_1 <= (4*PH_ymin_2 + PH_ymax_2)/5 and
                                        vra[PH_idx_1, PH_idx_1][4] > -0.1):
                                    annot_pvr += PH_label_1_ns + \
                                        " is_on_shoulder_of/is_at_shoulder_level_of " + PH_label_2_ns + "\n"
                                elif (vra[PH_idx_2, PH_idx_2][4] < -0.1 and
                                      PH_ymin_2 < PH_baseline_1 <= (4*PH_ymin_2 + PH_ymax_2)/5):
                                    annot_pvr += PH_label_1_ns + " is_on_top_of " + PH_label_2_ns + "\n"
                                elif (vra[PH_idx_2, PH_idx_2][4] < -0.1 and
                                      (4*PH_xmin_2 + PH_xmax_2)/5 <= vra[PH_idx_1, PH_idx_1][1][0] <= (PH_xmin_2 + 4*PH_xmax_2)/5 and
                                      (4*PH_ymin_2 + PH_ymax_2)/5 <= vra[PH_idx_1, PH_idx_1][1][1] <= (PH_ymin_2 + 4*PH_ymax_2)/5 and
                                        PH_bro_1_2 >= bro_th/2):
                                    # relaxed overlap criterion relative to bird
                                    annot_pvr += PH_label_1_ns + " is_next_to " + PH_label_2_ns + "\n"
                                    annot_pvr += PH_label_1_ns + " is_at_mid-section_of " + PH_label_2_ns + "\n"
                                elif (vra[PH_idx_2, PH_idx_2][4] >= -0.05 and
                                      (PH_ymin_2 + 4*PH_ymax_2)/5 < PH_baseline_1 <= 1.1*PH_ymax_2):
                                    annot_pvr += PH_label_1_ns + " is_at_feet_of " + PH_label_2_ns + "\n"
                                else:
                                    pass
                            elif (PH_ppa_label_2 in {'hand'} and
                                  vra[PH_idx_1, PH_idx_2][3] in {'NN', 'NE', 'NW'}):
                                annot_pvr += PH_label_1_ns + " sits_on " + PH_label_2_ns + "\n"

                                if vra[PH_idx_1, PH_idx_1][4] <= 0:
                                    annot_soa_1 += PH_label_1_ns + " has_deployed_wings\n"
                                else:
                                    annot_soa_1 += PH_label_1_ns + " has_folded_wings\n"
                            else:
                                pass

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        if ((ppa_label_1 in person_like_instances and ppa_label_2 in {'hand'})
                            or
                                (ppa_label_2 in person_like_instances and ppa_label_1 in {'hand'})):

                            if ppa_label_1 in {'hand'}:
                                PH_label_1 = label_1
                                PH_label_2 = label_2
                                PH_label_1_ns = label_1_ns
                                PH_label_2_ns = label_2_ns
                                PH_ppa_label_1 = ppa_label_1
                                PH_ppa_label_2 = ppa_label_2
                                PH_idx_1 = idx_1
                                PH_idx_2 = idx_2
                                PH_ppa_idx_1 = ppa_idx_1
                                PH_ppa_idx_2 = ppa_idx_2
                                PH_baseline_1 = baseline_1
                                PH_baseline_2 = baseline_2
                                PH_bro_1_2 = bro_1_2
                                PH_bro_2_1 = bro_2_1
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_1, ymin_1, xmax_1, ymax_1
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_2, ymin_2, xmax_2, ymax_2
                            else:
                                PH_label_1 = label_2
                                PH_label_2 = label_1
                                PH_label_1_ns = label_2_ns
                                PH_label_2_ns = label_1_ns
                                PH_ppa_label_1 = ppa_label_2
                                PH_ppa_label_2 = ppa_label_1
                                PH_idx_1 = idx_2
                                PH_idx_2 = idx_1
                                PH_ppa_idx_1 = ppa_idx_2
                                PH_ppa_idx_2 = ppa_idx_1
                                PH_baseline_1 = baseline_2
                                PH_baseline_2 = baseline_1
                                PH_bro_1_2 = bro_2_1
                                PH_bro_2_1 = bro_1_2
                                PH_xmin_1, PH_ymin_1, PH_xmax_1, PH_ymax_1 = xmin_2, ymin_2, xmax_2, ymax_2
                                PH_xmin_2, PH_ymin_2, PH_xmax_2, PH_ymax_2 = xmin_1, ymin_1, xmax_1, ymax_1

                            if vra[PH_idx_2, PH_idx_2][4] >= 0.1:
                                annot_soa_2 += PH_label_2_ns + " stands\n"

                                if (size_criterion <= 1.5 * size_th and PH_bro_1_2 >= 90):
                                    annot_pvr += PH_label_2_ns + " has " + PH_label_1_ns + "\n"

                                if (3*PH_ymin_2 + PH_ymax_2)/4 <= vra[PH_idx_1, PH_idx_1][1][1] <= (PH_ymin_2 + 3*PH_ymax_2)/4:
                                    annot_pvr += PH_label_1_ns + " is_at_mid-section_of " + PH_label_2_ns + "\n"
                                elif vra[PH_idx_1, PH_idx_1][1][1] <= (2*PH_ymin_2 + PH_ymax_2)/3:
                                    annot_pvr += PH_label_2_ns + " lifts_up " + PH_label_1_ns + "\n"
                                else:
                                    pass
                            elif -0.1 <= vra[PH_idx_2, PH_idx_2][4] <= 0.1:
                                annot_soa_2 += PH_label_2_ns + " sits/kneels/bows/bends/crouches/squats/is_partial\n"
                            else:
                                annot_soa_2 += PH_label_2_ns + " lies/reclines\n"

                            if annot_soa_1:
                                soa_1_key = PH_label_1_ns + '++' + 'attr'

                            if annot_soa_2:
                                soa_2_key = PH_label_2_ns + '++' + 'attr'

                        # Add more rules here
                        if True:
                            pass

                        # ####################
                        if annot_pvr:
                            # Build keys for "pairs of overlapping labels" (pol) in result textual annotations dictionary
                            pol_key_1_2 = label_1_ns + '++' + label_2_ns
                            pol_key_2_1 = label_2_ns + '++' + label_1_ns

                            if pol_key_1_2 in annot_txt.keys():
                                annot_txt[pol_key_1_2] += list(
                                    annot_pvr.split('\n')[:-1])
                                annot_txt[pol_key_1_2] = list(
                                    set(annot_txt[pol_key_1_2]))  # suppress dupes
                            elif pol_key_2_1 in annot_txt.keys():
                                annot_txt[pol_key_2_1] += list(
                                    annot_pvr.split('\n')[:-1])
                                annot_txt[pol_key_2_1] = list(
                                    set(annot_txt[pol_key_2_1]))  # suppress dupes
                            else:
                                annot_txt[pol_key_1_2] = list(
                                    set(annot_pvr.split('\n')[:-1]))  # suppress dupes

                            # Add "single object attribute" (soa) keys for "instance of non-overlapping labels"
                            if soa_1_key != None:
                                if soa_1_key in annot_txt.keys():
                                    annot_txt[soa_1_key] += list(
                                        annot_soa_1.split('\n')[:-1])
                                else:
                                    annot_txt[soa_1_key] = list(
                                        annot_soa_1.split('\n')[:-1])

                                annot_txt[soa_1_key] = list(
                                    set(annot_txt[soa_1_key]))  # suppress dupes

                            if soa_2_key != None:
                                if soa_2_key in annot_txt.keys():
                                    annot_txt[soa_2_key] += list(
                                        annot_soa_2.split('\n')[:-1])
                                else:
                                    annot_txt[soa_2_key] = list(
                                        annot_soa_2.split('\n')[:-1])

                                annot_txt[soa_2_key] = list(
                                    set(annot_txt[soa_2_key]))  # suppress dupes
        else:
            pass

        return annot_txt

    # Function to derive image composition information from detected object locations

    def get_composition(width: int,
                        height: int,
                        bbx_loc: list,
                        vra: 'np.ndarray',
                        ref_objs: list
                        ) -> dict:
        '''
        get_composition():
        Computes which bbx are in background, or in foreground as well as their relative position wrt
        the main topic bbx, when a main topic can be ascertained. Processing is based on bbx' locations,
        as well as on 'vra' and 'ppa' arrays.

        Input:
          - 1st arg. (int) image height in pixels
          - 2nd arg. (int) image width in pixels
          - 3rd arg. (list of tuples): bbx_loc = (label, label_id, xmin, ymin, xmax, ymax)
          - 4th arg. (np.ndarray): 'vra', image visual relationship array
                  - on diagonal:  (obj_uniq_label,(x_cpt,y_cpt),oloc,rsa,off)
                  - off diagonal: (dcpt,cpix,bro_1_2,rpos_1_2)
          - 5th arg. (list of tuples): 'ref_objs' = [(uniq_label, (x_cpt, y_cpt), oloc. rsa), (...)]
                     as main topic candidates bbx(es)

        Output:
          - 1 object (dict): structure as follow ...
            {'clusters':{}, 'foreback':{},'omt':{},
                'amt':{},'bmt':{}, 'mylomt':{},'myromt':{}}
            where values are dictionaries.
               'clusters' = dict
                       key: uniq_label of one of bbx pertaining to set.
                       value: set of 'ref_objs' uniq_label that overlap.
               'foreback' = dict of sets of ('uniq_label','base_criterion','size_criterion') tuples,
                       situated in the foreground (dist-factor >0) or in the background (dist_factor <0)
                       with respect to the plane of representation of the 'main_topic'
                       keys are clusters labels.
               'omt' = dict of sets of 'uniq_label' fully situated within bbx of "main topic"
                       i.e. fully overlapped by it.
                       keys are cluster labels.
               'amt' = dict of sets of 'uniq_label' situated "above main topic".
                       keys are cluster labels.
               'bmt' = dict of sets of 'uniq_label' situated "beneath main topic".
                       keys are cluster labels.
               'mylomt' = dict of sets of 'uniq_label' situated "left of main topic".
                        keys are cluster labels.
               'myromt' = dict of sets of 'uniq_label' situated "right of main topic".
                        keys are cluster labels.

        '''

        composition = dict()
        clusters = dict()
        foreback = dict()
        omt = dict()
        amt = dict()
        bmt = dict()
        mylomt = dict()
        myromt = dict()
        oloc_val_map = {'tl': 60, 'tc': 90, 'tr': 40,
                        'cl': 75, 'cc': 100, 'cr': 55,
                        'bl': 45, 'bc': 80, 'br': 32}

        # Build array of tuples (uniq_label, (x_cpt, y_cpt), oloc, rsa)
        img_objs_lst = [(vra[idx, idx][0], vra[idx, idx][1], vra[idx, idx]
                         [2], vra[idx, idx][3]) for idx in range(len(vra))]
        img_objs_labels = [x[0] for x in img_objs_lst]
        ref_objs_labels = [x[0] for x in ref_objs]

        if len(ref_objs) > 1:
            for label_1 in ref_objs_labels:
                idx_1 = [vra[idx, idx][0] for idx in range(len(vra))].index(
                    label_1)  # corresponding vra index
                try:
                    if isinstance(len(clusters[label_1]), builtins.int):
                        clusters[label_1].append(label_1)
                except (KeyError,):
                    # label does not exist yet; create it.
                    clusters[label_1] = [label_1, ]

                for label_2 in ref_objs_labels:
                    idx_2 = [vra[idx, idx][0] for idx in range(len(vra))].index(
                        label_2)   # corresponding vra index
                    try:
                        if isinstance(len(clusters[label_2]), builtins.int):
                            clusters[label_2].append(label_2)
                    except (KeyError,):
                        # label does not exist yet; create it
                        clusters[label_2] = [label_2, ]

                    # Use idx_1 and idx_2 as label_1's and label_2's indices in vra
                    if (label_1 == label_2 or vra[idx_1, idx_2][2] > 0.0 or vra[idx_1, idx_2][1] == 0):
                        clusters[label_1].append(label_2)
                        clusters[label_2].append(label_1)

            # list of tuples (cluster_idx, cluster_key)
            clusters_enum = list(enumerate(clusters))
            for idx in range(len(clusters_enum)):
                value_set = set(clusters[clusters_enum[idx][1]])
                if len(value_set) == 1:
                    # delete "clusters" made of 1 element
                    del clusters[str(clusters_enum[idx][1])]

            # list of tuples (cluster_idx, cluster_key)
            clusters_enum = list(enumerate(clusters))

            # Consolidate clusters by joining cluster sets whose intersection is not void
            # Delete any cluster set from dictionary 'clusters', when merged with other cluster set
            flag = 1
            cnt = 0

            while flag:
                new_clusters = dict()
                cnt += 1
                for idx_1 in range(len(clusters_enum)):
                    set1 = set(clusters[str(clusters_enum[idx_1][1])])
                    for idx_2 in range(idx_1+1, len(clusters_enum)):
                        set2 = set(clusters[str(clusters_enum[idx_2][1])])
                        # print("Cluster sets:\n{}\n{}".format(set1,set2))     # TESTING
                        set_intersection = set1.intersection(set2)
                        # Find indices and 'rsa' values in vra for cluster keys 1 and 2
                        if set_intersection:
                            cnt = 0
                            # print("Set intersection:{}\n".format(set_intersection))     # TESTING
                            vra_idx = [vra[idx, idx][0] for idx in range(
                                len(vra))].index(clusters_enum[idx_1][1])
                            rsa_1 = vra[vra_idx, vra_idx][3]
                            vra_idx = [vra[idx, idx][0] for idx in range(
                                len(vra))].index(clusters_enum[idx_2][1])
                            rsa_2 = vra[vra_idx, vra_idx][3]

                            # Keep as only cluster key, that of object w/ bigger 'rsa'; merge & delete other one in-place
                            union_sets_1_2 = set1.union(set2)
                            idx_to_keep = idx_1 if (oloc_val_map[str(
                                vra[idx_1, idx_1][2])]*rsa_1 > oloc_val_map[str(vra[idx_2, idx_2][2])]*rsa_2) else idx_2
                            try:
                                if isinstance(len(new_clusters[clusters_enum[idx_to_keep][1]]), builtins.int):
                                    new_clusters[clusters_enum[idx_to_keep][1]] = new_clusters[clusters_enum[idx_to_keep][1]].union(
                                        union_sets_1_2)
                            except (KeyError,):
                                # label does not exist yet; create it
                                new_clusters[clusters_enum[idx_to_keep]
                                             [1]] = union_sets_1_2

                clusters = new_clusters
                clusters_enum = list(enumerate(clusters))

                if (len(clusters_enum) == 1 or cnt != 0):
                    flag = 0

        elif len(ref_objs) == 1:
            clusters[list(ref_objs)[0][0]] = [ref_objs[0][0], ]
        else:
            # empty 'ref_objs'
            pass

        for img_main_label in clusters:
            img_main_label_idx = img_objs_labels.index(
                img_main_label)    # img_main_label's vra idx
            # main_topic's bbx' base line
            main_baseline = bbx_loc[img_main_label_idx][5]
            # main topic's bbx' height
            main_height = bbx_loc[img_main_label_idx][5] - \
                bbx_loc[img_main_label_idx][3]
            # main_topic's bbx' 'rsa'
            main_rsa = vra[img_main_label_idx, img_main_label_idx][3]
            img_other_objs_idx = list(range(len(vra)))
            del img_other_objs_idx[img_main_label_idx]

            # non-unique label for img main_topic obj
            ppa_main_label = '_'.join(img_main_label.split(sep='_')[:-1])
            # use global variable 'class_lst'
            ppa_main_label_idx = class_lst.index(ppa_main_label)

            for idx in img_other_objs_idx:
                # Retrieve other objects' details
                # 'uniq_label'
                obj_uniq_label = vra[idx, idx][0]
                ppa_obj_label = '_'.join(obj_uniq_label.split(sep='_')[:-1])
                # use global variable 'class_lst'
                ppa_obj_label_idx = class_lst.index(ppa_obj_label)
                # 'ymax' = baseline
                obj_baseline = bbx_loc[idx][5]
                obj_height = bbx_loc[idx][5] - \
                    bbx_loc[idx][3]          # 'ymax'-'ymin'
                obj_rsa = vra[idx, idx][3]

                if ppa_main_label == ppa_obj_label:
                    # main_topic object instanced more than once in image
                    obj_rel_prop_to_main = 1
                else:
                    # rel prop of obj to 'main_topic' bbx size
                    obj_rel_prop_to_main = ppa[ppa_obj_label_idx][ppa_main_label_idx]

                # 'rpos' wrt to 'main_label'
                obj_rel_pos_to_main = vra[idx, img_main_label_idx][3]
                # 'bro' (bbx relative overlap) relative to self'
                obj_bro_wrt_self = vra[idx, img_main_label_idx][2]
                # 'bro' (bbx relative overlap) relative to 'main_label'
                obj_bro_wrt_main = vra[img_main_label_idx, idx][2]

                # If either ppa_main_label or current ppa_other_objs_label are of type "crucifixion"
                #   do not compare bbx' baselines but rather bbx' RSA
                if (ppa_main_label in {'book', 'scroll', 'banner',
                                       'apple', 'banana', 'lily', 'palm', 'tree',
                                       'angel', 'crucifixion', 'construction', 'rock',
                                       'bird', 'devil', 'butterfly', 'dove', 'swan', 'eagle', 'fish', 'cat', 'monkey', 'mouse',
                                       'rooster', 'serpent', 'pegasus', 'horn', 'sea shell',
                                       'saturno', 'camauro', 'zuccheto', 'mitre', 'tiara', 'crown', 'halo', 'stole',
                                       'crown of thorn', 'helmet',
                                       'hand', 'severed head', 'skull', 'wing',
                                       'lance', 'shield', 'arrow', 'sword',
                                       'jug', 'chalice', 'key of heaven', }
                    or ppa_obj_label in {'book', 'scroll', 'banner',
                                         'apple', 'banana', 'lily', 'palm', 'tree',
                                         'angel', 'devil', 'crucifixion', 'construction', 'rock',
                                         'bird', 'butterfly', 'dove', 'swan', 'eagle', 'fish', 'cat', 'monkey', 'mouse',
                                         'rooster', 'serpent', 'pegasus', 'horn', 'sea shell',
                                         'saturno', 'camauro', 'zuccheto', 'mitre', 'tiara', 'crown', 'halo', 'stole',
                                         'crown of thorn', 'helmet',
                                         'hand', 'severed head', 'skull', 'wing',
                                         'lance', 'shield', 'arrow', 'sword',
                                         'jug', 'chalice', 'key of heaven', }):
                    base_criterion = None
                else:
                    base_criterion = round(
                        (obj_baseline - main_baseline) / height, 2)

                size_criterion = round(
                    (obj_rsa - obj_rel_prop_to_main * main_rsa)/100, 2)

                try:
                    if isinstance(len(foreback[img_main_label]), builtins.int):
                        foreback[img_main_label].append(
                            (obj_uniq_label, base_criterion, size_criterion))
                except(KeyError,):
                    foreback[img_main_label] = [
                        (obj_uniq_label, base_criterion, size_criterion), ]

                # Total overlap of img_obj_label in img_main_label's bbx
                criterion = obj_bro_wrt_self / 100                                    # bounded
                if criterion >= bro_th:
                    try:
                        if isinstance(len(omt[img_main_label]), builtins.int):
                            omt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        omt[img_main_label] = [obj_uniq_label, ]

                # Objs positioned either:
                #      - above main_label's bbx: amt       {'NN', 'NW', 'NE'}
                #      - beneath main_label's bbx: bmt     {'SS', 'SW', 'SE'}
                #      - to my left of main topic: mylomt  {'WW'}
                #      - to my right of main topic: myromt {'EE'}
                criterion = obj_rel_pos_to_main
                if criterion in {'NN', 'NW', 'NE'}:
                    try:
                        if isinstance(len(amt[img_main_label]), builtins.int):
                            amt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        amt[img_main_label] = [obj_uniq_label, ]

                    if criterion in {'NW'}:
                        try:
                            if isinstance(len(mylomt[img_main_label]), builtins.int):
                                mylomt[img_main_label].append(obj_uniq_label)
                        except(KeyError,):
                            mylomt[img_main_label] = [obj_uniq_label, ]
                    elif criterion in {'NE'}:
                        try:
                            if isinstance(len(myromt[img_main_label]), builtins.int):
                                myromt[img_main_label].append(obj_uniq_label)
                        except(KeyError,):
                            myromt[img_main_label] = [obj_uniq_label, ]

                elif criterion in {'SS', 'SW', 'SE'}:
                    try:
                        if isinstance(len(bmt[img_main_label]), builtins.int):
                            bmt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        bmt[img_main_label] = [obj_uniq_label, ]

                    if criterion in {'SW'}:
                        try:
                            if isinstance(len(mylomt[img_main_label]), builtins.int):
                                mylomt[img_main_label].append(obj_uniq_label)
                        except(KeyError,):
                            mylomt[img_main_label] = [obj_uniq_label, ]

                    elif criterion in {'SE'}:
                        try:
                            if isinstance(len(myromt[img_main_label]), builtins.int):
                                myromt[img_main_label].append(obj_uniq_label)
                        except(KeyError,):
                            myromt[img_main_label] = [obj_uniq_label, ]

                elif criterion in {'WW'}:
                    try:
                        if isinstance(len(mylomt[img_main_label]), builtins.int):
                            mylomt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        mylomt[img_main_label] = [obj_uniq_label, ]

                else:
                    # if criterion in {'EE'}:
                    try:
                        if isinstance(len(myromt[img_main_label]), builtins.int):
                            myromt[img_main_label].append(obj_uniq_label)
                    except(KeyError,):
                        myromt[img_main_label] = [obj_uniq_label, ]

        composition['clusters'] = clusters
        composition['foreback'] = foreback
        composition['omt'] = omt
        composition['amt'] = amt
        composition['bmt'] = bmt
        composition['mylomt'] = mylomt
        composition['myromt'] = myromt

        return composition

    # Function to obtain the main topic(s) represented in an image

    def get_main_topics(vra: 'np.ndarray') -> dict:
        '''
        get_main_topics():
        Detects candidates objects to promote them to values under the key 'main_topic(s)'.

        Input:
        @param      vra: image visual relationships array
        @type       np.ndarray

        Output:
        @param      img_main_objs
                    {
                        'notable_objs':notable_objs,
                        'bigger_objs':bigger_objs,
                        'ref_objs':ref_objs
                    }
                    where each value is a list of unique labels (strings)
        @type       dict formatted as json
        '''

        # Processing based on vra (visual relationships array) and ppa (pairwise proportion array),
        #   both of type numpy.ndarray

        rel_sizes = np.array(ppa)
        img_main_objs = dict()

        # Build array of tuples (uniq_label, (x_ctp, y_ctp), oloc, rsa)
        if len(vra[0, :]) == 0:
            pass
        else:
            img_class_lst = [(vra[idx, idx][0], vra[idx, idx][1], vra[idx, idx]
                              [2], vra[idx, idx][3]) for idx in range(len(vra[0, :]))]
            main_topic_objs = [ppa[idx][idx][0] for idx in range(
                len(ppa)) if ppa[idx][idx][1] == 'main_topic']

            notable_center_objs = [x for x in img_class_lst
                                   if x[2] in {'tc', 'cc', 'bc'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
            # notable_objs = [(x[0],['bc','cc','tc'].index(x[2]),x[3]) for x in img_class_lst
            #                if x[2] in {'tc','cc','bc'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
            notable_left_objs = [x for x in img_class_lst
                                 if x[2] in {'tl', 'cl', 'bl'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
            notable_right_objs = [x for x in img_class_lst
                                  if x[2] in {'tr', 'cr', 'br'} and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs]
            # notable_sides_objs = list(set(notable_left_objs).union(set(notable_right_objs)))
            # because every obj in lists is unique
            notable_sides_objs = notable_left_objs + notable_right_objs

            if (len(notable_center_objs) != 0 and len(notable_sides_objs) != 0):
                center_objs_to_remove = []
                sides_objs_to_remove = []

                for center_obj in notable_center_objs:
                    center_obj_idx = img_class_lst.index(center_obj)
                    # center_obj_label = center_obj[0] + '_' + center_obj[1]
                    center_obj_ppa_idx = class_lst.index(
                        '_'.join(center_obj[0].split(sep='_')[:-1]))
                    for sides_obj in notable_sides_objs:
                        sides_obj_idx = img_class_lst.index(sides_obj)
                        # sides_obj_label = sides_obj[0] + '_' + sides_obj[1]
                        sides_obj_ppa_idx = class_lst.index(
                            '_'.join(sides_obj[0].split(sep='_')[:-1]))
                        if center_obj_ppa_idx != sides_obj_ppa_idx:
                            rel_pp_center2side = rel_sizes[center_obj_ppa_idx,
                                                           sides_obj_ppa_idx]
                        else:
                            rel_pp_center2side = 1

                        if vra[center_obj_idx, center_obj_idx][3] >= 1.3 * rel_pp_center2side * vra[sides_obj_idx, sides_obj_idx][3]:
                            sides_objs_to_remove.append(sides_obj)
                        elif vra[center_obj_idx, center_obj_idx][3] <= 0.7 * rel_pp_center2side * vra[sides_obj_idx, sides_obj_idx][3]:
                            center_objs_to_remove.append(center_obj)
                        else:
                            pass

                notable_center_objs = [
                    x for x in notable_center_objs if x not in center_objs_to_remove]
                notable_sides_objs = [
                    x for x in notable_sides_objs if x not in sides_objs_to_remove]

            if (len(notable_center_objs) == 0 and len(notable_sides_objs) != 0):
                # if no notably situated eligible object are detected as main topic candidate, try at left
                notable_objs = notable_sides_objs
            elif (len(notable_center_objs) != 0 and len(notable_sides_objs) != 0):
                notable_objs = notable_center_objs + notable_sides_objs
            else:
                notable_objs = notable_center_objs

            notable_objs = sorted(notable_objs,
                                  key=lambda x: (x[2], x[3]),
                                  reverse=True)  # order according to x[2] ('oloc'); use x[3] ('rsa') as tie-breaking key

            # Define 'bigger_objs' as any object whose bbx' surface area is at least 10% of that of the whole image.
            #    Classification as background or foreground left to be tested
            bigger_objs = sorted([x for x in img_class_lst if x[3] > 10 and '_'.join(x[0].split(sep='_')[:-1]) in main_topic_objs],
                                 key=lambda x: x[3],
                                 reverse=True)

            if set(notable_objs) & set(bigger_objs):
                ref_objs = list(set(notable_objs) & set(bigger_objs))
            elif notable_objs:
                ref_objs = notable_objs
            elif bigger_objs:
                ref_objs = bigger_objs
            else:
                # Case where no reference objects can be found according to above citeria of centrality and size
                ref_objs = []   # TODO  (other criteria ?)

            img_main_objs = {'notable_objs': notable_objs,
                             'bigger_objs': bigger_objs, 'ref_objs': ref_objs}

        return img_main_objs

    # Function to obtain visual relationships descriptoids
    def get_descriptoids(vra: np.ndarray,
                         size_th: float,
                         base_th: float,
                         bro_th: float,
                         width: int,
                         height: int,
                         bbx_loc: List[Tuple[Union[str, int]]]
                         ) -> dict:
        '''
        get_descriptoids():
        Computes basic description elements from visual relationships between bbxes in an image.

        Input:
        @param       vra: visual relationships array
        @type        vra: np.ndarray

        @param       size_th: bbx' size criterion's threshold
        @type        size_th: float

        @param       base_th: bbx' baseline criterion's threshold
        @type        base_th: float

        @param       bro_th: bbx' percent surface overlap criterion's threshold
        @type        bro_th: float

        @param       width: image width in pixels
        @type        int

        @param       height: image height in pixels
        @type        int

        @param       bbx_loc: 
        @type        List[Tuple[str, str, int, int, int, int]]


        Output:
        @param       vrd
                     {'artist':{'name':{'firstname':None, 'lastname':None},
                                'life_dates':{'birth_year':None, 'death_year':None}
                               },
                      'artwork':{'title':{
                                          'en':None
                                         },
                                 'work_dates':{'year':[],
                                               'century':None
                                              },
                                 'dimensions':{'width':None,'height':None}
                                },
                      'main_topic(s)':{
                                       'notable_objs':[],
                                       'bigger_objs':[],
                                       'ref_objs':[]
                                      },
                      'composition':{
                                     'foreback':{},
                                     'omt':{},
                                     'amt':{},
                                     'bmt':{},
                                     'mylomt':{},
                                     'myromt':{}
                                    },
                      'annot_txt':{}
                     }

                     Detailed structure:
                        'main_topics': elements are lists of tuples (uniq_label, (x_ctp, y_ctp), oloc, rsa)
                        'composition': values are sets of 'uniq_label':
                            'cluster(s)': dict 
                                key: uniq_label of one of bbx pertaining to set.
                                value: set of 'ref_objs' uniq_label that overlap.
                            'foreback': dict of sets of ('uniq_label','base_criterion','size_criterion') tuples, 
                                situated in the foreground (dist-factor >0) or in the background (dist_factor <0) 
                                with respect to the plane of representation of the 'main_topic'
                                keys are clusters labels.
                            'omt': dict of sets of 'uniq_label' situated "over main topic"
                                i.e. near or on top of it, behind or in front of it.
                                keys are cluster labels.
                            'amt': dict of sets of 'uniq_label' situated "above main topic".
                                keys are cluster labels.
                            'bmt': dict of sets of 'uniq_label' situated "beneath main topic".
                                keys are cluster labels.
                            'mylomt': dict of sets of 'uniq_label' situated to "my left of main topic".
                                keys are cluster labels.
                            'myromt': dict of sets of 'uniq_label' situated to "my right of main topic".
                                keys are cluster labels.
                        'annot_txt': dict
                            key = 'uniq_label_1+uniq_label_2'  for pairwise visual relationships between objects
                            key = 'uniq_label_1'+'attr'        for single/isolated objects' attributes
        @type:      dict, formatted as json
        '''

        rel_sizes = np.array(ppa)

        vrd = dict()
        annot_txt = dict()

        # 'main topic(s)':
        #   List of tuples (uniq_label, (x_ctp, y_ctp), oloc, rsa)   # <<< Add 'off', 'rank' ?
        #   'main topic(s)'object location (oloc) is usually restricted to 'oloc' in {'tc','cc','bc'}
        #    and/or to large values of 'rsa'
        # 'composition':
        #   'cluster(s)': dict
        #      key: uniq_label of one of bbx pertaining to set
        #      value: set of 'ref_objs' uniq_label that overlap
        #      Note: detected ref-objs' unique labels can be part of exactly one cluster.
        #   'foreback' = dict of sets of ('uniq_label','dist_factor') tuples,
        #      situated in the foreground (dist-factor >0) or in the background (dist_factor <0)
        #   'omt' = dict of sets of 'uniq_label' situated "over main topic",
        #      i.e. near or on top of it, behind or in front of it
        #   'amt' = dict of sets of 'uniq_label' situated "above main topic"
        #   'bmt' =  dict of sets of 'uniq_label' situated "beneath main topic"
        #   'mylomt' = dict of sets of 'uniq_label' situated "left of main topic"
        #   'myromt' = dict of sets of 'uniq_label' situated "right of main topic"
        # Physical overlap of detected objects:
        #   'phys_overlap' = dictionary of objects in possible direct contact w/:
        #     - key is 'uniq_label_1+uniq_label_2'
        #     - value is [free_text_str,
        #                 dcpt,
        #                 (cpt_1, cpt_2),
        #                 (bro_1_2, bro_2_1),
        #                 rpos_1_2,
        #                 (off_1,off_2)]

        vrd = {'artist': {'name': {'firstname': None, 'lastname': None},
                          'life_dates': {'birth_year': None, 'death_year': None}},
               'artwork': {'title': {'en': None, },
                           'work_dates': {'year': [], 'century': None},
                           'dims_cm': {'width': None, 'height': None},
                           'dims_pixel': {'width': width, 'height': height}},
               'main_topics': {'notable_objs': [], 'bigger_objs': [], 'ref_objs': []},
               'composition': {'clusters': {}, 'foreback': {}, 'omt': {}, 'amt': {}, 'bmt': {}, 'mylomt': {}, 'myromt': {}},
               'annot_txt': annot_txt
               }

        if len(vra) == 0:
            pass
        else:
            vrd['main_topics'].update(get_main_topics(vra))
            ref_objs = vrd['main_topics']['ref_objs']
            vrd['composition'] = get_composition(
                width, height, bbx_loc, vra, ref_objs)
            vrd['annot_txt'] = get_annot_txt(
                width, height, bbx_loc, vra, size_th, base_th, bro_th)

        return vrd

    #  ===============================
    # Processing
    #  ===============================

    vrd = dict()

    # Define or load hyperclasses
    person_like_instances = {"person", "child", "devil", "angel", "prayer", "man", "woman",
                             "monk", "nun", "pope", "knight", "king", "queen", "judith",
                             "peasant", "shepherd", "fisherman", "hermit", "hunter", "nude", }
    small_objects = {'banana', 'apple', 'orange', 'skull', 'sword', 'helmet', 'bird', 'dove',
                     'rooster', 'mouse', 'sea shell', 'book', 'scroll', 'cross', 'chalice', 'arrow',
                     'jug', 'hand', 'severed head', 'sculpture', 'crown', 'crown of thorns',
                     'mitre', 'saturno', 'tiara', 'zucchetto', }
    winged_things = {'angel', 'devil', 'dragon', 'pegasus',
                     'bird', 'rooster', 'butterfly', 'dove', 'eagle', 'swan'}
    flying_birds = winged_things - {'angel', 'devil', 'dragon', 'pegasus',
                                    'rooster', }
    quadrupeds = {'dragon', 'pegasus', 'centaur',
                  'horse', 'donkey', 'sheep', 'cow', 'cat', 'dog',
                  'mouse',
                  'elephant', 'lion', 'monkey', 'zebra', 'bear', 'deer', }
    mountworthy_animals = {'horse', 'donkey', 'zebra', 'deer', 'pegasus', 'centaur', 'bear', 'lion', 'elephant',
                           'cow', 'sheep', }
    animal_youngs = {'horse': 'foal', 'zebra': 'colt', 'bear': 'cub', 'lion': 'cub', 'cow': 'calf', 'elephant': 'calf',
                     'dog': 'brat', 'cat': 'kitten', 'person': 'infant', 'monkey': 'infant', 'deer': 'fawn',
                     'sheep': 'lamb', 'rooster': 'chick', 'mouse': 'pup', 'swan': 'flapper', 'snake': 'snakelet', }

    vrd = dict()
    bbx_loc = list()

    ulabels_cnt = img_dict['bbx_cnt']
    ulabels = list(img_dict['bbx'].keys())

    for ulabel in ulabels:
        label_cpts = ulabel.split('_')
        label = '_'.join(label_cpts[:-1])
        label_id = label_cpts[-1]
        xmin, ymin, xmax, ymax = list(img_dict['bbx'][ulabel].values())[:4]
        bbx_loc.append((label, label_id, xmin, ymin, xmax, ymax))

    vrd = get_descriptoids(vra,
                           size_th,
                           base_th,
                           bro_th,
                           width,
                           height,
                           bbx_loc
                           )

    return vrd
