# %%
"""
### Problem statement

This notebook does 3 things:

**1)** It parses xml files and after processing ultimately persists json
formatted files to disk with the same basename and a .json suffix.
Processing involves parsing previously scraped .xml files and calculating bbxes' metadata.
For each bbx metadata includes the bbx' cpt, oloc, rsa, off parameters<BR>
  - cpt = center point coords (xc,yc) of the bbx' rectangle,<BR>
  - oloc = object's location on image plane; <BR>
Vertical and horizontal directions are divided in 3 regions: t(op), c(enter), b(ottom) and l(eft), c(enter), r(ight) respectively. 'oloc' takes its values in  {'tl','cl','bl','tc','cc','bc','tr','cr','br'} <BR>
  - rsa = percentage surface area of the bbx relative to that of the entire image <BR>
  - off = 'orientation and form-factor' attribute<BR>
The json file also includes bbx' cartesian coordinates where the origin of the
x ans y axes is at the upper left corner of the image. Coordinate units are in
pixels.

**2)** It displays images and decorate each with the bbxes of detected
objects.

**3)** It computes visual relationships metadata and persists them in an np.ndarray
format on disk in files with identical basenames and the .vra suffix. Components
(i,j) of the asymmetric np.ndarray are lists:

  - for i=j: [uniq_label,(xc,yc),oloc,rsa,off]

  - for i!=j: [dcpt, cpix, bro_1_2, rpos_1_2], where<BR>
      - dcpt = (float) pairwise distance (center to center) between bbxes<BR>
      - cpix = (float)  pairwise distance (closest two pixels) between bbxes<BR>
      - bro = (float) relative overlap of 'bbx1' onto 'bbx2' as a percentage of the total surface area of 'bbx1'.<BR>
      - rpos = (string) relative position of 'bbx1' wrt to 'bbx2' takes its values in {'N','NE','E','SE','S','SW','W','NW'}

"""

# %%
"""
### Licensing terms and copyright

This code and the code contained in the repo (https://gitlab.bsc.es/sgoab/visrel)
are protected by the terms and conditions of the GNU_GPL-v3 copyleft license.

Copyright (C) 2021 Cedric Bhihe

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR ANY PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>, or consult the
License.md file in this repo.

To contact the author(s), use: cedric dot bhihe at gmail dot com.

"""

# %%
import os, sys, locale 
import re, json

import random
import math as m
import numpy as np
from typing import Union, Any, List, Optional, Tuple, cast
import xml.etree.ElementTree as ET

from PIL import Image, ImageDraw, ImageFont

# %%
def json_parse(data: dict, key_stem='bbx', find='first', key_suffix=('',)): # -> tuple():#-> tuple(list[str, str, int, int, int, int],list[str, str, dict, str, float, float]):
    '''
    json_parse()::
      - Parse previosuly loaded json file. 
      - Yield two lists by recursively parsing bbx labels in arbitrary json trees.
    
    Input:
    - 1st arg: positional parameter, arbitrarily nested json string as dictionary.
    - 2nd arg: kw-parameter (default is 'bbx'), json sought key_stem as string.
    - 3rd arg: kw-parameter (default is 'first')
      Other value is 'all'. When set default, the json file is parsed until the
      first occurence of the sought 'key_stem' is found. When set to 'all', the whole 
      json file is parsed no matter how many occurences occurences of sought 'key_stem'
      are found.
    - 4th arg: (default is a tuple with one empty string '')
      The default applies when no 'key_stem' suffix are needed or considered.
      When keys are provided as 'key_stem' concatenated with suffix(es), then all or only
      part of those keys = 'key stem' + 'key_suffix' can be specified in a tuple of 
      'key_suffix' string(s). 
      For example if "key_stem='product'" and the sought after keys are 'product_basic', 
      'product_midrange' and/or 'product_topline', then specifying "key_suffix=('',)" will
      produce zero pertinent result. 
      On the other hand "key_suffix=('_basic','_topline')" would only retrieve key values 
      for the two suffixes '_basic' and '_topline'.
    
    Output: a tuple containing two lists: 
    - 1st list: each bbx' label, label_id and xmin, ymin, xmax, ymax
    - 2nd list: each bbx' label, label_id, and 4 computed attributes:'cpt','oloc','rsa','off'
    '''
    if find not in ['first','all']:
        raise ValueError('Bad argument specification for argument find=...\nOnly "first" or "all" are allowed.')

    
    def build_obj_locmtd_list(sought_keys, find, k, v) -> None:
        '''
        Function defined for readability
        '''
        nonlocal stack, visited_keys, obj_loc, obj_mtd
        
        #if ((k not in sought_keys) and isinstance(v, __builtins__.dict)):
        if ((k not in sought_keys) and isinstance(v, dict)):                
                if k not in visited_keys:
                    stack.extend(v.items())
                
        #elif ((k in sought_keys) and isinstance(v, __builtins__.dict) and v != {}):
        elif ((k in sought_keys) and isinstance(v, dict) and v != {}):
            for label, coord in list(v.items()):
                try:
                    label_cmpnts = label.split('_')
                    label = '_'.join(label_cmpnts[:-1])
                    label_id = label_cmpnts[-1]
                    try:
                        xmin, ymin, xmax, ymax = [int(xy) for xy in list(coord.values())[:4]]
                        cpt, oloc, rsa, off = [coro for coro in list(coord.values())[4:8]]
                    except (AttributeError, ValueError) as ave:
                        print(ave + '\n ==> Missing or non integer bbx coordinates.')
                    except Exception as ex:
                        print(ex)
                        print('Unsure about what happened. Probably unsafe to continue; parsing interrupted.')
                        raise ex
                    obj_loc.append((label, label_id, xmin, ymin, xmax, ymax)) 
                    obj_mtd.append((label, label_id, cpt, oloc, rsa, off))
                except (NameError,):
                    pass
            
            if find == 'first':
                stack = []  
        else:
            pass
        
        if find == 'first':
            visited_keys.add(k)
        elif find == 'all':
            pass
    
    
    visited_keys = set()
    obj_loc = []
    obj_mtd = []    
    stack = list(data.items())
    sought_keys = list(map(lambda x: key_stem + x,list(key_suffix)))

    """
    @deprecated? # not working for python 3.8
    while stack: 
        k, v = stack.pop()
        build_obj_locmtd_list(sought_keys, find, k, v)
    """
    for k,v in stack:
        #k, v = stack.pop()
        build_obj_locmtd_list(sought_keys, find, k, v)
    
    return obj_loc, obj_mtd


def rsa_off(width: int, height: int, xmin: int, xmax: int, ymin: int, ymax: int): #tuple(float,float):
    '''
    Function 'rsa_off()' computes: 
      1/ the surface area of a bbx, relative to that of the entire image.
         The returned result is a percentage (type float). 
      2/ a bbx' orientation and form factor realtive to that of the entire image.
         The returned result is a number between -1 and +1 (type float),
         where:
         * -1 <= off < 0   denotes a horizontally oriented bbx,
         *  off = 0        denotes a square bbx, 
         *  0 < off <= +1  denotes a vertically oriented bbx.
    '''
    try:
        width = int(width)
        height = int(height)
        xmin = int(xmin)
        ymin = int(ymin)
        xmax = int(xmax)
        ymax = int(ymax)
    except (ValueError) as ve:
        print(ve + '\nFunction \'rsa_f()\' ==> Non integer bbx coordinates, image width or image height.')
        raise ve
    
    a = ymax - ymin
    b = xmax - xmin
    rsa = round(b * a/(width * height) * 100, 2)
    off = 0. if a == b else round((a-b)*(height*width)**(-0.5) * pow(width/height,((a-b)/(2*abs(a-b)))),4)
    
    return rsa,off


#def euclid_dist(p1: tuple(float,float), p2: tuple(float,float)) -> float:
def euclid_dist(p1: tuple(), p2: tuple()): # -> float:
    '''
    Function 'euclid_dist(p1,p2)' computes the L2 (Euclidean) distance between two 
    points in the Cartesian plane.
    Input: 2 tuples, made of 2 coordinates each, all being floating point nbrs.
    Output: 1 floating point number as the computed distance of interest.
    '''
    dpt = (sum((comp1 - comp2) ** 2.0 for comp1, comp2 in zip(p1, p2)))**0.5
    
    return round(dpt)


def shortest_pix_dist(cpt1: tuple(), 
                      coord1: tuple(),
                      cpt2: tuple(), 
                      coord2: tuple()) -> float:
    '''
    Function 'shortest_pix_dist(cpt1, coord1, cpt2, coord2)' takes four tuples as input:
       * cpt1: 1st bbx' center coordinates (xc1, yc1)
       * coord1: 1st bbx' coordinates (xmin1, ymin1, xmax1, ymax1)
       * cpt2: 2nd bbx' center coordinates (xc2, yc2)
       * coord1: 2nd bbx' coordinates (xmin2, ymin2, xmax2, ymax2)
    Output: shortest L2 (Euclidean) (outer) distance between the two closest points of
    two arbitrary bbx in the Cartesian plane. If 2 bbxes touch, their closest pixels are
    at distance zero. If they overlap, we set the shortest distance value to -9999. 
    '''
    xc1, yc1 = cpt1
    xmin1, ymin1, xmax1, ymax1 = coord1
    xc2, yc2 = cpt2
    xmin2, ymin2, xmax2, ymax2 = coord2
    
    if xc2 >= xc1 and yc2 >= yc1:
        delta_x = xmin2 - xmax1
        if delta_x < 0:
            cpix = -9999 if (ymin2 - ymax1 < 0) else ymin2 - ymax1
        elif delta_x == 0:
            cpix = 0 if (ymin2 - ymax1 <= 0) else ymin2 - ymax1
        else:   # delta_x > 0
            if ymin2 - ymax1 <= 0:
                cpix = delta_x
            else:
                cpix = euclid_dist((xmax1,ymax1),(xmin2,ymin2))    
    elif xc2 <= xc1 and yc2 >= yc1:
        delta_x = xmin1 - xmax2
        if delta_x < 0:
            cpix = -9999 if (ymin2 - ymax1 < 0) else ymin2 - ymax1
        elif delta_x == 0:
            cpix = 0 if (ymin2 - ymax1 <= 0) else ymin2 - ymax1
        else:   # delta_x > 0
            if ymin2 - ymax1 <= 0:
                cpix = delta_x
            else:
                cpix = euclid_dist((xmin1,ymax1),(xmax2,ymin2)) 
    elif xc2 <= xc1 and yc2 <= yc1:
        delta_x = xmin1 - xmax2
        if delta_x < 0:
            cpix = -9999 if (ymin1 - ymax2 < 0) else ymin1 - ymax2
        elif delta_x == 0:
            cpix = 0 if (ymin1 - ymax2 <= 0) else ymin1 - ymax2
        else:   # delta_x > 0
            if ymin1 - ymax2 <= 0:
                cpix = delta_x
            else:
                cpix = euclid_dist((xmin1,ymin1),(xmax2,ymax2)) 
    elif xc2 >= xc1 and yc2 <= yc1:
        delta_x = xmin2 - xmax1
        if delta_x < 0:
            cpix = -9999 if (ymin1 - ymax2 < 0) else ymin1 - ymax2
        elif delta_x == 0:
            cpix = 0 if (ymin1 - ymax2 <= 0) else ymin1 - ymax2
        else:   # delta_x > 0
            if ymin1 - ymax2 <= 0:
                cpix = delta_x
            else:
                cpix = euclid_dist((xmax1,ymin1),(xmin2,ymax2))
    
    return round(cpix)


def bbx_overlap(coord1: tuple(), coord2: tuple()):#-> tuple():
    '''
    Function 'bbx_overlap(coord1, coord2)' takes two tuples of integers as input:
       * coord1: 1st bbx' coordinates (xmin1, ymin1, xmax1, ymax1)
       * coord1: 2nd bbx' coordinates (xmin2, ymin2, xmax2, ymax2)
    Output: tuple(float, float) representing 2 arbitrary bbxes' overlapping surface
    area relative to the surface area of the 1st bbx and to that of the 2nd bbx respectively.
    When 2 bbxes do not overlap or simply touch, the function returns 0.
    '''
    xmin1, ymin1, xmax1, ymax1 = coord1
    xmin2, ymin2, xmax2, ymax2 = coord2
    
    if ((ymin1 < ymax2 and ymax2 <= ymax1) or (ymin1 <= ymin2 and ymin2 < ymax1) or
        (ymin2 < ymax1 and ymax1 <= ymax2) or (ymin2 <= ymin1 and ymin1 < ymax2)):
        ymin = max(ymin1, ymin2)
        ymax = min(ymax1, ymax2)
        numerator = ymax - ymin + 1
        vbro_1_2 = min(1, numerator / (ymax1 - ymin1))
        vbro_2_1 = min(1, numerator / (ymax2 - ymin2))
    else:
        vbro_1_2 = vbro_2_1 = 0
            
     
    if ((xmin1 < xmax2 and xmax2 <= xmax1) or (xmin1 <= xmin2 and xmin2 < xmax1) or
        (xmin2 < xmax1 and xmax1 <= xmax2) or (xmin2 <= xmin1 and xmin1 < xmax2)):
        xmin = max(xmin1, xmin2)
        xmax = min(xmax1, xmax2)
        numerator = xmax - xmin + 1
        hbro_1_2 = min(1, numerator / (xmax1 - xmin1))
        hbro_2_1 = min(1, numerator / (xmax2 - xmin2))
    else:
        hbro_1_2 = hbro_2_1 = 0
        
    bro_1_2 = round(min(1,vbro_1_2 * hbro_1_2) * 100)
    bro_2_1 = round(min(1,vbro_2_1 * hbro_2_1) * 100)
        
    return (bro_1_2, bro_2_1)


def p2p_rpos(p1: tuple(), p2: tuple(), precision=15, sector='pict') -> str:
    '''
    Function 'p2p_rpos(p1, p2)' computes the slope of the segment P1-P2 in Euclidean
    space, based on a referential system, where the x and y axes are horizontal and
    vertical respectively, but with the origin of the axes located in the lower left 
    corner of the image (instead of the upper left corner). This means that the x 
    axis is left unchanged but the y axis is inversed under this change of referential.
    
    Input: 4 args. 
    The 1st 2 args are tuples containing 2 floats each:  P1(x1, y1) and
    P2(x2, y2). 
    The 3rd arg is the centering precision in pixel, useful in determining 
    whether a slope needs calculating
    The 4th arg specifies whether the 360 degree (2Pi radians) segmentation in 8
    sectors is of type 'pict' or 'uniform'. By default "sector='pict'", defines
    sectors that do not correspond to the same solid angles:
    - 'EE','NN', 'WW', and 'SS': each correspond to a 60 degree or Pi/3 radians sector.
    - 'NE', 'NW', 'SW' and 'SE': each correspond to a 30 degree or Pi/6 radians sector.
    This is to reflect the fact that cardinal directions are more important in paintings
    than oblique directions centered on the two diagonals at 45 degrees.
    When "secseg='uniform'" on the other hand, every one of the 8 sectors has a solid 
    angle equal to 45 degrees or Pi/4 radians.
    
    Output: 2 strings with values in {'C','EE','NE','NN','NW','WW','SW','SS','SE'}, where
    'C', 'EE', 'NN', 'WW' and 'SS' stand for 'Centered', 'East', 'North', 'West' and 'South'
    respectively in the new axes referential described above. The repetition of the same 
    initial indicates a cardinal direction. For example 'NN' is due-North.
    
    '''
    rpos_1_2 = rpos_2_1 = ''
    
    if abs(euclid_dist(p1,p2)) <= precision:
                rpos_1_2 = rpos_2_1 = 'C'
    else:
        x1, y1 = p1
        x2, y2 = p2
        if x1 == x2 or y1 == y2:
            if y1 > y2:
                rpos_1_2 = 'SS'
                rpos_2_1 = 'NN'
            elif y1 < y2:
                rpos_1_2 = 'NN'
                rpos_2_1 = 'SS'
            elif x1 > x2:
                rpos_1_2 = 'WW'
                rpos_2_1 = 'EE'
            else:
                rpos_1_2 = 'EE'
                rpos_2_1 = 'WW'
        else:
            
            if sector == 'pict':
                b_sec = m.pi/6
                seg_lst = [12,11,10,9,8,7,6,5,4,3,2,1]
            elif sector == 'uniform':
                b_sec = m.pi/8
                seg_lst = [16,15,13,12,11,9,8,7,5,4,3,1]
            else:
                print('p2p_rpos() ValueError exception: unknown sector segmentation specified as kw argument.')
                raise ValueError('Currently accepted function arguments include only "sector=\'pict\'" (default),\nor "sector=\'uniform\'". Execution aborted.')
                    
            slope = -(y2-y1)/(x2-x1)
            theta = m.acos((1+slope**2)**-0.5)
            
            if slope > 0 and x1 > x2:
                thetap = theta
                if 0 <= thetap and thetap < b_sec*seg_lst[-1]:
                    rpos_1_2 = 'EE'
                    rpos_2_1 = 'WW'
                elif b_sec*seg_lst[-1] <= thetap and thetap < b_sec*seg_lst[-2]:
                    rpos_1_2 = 'NE'
                    rpos_2_1 = 'SW'
                elif b_sec*seg_lst[-2] <= thetap and thetap < b_sec*seg_lst[-3]:
                    rpos_1_2 = 'NN'
                    rpos_2_1 = 'SS'
            elif slope < 0 and y1 < y2:
                thetap = m.pi - theta
                if b_sec*seg_lst[-3] <= thetap and thetap < b_sec*seg_lst[-4]:
                    rpos_1_2 = 'NN'
                    rpos_2_1 = 'SS'
                elif b_sec*seg_lst[-4] <= thetap and thetap < b_sec*seg_lst[-5]:
                    rpos_1_2 = 'NW'
                    rpos_2_1 = 'SE'
                elif b_sec*seg_lst[-5] <= thetap and thetap < b_sec*seg_lst[-6]:
                    rpos_1_2 = 'WW'
                    rpos_2_1 = 'EE'
            elif slope > 0 and x1 < x2:
                thetap = theta + m.pi
                if b_sec*seg_lst[-6] <= thetap and thetap < b_sec*seg_lst[-7]:
                    rpos_1_2 = 'WW'
                    rpos_2_1 = 'EE'
                elif b_sec*seg_lst[-7] <= thetap and thetap < b_sec*seg_lst[-8]:
                    rpos_1_2 = 'SW'
                    rpos_2_1 = 'NE'
                elif b_sec*seg_lst[-8] <= thetap and thetap < b_sec*seg_lst[-9]:
                    rpos_1_2 = 'SS'
                    rpos_2_1 = 'NN'
            elif slope < 0 and y1 > y2:
                thetap = 2*m.pi - theta
                if b_sec*seg_lst[-9] <= thetap and thetap < b_sec*seg_lst[-10]:
                    rpos_1_2 = 'SS'
                    rpos_2_1 = 'NN'
                elif b_sec*seg_lst[-10] <= thetap and thetap < b_sec*seg_lst[-11]:
                    rpos_1_2 = 'SE'
                    rpos_2_1 = 'NW'
                elif b_sec*seg_lst[-11] <= thetap and thetap < b_sec*seg_lst[-12]:
                    rpos_1_2 = 'EE'
                    rpos_2_1 = 'WW'
    
    return (rpos_1_2, rpos_2_1)


def bbx_visrel(data: dict, find='first') -> Any:
    '''
    Function 'bbx_visres(data: dict)' accepts up to two arguments as input.
    
    Input:
    1st arg: specially crafted json file, contains basic bbx information for an image.
    2nd arg: kw-parameter whose value defaults to 'first'. Other value is 'all'. 
    When set to default, the json file is parsed until the 1st ocurrence
    of the sought key is found. When set to 'all', the whole json file is parsed 
    no matter how many occurences occurences of 'sought_key' are found.
    
    Output: 
    Numpy array, where each non diagonal array elements are lists of length
    4, and diagonal elements are lists of length 5.

    ON DIAGONAL COMPONENTS ('bbx1' = 'bbx2'):
      Each array element of type 'list' contains:
      - uniq_label = label + '_' + label_id
      - cpt = center point coords (xc,yc)
      - oloc = object's location on image plane; vertical and horizontal are divided 
        in the three regions t(op), c(enter), b(ottom) and l(eft), c(enter), r(ight)
        respectively. In the end the set of 'oloc' values is {'tl','cl','bl','tc',
        'cc','bc','tr','cr','br'}
      - rsa = percentage surface area of the bbx relative to that of entire image 
      - off = 'orientation and form-factor' attribute
      
    OFF DIAGONAL COMPONENTS  ('bbx1' != 'bbx2'):
      The 1st bbx' label ('bbx1') corresponds to the first array index while
      'bbx2' corresponds to the second index.  Each array element of type 'list'
      contains:
      - dcpt = (float) pairwise distance (center to center) between bbxes 
      - cpix = (float)  pairwise distance (closest two pixels) between bbxes
      - bro = (float) relative overlap of 'bbx1' onto 'bbx2' as a percentage of the total 
           surface area of 'bbx1'.
      - rpos = (string) relative position of 'bbx1' wrt to 'bbx2'
           'rpos' takes its values in {'NN','NE','EE','SE','SS','SW','WW','NW'}

    '''
    bbx_loc, bbx_mtd = json_parse(data, key_stem='bbx', find='first', key_suffix=('_basic',))
    
    uniq_labels = [label + '_' + label_id for (label, label_id, _, _, _, _) in bbx_loc]
    label_cnt = len(uniq_labels)

    vra = np.empty([label_cnt, label_cnt], dtype=object)  # ragged elts 
    
    for idx_1 in range(label_cnt):
        row = list()
        label1, label_id1, xmin1, ymin1, xmax1, ymax1 = bbx_loc[idx_1]
        
        for idx_2 in range(label_cnt):
            label2, label_id2, xmin2, ymin2, xmax2, ymax2 = bbx_loc[idx_2]
        
            if idx_1 == idx_2:

                xc, yc = bbx_mtd[idx_1][2].values()
                row.append([uniq_labels[idx_1], (xc, yc)])
                row[idx_1].extend(bbx_mtd[idx_1][3:])
            else:
                _, _, cpt1, oloc_1, rsa_1, off_1 = bbx_mtd[idx_1]
                _, _, cpt2, oloc_2, rsa_2, off_2 = bbx_mtd[idx_2]
                
                cpt1 = tuple(cpt1.values())
                cpt2 = tuple(cpt2.values())
                
                dcpt = euclid_dist(cpt1, cpt2)
                cpix = shortest_pix_dist(cpt1,
                                         (xmin1, ymin1, xmax1, ymax1),
                                         cpt2,
                                         (xmin2, ymin2, xmax2, ymax2))
                bro_1_2, _ = bbx_overlap((xmin1, ymin1, xmax1, ymax1),
                                         (xmin2, ymin2, xmax2, ymax2))
                                 
                rpos_1_2, _ = p2p_rpos(cpt1, cpt2)  
                
                row.append([dcpt, cpix, bro_1_2, rpos_1_2])
        
        vra[idx_1,:] = row
    
    return vra


def show_image(f_jpg: str, f_json: str, display_only: float =1.0) -> None:
    '''
    Opens image file, draws bounding boxes, and display image using default external display utility.
    
    @type                    f_jpg: str
    @type                   f_json: str
    @type   of_files_to_show_pcent: float
    
    @param                   f_jpg: fully qualified jpeg or png image filename
    @param                  f_json: fully qualified json filename; contains annotations extracted from xml file
    @param  of_files_to_show_pcent: values between 0. and 1. (default) 
    
    @return:  None
    '''
    
    if not ( 0. <= display_only <= 1. ):
        print(f'Usage: kwarg \'of_files_show_pcent\' accepts values between 0.0 and 1.0. \nExecution defaults to 1.0 and proceeds.')
        display_only = 1.
        
    try:
        with open(f_json, "r") as infile:
            data = json.load(infile)
    except (IOError) as ioe:
        print(' ==> json file cannot be opened or loaded properly. Abort.')
        raise ioe
        
    img = Image.open(f_jpg, mode='r', formats=None)
    img.LOAD_TRUNCATED_IMAGES = True
    width, height = img.size
    
    ## Define font size, font path and load font
    path_to_font = '/usr/share/fonts/TTF/times.ttf'    # path is system specific
    size_multiply = max(1,int(m.log(5*width*height/(800 * 700))))
    font_size = 28 * size_multiply

    if os.path.exists(path_to_font):
        #print(f'os.path.exists({path_to_font}): {os.path.exists(path_to_font)}')
        try:
            label_font = ImageFont.truetype(path_to_font,font_size)
        except (NameError) as ne:
            print(ne + '\n ==> Missing \'from PIL import ImageFont\'\n If necessary install module PIL/Pillow.')
            raise ne
        except Exception as ex:
            print(ex)
            print('Unsure about what happened. Probably unsafe to continue; ttf loading interrupted.')
            raise ex
    else:
        print(' ==> Missing font file or erroneous path to file. Continue with default font.')
        label_font = ''

    draw = ImageDraw.Draw(img)
    
    bbx_loc, _ = json_parse(data, key_stem='bbx', find='first', key_suffix=('_basic',))
    
    for label, label_id, xmin, ymin, xmax, ymax in bbx_loc:
        points=((xmin, ymin), (xmax,ymin), (xmax,ymax), (xmin,ymax), (xmin, ymin), (xmax,ymin))
        draw.line(points,
                  #fill=None,
                  fill='red',
                  width= size_multiply)
        if label_font:
            draw.text(((xmax+xmin)/2-len(label)*random.uniform(0.5,1)*4, ymax-round(font_size*1.1,0)),
                      label+"_"+label_id, 
                      fill='white',
                      font=label_font)
        else:
            draw.text(((xmax+xmin)/2-len(label)*random.uniform(0.5,1)*4, ymax-round(font_size*1.1,0)),
                      label+"_"+label_id, 
                      fill='white')
    
    if random.random() <= display_only:
        img.show()


# %%
## Prepare destination directory for imported images
# ####################

path_lst = os.getcwd().split('/')

try:
    path_list_idx = [idx for idx,val in enumerate(path_lst) if val == 'Sgoab'][-1]
except (ValueError, IndexError) as err:
    path_list_idx = len(path_lst)-1
    
path_lst = path_lst[:path_list_idx+1]
path_lst

# %%
## Select source directory, basenames and basename suffix of files to be processed
# ####################

## Select image basename trailing suffix if any
ser_suffix = '_train' # '_test' # ''

## Select path to image files' and images' xml files' directory
data_dir = '/'.join(path_lst) + '/Data'
img_dir = data_dir + '/Images'
print('Location to keep images to process:\n    img_dir: {0}'.format(img_dir))

try:
    os.makedirs(img_dir)
except (OSError):
    pass

## List files basenames manually
#files = [...,...,]

# or
## Obtain list of all files in `new_dir` programmatically 
files = list()
for p, _, files_lst in os.walk(img_dir,topdown=True):
    if p == img_dir:
        for f in files_lst:
            if ser_suffix != '':
                f = '.'.join(f.split('.')[:-1]).split(ser_suffix)[0] 
            else:
                f = '.'.join(f.split('.')[:-1])
                
            if (os.path.isfile(img_dir + '/' + f + '.jpg') and
                os.path.isfile(img_dir + '/' + f + ser_suffix + '.xml')
               ):
                files.append(f)

exclude_files = []
test_set_files = []

files = list(set(files)) 
files = [file for file in files if file not in exclude_files + test_set_files]
files.sort()

# %%
## Select test switch (... continued)
# ##############
is_test = False # True #

if is_test:
    zeiganteil = 1.0
    loc_files = [(img_dir + '/' + file + '.jpg',
                  img_dir + '/' + file + ser_suffix + '.xml', 
                  img_dir + '/' + file + ser_suffix + '.json')
                 for file in ['04a6b064a-64f9-2691-6f7a-7c0c8d4de71b',]]
else:
    zeiganteil = 0.1 
    loc_files = [(img_dir + '/' + file + '.jpg',
                  img_dir + '/' + file + ser_suffix + '.xml',
                  img_dir + '/' + file + ser_suffix + '.json') 
                 for file in files]

# %%
## Load bbx-annotations in RAM from file 'f_xml'
## Save image's basic and computed metadata as json, in file 'f_json'  
# ####################

dimensions = ['width','height']
img_dict = {}

for loc_file in loc_files:
    labels = list()
    
    f_jpg, f_xml, f_json = loc_file

    basename=f_jpg.split(sep='/')[-1]    
    tree = ET.parse(f_xml)              # use ET.XMLPullParser for non-bloking read or parse, 
                                        # when data is received incrementally from socket/storage.
    root = tree.getroot()
    #print('{}'.format(root.tag,))
    
    for elem in root:
        all_descendants = [e.tag.split('}', 1) for e in elem.iter()]
        all_desc_label_lst.append([label for sublst in all_descendants for label in sublst][0])    
    
    size_label_idx = all_desc_label_lst.index('size')
    
    width=int(root[size_label_idx][0].text)
    height=int(root[size_label_idx][1].text) 
    img_dict = {'filename':basename, 
                'size':{'width':width,'height':height}, 
                'bbx_basic':{},'bbx_added':{}}

    obj_idx = [(idx, root[idx][0].text) 
               for idx, val in enumerate([child.tag for child in root]) 
               if val == 'object']
    
    for idx, label in obj_idx:
        cnt = labels.count(label)        # count nbr of previous ocurrences of "label" in list "labels"
        labels.append(label)             # update list "labels"
        label = label + '_' + str(cnt+1) # differentiate between all labels
        img_dict['bbx_basic'][label] = {}      # create new dict key as defined by label

        try:
            xmin = int(round(float(root[idx][4][0].text),0))
            ymin = int(round(float(root[idx][4][1].text),0))
            xmax = int(round(float(root[idx][4][2].text),0))
            ymax = int(round(float(root[idx][4][3].text),0))
        except (AttributeError, ValueError) as ave:
            print(ave + '\n ==> Missing or non integer bbx coordinates.')
            #raise ave  # to halt execution
        except Exception as ex:
            print(ex)
            print('Unsure about what happened. Probably unsafe to continue; parsing interrupted.')
            raise ex

        cpt = (xc, yc) = ((xmin + xmax)/2,(ymin + ymax)/2)
        oloc = ('t','c','b')[int(3*max(0,yc-1)/height)] + ('l','c','r')[int(3*max(0,xc-1)/width)]
        rsa, off = rsa_off(width,height,xmin,xmax,ymin,ymax)

        # populate new key with new value
        img_dict['bbx_basic'][label].update({'xmin':xmin,'ymin':ymin,
                                             'xmax':xmax,'ymax':ymax,
                                             'cpt':{'xc':xc,'yc':yc},
                                             'oloc':oloc,'rsa':rsa,'off':off})

    img_dict.update({'bbx_cnt':len(obj_idx)})
    # serialize json (to pass object ("put"/"update") to endpoint) using REST API
    # skip type check / don't encode keys when not in (str, int, float, bool, None)
    #    default: 'skipkeys=False'
    json_str = json.dumps(img_dict, skipkeys=True, indent = 4, separators=(',', ':')) 
    # persist to file on other side of endpoint
    with open(f_json, "w") as outfile:
        outfile.write(json_str)

# %%
## Show images from 'f_jpg' with latest bbx-annotations from json file
# ####################

## Select the number of image to be shown at random
sample_idx = random.sample(range(len(loc_files)),int(zeiganteil * len(loc_files)))

print(f'Processing {ser_suffix} images:')
for idx in sample_idx:
    f_jpg, _, f_json = loc_files[idx]
    if ser_suffix != '':
        print(f' \u27A2 {f_jpg.split("/")[-1].split("_")[:_1]}')
    else:
        print(f' \u27A2 {f_jpg.split("/")[-1]}')
    show_image(f_jpg, f_json, display_only = zeiganteil)

# %%
## Build one VRA (visual relationship array) per image
# ####################

print('Processing json files:')

for loc_file in loc_files:
    _, _, f_json = loc_file
    file_to = '.'.join(f_json.split('.')[:-1]) + '.vra'
    print(f' - {file_to}')
    
    with open(f_json, "r") as infile:
        data = json.load(infile)
        infile.close()
    
    vra = bbx_visrel(data)
    with open(file_to, 'wb') as outfile:
        np.save(outfile, vra, allow_pickle=True)
    
    outfile.close()

# %%
## TESTING
# ###########

if __name__ == "__main__":

    ## Load '.vra' file containing a single object
    basename = '' # @smendoza, 18/10/2022 : <basename of processed file>
    file_from = img_dir + '/' + basename + ser_suffix + '.vra'

    with open(file_from, 'rb') as infile:
        vra = np.load(infile, allow_pickle=True)

    print([vra[idx,idx][0] for idx in range(len(vra))])
    print(len(vra))
    print(vra)

## TESTING END