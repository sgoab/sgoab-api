# %%
"""
# SVO extraction
## 'svo_xtor' dependency analysis
"""

# %%
"""
### Problem statement

This notebook does several things:

- it extracts visual relationships from caption seeds and from human reference summaries.<BR>
To do this we implement a dependency parser that permits semantic role labeling (SRL) and information extraction. A measure of syntactic chunking is also performed from dependency analysis in that, the code will capture generic attributes of nouns and verbs alike in the form of modifiers. It also captures lexical conjuncts of all kinds, for nouns, adjectives, verbs, prepositions and modifiers.
    
- it preprocesses the documents to be parsed to disambiguate any lexical contraction included in the text.
    
- it loads and tweaks the SpaCy tokenization and NER pipes by (i) enforcing no tokenization-split when the hyphenation infix is encountered, and (ii) adding a new NER pipes to recognize domain specific entities (such as Saints) and to agglomerate others under fewer types.
    
- it carries out extensive testing with sample sentences (not all shown here).
    
- In the last section, devoted to a specific crowdsourcing application with path dependent datasets not included in this repo, it extracts svo triples for arbitrary selection of reference and candidate captions. For that it differentiates between complete svo triples - as in (s,v,o) - and incomplete ones consisting mainly of subject and verbs - as in (s,v,) and dubbed _attributes_.
    
    
### How the notebook is organized - Sections

**License:**<BR>
- Licensing terms
- Copyright
- Disclaimer
    
**Installation notes:**<BR>
- Python virtual environment
- Python package requirements
- Custom interactive python kernel: `ipykernel`
- Notebook issued commands

**Part A: Methods for SVO extraction by dependency analysis**<BR>
- Methods
    
**Part B: Load and tweak NLP model pipelines**<BR>
- CNN / CPU only pipeline, model `en_core_web_lg` with static vector embedding
    - define and load English contractions' dictionary
    - patch the tokenizer with modified infix parsing rules
    - define custom NER pipe and load it
    - add a coreference processing pipe.

**Part C: Testing**<BR>
    
**Part D: Application**
"""

# %%
"""
### Licensing terms and copyright

Part of the code in this notebook is original and protected by the terms and conditions of the GNU_GPL-v3 copyleft license.

     Copyright (C) 2021 Cedric Bhihe

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

In short, THIS PROGRAM IS MADE AVAILABLE OR DISTRIBUTED IN THE HOPE OF IT BEING USEFUL, 
BUT WITHOUT WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE.  Look at the GNU General Public 
License for more details on its terms.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.

To contact the author, write to cedric dot bhihe at gmail dot com, or as a last resort to 
info at bsc dot es.

Sections of this notebook are based on:

- the **spaCy** NLP library and its English corpus trained models `en_core_web_trf` (RoBERTa-baseD) or `en_core_web_lg` (CNN-based). The pipelines roughly consist of 'transformer' or 'tokenizer', 'tagger', 'parser', 'attribute_ruler', 'lemmatizer', and 'ner' component pipes. 

SpaCy is distributed by [Explosion](https://explosion.ai/)  and protected under the terms of the [MIT](https://opensource.org/licenses/MIT) license.  

     Copyright (C) 2017-2021 Explosion
"""

# %%
"""
### Installation notes


**-- Linux Python 3.9.0 virtual environment --**

- Set up a Python 3.9.0 virtual environment

```
$ pyenv install 3.9.0
$ mkdir -p <my_directory>
$ cd <my_directory>
$ pyenv local 3.9.0
```

**-- Python package requirements --**

- Install the following packages and their dependencies:

```
$ python -m pip install <<<- 'EOF'
    setuptools==51.0.0
    matplotlib==3.5.1
    numpy==1.19.0
    scipy==1.7.3
    Cython==0.29.22
    spacy==3.2.1
    coreferee==1.2.0
EOF

```
followed by:

```
$ python -m pip install -U spacy[transformers,lookups]
$ python -m spacy download en_core_web_trf       # for the more accurate, transformer-based model pipeline, w/o
                                                 # the static word embeddings of the 'sm'/'md'/'lg' CNN models.
$ python -m spacy validate                       # verify that installed pipeline packages are compatible
                                                 #  w/ spaCy version
```

- Installing CPU-only TensorFlow 2.5 for Python 3.9 requires a different installation mechanism. 
    - download package from Google API repository
    - pip-install CPU only version from the 3.9.0 virtual environment:
    - issue iPython shell commands:


**-- Build custom interactive python kernel: `ipykernel` --**<BR>
(for Python 3.9.0)
"""

# %%
# Import "boiler-plate" and not so "boiler-plate" libraries
import os, sys, locale, time, ast, pickle, string, random, pickle
from typing import Union, Any, List, Tuple, Optional, Iterable, cast
import copy                      # enable 'copy.deepcopy()'

# Set cell display width, matplotlib image formats
import seaborn as sbn
import matplotlib.pyplot as plt
import matplotlib_inline
# @smendoza: what's that??? %matplotlib inline
# Several image formats can be enabled: 'png', 'retina', 'jpeg', 'svg', 'pdf'.
matplotlib_inline.backend_inline.set_matplotlib_formats('png', 'jpeg', quality=90)

# %%
# Import application-specific libraries
import re
import csv
import json
import numpy as np
import requests as req
#from termcolor import colored
from collections import Counter

import matplotlib.pyplot as plt
from PIL import Image

import spacy                                    # after `python -m pip install 'spacy==3.2.1'`
from spacy.tokens import Doc
from spacy.attrs import ORTH, NORM, LEMMA, IDS
from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER
from spacy.lang.char_classes import CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
from spacy.util import compile_infix_regex
import en_core_web_trf
import en_core_web_lg    # required for 'nlp = en_core_web_lg.load()'

import coreferee

# %%
class Color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

# %%
"""
### Part A:  Methods
"""

# %%
def cleanup(word: str, lower=True) -> str:
    '''
    Remove space at beginning and end of string provided as sole argument.
    Make argument string lowercase by default, when left unspecified.
    
    @type            text: str
    @param           text: input string
    
    @return: output string
    '''
    if lower:
       word = word.lower()
    
    return word.strip()


def split_on_the_dot(annots_refs_in: List[str]) -> List[List[str]]:
    '''
    Split provided list of summaries (as strings) on the end-of-sentence period, 
    whenever a summary is made of more than one sentence.
    
    @type     annots_refs_in: list of strings
    @param    annots_refs_in: textual summaries to be processed

    @return:  list of lists of strings; summaries' component sentences as individual 
              component summaries, ordered as input list was.
    '''
    annots_refs_split = list()
    annots_refs_in = [re.sub(r'\.$','',x) for x in annots_refs_in] 
    annots_refs_in = [s.split('.') for s in annots_refs_in]

    for subs in annots_refs_in:
        subset = list()
        for s in subs:
            s += '.'
            s = re.sub(r'^\s+','',s)
            subset.append(s)
        annots_refs_split.append(subset)
        
    del annots_refs_in, subs, subset
    
    return annots_refs_split


def clean_visrel_seeds(text: str, lower: bool =True, uniq_ent: bool =True) -> str:
    '''
    Clean provided string following regex pattern substitution.
    Make argument string lowercase by default, when left unspecified.
    
    Input:
    @type       text: str
    @param      text: input string to clean
    
    Output:
    @return:    Cleaned text (type: str)
    '''
    pattern1 = r'\(\(|\)\)'
    pattern2 = r'(\/([a-z_])*)+'
    pattern3 = r'_[0-9]+'
    pattern4 = r'_(?=[^0-9]+)'
    pattern5 = r'\s*[^.]+\sis\spartial\.*\s*'
    pattern6 = r'(^\s*|\s*$)'
    
    text = re.sub(pattern1,'',text)
    text = re.sub(pattern2,'',text)
    if not uniq_ent:
        text = re.sub(pattern3,'',text)
    
    text = re.sub(pattern4,' ',text)  # 'text = " ".join(text.split("_"))'
    text = re.sub(pattern5,'',text)
    text = re.sub(pattern6,'',text)   # 'text = text.strip()'
    
    if lower:
       text = text.lower()
    
    return text


def add_end_period(text: str) -> str:
    '''
    Add period at end of string provided no punctuation is there already.
        
    @type            text: str
    @param           text: input string to end with dot
    
    @return: output string
    '''
    text = re.sub(r'([^\.!?:;,]+$)','\\1.',text)
    
    return text


def remove_end_punct(text: str) -> str:
    '''
    Remove any compbination of punctuation symbol(s) and space(s) at end of string provided as sole argument.
    
    @type     text: str
    @param    text: input string at end of which any number of punctuation symbols and spaces are removed
    
    @return output: string
    '''
    pattern = re.compile(r'\s*[\!?¿)-_,(:\.;]*\s*$')
    
    return re.sub(pattern, '', text)


def is_noise(token: spacy.tokens.token.Token, noisy_pos_tags=['IN',], min_token_length=2) -> bool:
    '''
    Test whether 'token' (SpaCy parsed token) is "noise" or not
    
    @type  token: 'spacy.tokens.token.Token'
    @param token: input token to be tested
    
    @return isNoise: Boolean
    '''
    isNoise = False
    if token.pos_ in noisy_pos_tags:
        isNoise = True 
    elif token.is_stop == True:
        isNoise = True
    elif len(token.text) <= min_token_length:
        isNoise = True

    return isNoise


def find_index(cpt: Any, lst: List) -> int:
    '''
    find_index():
        Modified Python <iterable>.index() method 
        Find the index of a component in a heterogeneously typed lst
        
        @type     cpt: Any
        @type     lst: List[]
        
        @param    cpt: component of list, whose index is returned
        @param    lst: list whose component's index we return
    '''
    types = [type(x) for x in lst]
    if types.count(type(cpt)) == len(lst):
        return lst.index(cpt) 
    else:
        for idx in range(len(lst)):
            try:
                if lst[idx] == cpt:
                    return idx
            except:
                pass


def set_matplotlib_formats(*formats, **kwargs):
    '''
    set_matplotlib_formats():
        Select figure formats for the inline backend. Optionally pass quality for JPEG.

        Usage: 
        To enable 'png' and 'jpg' output with quality of 90%:
            In [1]: set_matplotlib_formats('png', 'jpeg', quality=90)

        @type          *formats: str
        @type:         **kwargs: key=value

        @param         *formats: 1 or more figure formats to enable: 'png', 'retina', 'jpeg', 'svg', 'pdf'.
        @param         **kwargs: keyword args, e.g. 'quality=90' (relayed to 'figure.canvas.print_figure')

        @return:        nothing
    '''
    from IPython.core.interactiveshell import InteractiveShell
    from IPython.core.pylabtools import select_figure_formats
    from ipykernel.pylab.config import InlineBackend
    
    # build kwargs, starting with InlineBackend config
    kw = {}
    cfg = InlineBackend.instance()
    kw.update(cfg.print_figure_kwargs)
    kw.update(**kwargs)
    shell = InteractiveShell.instance()
    select_figure_formats(shell, formats, **kw)
    
    
def set_matplotlib_close(close=True):
    '''
    Set whether inline backend used in IPython Notebook closes all figures automatically or not,
    after cell is run.
    
    Order inline backend used in IPython Notebook, to not close all 'matplotlib' figures 
    automatically after each cell is run. 
    Default is: close all 'matplotlib' figures automatically after each cell is run. This means
    that plots in different cells suffer no interference. Sometimes though, making a plot in one
    cell available to other cells is necessary to refine it in later cells. 
    
    Usage:
    To disable default closing of figures after cell is run:
        In [1]: set_matplotlib_close(close=False)
    
    Convert this in config file settings instead, with:
        c.InlineBackend.close_figures = False
    
    @type:     close: bool
    @param     close: 'True' (default)/ 'False'  
               (Should all matplotlib figures be automatically closed after each cell is run?)
    
    @return:   nothing
        
    '''
    from ipykernel.pylab.config import InlineBackend
    
    cfg = InlineBackend.instance()
    cfg.close_figures = close


def display_dep_graph(parsed_doc: 'spacy.tokens.doc.Doc') -> None:
    '''
    Print dependency graph and relation of POS tags for the provided SpaCy-parsed doc,
    from within jupyter notebooks.
    
    Input:
    @type     parsed doc: spacy.tokens.doc.Doc  
    @param    parsed_doc: spaCy-parsed summary
    
    Output:
    @return:   None
    '''
    token_idx_dic= dict()
    for idx, tok in enumerate(parsed_doc):
        token_idx_dic[tok] = idx 
            
    spacy.displacy.render(parsed_doc, style="dep", jupyter=True, 
                          options={'compact':True,
                                   'add_lemma': True,
                                   'collapse_punct':True,
                                   'offset_x':50,
                                   'color':'blue',
                                   'arrow_spacing':20,
                                   'word_spacing':30,
                                   'distance':90})
    
    print(f'{"tok_idx":<8}{"token":<12}{"lemma":<12}{"cg-POS":<8}{"fg-POS":<8}{"DEP":<8}')
    print(f'{"----":<8}{"-------":<12}{"-------":<12}{"-------":<8}{"-------":<8}{"-------":<8}')
    
    for tok in parsed_doc:
        tok_idx = token_idx_dic[tok]
        tok_head = tok.head
        tok_lemma = tok.lemma_
        if (tok.text == "'d" and tok_head.pos_ in {'VERB', 'AUX',}):
            tok_head_morph_dic = tok_head.morph.to_dict()
            try:
                tok_head_tense = str(tok_head_morph_dic['Tense']).lower()   #str(token.morph.get("Number"))
            except KeyError:
                tok_head_tense = ''
            
            if tok_head_tense == 'past':
                tok_lemma = 'have'
            else:
                tok_lemma = 'would'
        
        print(f'{tok_idx:<8}{tok.text:<12}{tok_lemma:<12}{tok.pos_:<8}{tok.tag_:<8}{tok.dep_:<8}   {spacy.explain(tok.dep_.lower())}')
    
    print()
    
    return None

        
def print_parsed_ents(parsed_doc: 'spacy.tokens.doc.Doc', hi_if_not: set[str] = {'PERSON',}) -> None:
    '''
    Print entities from spaCy-parsed doc. 
    Highlight printouts when entities are not in 'hi_not_in' set
    
    @type     parsed_doc: spaCy.tokens.doc.Doc
    @param    parsed_doc: spaCy-parsed summary or sentence or string.
    
    @ type     hi_if_not: set of strings
    @param     hi_if_not: entity labels, not to highlight when displaying
                          default: {'PERSON',}
     
    @return:   None
    '''
    entities = [(e.text, e.start_char, e.end_char, e.label_) for e in parsed_doc.ents]
    print(Color.BOLD + '- Detection of ents:' + Color.END)
    
    for ent in entities:
        if ent[3] not in hi_if_not:
            print(Color.BOLD + str(ent) + Color.END)
        else:
            print(ent)

    return None


def has_conj(token_lst: List[Union[spacy.tokens.token.Token,str]],
             is_verb_group: bool =False) -> List[List[Union[spacy.tokens.token.Token,str]]]:
    '''
    has_conj():
    - detects conjuncts associated to submitted tokens
    - for each detected conjunct, returns a list of type: Union['spacy.tokens.token.Token',str]
      so output consists of a list of lists, each sublist matching a single conjunct syntactic analysis.
    
    @type        token_lst: List[Union['spacy.tokens.token.Token',str]]
    @param       token_lst: Input list of strings and/or SpaCy tokens whose 'conj' dependencies are detected 
    
    @param   is_verb_group: Input flag to indicate whether detection applies to verb groupings (value: True)
                            or to either subjects or objects groupings (value: False).
                            Default: False
    @type    is_verb_group: bool
    
    @return:  List[List[Union['spacy.tokens.token.Token',str]]]
    '''
    token_conjs = [token_lst]
    
    if not isinstance(token_lst,(list,(spacy.tokens.token.Token or str))):
        return []
    
    tokens = [obj for obj in token_lst if isinstance(obj,spacy.tokens.token.Token)]
    
    if any(len(tok.conjuncts) for tok in tokens):
        sep = '::' if is_verb_group else ';;'
        branching = dict()
        
        for idx, obj in enumerate(token_lst):
            if isinstance(obj,spacy.tokens.token.Token):
                branching[idx] = list(obj.conjuncts)
                    
        for idx in branching:
            token_conjs.extend([[token_lst[:idx].append(conj)] for conj in branching[idx]])
        
    else:
        pass
    
    return token_conjs


def has_cmpd_cpnts(token: 'spacy.tokens.token.Token',
                   called_recursively: bool =False,
                   cmpd_tokens: List['spacy.tokens.token.Token'] = []
                ) -> List[List['spacy.tokens.token.Token']]:
    '''    
    has_cmpd_cpnts():
    - Determines whether token is part of compounded word construct
    - Works for descending and ascending stems, to avoid compound truncation, when input token, 'tok',
      not situated upstream from compound chain.
    - Checks for existence of multiple compound chains, separated by a comma, in particular where 
      tok.head.dep_ == 'appos' yields True
    - Returns compound token(s) in a list of lists, respecting the parsed document's original order.
    
    Input:
    @type                  tok: 'spacy.tokens.token.Token'
    @param                 tok: input token whose properties are explored
    
    @type   called_recursively: boolean value
    @param  called_recursively: values: True, when the function is called recursively, 
                                default: False
    
    @type          cmpd_tokens: list of lists of spacy.tokens.token.Token
    @param         cmpd_tokens: list compound strands
                                default is empty list
    
    Output:
    @type          cmpd_tokens: List[List['spacy.tokens.token.Token']]
    @param         cmpd_tokens: compound token(s)
    '''
    if not called_recursively:
        tok = token
        
        if token.dep_ == 'appos':
            cmpd_tok_cands = [token.head]
            
        while tok.head.dep_ in {'compound', 'poss',} or tok.dep_ in {'poss','nmod',}:
            tok = tok.head
            if tok.dep_ == 'poss':
                tok  = tok.head
                
            token = tok
            if tok.head == tok:
                break
            
        if cmpd_tokens != []:
            cand_cmpd_tokens = [tok for cmpd_stem in cmpd_tokens for tok in cmpd_stem]

        cmpd_tokens = [token,]
        
    token_children = token.children
    try:
        cmpd_tok_cands.extend(list(filter(lambda x: x.dep_ in {'compound','poss','appos','nmod',},token_children)))
    except:
        cmpd_tok_cands = list(filter(lambda x: x.dep_ in {'compound','poss','appos','nmod',},token_children))
    
    while cmpd_tok_cands != []:
        cmpd_tokens.append(cmpd_tok_cands[-1])
        has_cmpd_cpnts(cmpd_tok_cands[-1],
                       called_recursively=True, 
                       cmpd_tokens=cmpd_tokens)
        cmpd_tok_cands.pop()
    
    cmpd_tokens = list(set(cmpd_tokens))
    cmpd_tokens.sort(key = lambda x: x.idx)
    
    return cmpd_tokens


def has_named_ents(doc: str) -> List[Tuple[Tuple[int,int],'spacy.tokens.token.Token',str]]:
    '''    
    has_named_ents()::
    - Determines whether summary contains named entity.ies (NE).
    - Sort tuples by starting position of detected entities.
    
    - Returns list of tuples.  Each tuple contains: 
       - tuple made of NE begin and end positions in 'text'
       - NE string
       - NE type
    
    Input: 
    @type            doc: str    
    @param           doc: Document to parse
    
    Output:
    @return:         nes: Enriched list of NE(s), where each NE tuple is formed as:
                           (span, space_separated_text_string, NE_label)
    '''
    nes = list()
    parsed_doc = nlp(u'%s'%doc)
    
    w_lst = [tok.orth_ for tok in parsed_doc if tok.tag_ not in {'POS',}]
    parsed_doc = nlp(u'%s'%' '.join(w_lst))
    
    ## Eliminate possessives
    labels = set([e.label_ for e in parsed_doc.ents]) 
    for label in labels: 
        rgxs = [re.compile(r"%s"%re.escape(e.orth_)) for e in parsed_doc.ents if label==e.label_]
        matches = [re.finditer(rgx,doc) for rgx in rgxs]
        nes.extend([([m.span() for m in re.finditer(rf'{re.escape(e.orth_)}',doc)], e, e.label_)
                     for e in parsed_doc.ents 
                     if label==e.label_
                   ]
                  )
    
    nes = [(span_tup,span_toks,span_lbl)
           for span_tup_lst,span_toks,span_lbl in nes 
           for span_tup in span_tup_lst]
    sorted(nes, key=lambda x: x[0][0])
    
    ## Eliminate nested NEs
    idx_to_pop = list()
    for tup_idx1 in range(len(nes)):
        for tup_idx2 in range(tup_idx1+1,len(nes)):
            str1_lst = list(nes[tup_idx1][1])
            str2_lst = list(nes[tup_idx2][1])
            if set(str1_lst).intersection(set(str2_lst)):
                if (nes[tup_idx1][0][0] <= nes[tup_idx2][0][0] and 
                    nes[tup_idx2][0][1] <= nes[tup_idx1][0][1]):
                    idx_to_pop.append(tup_idx2)
                elif (nes[tup_idx2][0][0] <=nes[tup_idx1][0][0] and 
                      nes[tup_idx1][0][1] <= nes[tup_idx2][0][1]):
                    idx_to_pop.append(tup_idx1)
                else:
                    pass
    for idx in idx_to_pop:
        nes.pop(idx)
    
    return nes


def coref_resolv(doc: str, _display: bool = False) -> str:
    '''
    coref_resolv()::
    - Resolves detected coreferences by means of `coreferee` for anaphors 
        
    Input:
    @type       doc: str
    @param      doc: Summary to analyze

    @type  _display: bool
    @param _display: spaCy syntactic dependency parsing display flag
                     (default: False)
    Output:
    @return     doc: Summary (str) with resolved coreferences
    '''
    corefs_lst = list()
    length_delta_pos= list()
    
    nes = has_named_ents(doc)
    nes_toks_by_lists = [list(ne[1]) for ne in nes]
    parsed_doc = nlp(u'%s'%doc)
    
    if _display:
        display_dep_graph(parsed_doc)
    
    nchunks = has_nchunks(parsed_doc)
    coref_chains = parsed_doc._.coref_chains
    
    if coref_chains:
        for coref_chain in coref_chains:   
            corefs_lst.append([(parsed_doc[idx],parsed_doc[idx].idx,parsed_doc[idx].idx+len(parsed_doc[idx]))
                               for sublist in list(coref_chain)
                               for idx in sublist
                              ])
        
        for coref_idx, coref in enumerate(corefs_lst):
            refs = [tup for tup in coref if tup[0].pos_ in {'NOUN','PROPN',}]
            coref_toks_nbrs = list()
            
            coref_toks = [tup[0] for tup in coref]
            for tok in coref_toks:
                if (tok.tag_ in {'NNP',} and tok.orth_.lower().endswith('s')):
                    coref_toks_nbrs.append('Plur')
                else:
                    try:
                        coref_toks_nbrs.append(tok.morph.to_dict()["Number"])
                    except KeyError:
                        pass
            
            nbr_coherence = all(nbr == coref_toks_nbrs[0] for nbr in coref_toks_nbrs)
            
            genus_coherence = False
            coref_toks_metamorph = list()
            coref_nes_types = list()
            pron_toks_genus = list()
            for tok in coref_toks:
                for sublist_idx, nes_sublist in enumerate(nes_toks_by_lists):
                    nes_sublist_text = [elt.orth_.lower() for elt in nes_sublist]
                    if tok.orth_.lower() in nes_sublist_text:
                        coref_toks_metamorph.append((tok,nes[sublist_idx][2]))
                        coref_nes_types.append(nes[sublist_idx][2])
                    else:
                        coref_toks_metamorph.append((tok,tok.morph.to_dict()))
                        if (tok.pos_ in {'PRON',} and 
                            ('Number' not in set(tok.morph.to_dict().keys()) or tok.morph.to_dict()['Number'] != 'Plur')
                           ):
                            try:
                                pron_toks_genus.append(tok.morph.to_dict()['Gender'])
                            except KeyError:
                                pass
                        
            if len(coref_nes_types) == 0:
                ne_type_coherence = True
                if (not pron_toks_genus or 
                    all(genus in {'Neut'} for genus in pron_toks_genus) or 
                    all(genus in {'Fem'} for genus in pron_toks_genus) or
                    all(genus in {'Masc'} for genus in pron_toks_genus)):
                    genus_coherence = True
            else:     
                ne_type_coherence = all(ne_type == coref_nes_types[0] for ne_type in coref_nes_types)
                if (ne_type_coherence and 
                    ((coref_nes_types[0] in {'PERSON',} and 
                      all(genus in {'Masc','Fem'} for genus in pron_toks_genus)) or
                     (coref_nes_types[0] not in {'PERSON',} and
                      (not pron_toks_genus or all(genus in {'Neut'} for genus in pron_toks_genus)))
                    )
                   ):
                    genus_coherence = True
            
            most_specific_tok = parsed_doc[coref_chains[coref_idx][coref_chains[coref_idx].most_specific_mention_index][0]]
            
            if (len(refs) == 0 or not nbr_coherence or not ne_type_coherence) or not genus_coherence:
                pass
            else:
                morph_dic = most_specific_tok.morph.to_dict()
                
                refs.sort(key=lambda tup: len(tup[0]), reverse=True)
                if most_specific_tok.pos_ == 'PROPN':
                    coref_root = most_specific_tok
                else:
                    coref_root = refs[0][0]
                
                nc_coref_root = coref_root
                if nchunks:
                    for nchunk in nchunks:
                        if coref_root in set(nchunk[1]):
                            nc_coref_root = nchunk[1]
                            break
                
                for tup in coref:
                    if tup[0].orth_.lower() != coref_root.orth_.lower():
                        if nchunks:
                            for nchunk in nchunks:
                                if (tup[0] in set(nchunk[1]) and
                                    tup[0].tag_ not in {'PRP$',}
                                   ):
                                    nc_tup = nchunk[1]
                                    nc_tup_start = parsed_doc[list(nc_tup)[0].i].idx
                                    nc_tup_end = nc_tup_start + len(nc_tup.orth_)
                                    break
                                else:
                                    nc_tup = tup[0]
                                    nc_tup_start = parsed_doc[nc_tup.i].idx
                                    nc_tup_end = nc_tup_start + len(nc_tup.orth_)
                        else:
                            nc_tup = tup[0]
                            nc_tup_start = tup[1]
                            nc_tup_end = tup[2]
                        
                        length_delta = len(nc_coref_root.orth_) - len(nc_tup.orth_)
                        
                        length_delta_pos.append((nc_tup_start,length_delta))
                        
                        length_delta_pos.sort(key=lambda ldp: ldp[0], reverse=False)    # inefficient
                        
                        pos_adjust = 0
                        for subst_tup in length_delta_pos:
                            if subst_tup[0] < nc_tup_start:
                                pos_adjust += subst_tup[1]
                            else:
                                break
                        
                        doc = doc[:nc_tup_start+pos_adjust] + nc_coref_root.orth_ + doc[nc_tup_end+pos_adjust:]
    
    return doc.lower()


def has_vp(pred: 'spacy.tokens.token.Token') -> List[List[Union['spacy.tokens.token.Token',str]]]:
    '''
    has_vp():
    - Detects verb phrases in textual document.
    - Returns list of lists, each sublist corresponding to one enriched predicate strand 
      Each sub-list of the enriched predicate list consists of the input predicate token, optionally preceded
      by auxiliary(ies), negator(s), and/or followed by ordered combination of particle(s), preposition(s)
      and/or adverbial modifiers. 
        
    Input:
    @type               pred: spacy.tokens.token.Token
    @parameter          pred: Input: spaCy-parsed token to analyze as predicate (verb)
    
    Output:
    @return     enriched_pred: Output: list of lists, populated with sequences of phrasal verb
                               component tokens and strings.
    '''
    pred_children = list(pred.children)
    pred_head = pred.head
    pred_dep = pred.dep_

    attrs = []
    
    enriched_pred = [[pred],]
    
    vpc_cands_dict = {tok: 0 if tok.dep_ in {'attr',} else 1 
                      for tok in pred_children 
                     }
    vpc_cands = [tok for tok, _ in sorted(vpc_cands_dict.items(), key=lambda x: x[1], reverse=False)
                 if tok.dep_ in {'prt','oprd','prep','ccomp','advmod','npadvmod','attr',
                                 'acomp','xcomp','auxpass','aux','dative','neg','agent',
                                 'advcl','nsubj','nsubjpass',}
                ]
    
    prt_lst = list(filter(lambda x: (x.dep_ == 'prt' and
                                     not (pred.lemma_ == 'keep' and x.lemma_ == 'on')
                                    ), vpc_cands)) 
    oprd_lst = list(filter(lambda x: x.dep_ == 'oprd', vpc_cands))
    auxpass_lst = list(filter(lambda x: x.dep_ == 'auxpass', vpc_cands))
    aux_lst = list(filter(lambda x: (x.dep_ in {'aux',} and x.tag_ in {'MD',}
                                    ),vpc_cands))
    prep_lst = list(filter(lambda x: (x.dep_ == 'prep' and
                                      (x.lemma_.lower() not in {'by',} or not auxpass_lst)
                                     ), vpc_cands))
    ccomp_lst = list(filter(lambda x: x.dep_ == 'ccomp', vpc_cands))
    advmod_lst = list(filter(lambda x: (x.dep_ == 'advmod' and
                                        x.text not in {'then',}), vpc_cands))
    attr_lst = list(filter(lambda x: x.dep_ == 'attr', vpc_cands))
    acomp_lst = list(filter(lambda x: x.dep_ == 'acomp', vpc_cands))
    xcomp_lst = list(filter(lambda x: x.dep_ == 'xcomp', vpc_cands))
    dative_lst = list(filter(lambda x: x.dep_ == 'dative', vpc_cands))
    neg_lst = list(filter(lambda x: x.dep_ == 'neg', vpc_cands))
    agent_lst = list(filter(lambda x:(x.dep_ in {'agent',} or 
                                      (x.dep_ in {'prep',} and x.lemma_.lower() in {'by',})
                                     ), vpc_cands))
    npadvmod_lst = list(filter(lambda x: x.dep_ == 'npadvmod', vpc_cands))
    advcl_lst = list(filter(lambda x: (x.dep_ == 'advcl' and
                                       x.tag_ in {'JJ',}
                                      ), vpc_cands))
    subj_lst = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass','csubj','csubjpass',},vpc_cands))
    
    has_auxpass = True if auxpass_lst else False
    pred_has_prep = True if prep_lst else False    
    
    if (not agent_lst and pred.tag_ in {'VBN','VBD',}):
        pred_old = pred        
        pred_desc_has_agent = False
        break_outerloop = False
        
        for p_conj in [pred] + list(pred.conjuncts):
            for tok in list(p_conj.children):
                if tok.text == 'by':
                    pred_desc_has_agent = True
                    pred_desc_agent_prep = tok
                    break_outerloop = True
                    break

            if break_outerloop:
                break
        
        if (pred_dep in {'ROOT',} and has_auxpass):
            for idx, sublist in enumerate(enriched_pred):
                if pred_desc_has_agent:
                    sublist.extend(['::',pred_desc_agent_prep])
                enriched_pred[idx] = sublist

        elif (pred_dep in {'conj',} and not has_auxpass):
            while pred.dep_ not in {'ROOT',}:
                pred = pred.head
                h_auxpass_lst = list(filter(lambda x: x.dep_ == 'auxpass', list(pred.children)))
                pred_pos = pred.pos_

                if h_auxpass_lst:
                    has_auxpass = True
                    for idx, sublist in enumerate(enriched_pred):
                        sublist.insert(find_index(pred_old,sublist),h_auxpass_lst[-1])
                        if pred_desc_has_agent:
                            sublist.extend(['::',pred_desc_agent_prep])
                        enriched_pred[idx] = sublist
                    break

                elif pred_pos in {'AUX',} and pred_old in {'VBN',}:
                    has_auxpass = True
                    for idx, sublist in enumerate(enriched_pred):
                        sublist.insert(find_index(pred_old,sublist),pred)
                        if pred_desc_has_agent:
                            sublist.extend(['::',pred_desc_agent_prep])
                        enriched_pred[idx] = sublist
                    break

                else:
                    continue
                    
        elif (pred_dep not in {'ROOT','conj',} and not has_auxpass):
            if pred_desc_has_agent:
                for idx, sublist in enumerate(enriched_pred):
                    sublist.insert(find_index(pred_old,sublist),'be')
                    sublist.extend(['::',pred_desc_agent_prep])
                enriched_pred[idx] = sublist
        
        else:
            pass
        
        pred = pred_old
        
    for vpc_cand in vpc_cands:
        if vpc_cand in neg_lst:
            neg_lst_set = set(neg_lst).remove(vpc_cand)
            
            if (len(neg_lst) == 1 or 
                not any([abs(token_idx_dic[elt] - token_idx_dic[vpc_cand]) == 1 for elt in neg_lst_set])
               ):
                for idx in range(len(enriched_pred)):
                    _pred_index = find_index(pred,enriched_pred[idx])
                    enriched_pred[idx].insert(_pred_index,vpc_cand)
                    enriched_pred[idx].insert(_pred_index+1,';;')
                
        
        elif vpc_cand in agent_lst:
            if not auxpass_lst:
                pred_old = pred
                has_auxpass = False
                
                while pred.dep_ not in {'ROOT',}:
                    pred = pred.head
                    h_auxpass_lst = list(filter(lambda x: x.dep_ == 'auxpass', list(pred.children)))
                    pred_pos = pred.pos_

                    if h_auxpass_lst:
                        has_auxpass = True
                        for idx, sublist in enumerate(enriched_pred):
                            sublist.insert(find_index(pred_old,sublist),h_auxpass_lst[-1])
                            enriched_pred[idx] = sublist
                        break

                    elif pred_pos in {'AUX',}:
                        has_auxpass = True
                        for idx, sublist in enumerate(enriched_pred):
                            sublist.insert(find_index(pred_old,sublist),pred)
                            enriched_pred[idx] = sublist
                        break

                    else:
                        continue
                
                pred = pred_old
                if not has_auxpass and pred.tag_ in {'VBN',}:
                    for idx, sublist in enumerate(enriched_pred):
                        sublist.insert(find_index(pred_old,sublist),'be')
                        enriched_pred[idx] = sublist
            
            for idx, sublist in enumerate(enriched_pred):
                sublist.extend(['::',agent_lst[0]])
                enriched_pred[idx] = sublist
        
        
        elif vpc_cand in auxpass_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            _enriched_pred_tmp = list()
            for _ in range(len(c_conjs)):
                for sublist in enriched_pred:
                    _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
            enriched_pred = _enriched_pred_tmp
            
            for idx in range(len(enriched_pred)):
                _pred_index = find_index(pred,enriched_pred[idx])
                c_conj = c_conjs[idx%len(c_conjs)]
                enriched_pred[idx].insert(_pred_index,c_conj)
        
        
        elif vpc_cand in aux_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            _enriched_pred_tmp = list()  
            
            for _ in range(len(c_conjs)):
                for sublist in enriched_pred:
                    _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
            enriched_pred = _enriched_pred_tmp
            
            for idx in range(len(enriched_pred)):
                _pred_index = find_index(pred,enriched_pred[idx])
                c_conj = c_conjs[idx%len(c_conjs)]
        
        
        elif vpc_cand in attr_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            _enriched_pred_tmp = list()
            for _ in range(len(c_conjs)):
                for sublist in enriched_pred:
                    _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
            
            enriched_pred = _enriched_pred_tmp
            if (pred.pos_ in {'AUX'} and subj_lst):
                for idx in range(len(enriched_pred)):
                    _pred_idx = find_index(pred,enriched_pred[idx])
                    enriched_pred[idx].insert(_pred_idx+1,c_conjs[idx%len(c_conjs)])

                _enriched_pred_tokens = list()
                for sublist in enriched_pred:
                    _enriched_pred_tokens.extend([[tok for tok in sublist
                                                   if isinstance(tok,spacy.tokens.token.Token)]
                                                 ])
                
                for c_conj in c_conjs:
                    _gc_prep = list(filter(lambda x: x.dep_ in {'prep',},c_conj.children))
                    _gc_amod = list(filter(lambda x: x.dep_ in {'amod',},c_conj.children))
                    this_enriched_pred_cpt_idx = list(filter(lambda idx: c_conj in _enriched_pred_tokens[idx],
                                                             list(range(len(enriched_pred)))
                                                            )
                                                     )
                    for gc in _gc_amod:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        for idx in this_enriched_pred_cpt_idx:
                            for gc_conj in gc_conjs:
                                enriched_pred[idx].extend(['::',gc_conj])
                        
                    for gc in _gc_prep:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        for idx in range(len(enriched_pred)):
                            for gc_conj in gc_conjs:
                                enriched_pred[idx].extend(['::',gc_conj])

            elif (pred.pos_ in {'AUX'} and not subj_lst):
                pass
            
        
        elif vpc_cand in xcomp_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            c_conjs_ext = [tok
                           for c_conj in c_conjs
                           for tok in list(c_conj.children)
                           if (tok.pos_ in {'VERB',} and tok.dep_ in {'advcl',})
                          ]
            c_conjs.extend(c_conjs_ext)
            
            _enriched_pred_tmp = list()
            for _ in range(len(c_conjs)):
                for sublist in enriched_pred:
                    _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
            
            enriched_pred = _enriched_pred_tmp
            
            if (vpc_cand.pos_ in {'VERB',} and
                vpc_cand.tag_ not in {'VB',} and
                not any([(gc.tag_ in {'TO',} and
                          gc.dep_ in {'aux',} and
                          token_idx_dic[gc] < token_idx_dic[vpc_cand])
                         for gc in list(vpc_cand.children)]
                       )
               ):
                
                for idx, this_enriched_pred in enumerate(enriched_pred):
                    pred_idx = find_index(pred, this_enriched_pred)
                    this_enriched_pred.insert(pred_idx+1,c_conjs[idx%len(c_conjs)])
                    enriched_pred[idx] = this_enriched_pred
                
                _enriched_pred_tokens = list()
                for sublist in enriched_pred:
                    _enriched_pred_tokens.extend([[tok for tok in sublist 
                                                   if isinstance(tok,spacy.tokens.token.Token)]
                                                 ])

                for c_conj in c_conjs:
                    this_enriched_pred_cpt_idx = list(filter(lambda idx: c_conj in _enriched_pred_tokens[idx],
                                                             list(range(len(enriched_pred)))
                                                            )
                                                     )
                    gc_prep_advmod_acomp = list(filter(lambda x: x.dep_ in {'prep','advmod','acomp',},c_conj.children))
                    
                    for gc in gc_prep_advmod_acomp:
                        gc_conjs = [gc] + list(gc.conjuncts)
                    
                        for idx in this_enriched_pred_cpt_idx:
                            for gc_conj in gc_conjs:
                                _ggc_advmod = list(filter(lambda x: x.dep_ in {'advmod',},gc_conj.children))
                                if _ggc_advmod:
                                    _gc_conj_dic_lst = [{gc_conj:token_idx_dic[gc_conj],ggc:token_idx_dic[ggc]} 
                                                        for ggc in _ggc_advmod]
                                    _gc_conj = [[tok 
                                                 for tok,_ in sorted(_gc_conj_dic.items(), key=lambda x: x[1], reverse=False)]
                                                for _gc_conj_dic in _gc_conj_dic_lst
                                               ]
                                    for _sublist_gc_conj in _gc_conj:
                                        enriched_pred[idx].extend(['::']+_sublist_gc_conj)
                        
                                else:
                                    if gc_conj.dep_ in {'conj',}:
                                        tok = gc_conj
                                        while not _ggc_advmod and tok.dep_ in {'conj',}:
                                            tok = tok.head
                                            _ggc_advmod = list(filter(lambda x: x.dep_ in {'advmod',},tok.children))
                                            if _ggc_advmod:
                                                _gc_conj_dic_lst = [{gc_conj:token_idx_dic[gc_conj],ggc:token_idx_dic[ggc]} 
                                                                    for ggc in _ggc_advmod]
                                                _gc_conj = [[token 
                                                             for token,_ in sorted(_gc_conj_dic.items(), key=lambda x: x[1], reverse=False)]
                                                            for _gc_conj_dic in _gc_conj_dic_lst
                                                           ]
                                                for _sublist_gc_conj in _gc_conj:
                                                    enriched_pred[idx].extend(['::']+_sublist_gc_conj)
                                                break
                                    else:
                                        enriched_pred[idx].extend(['::',gc_conj])
                        
                    
                    gc_prt = filter(lambda x: x.dep_ in {'prt',},c_conj.children)
                    for gc in gc_prt:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        
                        for idx in this_enriched_pred_cpt_idx:
                            this_enriched_pred = enriched_pred[idx]
                            _pred_index = find_index(vpc_cand,this_enriched_pred)

                            for idx2, gc_conj in enumerate(gc_conjs):
                                if idx2 == 0:
                                    enriched_pred[idx] = (this_enriched_pred[:_pred_index+1] +
                                                           [gc_conj] + 
                                                           this_enriched_pred[_pred_index+1:]
                                                          )
                                else:

                                    enriched_pred.extend([this_enriched_pred[:_pred_index+1+idx2] +
                                                          [gc_conj] + 
                                                          this_enriched_pred[_pred_index+1+idx2:]]   # changed from 'this_enriched_pred[_pred_index+2:]'
                                                        )
                    
                    
                    if (not gc_prep_advmod_acomp and not gc_prt):
                        pass
                
        
        elif vpc_cand in dative_lst:    
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)   # identify conjuncts
            for idx in range(len(enriched_pred)):
                for c_conj in c_conjs:
                    enriched_pred[idx].extend(['::',c_conj])
                
        
        elif vpc_cand in prep_lst:
            pred_has_auxpass = True if auxpass_lst else False
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            pobj_nephews = [n for c_conj in c_conjs
                            for n in list(c_conj.children)
                            if n.dep_ in {'pobj','pcomp','amod',}]
            pobj_nephews.extend([tok for pobj in pobj_nephews for tok in list(pobj.conjuncts)])
            prep_nephews = [n for c_conj in c_conjs 
                            for n in list(c_conj.children)
                            if n.dep_ in {'prep',}]
            
            for prep in prep_nephews:
                prep_nephews.extend([tok for tok in list(prep.conjuncts)])
            
            pobj_grandnephews = list()
            
            for nephew in prep_nephews:
                pobj_grandnephews.extend([n for n in list(nephew.children) 
                                          if n.dep_ in {'pobj','pcomp',}])
            
            prep_has_pobj = True if pobj_nephews + pobj_grandnephews else False
            
            for c_conj in c_conjs:
                if prep_has_pobj or parsed_doc[token_idx_dic[c_conj]+1].text in {',',}:
                    for idx in range(len(enriched_pred)):
                        enriched_pred[idx].extend(['::',c_conj])
                else:
                    for idx in range(len(enriched_pred)):
                        pred_idx = find_index(pred,enriched_pred[idx])
                        enriched_pred[idx].insert(pred_idx+1,c_conj)
                        
                grandchildren = list(c_conj.children)
                _gc_prep = list(filter(lambda x: (x.dep_ == 'prep'),grandchildren))
                if _gc_prep:
                    for gc in _gc_prep:
                        for idx in range(len(enriched_pred)):
                            enriched_pred[idx].extend([gc])
        
        
        elif vpc_cand in npadvmod_lst:
            npadvmod_dic = {vpc_cand:token_idx_dic[vpc_cand]}
            npadvmod_dic.update({tok:token_idx_dic[tok] for tok in list(vpc_cand.children)})
            npadvmod = [tok for tok, _ in sorted(npadvmod_dic.items(),key=lambda x: x[1], reverse=False)]
                  
            for idx in range(len(enriched_pred)):
                enriched_pred[idx].extend(['::']+npadvmod)


        elif vpc_cand in advcl_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            
            for c_conj in c_conjs:
                for idx in range(len(enriched_pred)):
                    enriched_pred[idx].extend(['::',c_conj])
                    
        
        elif vpc_cand in advmod_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            c_conjs_dic = dict()
            for c_conj in c_conjs:
                c_conjs_dic.update({c_conj:(token_idx_dic[c_conj],any(map(lambda x: (x.dep_ in {'prep',} and
                                                                                     x.text in {'than',}),
                                                                          c_conj.children)))})
            c_conjs_enriched = [(k, v) for k, v in sorted(c_conjs_dic.items(), key=lambda x: x[1][0], reverse=False)]
            c_conjs_has_than_gc = any([c_conjs_dic[c_conj][1] for c_conj in c_conjs])
                
            for c_conj in c_conjs:    
                _gc_prep = list(filter(lambda x: (x.dep_ == 'prep'), c_conj.children))
                _gc_advmod = list(filter(lambda x: (x.dep_ in {'advmod',} and
                                                    x.orth_.lower() in {'less','more',}
                                                   ),c_conj.children ))
                if _gc_prep:
                    for gc in _gc_prep:
                        for idx in range(len(enriched_pred)):
                            if gc.text in {'than','to',}:
                                enriched_pred[idx].extend(['::',c_conj,gc])
                            else:
                                enriched_pred[idx].extend(['::',c_conj,'::',gc])
                
                else:
                    if c_conjs_has_than_gc:
                        try:
                            rhs_conjs_w_than_idx = [token_idx_dic[tok] for tok, v in c_conjs_enriched if v[1]].remove(token_idx_dic[c_conj])
                        except:
                            rhs_conjs_w_than_idx = [token_idx_dic[tok] for tok, v in c_conjs_enriched if v[1]]
                        
                        rhs_conj_w_than_idx = min(rhs_conjs_w_than_idx)
                        
                        if token_idx_dic[c_conj] < rhs_conj_w_than_idx:
                            rhs_conj_w_than = parsed_doc[rhs_conj_w_than_idx]
                            rhs_than_tok = [tok for tok in rhs_conj_w_than.children 
                                            if (tok.dep_ in {'prep',} and tok.text == 'than')
                                           ][0]
                            for idx in range(len(enriched_pred)):
                                if (c_conj.tag_ in {'JJR','RBR',} or _gc_advmod):
                                    enriched_pred[idx].extend(['::',c_conj, rhs_than_tok])
                                else:
                                    enriched_pred[idx].extend(['::',c_conj])

                        else:
                            for idx in range(len(enriched_pred)):
                                enriched_pred[idx].extend(['::',c_conj])
                    else:
                        for idx in range(len(enriched_pred)):
                            enriched_pred[idx].extend(['::',c_conj])
                                
                _enriched_pred_tokens = list()
                for sublist in enriched_pred:
                    _enriched_pred_tokens.extend([[tok for tok in sublist if isinstance(tok,spacy.tokens.token.Token)]])

                this_enriched_pred_cpt_idx = list(filter(lambda idx: c_conj in _enriched_pred_tokens[idx],
                                                         list(range(len(enriched_pred)))
                                                        )
                                                 )
                
                if _gc_advmod:
                    for idx in this_enriched_pred_cpt_idx:
                        insert_idx = find_index(c_conj,enriched_pred[idx])
                        enriched_pred[idx].insert(insert_idx,_gc_advmod[0])   
                else:
                    if c_conj.dep_ in {'conj',}:
                        tok = c_conj
                        while not _gc_advmod and tok.dep_ in {'conj',}:
                            tok = tok.head
                            _gc_advmod = list(filter(lambda x: (x.dep_ in {'advmod',} and x.text in {'less','more',}),tok.children))
                            if _gc_advmod:    
                                for idx in this_enriched_pred_cpt_idx:
                                    insert_idx = find_index(c_conj,enriched_pred[idx])
                                    enriched_pred[idx].insert(insert_idx,_gc_advmod[0])
                                
                                break
            
        
        elif vpc_cand in prt_lst:
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            _enriched_pred_tmp = list()
            for _ in range(len(c_conjs)):
                for sublist in enriched_pred:
                    _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]

            enriched_pred = _enriched_pred_tmp

            for idx, this_enriched_pred in enumerate(enriched_pred):
                _pred_index = find_index(pred,this_enriched_pred)
                this_enriched_pred.insert(_pred_index+1,c_conjs[idx%len(c_conjs)])
                enriched_pred[idx] = this_enriched_pred
            
        
        elif vpc_cand in acomp_lst:
            _enriched_pred_tokens = list()
            prep_sublist = dict()
            this_enriched_pred_cpt_idx = list()
            
            c_conjs = [vpc_cand] + list(vpc_cand.conjuncts)
            c_conjs_idx = [token_idx_dic[c_conj] for c_conj in c_conjs]
            if attr_lst:
                enriched_pred_old = enriched_pred
                enriched_pred_orig = [[pred]]
                _enriched_pred_tmp = list()
                for _ in range(len(c_conjs)):
                    for sublist in enriched_pred_orig:
                        _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
                
                enriched_pred = _enriched_pred_tmp
            
            else:
                _enriched_pred_tmp = list()
                for _ in range(len(c_conjs)):
                    for sublist in enriched_pred:
                        _enriched_pred_tmp = _enriched_pred_tmp + [[x for x in sublist]]
                
                enriched_pred = _enriched_pred_tmp
                
            enriched_pred_len = len(enriched_pred)
            
            for idx, this_enriched_pred in enumerate(enriched_pred):
                _pred_index = find_index(pred,this_enriched_pred)
                if pred.pos_ in {'AUX'}:
                    this_enriched_pred.insert(_pred_index+1,c_conjs[idx%len(c_conjs)])
                else:
                    this_enriched_pred.insert(_pred_index+1,'::')
                    this_enriched_pred.insert(_pred_index+2,c_conjs[idx%len(c_conjs)])
                
                enriched_pred[idx] = this_enriched_pred
            
            for idx, sublist in enumerate(enriched_pred):
                prep_sublist[idx] = []
                _enriched_pred_tokens.extend([[tok for tok in sublist
                                               if isinstance(tok,spacy.tokens.token.Token)]
                                             ])
            
            for c_conj in c_conjs[::-1]:
                this_enriched_pred_cpt_idx = list(filter(lambda x: c_conj in _enriched_pred_tokens[x],
                                                         list(range(len(enriched_pred)))
                                                        ))
            
                _gc_advmod = list(filter(lambda x: (x.dep_ == 'advmod'), c_conj.children))
                for gc in _gc_advmod:
                    gc_conjs = [gc] + list(gc.conjuncts)
                    for idx in this_enriched_pred_cpt_idx:
                        for gc_conj in gc_conjs:
                            enriched_pred[idx].extend(['::',gc_conj])
            
            _gc_preps = [tok for c_conj in c_conjs 
                        for tok in list(c_conj.children)
                        if tok.dep_ in {'prep',}]
            _gc_preps_ext = [gc for gc_prep in _gc_preps for gc in list(gc_prep.conjuncts)]
            _gc_preps += _gc_preps_ext
            
            if len(_enriched_pred_tokens) > 1:
                for idx in list(range(len(_enriched_pred_tokens)))[:0:-1]:
                    for gc_prep in _gc_preps:
                        if (token_idx_dic[_enriched_pred_tokens[idx-1][1]] < token_idx_dic[gc_prep] and
                            token_idx_dic[gc_prep] < token_idx_dic[_enriched_pred_tokens[idx][1]]
                           ):
                            prep_sublist[idx-1].append(gc_prep)
                            continue
                        elif token_idx_dic[_enriched_pred_tokens[-1][1]] < token_idx_dic[gc_prep]:
                            prep_sublist[len(enriched_pred)-1].append(gc_prep)
                            continue
                        else:
                            pass
                        
            elif len(_enriched_pred_tokens) == 1:
                prep_sublist[0].extend(_gc_preps)
            
            else:
                pass
            
            prep_sublist_keys = list(prep_sublist.keys())
            
            for idx, prep_key in enumerate(prep_sublist_keys[::-1]):
                enriched_pred_idx = enriched_pred_len -idx -1
                
                if len(prep_sublist[prep_key]) >= 1:
                    _this_prep_sublist = prep_sublist[prep_key]
                    _this_enriched_pred = [[x for x in enriched_pred[enriched_pred_idx]+['::',prep_tok]]
                                           for prep_tok in _this_prep_sublist
                                          ]
                    enriched_pred = enriched_pred[:enriched_pred_idx] + _this_enriched_pred + enriched_pred[enriched_pred_idx+1:]
                
                elif (len(prep_sublist[prep_key]) == 0 and idx > 0):
                    prep_sublist[prep_key] = prep_sublist[enriched_pred_len -idx]
                    
                    if prep_sublist[prep_key]:
                        _this_prep_sublist = prep_sublist[prep_key]
                        _this_enriched_pred = [[x for x in enriched_pred[enriched_pred_idx]+['::',prep_tok]]
                                               for prep_tok in _this_prep_sublist
                                              ]
                        enriched_pred = enriched_pred[:enriched_pred_idx] + _this_enriched_pred + enriched_pred[enriched_pred_idx+1:]
                    
                else: 
                    pass
            
            if attr_lst:
                enriched_pred += enriched_pred_old
        
        else:
            pass
        
    return enriched_pred


def has_subjs(pred: spacy.tokens.token.Token, 
              pred_subjs: List[spacy.tokens.token.Token] = []
             ) -> List[List[Union[spacy.tokens.token.Token, str]]]:
    '''
    has_subjs():
    - Determines whether a given SpaCy-parsed predicate has subject(s) in SpaCy-parsed English sentence.
    - Returns list of lists containing subject(s) as SpaCy token(s) and suffix-indicators of type 'str' ('::').
      If no subject is found, returns empty list.

    Inputs:
        @type               pred: spacy.tokens.token.Token  
        @param              pred: Input token (predicate) for which objects are sought.

        @type         pred_subjs: List[spacy.tokens.token.Token]
        @param        pred_subjs: User list of already known subjects provided as SpaCy tokens
                                  Default: []

    Output:
        @return   enriched_subjs: List of lists, populated with sequences of subjects component tokens and strings.
                                   Components may include preposition, particles or adjectival modifiers, other 
                                   modifiers, etc.
                                   List will generally consist of noun tokens, optionally 
                                   - preceded by particles or prepositions if any, separated from the noun by the
                                   noun phrase attribute (NPA) separator ';;', 
                                   - followed by  modifiers if any, separated from the noun by the noun phrase
                                   attribute (NPA) separator '::'. 
    '''
    try:
        pred_children = list(pred.children)
        pred_head = pred.head
        pred_dep = pred.dep_
        enriched_subjs = [[subj] for subj in pred_subjs]
    except:
        print(f'Input variable type error.')
        print(f'Function \'has_subjs()\' expects input variables of types:')
        print(f'        pred: \'spacy.tokens.token.Token\'\n   pred_subjs: List[\'spacy.tokens.token.Token\']')
        print(f'Execution skipped for input variables \'{pred}\' and \'{pred_subjs}\'.')
        return list()
    
    grandchildren_dep = list()
    siblings_dep = list()
    agent_lst = list(filter(lambda x: (x.dep_ in {'agent',} or
                                       (x.dep_ in {'prep',} and 
                                        x.lemma_.lower() in {'by',})
                                      ), pred_children))
    subj_lst = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass','csubj','csubjpass',},pred_children))
    attr_lst = list(filter(lambda x: x.dep_ in {'attr',},pred_children))
    
    if pred.head not in {'ROOT',}:
        siblings_tok = [tok for tok in list(pred_head.children) if tok != pred]
        
        if (pred_dep in {'advcl', 'conj',} and pred.tag_ in {'VBG',} and not subj_lst):
            init_pred = pred
            tok = pred
            
            while (tok.pos_ in {'VERB',} and
                   not list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass',},tok.children))
                  ):
                tok_subjs = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass',},tok.head.children))
                
                if tok_subjs:
                    subj_conjs = tok_subjs + [subj for tok in tok_subjs for subj in list(tok.conjuncts)]
                    enriched_subjs.extend([[subj] for subj in subj_conjs])
                    break
                
                elif tok == tok.head:
                    break
                
                else:
                    tok = tok.head
                    
        elif (pred_dep in {'relcl',} and 
              pred_head.pos_ in {'NOUN','PRON','PROPN',} and
              list(filter(lambda x: (x.dep_ in {'nsubj','nsubjpass',} and
                                     x.tag_ in {'WP','WP$','WRB','WDT',}
                                    ),pred_children
                         )
                  )
             ):
        
            subj_conjs = [pred_head] + list(pred_head.conjuncts)
            enriched_subjs.extend([[subj] for subj in subj_conjs])
            
        else:
            pass
    else:
        pass
    
    for child in pred_children:
        grandchildren = list(child.children)
        
        if (child.dep_ in {'nsubj','nsubjpass',} and 
            (pred.dep_ not in {'relcl',} or pred.pos_ not in {'AUX',}) and
            (pred.dep_ not in {'relcl',} or 
             pred_head.pos_ not in {'NOUN','PRON','PROPN',} or 
             child.tag_ not in {'WP','WP$','WRB','WDT',}
            )
           ):
            _gc_prep = list(filter(lambda gc: gc.dep_ in {'prep',},grandchildren))
            c_conjs = [child] + list(child.conjuncts)
            enriched_subjs.extend([[c_conj] for c_conj in c_conjs])
        
        elif (child.dep_ in {'nsubj','nsubjpass',} and
              pred.dep_ in {'relcl',} and 
              pred.pos_ in {'AUX',} 
             ):
            h_conjs = [pred.head] + list(pred.head.conjuncts)
            enriched_subjs.extend([[h_conj] for h_conj in h_conjs])
        
        elif (child.dep_ in {'attr',} and not subj_lst):
            c_conjs = [child] + list(child.conjuncts)
            enriched_subjs.extend([[c_conj] for c_conj in c_conjs])
        
        elif child.dep_ in {'csubj','csubjpass',}:
            enriched_subjs.extend([['+'+child.dep_+'+',';;',child]])
            enriched_subjs.extend([['+'+tok.dep_+'+',';;',tok] for tok in list(child.conjuncts)])
        
        elif (child.dep_ in {'nsubj',} and pred.dep_ in {'ROOT',} and pred.pos_ in {'AUX',}):
            c_conjs = [child] + list(child.conjuncts)
            enriched_subjs.extend([[c_conj] for c_conj in c_conjs])
        
        elif (child.dep_ in {'dobj',} and
              pred.dep_ in  {'ROOT',} and
              not subj_lst and
              pred.tag_ in {'VBG',}
             ):
            c_conjs = [child] +  list(child.conjuncts)
            enriched_subjs.extend([[child] for child in c_conjs])
        
        else:
            pass
    
    if (pred.dep_ in {'amod',} and pred.tag_ == 'VBG' and pred.head.pos_ == 'NOUN'):
        p_conjs = [pred.head] + list(pred.head.conjuncts)
        enriched_subjs.extend([[p_conj] for p_conj in p_conjs])
    
    elif pred_dep in {'ccomp',} and pred_head.pos_ == 'VERB':
        siblings = list(filter(lambda tok: tok.dep_ in {'dobj', 'iobj', 'pobj','obj',},siblings_tok))
        enriched_subjs.extend([[tok] for tok in siblings])
        
        siblings = filter(lambda tok: tok.dep_ in {'ccomp',},siblings_tok)
        if siblings:
            nephews = [nephew for sibling in siblings 
                       for nephew in list(sibling.children)
                       if nephew.dep_ in {'dobj','iobj','obj'}]
            enriched_subjs.extend([[tok] for tok in nephews])
    
    elif pred_dep in {'acl',} and pred_head.pos_ == 'NOUN':
        s_conjs = [pred_head] + list(pred_head.conjuncts)
        enriched_subjs.extend([[s_conj] for s_conj in s_conjs])
    
    elif pred_dep in {'advcl',} and pred_head.tag_ in {'VBG',}:
        s_conjs = list()
        siblings = list(filter(lambda tok: tok.dep_ in {'dobj',},siblings_tok))
        for sibling in siblings:
            s_conjs.extend([sibling]+list(sibling.conjuncts))

        enriched_subjs.extend([[child] for child in s_conjs])
    
    elif (pred.dep_ not in {'conj','pcomp',} and
          pred.tag_ in {'VBG',} and
          list(map(lambda x: x.dep_ in {'dobj',},pred_children)) and 
          not list(map(lambda x: x.dep_ in {'nsubj',},pred_children))
         ):
        c_conjs = list()
        c_objs = [child for child in pred_children if child.dep_ in {'dobj',}]
        for c_obj in c_objs:
            c_conjs.extend(list(c_obj.conjuncts)+[c_obj])

        enriched_subjs.extend([[child] for child in c_conjs])
    
    elif (pred.dep_ in {'conj','pcomp',} and pred.tag_ in {'VBG',}):
        
        c_dobj_lst = list(filter(lambda x: x.dep_ in {'dobj',},pred_children))
        c_prep_lst = list(filter(lambda x: x.dep_ in {'prep',},pred_children))
        c_nsubj_lst = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass',},pred_children))
        
        pred_ancestors = [tok for tok in list(pred.ancestors) if tok.pos_ in {'VERB',}]
        p_pcomp_lst = list(filter(lambda x: x.dep_ in {'pcomp',}, [pred] + pred_ancestors))
        
        if p_pcomp_lst:
            if pred.dep_ in {'pcomp',}:
                if not c_nsubj_lst:
                    for p_pred in pred_ancestors:
                        c_nsubj_lst = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass',},p_pred.children))
                        if c_nsubj_lst:
                            break

                c_conjs = c_nsubj_lst + [c_conj
                                         for c_nsubj in c_nsubj_lst 
                                         for c_conj in list(c_nsubj.conjuncts)
                                        ]

                enriched_subjs.extend([[child] for child in c_conjs])

            else:
                
                if c_nsubj_lst:
                    c_conjs = c_nsubj_lst + [c_conj
                                         for c_nsubj in c_nsubj_lst 
                                         for c_conj in list(c_nsubj.conjuncts)
                                        ]
                    
                    enriched_subjs.extend([[child] for child in c_conjs])
                    
                else:
                    for tok in pred_ancestors:
                        c_nsubj_lst = list(filter(lambda x: x.dep_ in {'nsubj','nsubjpass',}, list(tok.children)))
                        if c_nsubj_lst:
                            c_conjs = c_nsubj_lst + [c_conj
                                                     for c_nsubj in c_nsubj_lst 
                                                     for c_conj in list(c_nsubj.conjuncts)
                                                    ]
                            enriched_subjs.extend([[child] for child in c_conjs])
                            break
        
        else:
            pass

    return enriched_subjs


def has_objs(pred: spacy.tokens.token.Token, 
             enriched_objs: List[spacy.tokens.token.Token] = []
            ) -> List[List[Union[spacy.tokens.token.Token,str]]]:
    '''    
    has_objs():
    - Determines whether a given SpaCy-parsed predicate has object(s)in SpaCy-parsed English sentence.
    - Returns list of lists containing object(s) as SpaCy token(s) and prefix-indicators of type 'str' (';;').
      If no object is found, returns empty list.

    Input:
        @type            pred: spacy.tokens.token.Token    
        @param           pred: Input token (verb) for which objects are sought.
        
        @type   enriched_objs: List[spacy.tokens.token.Token]
        @param  enriched_objs: User list of already known objects provided as SpaCy tokens.
                               Default: [].
    Output:
        @return enriched_objs: Output of type: List[List[Union[spacy.tokens.token.Token,str]]]
                               List of SpaCy tokens, that qualify as lexical objects for a given predicate.
    '''
    try:
        pred_children = list(pred.children)
        pred_head = pred.head
        pred_dep = pred.dep_
        enriched_objs = [[obj] for obj in enriched_objs]
    except:
        print(f'Input variable type error.')
        print(f'Function \'has_objs()\' expects input variables of types:')
        print(f'        pred: \'spacy.tokens.token.Token\'\n   enriched_objs: List[\'spacy.tokens.token.Token\']')
        print(f'Execution skipped for input variables {pred} and {enriched_objs}.')
        return list()
    
    pred_has_auxpass = any(map(lambda x: x.dep_ in {'auxpass',},list(pred.children)))                
    prep_lst = list(filter(lambda x: (x.dep_ in {'prep',} and
                                      x.lemma_.lower() not in {'by',}),pred_children))
    nsubjpass_lst = list(filter(lambda x: x.dep_ == 'nsubjpass', pred_children))
    agent_lst = list(filter(lambda x: (x.dep_ in {'agent',} or
                                       (x.dep_ in {'prep',} and x.lemma_.lower() in {'by',})
                                      ), pred_children))
    pred_desc_agents = list()
    if not agent_lst:
        pred_desc_has_agent = False
        break_outerloop = False
        for p_conj in [pred] + list(pred.conjuncts):
            for tok in list(p_conj.children):
                if (tok.text == 'by' and p_conj.tag_ in {'VBN','VBD',}):
                    pred_desc_has_agent = True
                    pred_desc_agent_prep = tok
                    agent_lst = [tok]
                    pred_children.append(tok)
                    break_outerloop = True
                    break
                    
            if break_outerloop:
                break
                    
    for child in pred_children:
        
        if (child.dep_ in {'dobj','iobj','obj','oprd',} and
            (pred.tag_ not in {'VBG',} or pred.dep_ in {'conj','advcl','acl',}) and
            not any(map(lambda x: x.dep_ in {'nsubjpass',},pred_children)) and
            pred.dep_ not in {'relcl'}
           ):
            c_conjs = [child] + list(child.conjuncts)
            enriched_objs.extend([[child] for child in c_conjs])
            
        elif (child.dep_ in {'dobj','iobj','obj','oprd',} and 
              (pred.dep_ not in {'relcl'} or
               any(map(lambda x: (x.tag_ in {'WP',} and x.dep_ in {'nsubj',}),pred_children))
              )
             ):
            c_conjs = [child] + list(child.conjuncts)
            enriched_objs.extend([[child] for child in c_conjs])
            
        elif (child.dep_ in {'dobj','iobj','obj','oprd',} and 
              pred.tag_ in {'VBG',} and
              pred.dep_ in {'pcomp'}
             ):
            c_conjs = [child] + list(child.conjuncts)
            enriched_objs.extend([[child] for child in c_conjs])
        
        elif child in agent_lst:
            c_conjs = list()
            nsubjpass_conjs = list()
            _gc_cc = list()
            
            for c in agent_lst:
                c_conjs.extend([c] + list(c.conjuncts))
            
            for c_conj in c_conjs:
                _gc_cc.extend(list(filter(lambda x: x.dep_ in {'cc',},list(c_conj.children))))
            
            c_conjs.extend([tok
                            for gc_cc in _gc_cc 
                            for tok in list(gc_cc.children) 
                            if tok.dep_ in {'agent',}])
            gc_pobj = [gc 
                       for c_conj in c_conjs 
                       for gc in list(c_conj.children)
                       if gc.dep_ in {'pobj',}]
            gc_pobj.extend([gc for tok in gc_pobj for gc in list(tok.conjuncts)])
            enriched_objs.extend([[child,';;',pobj] for pobj in gc_pobj])
            
        elif (child.dep_ in {'npadvmod',} and
              pred.tag_ in {'VBG',} and 
              not any(map(lambda x: x.dep_ in {'nsubj', 'nsubjpass',},pred_children))
             ):
            c_conjs = [child] + list(child.conjuncts)
            enriched_objs.extend([[child] for child in c_conjs])

        elif child.dep_ in {'advmod',}:
            c_conjs = [child] + list(child.conjuncts)
            c_conjs_dic = dict()
            c_conjs_dic.update({c_conj:(token_idx_dic[c_conj],
                                        any(map(lambda x: (x.dep_ in {'prep',} and x.text in {'than',}),c_conj.children))
                                       ) for c_conj in c_conjs})
            c_conjs_ordered = [(k, v)
                               for k, v in sorted(c_conjs_dic.items(),key=lambda x: x[1][0],reverse=False)
                               ]
            c_conjs_has_than_gc = any([c_conjs_dic[c_conj][1] for c_conj in c_conjs])
            
            for c_conj in c_conjs:
                grandchildren = list(c_conj.children)
                _gc_advmod = list(filter(lambda x: (x.dep_ in {'advmod',} and
                                                    x.orth_.lower() in {'less','more',}
                                                   ),grandchildren))
                _gc_prep = list(filter(lambda x: x.dep_ in {'prep',},grandchildren))
                _gc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},grandchildren))
                
                if _gc_prep:
                
                    for gc in _gc_prep:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        for gc_conj in gc_conjs:
                            greatgrandchildren = list(gc_conj.children)
                            _ggc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},greatgrandchildren))

                            for ggc in _ggc_pobj:
                                ggc_conjs = [ggc] + list(ggc.conjuncts)
                                for ggc_conj in ggc_conjs:
                                    enriched_objs.extend([[c_conj,gc_conj,';;',ggc_conj]])
                
                else:
                
                    if c_conjs_has_than_gc:
                        try:
                            rhs_conjs_w_than_idx = [token_idx_dic[tok] for tok,v in c_conjs_ordered if v[1]].remove(token_idx_dic[c_conj])
                        except:
                            rhs_conjs_w_than_idx = [token_idx_dic[tok] for tok,v in c_conjs_ordered if v[1]]
                        
                        rhs_conj_w_than_idx = min(rhs_conjs_w_than_idx)
                        
                        rhs_conj_w_than = parsed_doc[rhs_conj_w_than_idx]
                        grandchildren = list(rhs_conj_w_than.children)
                        _gc_prep = list(filter(lambda x: x.dep_ in {'prep',},grandchildren))
                        _gc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},grandchildren))
                        
                        for gc in _gc_prep:
                            gc_conjs = [gc] + list(gc.conjuncts)
                            for gc_conj in gc_conjs:
                                greatgrandchildren = list(gc_conj.children)
                                _ggc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},greatgrandchildren))
                                
                                if token_idx_dic[c_conj] < rhs_conj_w_than_idx:
                                    rhs_than_tok = [tok for tok in rhs_conj_w_than.children 
                                                    if (tok.dep_ in {'prep',} and tok.text == 'than')
                                                   ][0]
                                    if (c_conj.tag_ in {'JJR','RBR',} or _gc_advmod):
                                        for ggc in _ggc_pobj:
                                            ggc_conjs = [ggc] + list(ggc.conjuncts)
                                            enriched_objs.extend([[c_conj,rhs_than_tok,';;',ggc_conj] 
                                                                  for ggc_conj in ggc_conjs]
                                                                )
                
                for gc in _gc_pobj:
                    gc_conjs = [gc] + list(gc.conjuncts)
                    enriched_objs.extend([[c_conj,';;',gc_conj] for gc_conj in gc_conjs])
                  
        elif (child.dep_ in {'ccomp',} and child.tag_ in {'VBG','JJ',}):
            c_conjs = [child] + list(child.conjuncts)
            for c_conj in c_conjs:
                grandchildren = list(c_conj.children)
                
                gc_conjs = list()
                _gc_obj = filter(lambda x: x.dep_ in {'dobj', 'iobj', 'pobj','obj',},grandchildren)
                for gc in _gc_obj:
                    gc_conjs = [gc] + list(gc.conjuncts)
                    enriched_objs.extend([[gc_conj] for gc_conj in gc_conjs])
                
                _gc_subj = filter(lambda x: x.dep_ in {'nsubj','nsubjpass',},grandchildren)
                for gc in _gc_subj:
                    gc_conjs.extend([gc] + list(gc.conjuncts))
                    enriched_objs.extend([[gc_conj] for gc_conj in gc_conjs])
        
        elif (child.dep_ in {'ccomp',} and child.tag_ in {'VB','VBD','VBN','VBP','VBZ',}):
            
            c_conjs = [child] + list(child.conjuncts)
            gc_conjs = list()
            
            for c_conj in c_conjs:
                grandchildren = [tok for tok in list(c_conj.children) if tok.dep_ not in {'mark',}]
                grandchildren += [c_conj]
                
                gc_tok_idx = {gc:token_idx_dic[gc] for gc in grandchildren}
                grandchildren_tok_ordered = [k for k, v in sorted(gc_tok_idx.items(), key = lambda x: x[1])]
                
                enriched_objs.extend([grandchildren_tok_ordered])               
        
        elif child.dep_ in {'acomp',} and child.pos_ in {'ADJ',}:  
            c_conjs = [child] + list(child.conjuncts)
            
            for c_conj in c_conjs:
                grandchildren = list(c_conj.children)
                if grandchildren:
                    _gc_prep = list(filter(lambda x: (x.dep_ == 'prep' and
                                                      token_idx_dic[x] >= token_idx_dic[child]+1),
                                           grandchildren))
                    if _gc_prep:
                        for gc in _gc_prep:
                            gc_conjs = [gc] + list(gc.conjuncts)

                        for gc_conj in gc_conjs:
                            greatgrandchildren = list(gc_conj.children)
                            _ggc_pobj = list(filter(lambda x: x.dep_ == 'pobj',greatgrandchildren))
                            for ggc in _ggc_pobj:
                                ggc_conjs = [ggc] + list(ggc.conjuncts)
                                enriched_objs.extend([[gc_conj,';;',ggc] for ggc in ggc_conjs])
                    
                    _gc_advmod = list(filter(lambda x: x.dep_ == 'advmod',grandchildren))
                    if _gc_advmod:
                        pass
                        
                else:
                    pass
        
        elif child.dep_ in {'xcomp',}:
            c_conjs = [child] + list(child.conjuncts)
            
            c_conjs_ext = [tok
                           for c_conj in c_conjs
                           for tok in list(c_conj.children)
                           if (tok.pos_ in {'VERB',} and tok.dep_ in {'advcl',})
                          ]
            
            c_conjs.extend(c_conjs_ext)            
            for c_conj in c_conjs:
                print(f'c_conj: {c_conj}')
                grandchildren = list(c_conj.children)
                gc_select = [gc for gc in grandchildren 
                             if ((gc.tag_ in {'TO',} and gc.dep_ in {'aux',}) or
                                 gc.dep_ in {'neg','advmod','prep','pobj','dobj',} or
                                 gc.tag_ in {'WP'}
                                )
                            ]    
                has_to_aux = True if list(filter(lambda x: (x.tag_ in {'TO',} and x.dep_ in {'aux',}),gc_select)) else False
                
                if (not c_conj.tag_ in {'VB',} and not has_to_aux):
                    _gc_obj = list(filter(lambda x: x.dep_ in {'dobj','iobj',}, grandchildren))
                    for gc in _gc_obj:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        enriched_objs.extend([[gc] for gc in gc_conjs])

                    _gc_prep = list(filter(lambda x: x.dep_ in {'prep',},grandchildren))
                    for gc in _gc_prep:
                        _gc_conjs = [gc] + list(gc.conjuncts)
                        for gc_conj in _gc_conjs:
                            greatgrandchildren = list(gc_conj.children)
                            _ggc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},greatgrandchildren))
                            for ggc in _ggc_pobj:
                                ggc_conjs = [ggc] + list(ggc.conjuncts)
                                enriched_objs.extend([[gc_conj,';;',ggc] for ggc in ggc_conjs])

                    _gc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},grandchildren))
                    for gc in _gc_pobj:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        enriched_objs.extend([[gc] for gc in gc_conjs])
                    
                    _gc_xcomp = list(filter(lambda x: x.dep_ in {'xcomp',},grandchildren))
                    for gc in _gc_xcomp:
                        gc_conjs = [gc] + list(gc.conjuncts)
                        for gc_conj in gc_conjs:
                            _ggc_obj = [gc_conj] + [ggc for ggc in gc_conj.children if ggc.dep_ not in {'conj',}]
                            _ggc_obj.sort(key=lambda ggc: token_idx_dic[ggc], reverse=False)
                            enriched_objs.extend([_ggc_obj])
                
                
                elif (c_conj.tag_ in {'VB',} and has_to_aux):
                    c_conj_tok_idx = token_idx_dic[c_conj]
                    gc_wp_xcomp = {gc for gc in gc_select if gc.tag_ in {'WP',}}
                    gc_to_xcomp = {gc for gc in gc_select if gc.tag_ in {'TO',}}
                    gc_xcomp = gc_wp_xcomp | gc_to_xcomp
                    
                    if (c_conj.dep_ not in {'xcomp',} and 
                        (not gc_wp_xcomp or not gc_to_xcomp)
                       ):
                        gc_select.extend(list(gc_xcomp - set(gc_select)))
                    
                    gc_tok_idx = {gc:token_idx_dic[gc] for gc in gc_select}
                    gc_select = [k for k, v in sorted(gc_tok_idx.items(), key = lambda x: x[1])]
                    
                    if c_conj_tok_idx < token_idx_dic[gc_select[0]]:
                        c_conj_insert_idx = 0
                    elif c_conj_tok_idx > token_idx_dic[gc_select[-1]]:
                        c_conj_insert_idx = len(gc_select)
                    else:
                        for idx in range(len(gc_select)-1):
                            if (token_idx_dic[gc_select[idx]] < c_conj_tok_idx < token_idx_dic[gc_select[idx+1]]):
                                c_conj_insert_idx = idx+1
                    
                    gc_select.insert(c_conj_insert_idx,c_conj)
                    
                    if len(gc_select) > c_conj_insert_idx+1:
                        enriched_objs.append(gc_select[0:c_conj_insert_idx+1]+['+rc'])
                    else:
                        enriched_objs.append(gc_select)
                    
                
                elif (c_conj.tag_ in {'VB',} and not has_to_aux):
                    pass

        elif child in prep_lst:
            if len(prep_lst) > 1:
                corner_cases = list()
                prep_lst.sort(key=lambda x: token_idx_dic[x],reverse=False)
                prep_tokidx_lst = [token_idx_dic[tok] for tok in prep_lst]
                
                for idx in range(len(prep_lst)-1):
                    a = prep_tokidx_lst[idx]
                    b = prep_tokidx_lst[idx+1]
                    if (b-a == 2 and parsed_doc[int((a+b)/2)].text == ','):
                        corner_cases.append((prep_lst[idx],prep_lst[idx+1]))
            
                for cc in corner_cases:
                    if cc[1] in c_conjs:
                        c_conjs.append(cc[0])
            
            c_conjs = [child] + list(child.conjuncts)
            c_conjs_idx = [token_idx_dic[c_conj] for c_conj in c_conjs]
            depseq_lst = [[c_conj,] for c_conj in c_conjs]
            
            for idx, c_sublist in enumerate([[c_conj,] for c_conj in c_conjs]):
                c_conj = c_sublist[0]
                grandchildren = list(c_conj.children)
                
                gcs = [[c_conj, gc] for gc in grandchildren if gc.dep_ in {'pobj','prep','pcomp','amod',}]
                for gc_sublist in [[c_conj, gc] for gc in grandchildren if gc.dep_ in {'pobj','prep','pcomp','amod',}]:
                    gc_conjs = list(gc_sublist[1].conjuncts)
                    gcs.extend([[c_conj, gc_conj] for gc_conj in gc_conjs])
                    
                    no_det_lst = [det 
                                  for sublist in gcs 
                                  for det in sublist[1].children 
                                  if (sublist[1].dep_ in {'pobj','amod',} and
                                      det.dep_ in {'det',} and 
                                      det.text == 'no')]
                
                c_prep_has_gc = True if gcs else False
                
                if c_prep_has_gc:
                    depseq_lst.pop(idx)
                    depseq_lst.extend(gcs)
            
            _depseq_tmp = list()
            for sublist in depseq_lst:
                _depseq_tmp = _depseq_tmp + [[x for x in sublist]]
            
            for idx, sublist in enumerate(_depseq_tmp):
                if len(sublist) > 1:
                    c_conj = sublist[0]
                    gc = sublist[1]
                    if gc.dep_ in {'prep',}:
                        ggcs = [[c_conj, gc, ggc] for ggc in list(gc.children) if ggc.dep_ in {'pobj','pcomp','amod',}]
                        ggcs.extend([[c_conj, gc, gcc_conj] for ggc_sublist in ggcs for gcc_conj in list(ggc_sublist[2].conjuncts)])
                        
                        no_det_lst = [det 
                                      for sublist in ggcs 
                                      for det in sublist[2].children 
                                      if (sublist[2].dep_ in {'pobj','amod',} and
                                          det.dep_ in {'det',} and 
                                          det.text == 'no')
                                     ]
                        
                        gc_prep_has_ggc = True if ggcs else False
                        
                        if gc_prep_has_ggc:
                            depseq_lst.pop(idx)
                            depseq_lst.extend(ggcs)
            
            depseq_tokidx_dic = dict()
            
            for sublist in depseq_lst:
                if len(sublist) == 1:
                    tup_tokidx=(token_idx_dic[sublist[0]],0,0)
                elif len(sublist) == 2:
                    tup_tokidx=(token_idx_dic[sublist[0]],token_idx_dic[sublist[1]],0)
                else:
                    tup_tokidx=(token_idx_dic[sublist[0]],token_idx_dic[sublist[1]],token_idx_dic[sublist[2]])
                
                depseq_tokidx_dic.update({tup_tokidx:sublist})
            
            depseq_lst = [v for k,v in sorted(depseq_tokidx_dic.items(),
                                              key = lambda x: (x[0][0],x[0][1],x[0][2]),
                                              reverse = False
                                             )
                         ]
            
            for idx, sublist in enumerate(depseq_lst[::-1]):
                depseq_idx = len(depseq_lst) -idx -1
                last_cpt = sublist[-1]
                
                if last_cpt.dep_ in {'pobj','amod',}:
                    last_pobj = last_cpt
                    pobj_conjs = [last_pobj] + list(last_pobj.conjuncts)
                    enriched_objs.extend([[x for x in sublist[:-1]] +[';;',pobj_conj] for pobj_conj in pobj_conjs] )
                
                elif last_cpt.dep_ in {'prep',}:
                    if 'last_pobj' in set(dir()):
                        enriched_objs.extend([[x for x in sublist]+[';;',pobj_conj] for pobj_conj in pobj_conjs])
                
                elif last_cpt.dep_ in {'pcomp',}:
                    if 'last_pobj' in set(dir()):
                        if last_pobj:
                            del(last_pobj)
                    if 'pobj_conjs' in set(dir()):
                        if pobj_conjs:
                            del(pobj_conjs)
                    enriched_objs.append([x for x in sublist[:-1]] +[';;',last_cpt])
                    
                else:
                    pass
            
            
        elif child.dep_ in {'dative',}:
            c_conjs = [child] + list(child.conjuncts)
            gc_conjs = list()
            for c_conj in c_conjs:
                grandchildren = list(c_conj.children)
                _gc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},grandchildren))
                for gc in _gc_pobj:
                    gc_conjs = [gc] + list(gc.conjuncts)

                enriched_objs.extend([[c_conj,';;',gc] for gc in gc_conjs])
        
        elif child.dep_ in {'attr',}:
            c_conjs = [child] + list(child.conjuncts)
            for c_conj in c_conjs:
                _gc_prep = list(filter(lambda x: x.dep_ in {'prep',},list(c_conj.children)))
                for gc in _gc_prep:
                    gc_conjs = [gc] + list(gc.conjuncts)
                    for gc_conj in gc_conjs:
                        _ggc_pobj = list(filter(lambda x: x.dep_ in {'pobj',},gc_conj.children))
                        for ggc in _ggc_pobj:
                            ggc_conjs = [ggc] + list(ggc.conjuncts)
                        
                        enriched_objs.extend([[gc_conj,';;',ggc] for ggc in ggc_conjs])
        else:      
            pass
        
    if pred.dep_ in {'relcl',} and pred.head.pos_ in {'NUM',}:
        _siblings_prep = [sibling 
                          for sibling in list(pred.head.children)
                          if (sibling.dep_ == 'prep' and sibling != pred)]

        if _siblings_prep:
            for tok in _siblings_prep:
                siblings_prep = [tok] + list(tok.conjuncts)
                
                for sibling in siblings_prep:
                    nephews_obj = list()
                    _nephews_obj = filter(lambda x: x.dep_ == 'pobj',sibling[1].children)
                    
                    for tok in _nephews_obj:
                        nephews_obj = [tok] + list(tok.conjuncts)
                        enriched_objs.extend([[sibling,';;',nephew] for nephew in nephews_obj])
        else:
            pass
    
    elif (pred.dep_ in {'relcl',} and
          pred.pos_ not in {'AUX',} and
          pred.head.pos_ in {'NOUN','PRON','PROPN',} and
          not any(map(lambda x: (x.tag_ in {'WP','WDT','WP$','WRB',} and
                                 x.dep_ in {'nsubj','nsubjpass',}),pred_children
                     )
                 )
         ):
        h_conjs = [pred.head] + list(pred.head.conjuncts)
        enriched_objs.extend([[h_conj] for h_conj in h_conjs])
    
    elif (pred.dep_ in {'relcl',} and
          pred.pos_ in {'AUX',} and
          pred.head.pos_ in {'NOUN','PRON','PROPN',}
         ):
        pass
    
    else:
        pass
        
    return enriched_objs


def has_cmpd_nes(tok:spacy.tokens.token.Token) -> List[Tuple[str,str,str]]:
    '''    
    has_cmpd_nes():
    - Determines whether:
        - input token is part of any lexical compound(s)
        - named-entities can be detected in compounds
        - the form of the corresponding attribute(s)
      in SpaCy-parsed English sentence.
    - Returns attributes as a list of tuples of 3 strings each: '(token's lemmma, named-entity, attr_type )'.

    Input:
        @type                  tok: spacy.tokens.token.Token
        @param                 tok: Input token whose properties are explored
                                     Default: []

    Output:
        @return:             attrs: List of tuples '(token's lemmma, named-entity, attr_type )'
    '''
    if isinstance(tok,spacy.tokens.token.Token):
        pass
    else:
        return list()
    
    cmpd_strands = list()
    attrs = list()
    idx = 0
    raw_cmpd_toks = has_cmpd_cpnts(tok,
                                   called_recursively=False,
                                   cmpd_tokens=[])
    
    enriched_cmpd_toks = list()
    for cmpd_tok in raw_cmpd_toks:
        #print(f'cmpd_tok: {cmpd_tok}')
        #print(f'token_idx_dic: {token_idx_dic}')
        enriched_cmpd_toks.append((token_idx_dic[cmpd_tok], cmpd_tok))
    
    while enriched_cmpd_toks[:-1] != []:
        rich_cmpd_tok = enriched_cmpd_toks[0]
        
        if idx == 0:
            start_idx = rich_cmpd_tok[0]
            strand = [rich_cmpd_tok[1]]
            enriched_cmpd_toks.pop(0)
        else:
            
            if rich_cmpd_tok[0] - start_idx == idx:
                strand.append(rich_cmpd_tok[1])
                enriched_cmpd_toks.pop(0)
                
                if len(enriched_cmpd_toks) == 1:
                    strand.append(enriched_cmpd_toks[-1][1])
                    cmpd_strands.append(strand)
            else:
                strand.append(enriched_cmpd_toks[-1][1])
                cmpd_strands.append(strand)
                idx = -1
            
        idx += 1
    
    for compound in cmpd_strands:
        compound_str = ' '.join([tok.text for tok in compound])
        ne_cand = has_named_ents(compound_str)
        tok_conjs = [tok] + list(tok.conjuncts)
        
        if len(ne_cand) > 0:
            '''
            NER labels left untreated:
                    LAW : Named documents made into laws.
                ORDINAL : "first", "second", etc.
                PRODUCT : Objects, vehicles, foods, etc. (not services)
            '''
            if ne_cand[0][2] in {'GPE','LOC','FAC'}:
                attrs.extend([(tok_conj.lemma_,ne_cand[0][1].orth_,'ENT:LOC') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'LANGUAGE'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:LANG') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'ORG'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:ORG') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'PERSON'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:PERS') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'MONEY','QUANTITY','PERCENT'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:QUAN') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'DATE','TIME'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:TIME') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'WORK_OF_ART'}:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:WOA') for tok_conj in tok_conjs])
                
            elif ne_cand[0][2] in {'NORP'}:
                # Nationalities or religious or political groups
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:NRPG') for tok_conj in tok_conjs])
                
            else:
                attrs.extend([(tok_conj.lemma_.lower(),ne_cand[0][1].orth_,'ENT:_') for tok_conj in tok_conjs])
        else:
            pass
            
    return attrs


def has_attrs(tok: spacy.tokens.token.Token) -> List[List[Union[spacy.tokens.token.Token,str]]]:
    '''    
    has_attrs():
    - Determine whether a SpaCy token in an English sentence either:
      - has modifier(s), or
      - is the subject of a verb in past participle form, whose function is adjectival, 'JJ' (tok.tag_).
    - Returns atribute(s) triple(s) in list.

    Input:
    @type            tok: spacy.tokens.token.Token   
    @param           tok: Input SpaCy parsed token whose properties are explored
        
    @type          attrs: List[Tuple[spacy.tokens.token.Token]]
    @param         attrs: List of tuples of SpaCy parsed tokens passed to the function
                              Default input value is empty list: '[]'
    Ouput:
    @return:       attrs: List of tuples: (input_token_lemmma, modifying_token,"")
    '''
    
    attrs = list()
    
    if isinstance(tok,spacy.tokens.token.Token):
        pass
    else:
        return attrs
    
    conjs = [tok] + list(tok.conjuncts)
    h_conjs = [tok.head] + list(tok.head.conjuncts)
    
    c_amod_lst = [c for c in tok.children if (c.dep_ in {'amod',} and tok.dep_ not in {'prep',})]
    
    c_acl_lst = [c for c in tok.children if (c.dep_ in {'acl',} and c.tag_ in {'VBZ',})]
    
    c_prep_lst = [c for c in tok.children if c.dep_ in {'prep',}]
    
    if c_amod_lst:
        for c_amod in c_amod_lst:
            c_conjs = [c_amod] + list(c_amod.conjuncts)
            attrs.extend([(tok.lemma_.lower(),c_conj.text.lower(),'') for c_conj in c_conjs])
    elif (not c_amod_lst and 
          not list(filter(lambda x: x.dep_ not in {'det',},list(tok.children)))
         ):
        if tok.dep_ in {'conj',}:
            tok_old = tok
            while tok.dep_ in {'conj',}:
                tok = tok.head
                h_amod_lst = [c for c in tok.children if c.dep_ in {'amod',}]
                c_conjs = [c_conj for h_amod in h_amod_lst for c_conj in [h_amod] + list(h_amod.conjuncts)]
                if h_amod_lst:
                    attrs.extend([(tok.lemma_.lower(),c_conj.text.lower(),'') for c_conj in c_conjs])
                    break
    
    if (tok.head.dep_ in {'ccomp',} and tok.head.tag_ in {'JJ','VBG',} and tok.dep_ in {'nsubj',}):
        attrs.extend([(tok.lemma_.lower(),h_conj.text.lower(),'') for h_conj in h_conjs])    
    
    return attrs


def has_nchunks(parsed_doc: 'spacy.tokens.doc.Doc') -> List[Tuple[Union['spacy.tokens.token.Token',
                                                                        'spacy.tokens.span.Span']]]:
    '''
    has_nchunks():
    - Extracts noun groups out of spaCy-parsed summary.
    
    Input:
    @type        parsed_doc: spacy.tokens.doc.Doc  
    @param       parsed_doc: Input spaCy-parsed summary to analyze
         
    Ouput:
    @return         nchunks: List of tuples of spaCy tokens. Tuples are: (noun_group_root, noun_group)
    '''
    nchunks = [(chunk.root,chunk)
               for chunk in parsed_doc.noun_chunks
               if (chunk.root.tag_ in {'CD','FW','NN','NNP','NNPS','NNS','PRP','PRP$','WDT','WP','WP$','WRB',} and
                   chunk.root.orth_ != chunk.orth_)
              ]
    
    return nchunks


def expd_token_span(nrch: List[Union['spacy.tokens.token.Token',str]],
                    nchunks: List[Tuple[Union['spacy.tokens.token.Token',
                                              'spacy.tokens.span.Span']]]) -> List[Union['spacy.tokens.token.Token',str]]:
    '''
    expd_token_span():
    - Expands noun-groups and clausal span anchor based on occurrence of 
        - token previously identified as noun-group's root per the passed variable `nchunks`
        - strings '+csubj+', '+csubjpass+', (etc), preceding the anchor token by two positions,
          if `anchor.pos_ in {'VERB','AUX',}`. In practice, the sought pattern is: 
                 nrch = [..., '<string_pattern>', ';;', anchor, ...]  
    - Expansion is carried out using:
        - the noun-group's full span in its entirety for noun-group expansions
        - the `anchor.subtree` when clausal subjects are involved. In that case expansion may include
          neither tokens, `tok`, such that `tok != anchor and tok.pos_ in {'VERB','AUX',}` nor their 
          subtree, unless their subtree length is 4 or less.
                     
    Input:
        @type                  nrch: List[Union['spacy.tokens.token.Token',str]]
        @param                 nrch: enriched subjects or objects token
        
        @type               nchunks: List['Tuple[Union['spacy.tokens.token.Token','spacy.tokens.span.Span']]]
        @param              nchunks: List of previously detected noun-groups tuples, shaped as
                                     (<noun_group_root, <noun_group_span>)
    Ouptut:
        @return            expd_nrch: conditionally expanded enriched subjects or objects token
    '''
    expd_nrch = list()
    nchunks_toks = [chunk[0] for chunk in nchunks]
    
    for cpt in nrch:
        if (isinstance(cpt,spacy.tokens.token.Token) and 
            cpt.tag_ in {'CD','FW','NN','NNP','NNPS','NNS','PRP','PRP$','WDT','WP','WP$','WRB',}
           ):
            try:
                tok_idx = nchunks_toks.index(cpt)
                toks_span = nchunks[tok_idx][1]
                expd_nrch.extend(list(toks_span))
            except ValueError:
                expd_nrch.append(cpt)
                
        else:
            expd_nrch.append(cpt)
            
    nrch = expd_nrch
    
    if len(nrch) >= 3:
        expd_nrch = list()
        cpt_idx = 0
        pattern = re.compile(r"(?ai)^\+csubj\+$|^\+csubjpass\+$")
        
        while cpt_idx < len(nrch)-2:    
            cpt = nrch[cpt_idx]
            anchor = nrch[cpt_idx+2]
            if (isinstance(cpt, str) and 
                isinstance(anchor,spacy.tokens.token.Token) and
                pattern.fullmatch(cpt) and 
                anchor.pos_ in {'VERB','AUX',}
               ):
                _subtree = anchor.subtree
                for tok in anchor.subtree:
                    if (tok.pos_ in {'VERB','AUX',} and
                        tok != anchor and
                        len(tok.subtree) > 5
                        ):
                        _subtree = list(set(_subtree)-set(tok.subtree))
                expd_nrch.extend(_subtree)
                cpt_idx +=3
            
            else:
                expd_nrch.append(cpt)
                cpt_idx +=1
            
        if cpt_idx < len(nrch):
            expd_nrch.extend(nrch[cpt_idx:])
            
    else:
        expd_nrch = nrch
        
    return expd_nrch


def format_svo(nrch: List[Union['spacy.tokens.token.Token',str]],
               nrch_type: str = '',
               **kwargs
              ) -> str:
    '''
    format_svo():
    - Applies selective formatting on "s", "v" and "o" triples.

    Input:
        @type                  nrch: List[Union['spacy.tokens.token.Token',str]]
        @param                 nrch: Input: enriched [s,v,o] list of objects 
                                     for subjects (s), verbs (v) and objects (o) in that order
        
        @type             nrch_type: str
        @param            nrch_type: Input: non-optional input kw argument, permitted values in {"s","v","o"}.
                                     Determines the differential formatting for subject, verb or object
                                     token enrichment as extracted
                                     (Default: '')
        
        @type                kwargs: <varname>=<varvalue>
        @param               kwargs: "pred_with_object as {True, False}"
                                     "formatted" as {True, False},
                                     "nchunks" as List['Tuple[Union['spacy.tokens.token.Token',
                                                                    'spacy.tokens.span.Span']]]
                                     (Defaults: True, True, [])
    Output:                                                                              
        @return  enriched_pred_view: enriched token   
    '''
    
    # kwargs
    if 'pred_with_object' in  kwargs.keys():
        pred_with_object = kwargs['pred_with_object']
    else:
        pred_with_object = True
        
    if 'formatted' in  kwargs.keys():
        formatted = kwargs['formatted']
    else:
        formatted = 'visual'
    
    if 'nchunks' in kwargs.keys():
        nchunks = kwargs['nchunks']
    else:
        nchunks = []
    
    if nrch_type == 'v':
        enriched_pred = nrch
        enriched_pred_view = list()
        verb_flag = 0
        
        for idx, tok in enumerate(enriched_pred):
            
            if not isinstance(tok, str) and tok.pos_ in {'VERB','AUX',}:
                verb_flag += 1
                if (verb_flag == 1 and
                    (tok.tag_ not in {'VBN',} or 
                     any(map(lambda x:x.pos_ in {'AUX',},tok.children)) or
                     tok.dep_ in {'auxpass',}) and
                    (not isinstance(enriched_pred[0],str) or enriched_pred[0] != 'be')
                   ):
                    if formatted in {'visual','graph'}:
                        enriched_pred_view.append(tok.lemma_.lower())
                    else:
                        enriched_pred_view.append(tok.orth_.lower())
                else:
                    if tok.tag_ in {'VBG','VBD', 'VBZ',}:
                        pass
                    enriched_pred_view.append(tok.orth_.lower())

            elif isinstance(tok, str):
                enriched_pred_view.append(tok)
                
            else:
                if (tok.dep_ in {'advmod',} or 
                    (tok.dep_ in {'conj',} and tok.head.dep_ in {'advmod'})):
                    enriched_pred_view.append(tok.orth_.lower())
                elif tok.dep_ in {'acomp',}:
                    enriched_pred_view.append(tok.orth_.lower())
                else:
                    if formatted in {'visual','graph'}:
                        enriched_pred_view.append(tok.lemma_.lower())
                    else:
                        enriched_pred_view.append(tok.orth_.lower())

        enriched_pred_view = " ".join(enriched_pred_view)
        pattern = r'\s+:{2}\s+'
        enriched_pred_view = re.sub(pattern,'::',enriched_pred_view)
        pattern = r'\s+;{2}\s+'
        enriched_pred_view = re.sub(pattern,';;',enriched_pred_view)
        
        if formatted in {'visual','graph'}:
            return "_".join(enriched_pred_view.split(' '))
        else:
            return enriched_pred_view
    
    
    elif nrch_type == 's':
        enriched_subj_view = list()
        pattern = r'\s+;{2}\s+'
        
        if formatted in {'visual','graph'}:
            enriched_subj = nrch
            
            for tok in enriched_subj:
                if isinstance(tok,str):
                    enriched_subj_view.append(tok)
                elif tok.pos_ in {'VERB',}:
                    enriched_subj_view.append(tok.orth_.lower())
                else:
                    enriched_subj_view.append(tok.lemma_.lower())
                
            enriched_subj_view = ' '.join(enriched_subj_view)
            enriched_subj_view = re.sub(pattern,';;',enriched_subj_view)
            return "_".join(enriched_subj_view.split(' '))
        
        else:
            enriched_subj = expd_token_span(nrch, nchunks)

            for tok in enriched_subj:
                if isinstance(tok,str):
                    enriched_subj_view.append(tok)
                else:
                    enriched_subj_view.append(tok.orth_.lower())
            
            enriched_subj_view = ' '.join(enriched_subj_view)
            enriched_subj_view = re.sub(pattern,';;',enriched_subj_view)
            return enriched_subj_view
    
    
    elif nrch_type == 'o':
        enriched_obj_view = list()
        pattern = r'\s+;{2}\s+'
        
        if formatted in {'visual','graph'}:
            enriched_obj = nrch
            
            for tok in enriched_obj:
                if isinstance(tok,str):
                    enriched_obj_view.append(tok)
                elif (tok.dep_ in {'advmod',} or
                      (tok.dep_ in {'conj',} and tok.head.dep_ in {'advmod'}) or
                      tok.tag_ in {'VBN','VBG'}
                     ):
                    enriched_obj_view.append(tok.orth_.lower())
                else:
                    enriched_obj_view.append(tok.lemma_.lower())
            
            enriched_obj_view = ' '.join(enriched_obj_view)
            enriched_obj_view = re.sub(pattern,';;',enriched_obj_view)
            return "_".join(enriched_obj_view.split(' '))
                
        else:
            enriched_obj = expd_token_span(nrch, nchunks)
            
            for tok in enriched_obj:
                if isinstance(tok,str):
                    enriched_obj_view.append(tok)
                else:
                    enriched_obj_view.append(tok.orth_.lower())
        
            enriched_obj_view = ' '.join(enriched_obj_view)
            enriched_obj_view = re.sub(pattern,';;',enriched_obj_view)
            return enriched_obj_view
        
    else:
        print(f'\nWarning:\nformat_svo()::\n  Parameter \'nrch_type\' with value \'{nrch_type}\' for \'{nrch}\' \
        not recognized. Continue.\n')
        pass
        

def extract_svo(parsed_doc: 'spacy.tokens.doc.Doc',
                _format: str = "visual",
                split_doc: bool = False) -> Tuple[List[Tuple[str,str,str]], List[Tuple[str,str,str]]]:
    '''
    extract_svo():
    - Extracts 'triples' and 'attributes' (incomplete triple extraction) from textual summaries,
      based on the SpaCy API.
    
    Inputs:
        @type               summary: spacy.tokens.doc.Doc  
        @param              summary: Input SpaCy parsed summary to analyze
        
        @type               _format: str
        @param              _format: * 'visual' yields (s,v,o) output prepared for triple visualization,
                                     * 'eval'   yields (s,v,o) output close to natural language sequence, without
                                       formatting,
                                     * 'graph'  yields (s,v,o) output where objects' prefixes are suppressed, each
                                       (s,v,o) verb is lemmatized and trailed by at most one suffix, per its lexical 
                                       dependency on its extracted object if any.
                                     (default: 'visual')
                                     
        #@type            split_doc: bool
        #@param           split_doc: Optional input kw argument. When set to False, summaries are analyzed as-is,
                                     i.e. without previous sentence splitting.
                                     (default: False)
    Output:
        @return triples, attributes: list of (s,v,o) tuples, list of (s,v,_tag_) tuples
                                     where '_tag_' specifies the attribute type
    '''
    noun_groups = has_nchunks(parsed_doc)
    
    enriched_subjs = dict()
    enriched_preds = dict()
    enriched_objs = dict()
    
    triples = list()
    attributes = list()
    
    preds = [verb for verb in parsed_doc 
             if (verb.pos_ in {'VERB','AUX'} and
                 (verb.dep_ in {'ROOT', 'parataxis', 'dep',} or
                  (verb.dep_ not in {'auxpass','amod','xcomp','conj',} and 
                   (verb.head.pos_ not in {'VERB',} or verb.head == verb)) or
                  (verb.dep_ in {'conj',} and verb.head.dep_ in {'acl','advcl','conj','ROOT','pcomp','ccomp',}) or
                  (verb.dep_ in {'ccomp','advcl',} and verb.head.pos_ in {'VERB',}) or
                  #(verb.dep_ in {'ccomp',} and verb.head.pos_ in {'VERB',}) or  
                  (verb.dep_ in {'amod','acl',} and verb.tag_ in {'VBG','VBN',})
                 ) and
                 verb.tag_ not in {'MD',}
                )
            ]
    
    for idx, pred in enumerate(preds):
        ## GENERATE SUBJECT(S)
        # Output: list of mixed list(s) of type Union[spacy.tokens.token.Token,str]
        enriched_subjs[pred] = has_subjs(pred, pred_subjs=[])
        
        if (not enriched_subjs[pred] and pred.dep_ in {'conj',}):
            pred_old = pred
            pred_new = pred.head
            while (pred_new.dep_ == 'conj' or pred_new.pos_ not in {'VERB','AUX',}):
                if pred_new == pred_new.head:
                    break
                else:
                    pred_new = pred_new.head
                    
            enriched_subjs[pred_old] = has_subjs(pred_new, pred_subjs=[])
        
        for sublist in enriched_subjs[pred]:
            for tok in sublist:
                attrs = has_cmpd_nes(tok)
                attributes.extend(attrs)
                
                attrs = has_attrs(tok)
                attributes.extend(attrs)
        
        ## GENERATE PREDICATE(S)
        # Output: list of mixed list(s) of type Union[spacy.tokens.token.Token,str]
        enriched_preds[pred] = has_vp(pred)
        
        ## GENERATE OBJECT(S)
        # Output: list of mixed list(s) of type Union[spacy.tokens.token.Token,str]
        enriched_objs[pred] = has_objs(pred, enriched_objs=[])
        
        for sublist in enriched_objs[pred]:
            
            for tok in sublist:
                attrs = has_cmpd_nes(tok)
                attributes.extend(attrs)
                
                attrs = has_attrs(tok)
                attributes.extend(attrs)
        
        ## FORMAT triples components
        subjects = list()
        for nrch in enriched_subjs[pred]:
            subjects.append(format_svo(nrch, nrch_type='s', formatted=_format, nchunks=noun_groups))
        
        verb_phrases = list()
        with_obj = True if len(enriched_objs[pred]) >= 1 else False
        for nrch in enriched_preds[pred]:
            verb_phrases.append(format_svo(nrch, nrch_type='v', pred_with_object = with_obj, formatted=_format))
        
        objects = list()
        for nrch in enriched_objs[pred]:
            objects.append(format_svo(nrch, nrch_type='o', formatted=_format, nchunks=noun_groups))
        
        
        ## BUILD triples
        if (len(objects) != 0 and len(subjects) != 0):
            _enriched_pred_tokens_lst = [{tok for tok in enriched_pred if isinstance(tok,spacy.tokens.token.Token)}
                                         for enriched_pred in enriched_preds[pred]
                                        ]
            
            _enriched_obj_tokens_lst = [{tok for tok in enriched_obj if isinstance(tok,spacy.tokens.token.Token)}
                                        for enriched_obj in enriched_objs[pred]
                                       ]
            
            for pred_idx in range(len(enriched_preds[pred])):
                _enriched_pred_tokens = _enriched_pred_tokens_lst[pred_idx]
            
                for subj in subjects:  
                    for obj_idx in range(len(enriched_objs[pred])):
                        _enriched_obj_tokens = _enriched_obj_tokens_lst[obj_idx]
                            
                        if pred.dep_ not in {'relcl',}:
                            
                            if len(_enriched_obj_tokens) == 1:
                                for tok in _enriched_obj_tokens:
                                    tok_init = tok
                                    tok = tok.head
                                    while (tok != pred and
                                           (tok.dep_ == 'conj' or
                                            tok.pos_ not in {'AUX','VERB',} or
                                            (tok_init.dep_ == 'nsubj' and tok_init in tok.children))
                                          ):
                                        tok = tok.head
                                        if (tok.pos_ in {'AUX','VERB',} and tok_init not in tok.children):
                                            break
                                        elif tok.head == tok:
                                            break
                                        else:
                                            pass
                                        
                                    _enriched_obj_tokens = _enriched_obj_tokens.union({tok})
                                
                            
                            else:
                                xcomp_lst = list(filter(lambda x: x.dep_ == 'xcomp', _enriched_obj_tokens))
                                conj_advcl_lst = list(filter(lambda x: x.dep_ in {'conj','advcl',}, _enriched_obj_tokens))
                                ccomp_lst = list(filter(lambda x: x.dep_ == 'ccomp', _enriched_obj_tokens))
                                
                                for tok in conj_advcl_lst:
                                    while (tok.dep_ in {'advcl','conj'} and tok.pos_ in {'VERB',}):
                                        tok = tok.head
                                        if tok.dep_ in {'xcomp',} :
                                            xcomp_lst =[tok]
                                            break
                                        if tok.dep_ in {'ccomp',} :
                                            ccomp_lst =[tok]
                                            break
                                        elif tok.head == tok:
                                            break
                                        else:
                                            pass
                                
                                if xcomp_lst:
                                    xcomp_tok = xcomp_lst[0]
                                    grandchildren = list(xcomp_tok.children)
                                    if (xcomp_tok.tag_ in {'VB',} and 
                                        any([(gc.tag_ in {'TO',} and 
                                              gc.dep_ in {'aux',} and token_idx_dic[gc] < token_idx_dic[xcomp_tok]) 
                                              for gc in grandchildren])
                                       ):
                                        _enriched_obj_tokens = _enriched_obj_tokens.union({xcomp_tok.head})
                                
                                if ccomp_lst:
                                    ccomp_tok = ccomp_lst[0]
                                    _enriched_obj_tokens = _enriched_obj_tokens.union({ccomp_tok.head})
                                    
                                else:
                                    pass
                                                        
                            if (len(_enriched_pred_tokens & _enriched_obj_tokens) >= 1):
                                triples.extend([(subj, verb_phrases[pred_idx], objects[obj_idx])])
                            else:
                                attributes.extend([(subj,verb_phrases[pred_idx],'')])
                                
                        
                        else:
                            triples.extend([(subj, verb_phrases[pred_idx], objects[obj_idx])])      
        
        elif (len(objects) == 0 and len(subjects) != 0):
            attributes.extend([(subj,verb_phrase,'')
                               for verb_phrase in verb_phrases
                               for subj in subjects
                               if verb_phrase])
        
        elif (len(objects) != 0 and len(subjects) == 0):
            pass
        
        else:
            pass
    
    
    if _format in {'visual',}:
        pass
    
    elif _format in {'eval',}:
        attributes = []
        triples = suppress_hooks(triples, attributes, _format)
    else:
        attrs = list()
        for attr in attributes:
            found = re.match(r'^ENT:(\w)+',attr[2])
            if found:
                pass
            else:
                attrs.append(attr)
        attributes = attrs
        triples = suppress_hooks(triples, attributes, _format)
    
    return triples, attributes


def suppress_hooks(in_triples: List[Tuple[str,str,str]],
                   in_attributes: List[Tuple[str,str,str]],
                   _format: str = 'graph') -> Union[List[str], List[Tuple[str]]]:
    '''
    suppress_hooks():
    - Selectively suppresses hooks in svo triples    
        - switch: 'eval'
        Suppresses verbs' hooks and non-alphanumerical formatting symbols (';;','::') when present in svo triples.
        
        - switch: 'graph' 
        Suppresses objects' hooks and some formatting symbols (';;','::') when present in svo triples.
        
        
        
    Input:
        @type     in-triples: List[Tuple[str,str,str]]
        @param    in-triples: (s,v,o) triples obtained by means of the extract_svo() method
        
        @type  in-attributes: List[Tuple[str,str,str]]
        @param in-attributes: (s,v,_) attributeses obtained by means of the extract_svo() method
        
        @type        _format: str
        @param       _format: * 'eval'  switch to format triples as input for evaluation
                              E.g. ('meeting','is cancelled::already::by','by;;director') 
                              -> ('meeting is cancelled already by director')
        
                              * 'graph' switch to format triples as input for graph display
                              (Default: 'graph')
                              E.g. ('meeting','be_cancelled::already::by','by;;director::irish') 
                              -> ('meeting','be_cancelled_by::already','director::irish')
    Output:
        @return    svo_sents: list of unit sentences derived from concatenated (s,v,o) triples, or
                              list of triples as tuples of strings 
    '''
    svo_pred_suffixes = set()
    svo_obj_prefixes = set()
    svo_sents = list()
    
    for triple in in_triples + in_attributes:
        if not isinstance(triple[1], spacy.tokens.span.Span):
            svo_pred_suffixes = svo_pred_suffixes | set(triple[1].split('::')[1:])
        else:
            pass
        
        if not isinstance(triple[2], spacy.tokens.span.Span):
            svo_obj_prefixes = svo_obj_prefixes | set(triple[2].split(';;')[:-1])
        else:
            pass
        
    hooks = svo_pred_suffixes & svo_obj_prefixes
        
    if _format in {'eval',}:
        pattern = r';{2}|_'
        for triple in in_triples + in_attributes:
            subj_view = re.sub(pattern,' ',triple[0])
            pred_view = re.sub(pattern,' ',triple[1]).split('::')
            pred_view = ' '.join([word for word in pred_view if word not in hooks])
            obj_view = re.sub(pattern,' ',triple[2])
            svo_sents.append(' '.join((subj_view, pred_view, obj_view)))

    elif _format in {'graph',}:
        for triple in in_triples:
            svo_pred = [triple[1].split('::')[0],]
            svo_obj = [triple[2].split(';;')[-1],]
            
            for cpt in triple[1].split('::')[1:]:
                if cpt not in hooks:
                    svo_pred.extend(['::',cpt])
                elif cpt in set(triple[2].split(';;')[:-1]):
                    svo_pred.insert(1,cpt)
                    svo_pred.insert(1,'_')
                else:
                    pass

            for cpt in triple[2].split(';;')[:-1]:
                if cpt not in set(triple[1].split('::')[1:]):
                    svo_obj.insert(0,';;')
                    svo_obj.insert(0,cpt)

            svo_sents.append((triple[0],''.join(svo_pred),''.join(svo_obj)))
            svo_sents += in_attributes
            
    else:
        print(f'\n *** Warning: unexpected switch \'_format\' value: {_format}\nSkip and continue.\n')
        
    return svo_sents

# %%
"""
### Part B:
#### Load and tweak NLP model pipeline
"""

# %%
## Load model
spacy.require_cpu()
nlp = en_core_web_lg.load()
lemmatizer = nlp.get_pipe("lemmatizer")
nlp.add_pipe('coreferee')
named_entities = nlp.pipe_labels['ner']

## Resolve contractions WITH CHANGE IN TEXT before parse (text preprocessing)
en_contractions_dict = dict()
with open('en_contractions_dict.pickled', 'rb') as f:
    # /home/ckb/Documents/Work/Projects/Bsc/Sgoab/Src/Syntax_parsing/en_contractions_dict.pickled
    en_contractions_dict = pickle.load(f)
    
## Add rule-based custom NER pipe
custom_ruler = spacy.load("Custom_ruler")
nlp.add_pipe('entity_ruler', name="added_ner", source=custom_ruler, before="ner")  # update existing pipeline
print(nlp.pipe_names,'\n')

## Modify model infix-rule 
infixes = (
    LIST_ELLIPSES
    + LIST_ICONS
    + [
        r"(?<=[0-9])[+\-\*^](?=[0-9-])",
        r"(?<=[{al}{q}])\.(?=[{au}{q}])".format(
            al=ALPHA_LOWER, au=ALPHA_UPPER, q=CONCAT_QUOTES
        ),
        r"(?<=[{a}]),(?=[{a}])".format(a=ALPHA),
        # r"(?<=[{a}])(?:{h})(?=[{a}])".format(a=ALPHA, h=HYPHENS),  
        # comment-out regex that splits on hyphens between letters
        r"(?<=[{a}0-9])[:<>=/](?=[{a}])".format(a=ALPHA),
    ]
)

infix_re = compile_infix_regex(infixes)
nlp.tokenizer.infix_finditer = infix_re.finditer

# %%
"""
### Part C: Testing
#### SVO triples extraction on hand-picked summaries
"""

# %%
print(Color.BOLD + ' --- identify (s,v,o) triples from VERBs and (s/o,attr,_) attribute properties:' \
      + Color.END,'\n')

test_docset = ["They're dead.",
               "We'll come out really slowly from the house.",
               'Look, the lady came to.',
               'He finally came to.',
               'The curve keeps going steadily up.',
               "Two women tend to her",
               'She slowly came to her senses.',
               'The old lady came to her senses.',
               'Who will win the bid in a week from now ?',
               "Who'll've won the call for bids in a week from now, is up for guess.",
               'Any food found here is up for grabs.',
               'I sung that for and to you.',
               'Away she went.',
               'She went away.',
               'They run cheaper.',
               'A shuttle rolled ponderously up to me.',
               'Shuttles run less slow or frequently for groups.',
               'Shuttles run much less frequently for groups.',
               'Shuttles run less slow and more frequently than buses for groups.'
               'Shuttles run much more frequently for groups.',
               'Shuttles run less frequently for groups.',
               'Shuttles run less slow but more frequently for groups.',
               'Shuttles run less frequently for groups and are larger than cars.',
               'Shuttles keep running cheaper for groups.',
               'Shuttles run cheaper than taxis.',
               'Shuttles run cheaper for groups.',
               'Shuttles run cheaper.',
               'Shuttles run slow.',
               'Shuttles run faster and cheaper than taxis.',
               'Shuttles run slow but cheaper than taxis.',
               'Shuttles run slower than trains but more frequently than buses for groups.',
               'The sliding part went up and down.',
               "The unicorn representation is normally exemplary and symbolic.",
               "Unicorns are normally exemplary, symbolic and positively connoted.",
               "The unicorn representation, whenever it occurs in Christian sacred art, is normally exemplary, symbolic and positively connoted.",
               "Angel holds a white lilium.",
               "The angel is clad in yellow robes.",
               "The lanky girl came to me.",
               "That person was some lady.",
               "Those flowers are lilies.",
               'Crocodiles came fast and en-masse at me.',
               'The elevator goes up then down, all night.',
               'I sung it with her, to and for you.',
               'I sung it before them, to and for you.',
               'Sitting woman, wearing a blue-grey hat and yellowish green cloak, and holding a naked infant on her lap.', 
               'The following is a short summary. Quietly sitting man and woman, wearing blue-grey cloaks and jointly holding a naked infant in a basket.',
               'Quietly sitting man and woman, wearing blue cloaks and jointly holding a naked infant in their arms.',
               'Seated man and woman, wearing blue cloaks and jointly holding a naked infant in their arms.',
               'New York City man and woman, wearing dark blue coats and look at a youngster playing with a bow.',
               'A woman sits, wearing a grey cloak and holding a naked infant on her lap.',
               'Cars and motorbikes were stopped by a policeman and the snow.',
               'A naked man is tied to a tree and has several arrows in his body.',
               'A naked man is tied to a tree with several arrows in his body.',
               'A naked man is tied to a tree with chains holding his legs and arms.',
               'A naked man is tied to a tree with chains holding one of his legs and arms.',
               'The twisted body of a man is tied to a tree, naked and pierced with arrows.',
               'The twisted body of a man is naked, tied to a tree, and pierced with arrows.',
               'We were fortunate. They found him exhausted.',
               'They were exhausted.',
               'They found them utterly exhausted.',
               'She found that it was too late already.',
               'They found that they were exhausted.',
               "Listen, you shouted, then threw things and insults at him.",
               'Listen; you were shouted at, then thrown things and insulted by him.',
               'You shouted, then threw things and insults at him.',
               "It is a vegetal and an animal and a human, all at the same time.",
               "Oddly, she managed to still look frail and strong at the same time.",
               "The horse kept going down the hill, and stopping at every bush on its way, gnawing at red berries.",
               "Horses represent might and wealth from the early Middle Ages on.",
               "Our horses are strong and delicate. They need care.",
               "The horse is green.",
               'A horse is with armor.',
               'A horse is protected by armor.',
               'He climbed swiftly up his horse.',
               'He slowly climbed down his horse.',
               'Horses are both strong and delicate quadrupeds and beautiful animals.',
               'Horses are both strong quadrupeds and delicate animals.',
               'The horse came uphill, slowly and without a sound.',
               'The horse came up slowly and without a sound.',
               'Horses are symbols of might.',
               "Horses stand for might and wealth.",
               "The horse is a symbol of might.",
               "The horse stands sideways before a background of trees. A richly dressed man rides it.",
               'Horses are beautiful.',
               "The horse came up without a sound.",
               "The horse is normally symbolic of might and wealth.",
               "The horse is normally exemplary and symbolic of might and wealth.",
               'A knight in armor, attacked by dogs, throws himself in the river, and drowns shortly.'
               'Please, listen. Call me up without fail.',
               'They are firing at us!',
               'A man is tied to a tree with arrows in his body.',
               'The twisted body of a man is naked and tied to a tree and pierced with arrows.',
               'The twisted body of a man is tied naked to a tree and pierced with arrows.',
               "The man's right hand touches the side of his head.",
               "His left index points to the points that he has won at the last game.",
               "The man he had killed had seemed healthy enough.",
               'He kept climbing down very slowly.',
               'He kept climbing down the ladder slowly.',
               'The person wears a white collared piece of Venice clothing',
               'The person wears a white collared piece of Venice clothing that is primarily black.',
               'He slowly kept climbing down the ladder.',
               'It slowly came down to nothing.',
               'Look, the curve keeps going steadily up.',
               "We went fishing on Sunday. We had a good time and caught some fish.",
               "They don't teach how to fish.",
               "They don't teach what to fish.",
               "They do teach how to keep fish for a long time.",
               "They don't teach how to keep fishing.",
               "They don't teach us how to keep fishing."
               "Well! They don't teach us what to fish."
               "They taught you what to fish and how to cook the fish.",
               "They saw you fishing.",
               "They don't teach what to fish or not to fish.",
               'I want to go up there and stay.',
               "你好, 小任. I kept the thing for and away from you.",
               "They will keep on trying until the bell rings.",
               "Don't keep trying until the bell rings. It's no use.",
               "They will continue to try until the bell rings."
               "We kept trying to do it, always failing at it.",
               "Sorry, but we kept trying and failing at it.",
               "I tend to think not",
               "I consent to his election.",
               "I agree to that.",
               "We're simply asked to do that.",
               "I agree to his being promoted.",
               "I look forward to meeting with you.",
               "I look forward to meeting with you and talking to you.",
               "I look forward to meeting with you and discussing those points with you.",
               "I look forward to meeting them and discussing that point with you.",
               "The caravan goes steadily up the hill.", 
               "The caravan goes steadily uphill.",
               'Now, do it',
               'Look, she came up slowly from the house.',
               'We were fortunate. We could drive away unscathed.',
               'Yes, we can!',
               'It goes on and off especially during the night.',
               'It goes back and forth slowy, time and again.',
               'They visit all the time.',
               'A man shouted to and at her.',
               'A man was shouting something to her apparently.',
               'Ha! I sung it at, to and for you.',
               "It's of no use.",
               "It's no use.",
               "The meeting had been already cancelled by the director, and immediately replaced with another one.",
               "The meeting had been already cancelled by the director. It was immediately replaced with another one.",
              ]

test_docset = ['he wears a hat with a seashell on it.']
test_docset = ["The meeting had been already cancelled by the German director. It was immediately replaced with another one.",]
test_docset = ["Child Maria-Cristina holds a crucifix.",]
test_docset = ["a man sits nonchalently on Saint Peter's cross. He is bald",]
test_docset = ["A bird sits on the top left branch and has open wings."]
test_docset = ["the head of the murdered man is in Judith's hand."]

_display = False # True #
_format = 'graph' # 'visual' #'eval' # 'graph' #

idx_tot = 0
idx = 0

for doc in test_docset:
    idx += 1
    for original, replacement in en_contractions_dict.items():
        pattern = re.compile(r'(?i)' + re.escape(original))
        doc = pattern.sub(replacement,doc)

    print('Before coref resolution:\n   ', doc)
    doc = coref_resolv(doc)
    print('After coref resolution:\n   ', doc,'\n')
    parsed_doc = nlp(u'%s'%doc)
    
    u_sent_idx = 0
    for u_sent in parsed_doc.sents:
        u_sent_idx += 1
        print(Color.BOLD + f'UNIT SENT: \'{u_sent.text}\'' + Color.END)
        nes = has_named_ents(u_sent.text)
        print(f'NEs:  \'{nes}\'')

    token_idx_dic = dict()
    for idx, token in enumerate(parsed_doc):
        token_idx_dic[token] = idx

    if _display:
        display_dep_graph(parsed_doc)
    triples, attributes = extract_svo(parsed_doc, _format=_format, split_doc=False)
       
    print(Color.BOLD + '   Triples:' + Color.END,list(set(triples)))
    print(Color.BOLD + 'Attributes:' + Color.END,list(set(attributes)),'\n')
    
    idx_tot += u_sent_idx

print(f'\nTotal number of unit sentences processed: {idx_tot}')

# %%
"""
### Part D: Application

This section is specific to SGoaB's internal crowdsourcing trial. All paths and directory-container names are host-dependent and point to images and summary datasets not included in this repo. 

***Read this first***<BR>
In the following, we dub "test" or "train" two different kind of machine generated caption seeds. In no case are the two kw ("test" or "train") related in any way to a model learning process. Instead they describe:
- in the first case, image caption seeds for which the underlying labeled objects' bbxes were generated
by the NN model, before being processed by VisRel.
- in the second case, image caption seed generated by VisRel processing from manually annotated bbxes.

#### Application to summaries' SVO triples extraction
"""

# %%
## Define working directories
path_lst = os.getcwd().split('/')

try:
    path_lst_idx = [idx for idx,val in enumerate(path_lst) if val == 'Sgoab'][-1]
except (ValueError, IndexError) as err:
    path_lst_idx = len(path_lst)-1

path_lst = path_lst[:path_lst_idx+1]
print(f'PWD:\n  /{"/".join(path_lst[1:])}\n')

data_dir = '/'.join(path_lst) + '/Data/'
img_dir = data_dir + 'Prado_train_dataset/'
print("Location of images and annotations to process:\n   Images:  {0}".format(img_dir))

files = [file for file in os.listdir(img_dir) if file.endswith('.png') or file.endswith('.jpg')]
print(f"Number of image files in directory: {len(files)}")

# %%
## List of files to process

files = ['30116401776',
 '57',
 'A_Vaccaro_Encuentro_de_Rebecca_e_Isaac_Lienzo._195_x_246_cm._Museo_del_Prado',
 'Abril__28Tauro_29',
 'Adoraci_C3_B3n_de_los_Reyes_Magos_2C_de_Eugenio_Caj_C3_A9s__28Museo_del_Prado_29',
 'Adoraci_C3_B3n_de_los_Reyes_Magos_2C_de_Francesco_Bassano__28Museo_del_Prado_29',
 'Adoraci_C3_B3n_de_los_pastores_-_Luis_de_Morales',
 'Adoraci_C3_B3n_de_los_pastores__28Bonifazio_de_27_Pitati_29',
 'Adoraci_C3_B3n_del_Primer_Mago_2C_de_Pedro_Berruguete__28Museo_del_Prado_29',
 'Adriaen_Isenbrant_-_Mass_of_St_Gregory_-_WGA11872',
 'Adriaen_van_Cronenburg_Portrait_of_a_Lady_and_a_Young_Girl',
 'Adriaen_van_Ostade_-_Country_Concert_-_WGA16718',
 'Alegor_C3_ADa_de_la_industria(1)',
 'Alegor_C3_ADa_del_Nuevo_Testamento_2C_de_Jos_C3_A9_Guti_C3_A9rrez_de_la_Vega__28Museo_del_Prado_29',
 'Alejandro_de_Loarte_San_Juan_Bautista',
 'Andrea_Mantegna_047',
 'Andrea_del_Sarto_-_Il_sacrificio_di_Isacco__28Prado_29',
 'Aniello_Falcone_-_The_expulsion_of_the_merchants_from_the_Temple',
 'Annibale_Carracci_Assumption_of_the_Virgin',
 'Annunciation_by_El_Greco__281570-1575_2C_Prado_29',
 'Antes_de_continuar_el_retrato_2C_de_Pedro_Rodr_C3_ADguez_de_la_Torre__28Museo_del_Prado_29',
 'Anthony_van_Dyck_-_Equestrian_portrait_of_Charles_I_of_England__28Copy_29',
 'Anthony_van_Dyck_-_Saint_Rosalie',
 'Anthony_van_Dyck_-_The_Head_of_an_Old_Man',
 'Antoine_Watteau_-_Gathering_in_a_Park_-_WGA25441',
 'Anton_van_Dyck_-_Saint_Francis_of_Assisi_in_Ecstasy',
 'Antonio_Pereda_y_Salgado_2C__22La_Anunciaci_C3_B3n_22_2C_1637',
 'Anunciacion_Prado_282_29',
 'Aparici_C3_B3n_del_ap_C3_B3stol_San_Pedro_a_San_Pedro_Nolasco',
 'Archduke_Leopold_and_Archduchess_Maria_Anna',
 'Arroyo_del_Bat_C3_A1n__28Escorial_29',
 'Ascensi_C3_B3n_del_Se_C3_B1or__28Antonio_Lanchares_29',
 'Assumpti',
 'Atropos_o_Las_Parcas',
 'Aves_muertas_por_Francisco_de_Goya(1)',
 'Bajada_de_Cristo_al_Limbo__28Sebastiano_del_Piombo_29',
 'Banquete_de_Tel_C3_A9maco_y_Mentor__28Museo_del_Prado_29',
 'Bautismo_de_Cristo_por_Navarrete_el_Mudo',
 'Bautizo_en_la_sacrist_C3_ADa_2C_de_Pedro_Rodr_C3_ADguez_de_la_Torre__28Museo_del_Prado_29',
 'Bernardo_Lorente_Germ_C3_A1n_-_The_divine_shepherdess',
 'Berruguete_ordeal',
 'Bodas_de_Psiquis_y_Cupido__28Perino_del_Vaga_29',
 'Bodeg_C3_B3n_de_uvas_2C_manzanas_y_ciruelas_2C_de_Juan_de_Espinosa',
 'Bramer-abraham_y_los_tres_angeles',
 'Bufon_Francisco_Bazan_carreno_de_miranda',
 'C_C3_B3micos_ambulantes',
 'Cabeza_de_San_Pablo_2C_copia_de_un_original_de_Murillo__28Museo_del_Prado_29',
 'Calma_en_el_puerto_de_Valencia_2C_de_Rafael_Monle_C3_B3n__28Museo_del_Prado_29',
 'Camilo-san_jeronimo-prado',
 'Cardenal_luis_maria-WIKI',
 'Carducho._Pinturas_del_claustro_de_El_Paular_02',
 'Carducho._Pinturas_del_claustro_de_El_Paular_12',
 'Carlos_IV__28Goya_29',
 'Caza_con_reclamo',
 'Caza_muerta_y_una_cesta_2C_de_Gin_C3_A9s_Andr_C3_A9s_de_Aguirre__28Museo_del_Prado_29',
 'Ceres_en_casa_de_H_C3_A9cuba__28Elsheimer_29',
 'Cerezo-el_juicio_de_un_alma',
 'Cerezo-vision_de_san_agustin-prado',
 'Conversi_C3_B3n_de_San_Pablo_2C_de_Palma_el_Joven__28Museo_del_Prado_29',
 'Cornelis_de_Vos_-_Apollo_and_the_Python_2C_1636-1638',
 'Cornelis_de_Vos_-_Nacimiento_de_Venus',
 'Coxie_2C_Michiel_-_Saint_Cecilia_-_Prado',
 'Cristo_abrazado_a_la_cruz__28El_Greco_2C_Museo_del_Prado_29',
 'Cristo_crucificado__28Murillo_29',
 'Cristo_en_la_Cruz__28Vel_C3_A1zquez_2C_1631_29',
 'Cristo_en_la_cruz__28Goya_29',
 'Cristo_muerto_2C_sostenido_por_un__C3_A1ngel__28Antonello_da_Messina_29',
 'Cristo_yacente_2C_de_Francisco_Camilo__28Museo_del_Prado_29',
 'Danza_aldeanos_Rubens_lou',
 'David_Gerard_Prado_Virgin_Child',
 'David_Teniers__28II_29_-_Armida_and_Reinaldo_separated',
 'David_Teniers__28II_29_-_Rinaldo_27s_feats_against_the_Egyptians',
 'David_Teniers__28II_29_-_Smokers',
 'David_de_coninck-aves-prado',
 'Diego_Vel_C3_A1zquez_-_Coronation_of_the_Virgin_-_Prado',
 'Do_C3_B1a_Mariana_de_Neoburgo_a_caballo',
 'Ecce_Homo_2C_de_Juan_de_Juanes__28Museo_del_Prado_29',
 'Ecce_Homo__28Murillo_29',
 'El_Salvador_con_la_Eucarist_C3_ADa_entre_Melquisedec_y_Aar_C3_B3n__28Museo_del_Prado_29',
 'El_arquitecto_Juan_Guas__28Museo_del_Prado_29',
 'El_bebedor',
 'El_cacharrero_2C_por_Francisco_de_Goya',
 'El_cardenal_Juan_Jos_C3_A9_Bonel_y_Orbe__28Museo_del_Prado_29',
 'El_coronel_Juan_de_Zengotita_Bengoa__28Museo_del_Prado_29',
 'El_pescador_de_ca_C3_B1a',
 'El_sacrificio_de_Isaac__28Domenichino_29',
 'El_sue_C3_B1o_de_San_Jos_C3_A9_2C_de_Antonio_Palomino',
 'El_sue_C3_B1o_de_San_Jos_C3_A9__28Museo_del_Prado_29',
 'El_vi_C3_A1tico_en_la_aldea_2C_de_Enrique_Mart_C3_ADnez_Cubells__28Museo_del_Prado_29',
 'Encuentro_de_Sancho_Panza_con_el_Rucio_2C_Museo_del_Prado',
 'Enrique_Simonet_-_Flevit_super_illam_1892',
 'Entierro_de_Santa_Leocadia__28Museo_del_Prado_29',
 'Escalante-yacente',
 'Finsonius-anunciacion-prado',
 'Francesco_Albani_-_The_Judgement_of_Paris_2C_1650-60',
 'Francisco_de_Zurbar_C3_A1n_014',
 'Frans_Snyders_-_Deer_Hunting',
 'Gaspar_de_Crayer_-_Saint_Dominic',
 'Gaspar_de_Crayer_-_St_John_the_Baptist',
 'Gerard_David_-_Virgin_and_Child_with_Angels__28Museo_del_Prado_29',
 'Germ_C3_A1n_Hern_C3_A1ndez_Amores_-_The_Journey_of_Mary_and_Joseph_at_Ephesus_2C_1862',
 'Giovanni_Domenico_Tiepolo_The_Agony_in_the_Garden__28La_Oraci_C3_B3n_en_el_Huerto_29_1772._Oil_on_canvas_2C_125_x_142cm._Museo_Nacional_del_Prado',
 'Giovanni_Paolo_Pannini_-_Ruins_with_St_Paul_Preaching_-_WGA16981',
 'Giuseppe_bonito-esquilache',
 'Gossaert_Deesis',
 'Goya_Maria_Teresa',
 'H_C3_A9rcules_y_el_jabal_C3_AD_de_Erimanto_2C_por_Zurbar_C3_A1n',
 'Hans_Baldung_009',
 'Hemessen-cirujano-prado',
 'Imposici_C3_B3n_de_la_casulla_a_san_Ildefonso._Santiago_Mor_C3_A1n_el_Viejo',
 'Inmaculada_Concepci_C3_B3n__28Murillo_2C_1665_29',
 'Isabel_de_Parma',
 'Jacob_Jordaens_-_La_Piedad_2C_1650-1660',
 'Jacob_Jordaens_-_Ofrenda_a_Ceres',
 'Jos_C3_A9_de_Ribera_024',
 'Juan_Carreno_de_Miranda_004',
 'La_Abundancia_2C_de_Jos_C3_A9_del_Castillo',
 'La_Anunciaci_C3_B3n_2C_por_Robert_Campin',
 'La_Crucifixi_C3_B3n_2C_de_un_seguidor_de_Rogier_van_del_Weyden__28Museo_del_Prado_29',
 'La_Magdalena_penitente__28Luca_Giordano_29',
 'La_Natividad_2C_por_Juan_Correa_de_Vivar',
 'La_muerte_de_Adonis_2C_por_Eugenio__C3_81lvarez_Dumont',
 '_C2_A1Siempre_incompleta_la_dicha_21__28Museo_del_Prado_29',
 '_C3_9Altimos_momentos_del_rey_Fernando_IV_de_Castilla_y_Le_C3_B3n',
 'a3d9534ce326882a2270fe0e444d3aab',]

# %%
"""
#### Extract SVOs from VisRel-generated candidate summaries (whole summaries)
"""

# %%
start = time.time()

dep_display = False # True #
_uniq_ent = True # False #
_format = 'graph' # 'visual' # 'eval' #

img_caption_seeds = list()
cnt = 0

## candidate summaries compilation
vrd_json_files = [file for file in os.listdir(img_dir)
                  if (file.endswith('_vrd.json') and
                      ''.join(file.split('_vrd.json')[:-1]) in files)
                 ]

vrd_json_files.sort()
#print(f'Nbr of *_vrd.json files: {len(vrd_json_files)}')

for f in vrd_json_files:
    cnt +=1
    
    data = list()
    with open(img_dir+f,'r') as infile:
        data = infile.readlines()
    
    data_json = ast.literal_eval(ast.literal_eval(data[0]))
    annots = data_json["annot_txt"]
    #print(f'file: {f}\n   annot_txt: {data_dict["annot_txt"]}')
    
    img_caption_seeds = [clean_visrel_seeds(seed, uniq_ent=_uniq_ent)
                         for key in annots 
                         for seed in annots[key]
                        ]
    
    if len(img_caption_seeds) != 0:
        img_caption_seeds = [seed for seed in set(img_caption_seeds) 
                             if seed != '']
    else:
        img_caption_seeds = ['.']
    
    if img_caption_seeds != ['.']:
        parsed_doc = nlp(u'%s'%'. '.join(img_caption_seeds)+'.')
        #print(f'parsed_doc: {parsed_doc}')
        
        if dep_display:
            display_dep_graph(parsed_doc)
        
        token_idx_dic = dict()
        for idx, token in enumerate(parsed_doc):
            token_idx_dic[token] = idx
        
        triples, attributes = extract_svo(parsed_doc, _format=_format, split_doc=False)
    
    print(Color.BOLD + f'file: {"".join(f.split("_vrd.json")[:-1])}' + Color.END)
    print(f'   img_caption_seeds: {img_caption_seeds}\n')
    print(f'   triples: {triples}\n')
    print(f'attributes: {attributes}\n')
    
    
end = time.time()
elapsed = round(end - start,1)
print(f'{elapsed}s elapsed for {cnt} processed images.')

# %%
