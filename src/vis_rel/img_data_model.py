""" 
============================
Licensing terms and copyright
This code is protected by the terms and conditions of the GNU_GPL-v3 copyleft license.

Copyright (C) 2021 Cedric Bhihe

This code is free software and was developed by its author while at the Barcelona Supercomputing Center (https://www.bsc.es). You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>, or consult the License.md file in this repo.

To contact the author of the code, use cedric.bhihe@gmail.com.

============================
"""

from typing import Tuple
import numpy as np

# Global Labels from used by the object-detection
LABEL_MAP = ["BG", "crucifixion", "angel", "person", "crown of thorns", "horse", "dragon", "bird", "dog", "boat", "cat", "book",
             "sheep", "shepherd", "elephant", "zebra", "crown", "tiara", "camauro", "zucchetto", "mitre", "saturno", "skull",
             "orange", "apple", "banana", "nude", "monk", "lance", "key of heaven", "banner", "chalice", "palm", "sword", "rooster",
             "knight", "scroll", "lily", "horn", "prayer", "tree", "arrow", "crozier", "deer", "devil", "dove", "eagle", "hand",
             "head", "lion", "serpent", "stole", "trumpet", "judith", "halo", "helmet", "shield", "jug", "holy shroud", "god the father",
             "swan", "butterfly", "bear", "centaur", "pegasus", "donkey", "mouse", "monkey", "cow", "unicorn"]
label_map = LABEL_MAP.copy()  # rename for debugging


def img_data_model(detection_output: dict, width: int, height: int) -> Tuple[dict, np.ndarray]:

    import math as m
    import numpy as np
    from typing import Union, Any, List, Optional, Tuple, cast

    '''
    Input: 
        @param      detection_output: json file obtained from R-CNN processing
        @type       dict
        
        @param      width: image width in pixels
        @type       int
        
        @param      height: image height in pixel
        @type       int    
    
    Output:
         @param     img_dict: image metadata and computed metrics
                    {
                        'size': {'width':width,'height':height},
                        'bbx': {ulabel1: {'xmin':xmin1,
                                          'ymin':ymin1,
                                          'xmax':xmax1,
                                          'ymax':ymax1,
                                          'cpt':{'xc':xc1,'yc':yc1},
                                          'oloc':oloc1,
                                          'rsa':rsa1,
                                          'off':off1,
                                          'score':score1
                                          },
                                ulabel2: {'xmin':xmin2,
                                          'ymin':ymin2,
                                          'xmax':xmax2,
                                          'ymax':ymax2,
                                          'cpt':{'xc':xc2,'yc':yc2},
                                          'oloc':oloc2,
                                          'rsa':rsa2,
                                          'off':off2,
                                          'score':score2
                                          },
                                ulabel3: {...}
                                },
                        'bbx_cnt': bbx_cnt
                    }
         @type      dict
         
         @param     vra, visual relationship array between bbxes pairwise
                    Non diagonal elts are lists of length 4, diagonal elts are lists of length 5.
                    
                    ON DIAGONAL ELEMENTS ('bbx1' = 'bbx2'):
                      Each array element of type 'list' contains:
                      - uniq_label = label + '_' + label_id
                      - cpt = center point coords (xc,yc)
                      - oloc = object's location on image plane; vertical and horizontal are divided 
                        in the three regions t(op), c(enter), b(ottom) and l(eft), c(enter), r(ight)
                        respectively. In the end the set of 'oloc' values is {'tl','cl','bl','tc',
                        'cc','bc','tr','cr','br'}
                      - rsa = percentage surface area of the bbx relative to that of entire image 
                      - off = 'orientation and form-factor' attribute

                    OFF DIAGONAL ELEMENTS  ('bbx1' != 'bbx2'):
                      The 1st bbx' label ('bbx1') corresponds to the first array index while
                      'bbx2' corresponds to the second index.  Each array element of type 'list'
                      contains:
                      - dcpt = (float) pairwise distance (center to center) between bbxes 
                      - cpix = (float)  pairwise distance (closest two pixels) between bbxes
                      - bro = (float) relative overlap of 'bbx1' onto 'bbx2' as a percentage of the total 
                           surface area of 'bbx1'.
                      - rpos = (string) relative position of 'bbx1' wrt to 'bbx2'
                           'rpos' takes its values in {'NN','NE','EE','SE','SS','SW','WW','NW'}
         @type      np.ndarray
    '''

    #  ===============================
    # Local methods
    #  ===============================

    # Function rsa_off()
    def rsa_off(width: int, height: int,
                xmin: int, xmax: int,
                ymin: int, ymax: int) -> Tuple[float, float]:
        '''
        'rsa_off()'
        Computes: 
          1/ the surface area of a bbx, relative to that of the entire image.
             The returned result is a percentage (type float). 
          2/ a bbx' orientation and form factor realtive to that of the entire image.
             The returned result is a number between -1 and +1 (type float),
             where:
             * -1 <= off < 0   denotes a horizontally oriented bbx,
             *  off = 0        denotes a square bbx, 
             *  0 < off <= +1  denotes a vertically oriented bbx.
        '''
        try:
            width = int(width)
            height = int(height)
            xmin = int(xmin)
            ymin = int(ymin)
            xmax = int(xmax)
            ymax = int(ymax)
        except (ValueError) as ve:
            print(
                ve + '\nFunction \'rsa_f()\' ==> Non integer bbx coordinates, image width or image height.')
            raise ve

        a = ymax - ymin
        b = xmax - xmin
        rsa = round(b * a/(width * height) * 100, 2)
        off = 0. if a == b else round(
            (a-b)*(height*width)**(-0.5) * pow(width/height, ((a-b)/(2*abs(a-b)))), 4)

        return (rsa, off)

    # Function euclid_dist()
    def euclid_dist(p1: Tuple[float, float],
                    p2: Tuple[float, float]) -> float:
        '''
        'euclid_dist()'
        Computes the L2 distance between 2 points in Cartesian plane.
        Input: 2 tuples, each containing xy coordinates, all floats.
        Output: 1 float.
        '''

        #dpt = ((x2-x1)**2+(y2-y1)**2)**0.5
        dpt = (sum((comp1 - comp2) ** 2.0 for comp1, comp2 in zip(p1, p2)))**0.5

        return round(dpt)

    # Function shortest_pix_dist()
    def shortest_pix_dist(cpt1: Tuple[float, float],
                          coord1: Tuple[int, int, int, int],
                          cpt2: Tuple[float, float],
                          coord2: Tuple[int, int, int, int]) -> float:
        '''
        'shortest_pix_dist()'
        Admits four tuples as input:
           * cpt1: 1st bbx' center coordinates (xc1, yc1)
           * coord1: 1st bbx' coordinates (xmin1, ymin1, xmax1, ymax1)
           * cpt2: 2nd bbx' center coordinates (xc2, yc2)
           * coord1: 2nd bbx' coordinates (xmin2, ymin2, xmax2, ymax2)
        Output: shortest L2 (Euclidean) (outer) distance between the two closest points of
        2 arbitrary bbx in the Cartesian plane. 
            - When 2 bbxes touch, distance value = 0. 
            - When 2 bbxes overlap, distance value = -9999. 
        '''

        xc1, yc1 = cpt1
        xmin1, ymin1, xmax1, ymax1 = coord1
        xc2, yc2 = cpt2
        xmin2, ymin2, xmax2, ymax2 = coord2

        if xc2 >= xc1 and yc2 >= yc1:
            delta_x = xmin2 - xmax1
            if delta_x < 0:
                cpix = -9999 if (ymin2 - ymax1 < 0) else ymin2 - ymax1
            elif delta_x == 0:
                cpix = 0 if (ymin2 - ymax1 <= 0) else ymin2 - ymax1
            else:   # delta_x > 0
                if ymin2 - ymax1 <= 0:
                    cpix = delta_x
                else:
                    cpix = euclid_dist((xmax1, ymax1), (xmin2, ymin2))
        elif xc2 <= xc1 and yc2 >= yc1:
            delta_x = xmin1 - xmax2
            if delta_x < 0:
                cpix = -9999 if (ymin2 - ymax1 < 0) else ymin2 - ymax1
            elif delta_x == 0:
                cpix = 0 if (ymin2 - ymax1 <= 0) else ymin2 - ymax1
            else:   # delta_x > 0
                if ymin2 - ymax1 <= 0:
                    cpix = delta_x
                else:
                    cpix = euclid_dist((xmin1, ymax1), (xmax2, ymin2))
        elif xc2 <= xc1 and yc2 <= yc1:
            delta_x = xmin1 - xmax2
            if delta_x < 0:
                cpix = -9999 if (ymin1 - ymax2 < 0) else ymin1 - ymax2
            elif delta_x == 0:
                cpix = 0 if (ymin1 - ymax2 <= 0) else ymin1 - ymax2
            else:   # delta_x > 0
                if ymin1 - ymax2 <= 0:
                    cpix = delta_x
                else:
                    cpix = euclid_dist((xmin1, ymin1), (xmax2, ymax2))
        elif xc2 >= xc1 and yc2 <= yc1:
            delta_x = xmin2 - xmax1
            if delta_x < 0:
                cpix = -9999 if (ymin1 - ymax2 < 0) else ymin1 - ymax2
            elif delta_x == 0:
                cpix = 0 if (ymin1 - ymax2 <= 0) else ymin1 - ymax2
            else:   # delta_x > 0
                if ymin1 - ymax2 <= 0:
                    cpix = delta_x
                else:
                    cpix = euclid_dist((xmax1, ymin1), (xmin2, ymax2))

        return round(cpix)

    # Function bbx_overlap()
    def bbx_overlap(coord1: Tuple[int, int, int, int],
                    coord2: Tuple[int, int, int, int]) -> Tuple[float, float]:
        '''
        'bbx_overlap()'
        Admits 2 tuples of type Tuple[int, int, int, int] as input:
           * coord1: 1st bbx' coordinates (xmin1, ymin1, xmax1, ymax1)
           * coord1: 2nd bbx' coordinates (xmin2, ymin2, xmax2, ymax2)
        Output: tuple(float, float) representing 2 arbitrary bbxes' overlapping surface
        area relative to the surface area of the 1st bbx and to that of the 2nd bbx respectively.
        When 2 bbxes do not overlap or simply touch, the function returns 0.
        '''

        xmin1, ymin1, xmax1, ymax1 = coord1
        xmin2, ymin2, xmax2, ymax2 = coord2

        if ((ymin1 < ymax2 and ymax2 <= ymax1) or (ymin1 <= ymin2 and ymin2 < ymax1) or
                (ymin2 < ymax1 and ymax1 <= ymax2) or (ymin2 <= ymin1 and ymin1 < ymax2)):
            ymin = max(ymin1, ymin2)
            ymax = min(ymax1, ymax2)
            numerator = ymax - ymin + 1
            vbro_1_2 = min(1, numerator / (ymax1 - ymin1))
            vbro_2_1 = min(1, numerator / (ymax2 - ymin2))
        else:
            vbro_1_2 = vbro_2_1 = 0

        if ((xmin1 < xmax2 and xmax2 <= xmax1) or (xmin1 <= xmin2 and xmin2 < xmax1) or
                (xmin2 < xmax1 and xmax1 <= xmax2) or (xmin2 <= xmin1 and xmin1 < xmax2)):
            xmin = max(xmin1, xmin2)
            xmax = min(xmax1, xmax2)
            numerator = xmax - xmin + 1
            hbro_1_2 = min(1, numerator / (xmax1 - xmin1))
            hbro_2_1 = min(1, numerator / (xmax2 - xmin2))
        else:
            hbro_1_2 = hbro_2_1 = 0

        bro_1_2 = round(min(1, vbro_1_2 * hbro_1_2) * 100)
        bro_2_1 = round(min(1, vbro_2_1 * hbro_2_1) * 100)

        #return tuple(bro_1_2, bro_2_1)
        return bro_1_2, bro_2_1

    # Function p2p_rpos()
    def p2p_rpos(p1: Tuple[float, float],
                 p2: Tuple[float, float],
                 precision=15,
                 sector='pict') -> str:
        '''
        'p2p_rpos()'
        Computes the slope of the segment P1-P2 in Euclidean space, based on a referential
        system, where the x and y axes are horizontal and vertical respectively, but with
        axes' origin located in image lower left corner of image (instead of image upper left
        corner). Under this change of referential, x axis is unchanged but y axis is inversed.

        Input: 4 args. 
        The 1st 2 args are tuples containing 2 floats each:  P1(x1, y1) and
        P2(x2, y2). 
        The 3rd arg is the centering precision in pixel, useful in determining 
        whether a slope needs calculating
        The 4th arg specifies whether the 360 degree (2Pi radians) segmentation in 8
        sectors is of type 'pict' or 'uniform'. By default "sector='pict'", defines
        sectors that do not correspond to the same solid angles:
        - 'EE','NN', 'WW', and 'SS': each correspond to a 60 degree or Pi/3 radians sector.
        - 'NE', 'NW', 'SW' and 'SE': each correspond to a 30 degree or Pi/6 radians sector.
        This is to reflect the fact that cardinal directions are more important in paintings
        than oblique directions centered on the two diagonals at 45 degrees.
        When "secseg='uniform'" on the other hand, every one of the 8 sectors has a solid 
        angle equal to 45 degrees or Pi/4 radians.

        Output: 2 strings with values in {'C','EE','NE','NN','NW','WW','SW','SS','SE'}, where
        'C', 'EE', 'NN', 'WW' and 'SS' stand for 'Centered', 'East', 'North', 'West' and 'South'
        respectively in the new axes referential described above. The repetition of the same 
        initial indicates a cardinal direction. For example 'NN' is due-North.
        '''

        rpos_1_2 = rpos_2_1 = ''

        # test whether 2 points are within circle with a given radius
        if abs(euclid_dist(p1, p2)) <= precision:
            rpos_1_2 = rpos_2_1 = 'C'
        else:
            x1, y1 = p1
            x2, y2 = p2
            if x1 == x2 or y1 == y2:
                if y1 > y2:              # infinite slope
                    rpos_1_2 = 'SS'
                    rpos_2_1 = 'NN'
                elif y1 < y2:
                    rpos_1_2 = 'NN'
                    rpos_2_1 = 'SS'
                elif x1 > x2:            # zero slope
                    rpos_1_2 = 'WW'
                    rpos_2_1 = 'EE'
                else:
                    rpos_1_2 = 'EE'
                    rpos_2_1 = 'WW'
            else:

                if sector == 'pict':
                    b_sec = m.pi/6
                    seg_lst = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
                elif sector == 'uniform':
                    b_sec = m.pi/8
                    seg_lst = [16, 15, 13, 12, 11, 9, 8, 7, 5, 4, 3, 1]
                else:
                    print(
                        'p2p_rpos() ValueError exception: unknown sector segmentation specified as kw argument.')
                    raise ValueError(
                        'Currently accepted function arguments include only "sector=\'pict\'" (default),\nor "sector=\'uniform\'". Execution aborted.')

                slope = -(y2-y1)/(x2-x1)
                theta = m.acos((1+slope**2)**-0.5)

                if slope > 0 and x1 > x2:
                    thetap = theta
                    if 0 <= thetap and thetap < b_sec*seg_lst[-1]:
                        rpos_1_2 = 'EE'
                        rpos_2_1 = 'WW'
                    elif b_sec*seg_lst[-1] <= thetap and thetap < b_sec*seg_lst[-2]:
                        rpos_1_2 = 'NE'
                        rpos_2_1 = 'SW'
                    elif b_sec*seg_lst[-2] <= thetap and thetap < b_sec*seg_lst[-3]:
                        rpos_1_2 = 'NN'
                        rpos_2_1 = 'SS'
                elif slope < 0 and y1 < y2:
                    thetap = m.pi - theta
                    if b_sec*seg_lst[-3] <= thetap and thetap < b_sec*seg_lst[-4]:
                        rpos_1_2 = 'NN'
                        rpos_2_1 = 'SS'
                    elif b_sec*seg_lst[-4] <= thetap and thetap < b_sec*seg_lst[-5]:
                        rpos_1_2 = 'NW'
                        rpos_2_1 = 'SE'
                    elif b_sec*seg_lst[-5] <= thetap and thetap < b_sec*seg_lst[-6]:
                        rpos_1_2 = 'WW'
                        rpos_2_1 = 'EE'
                elif slope > 0 and x1 < x2:
                    thetap = theta + m.pi
                    if b_sec*seg_lst[-6] <= thetap and thetap < b_sec*seg_lst[-7]:
                        rpos_1_2 = 'WW'
                        rpos_2_1 = 'EE'
                    elif b_sec*seg_lst[-7] <= thetap and thetap < b_sec*seg_lst[-8]:
                        rpos_1_2 = 'SW'
                        rpos_2_1 = 'NE'
                    elif b_sec*seg_lst[-8] <= thetap and thetap < b_sec*seg_lst[-9]:
                        rpos_1_2 = 'SS'
                        rpos_2_1 = 'NN'
                elif slope < 0 and y1 > y2:
                    thetap = 2*m.pi - theta
                    if b_sec*seg_lst[-9] <= thetap and thetap < b_sec*seg_lst[-10]:
                        rpos_1_2 = 'SS'
                        rpos_2_1 = 'NN'
                    elif b_sec*seg_lst[-10] <= thetap and thetap < b_sec*seg_lst[-11]:
                        rpos_1_2 = 'SE'
                        rpos_2_1 = 'NW'
                    elif b_sec*seg_lst[-11] <= thetap and thetap < b_sec*seg_lst[-12]:
                        rpos_1_2 = 'EE'
                        rpos_2_1 = 'WW'

        #return tuple(rpos_1_2, rpos_2_1)
        return rpos_1_2, rpos_2_1

    # Function bbx_visrel()

    def bbx_visrel(img_dict: dict) -> np.ndarray:
        '''
        'bbx_visres()'
        Admits up to 2 arguments as input.

        Input:
        - 1st arg: specially crafted json file, contains basic bbx info + metadata for one image.

        Output:
        - np.ndarray
        Each non diagonal elts of "visual relation array" (vra) are lists of length 4, 
        Each diagonal elts are lists of length 5.

        ON DIAGONAL COMPONENTS ('bbx1' = 'bbx2'):
          Each array element of type 'list' contains:
          - uniq_label = label + '_' + label_id
          - cpt = center point coords (xc,yc)
          - oloc = object's location on image plane; vertical and horizontal are divided 
            in the three regions t(op), c(enter), b(ottom) and l(eft), c(enter), r(ight)
            respectively. In the end the set of 'oloc' values is {'tl','cl','bl','tc',
            'cc','bc','tr','cr','br'}
          - rsa = percentage surface area of the bbx relative to that of entire image 
          - off = 'orientation and form-factor' attribute

        OFF DIAGONAL COMPONENTS  ('bbx1' != 'bbx2'):
          The 1st bbx' label ('bbx1') corresponds to the first array index while
          'bbx2' corresponds to the second index.  Each array element of type 'list'
          contains:
          - dcpt = (float) pairwise distance (center to center) between bbxes 
          - cpix = (float)  pairwise distance (closest two pixels) between bbxes
          - bro = (float) relative overlap of 'bbx1' onto 'bbx2' as a percentage of the total 
               surface area of 'bbx1'.
          - rpos = (string) relative position of 'bbx1' wrt to 'bbx2'
               'rpos' takes its values in {'NN','NE','EE','SE','SS','SW','WW','NW'}
        '''

        ulabels = list(img_dict['bbx'].keys())
        label_cnt = len(ulabels)

        try:
            assert(label_cnt == img_dict['bbx_cnt'])
        except AssertionError:
            print(f'Inconsistency in reported number of bbxes. Interrupt')
            raise ae

        # 'dtype=object' for ragged elts
        vra = np.empty([label_cnt, label_cnt], dtype=object)
        # access np.ndarray w/ 'vra[idx1,idx2]'
        for idx_1 in range(label_cnt):
            row = list()
            ulabel1 = ulabels[idx_1]
            label_cpts = ulabel1.split('_')
            label1 = '_'.join(label_cpts[:-1])
            label_id1 = label_cpts[-1]

            xmin1 = img_dict['bbx'][ulabel1]['xmin']
            ymin1 = img_dict['bbx'][ulabel1]['ymin']
            xmax1 = img_dict['bbx'][ulabel1]['xmax']
            ymax1 = img_dict['bbx'][ulabel1]['xmax']
            cpt1 = tuple(img_dict['bbx'][ulabel1]['cpt'].values())
            oloc1 = img_dict['bbx'][ulabel1]['oloc']
            rsa1 = img_dict['bbx'][ulabel1]['rsa']
            off1 = img_dict['bbx'][ulabel1]['off']
            #label1, label_id1, xmin1, ymin1, xmax1, ymax1 = bbx_loc[idx_1]

            for idx_2 in range(label_cnt):
                ulabel2 = ulabels[idx_2]
                label_cpts = ulabel2.split('_')
                label2 = '_'.join(label_cpts[:-1])
                label_id2 = label_cpts[-1]

                xmin2 = img_dict['bbx'][ulabel2]['xmin']
                ymin2 = img_dict['bbx'][ulabel2]['ymin']
                xmax2 = img_dict['bbx'][ulabel2]['xmax']
                ymax2 = img_dict['bbx'][ulabel2]['xmax']
                cpt2 = tuple(img_dict['bbx'][ulabel2]['cpt'].values())
                oloc2 = img_dict['bbx'][ulabel2]['oloc']
                rsa2 = img_dict['bbx'][ulabel2]['rsa']
                off2 = img_dict['bbx'][ulabel2]['off']
                #label2, label_id2, xmin2, ymin2, xmax2, ymax2 = bbx_loc[idx_2]

                if idx_1 == idx_2:
                    # print(ulabels[idx_1])
                    # row.append([uniq_label,(xc,yc)])
                    row.append([ulabel1, cpt1])
                    # row.extend([oloc, rsa, off])
                    row[idx_1].extend([oloc1, rsa1, off1])
                else:
                    dcpt = euclid_dist(cpt1, cpt2)
                    cpix = shortest_pix_dist(cpt1,
                                             (xmin1, ymin1, xmax1, ymax1),
                                             cpt2,
                                             (xmin2, ymin2, xmax2, ymax2))
                    bro_1_2, _ = bbx_overlap((xmin1, ymin1, xmax1, ymax1),
                                             (xmin2, ymin2, xmax2, ymax2))
                    # use bbx center points to situate 'bbx1' wrt 'bbx2'
                    rpos_1_2, _ = p2p_rpos(cpt1, cpt2)
                    row.append([dcpt, cpix, bro_1_2, rpos_1_2])

            vra[idx_1, :] = row

        return np.array(vra)

    #  ===============================
    # Processing
    #  ===============================

    try:
        assert('label_map' in globals())
    except AssertionError as ae:
        print(f'Error: variable \'label_map\' must be defined globally. Interrupt.')
        raise ae

    labels = list()
    obj_loc = list()
    obj_mtd = list()
    obj_mmttdd = list()
    img_dict = dict()

    img_dict['size'] = {'width': width, 'height': height}
    img_dict['bbx'] = dict()

    for idx, class_id in enumerate(detection_output['classes']):
        label = label_map[class_id]             # global variable: 'label_map'
        cnt = labels.count(label)
        labels.append(label)
        # differentiate between all (repeated) labels
        ulabel = label + '_' + str(cnt+1)
        img_dict['bbx'][ulabel] = dict()

        try:
            xmin = int(detection_output['bbx'][idx][0])
            ymin = int(detection_output['bbx'][idx][1])
            xmax = int(detection_output['bbx'][idx][2])
            ymax = int(detection_output['bbx'][idx][3])
            score = float(detection_output['scores'][idx])
        except (AttributeError, ValueError) as ave:
            print(
                ave + '\n ==> Missing or non-comforming type in either bbx coordinates or in label score.')
            #raise ave
        except Exception as ex:
            print(ex)
            print('Unsure about what happened. Probably unsafe to continue. Interrupt.')
            raise ex

        cpt = (xc, yc) = ((xmin + xmax)/2, (ymin + ymax)/2)
        oloc = ('t', 'c', 'b')[int(3*max(0, yc-1)/height)
                               ] + ('l', 'c', 'r')[int(3*max(0, xc-1)/width)]
        rsa, off = rsa_off(width, height, xmin, xmax, ymin, ymax)

        # populate new key with new value
        img_dict['bbx'][ulabel].update({'xmin': xmin,
                                        'ymin': ymin,
                                        'xmax': xmax,
                                        'ymax': ymax,
                                        'cpt': {'xc': xc, 'yc': yc},
                                        'oloc': oloc,
                                        'rsa': rsa,
                                        'off': off,
                                        'score': score})

    img_dict.update({'bbx_cnt': idx+1})
    vra = bbx_visrel(img_dict)          # object type: np.ndarray

    return img_dict, vra
