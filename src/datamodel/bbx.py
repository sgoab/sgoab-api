#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

from turtle import width


class BoundingBox():
    xmin: int = None
    ymin: int = None
    xmax: int = None
    ymax: int = None

    def __init__(self) -> None:
        pass

    def get_xmin(self) -> int:
        return self.xmin

    def get_ymin(self) -> int:
        return self.ymin

    def get_xmax(self) -> int:
        return self.xmax

    def get_ymax(self) -> int:
        return self.ymax

    def set_xmin(self, value: int):
        self.xmin = value

    def set_ymin(self, value: int):
        self.ymin = value

    def set_xmax(self, value: int):
        self.xmax = value

    def set_ymax(self, value: int):
        self.ymax = value


class ImageURL():
    url: str = None
    width: int = None
    height: int = None

    def __init__(self) -> None:
        pass


class PascalVOC():

    class BoundingBox():
        class_label: str = None
        bbx: list = None
        bbx_score: float = None

    # attributes
    imagepath: str = None
    image_width: int = None
    image_height: int = None
    europeana_id: str = None  # if europeana image
    datetime: str = None
    lang: str = None
    bbxes: list = None  # list bounding boxes
