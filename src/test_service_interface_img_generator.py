#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

from asyncore import write
import unittest
import numpy as np
import json

# tested modules
from enrichment import object_detection as od

# utils libraries
import libaux

from service_interface import ANNOTATION_SIMPLE, ANNOTATION_COMPLETE
from enrichment import object_detection
from enrichment.object_detection import output_plt, output_plt_save
from enrichment import utils as enrutils
import json


def create_input(item, output_default: str = ANNOTATION_SIMPLE):
    return {
        "item": item,
        "enrichments": ['object-detection'],
        "output-format": ANNOTATION_SIMPLE
    }


DHOME = "/laptop"


def ratio_to_px(input_bbxs: list, im_width: int, im_height: int) -> list:
    # convert ratio:float to px:int (bounding boxes)
    # conversion round down
    for bbx in input_bbxs:
        bbx[0] = int(float(bbx[0])*float(im_width))  # 'xmin'
        bbx[1] = int(float(bbx[1])*float(im_height))  # 'ymin'
        bbx[2] = int(float(bbx[2])*float(im_width))  # 'xmax'
        bbx[3] = int(float(bbx[3])*float(im_height))  # 'ymax'
    return input_bbxs


def percentage_to_px(input_bbxs: list, im_width: int, im_height: int) -> list:
    # convert ratio:float to px:int (bounding boxes)
    # conversion round down
    for bbx in input_bbxs:
        bbx[0] = int(float(bbx[0])*im_width/100)  # 'xmin'
        bbx[1] = int(float(bbx[1])*im_height/100)  # 'ymin'
        bbx[2] = int(float(bbx[2])*im_width/100)  # 'xmax'
        bbx[3] = int(float(bbx[3])*im_height/100)  # 'ymax'
    return input_bbxs


class TestServiceInterface(unittest.TestCase):

    def test_generate_images_one_bbx(self):
        files_in = ["mythological_eval1.txt"]
        dir_out = ['eval']

        import shutil
        import os

        in_dir = "/home/smendoza/tmp/sgoab-evaluation/in/"
        # Ele mythological bounding boxes generation
        #in_dir = "/home/smendoza/tmp/sgoab-evaluation/in/"

        for fin in range(0, len(files_in)):
            out_dir = '/home/smendoza/tmp/sgoab-evaluation/out/{}/'.format(
                dir_out[fin])
            # clear out directory
            shutil.rmtree(out_dir)
            os.makedirs(out_dir)

            # start generating
            with open("{}{}".format(in_dir, files_in[fin]), 'r') as f:
                lines = f.read().splitlines()
                imgpath = None
                classname = None

                for line in lines:
                    line = str(line)
                    # line1 = "https://lh3.googleusercontent.com/z9-BqRdsJ84_cAadStw1jkRjC3q366wwF4s2naW0y_PoCOzVQCjwYW-a2JDr5JA7Y0W69pPxxU1vPm-v2jjJfT2jxJFG=s0	person	100%(137,1529,681,1919) 100%(753,1479,1154,1844) 100%(544,1999,1189,2482) 100%(70,620,732,1182) 100%(854,258,1206,589) 100%(126,140,671,577) 100%(363,2038,684,2371) 100%(877,1464,1197,1852) 100%(133,2274,697,2580) 100%(18,633,566,927) 100%(164,2523,702,2755) 100%(2,287,517,525) 100%(22,1790,610,2071) 100%(140,1944,658,2296) 100%(116,1143,732,1610) 100%(83,421,709,816) 99%(254,2639,837,2884) 98%(502,3,1106,207) 98%(510,2351,1223,2789) 96%(0,0,321,181)"
                    # line2 = "	person	99%(1074,2161,1534,2463) 99%(819,1174,1146,1487) 99%(949,2098,1228,2271) 99%(1146,2009,1530,2214) 99%(745,1541,1087,1768) 98%(1080,1035,1565,1497) 98%(664,1764,886,1959) 97%(886,227,1249,372) 97%(852,2315,1316,2500) 96%(732,645,899,760) 95%(1039,301,1584,598) 95%(1137,647,1640,884) 94%(689,1495,958,1635) 94%(967,1713,1331,2167) 90%(674,2288,865,2436) 89%(758,127,1239,315) 89%(646,962,774,1027)"
                    lines_plitted = line.split('\t')
                    out_bbxs = []
                    # clean confidence & bbxes
                    if len(lines_plitted) == 3:
                        if lines_plitted[0] == '':
                            # do nothing
                            pass
                        else:
                            imgpath = lines_plitted[0]
                        classname = lines_plitted[1]
                        bounding_boxes = lines_plitted[2].split(' ')
                        for conf_bbx in bounding_boxes:
                            output = conf_bbx.replace(')', '').split('(')
                            out_confidence = output[0]
                            out_bbxs.append(output[1].split(','))

                        # convert bbx to px
                        im_width, im_height = libaux.image_width_height(
                            imgpath)
                        #out_bbxs = ratio_to_px(out_bbxs, im_width, im_height)
                        #out_bbxs = percentage_to_px(out_bbxs, im_width, im_height)
                        out_bbxs = percentage_to_px(out_bbxs, im_height, im_width)

                        # image generation for class 'aclass'
                        fig = enrutils.output_plt_bbx(
                            imgpath, out_bbxs, classname, img_is_URL=True)
                        import re
                        imgpath_clean = re.sub(r'[^\w]', '', imgpath)
                        filename = '{}_{}_{}'.format(
                            imgpath_clean, classname, len(out_bbxs))

                        outpath = enrutils.output_plt_save(
                            fig, imgpath, fname=filename, outdir=out_dir)
                        # print('{}\t{}'.format(line,outpath))
                        uri = 'http://growsmarter.bsc.es/static/europeana/{}/{}.jpg'.format(
                            dir_out[fin], filename)
                        print('{}'.format(uri))

            outpath = enrutils.output_plt_save(
                fig, imgpath, fname="test.jpeg", outdir='/tmp/')
        pass

    @unittest.skip("skipping test_enrichments_europeana")
    # test vis-rel model generation
    def test_generate_images(self):
        import service_interface
        si = service_interface.ServiceInterface()

        import hashlib
        with open(DHOME+"/home/smendoza/Downloads/in_brunos.json", 'r') as f:
            input = json.load(f)
            for item in input:
                qinput = create_input(item, output_default="JSON")
                out = si.unique_endpoint_enrichment(
                    item, ["object-detection"], output_format="JSON")
                out_bbxs = out['output']['object_detection']
                nbbxs = len(out_bbxs['bbx'])

                imgpath = out['img_path']
                classes = list(set(out_bbxs['classes-labels']))

                for aclass in classes:
                    bbx_list = []
                    # get all bbxes of aclass type
                    for ibbx in range(0, nbbxs):
                        if aclass == out_bbxs['classes-labels'][ibbx]:
                            bbx_list.append(out_bbxs['bbx'][ibbx])
                        else:
                            pass
                    # image generation for class 'aclass'
                    fig = enrutils.output_plt_bbx(
                        imgpath, bbx_list, aclass, img_is_URL=True)
                    import re
                    imgpath_clean = re.sub(r'[^\w]', '', imgpath)
                    filename = '{}_{}_{}'.format(
                        imgpath_clean, aclass, len(bbx_list))

                    outpath = enrutils.output_plt_save(
                        fig, imgpath, fname=filename, outdir=DHOME+'/tmp/')
                    with open(DHOME+'/home/smendoza/local/sgoab/evaluation.csv', 'a') as fout:
                        line = "{},{},{}\n".format(
                            filename, aclass, len(bbx_list))
                        fout.write(line)

            """
            for item in input:
                qinput = create_input(item, output_default="JSON")
                out = si.unique_endpoint_enrichment(
                    item, ["object-detection"], output_format="JSON")
                out_bbxs = out['output']['object_detection']
                nbbxs = len(out_bbxs['bbx'])
                for ibbx in range(0, nbbxs):
                    imgpath = out['img_path']
                    bbx = out_bbxs['bbx'][ibbx]
                    classname = out_bbxs['classes-labels'][ibbx]
                    fig = enrutils.output_plt(
                        imgpath, bbx, classname, img_is_URL=True)
                    
                    import re
                    imgpath_clean = re.sub(r'[^\w]', '', imgpath)
                    filename = '{}_{}_{}'.format(
                        imgpath_clean, classname, ibbx)

                    outpath = enrutils.output_plt_save(
                        fig, imgpath, fname=filename, outdir=DHOME+'/tmp/')
                    print(outpath)
            """
        pass

    @unittest.skip("skipping test_enrichments_europeana")
    # test vis-rel model generation
    def test_enrichments_europeana_large(self):

        import service_interface
        si = service_interface.ServiceInterface()

        europeana_id_list_path = "/home/smendoza/local/sgoab/sgoab-api/data/test/europeana_id_list.json"
        enrichments_output_filepath = "/home/smendoza/local/sgoab/sgoab-api/data/test/test-service_interface.out"
        europeana_id_list_ = []
        with open(europeana_id_list_path, 'r') as eidfile:
            europeana_id_list = json.load(eidfile)

        for europeana_id in europeana_id_list:
            input = create_input(europeana_id)
            output = si.unique_endpoint_enrichment(
                europeana_id, ["object-detection"])
            with open(enrichments_output_filepath, 'w') as outfile:
                outfile.write(outline)
            # print(outline)
            # outfile.writelines([outline])
        pass


if __name__ == '__main__':
    unittest.main()
