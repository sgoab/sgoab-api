#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################



import unittest
import json

# tested modules
from europeanifier import europeanify

# utils libraries
import libaux


def create_input(item):
    return {"item": item,
            "object_detection": True,
            "visual_relations": True,
            }


class TestEuropeanify(unittest.TestCase):

    # @unittest.skip("skipping test_enrichments_europeana")
    # test annotation model generation
    def test_europeanify_annotation(self):
        right = "http://www.webumenia.sk/dielo/SVK:SNG.O_156/stiahnut#xywh=240,156,276,324"

        imgURI = "http://www.webumenia.sk/dielo/SVK:SNG.O_156/stiahnut"
        europeana_id = "/07101/O_156"
        out = europeanify.europeanify_annotation(
            imagepath=imgURI, europeana_id=europeana_id, class_label='crucifixion', lang='en', bbx=[240, 156, 276, 324])
        print("test_europeanify: {}".format(out))
        print("test_europeanify: {}".format(json.loads(out)))
        pass

    # @unittest.skip("skipping test_enrichments_europeana")
    # test annotation image-URI + bbx model generation

    def test_europeanify_image_bbx(self):
        imgpath = "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg"

        bbx = [240, 156, 276, 324]
        out = europeanify.europeanify_image_bbx(imgpath, bbx)
        print("europeanify_image_bbx(): {}".format(out))
        # assert out == "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg#xywh=20,280,128,380"
        pass


if __name__ == '__main__':
    unittest.main()
