#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    label_map (list): list of labels of the object-detection classes. The identifier, is the position in the list. Module level variables may be documented in

.. _Google Python Style Guide:
   https://google.github.io/styleguide/pyguide.html

"""

from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
import requests
from flask import url_for
import json
import time
import logging

# Global variables (TBD as .env file)

label_map = ["BG", "crucifixion", "angel", "person", "crown of thorns", "horse", "dragon", "bird", "dog", "boat", "cat", "book",
             "sheep", "shepherd", "elephant", "zebra", "crown", "tiara", "camauro", "zucchetto", "mitre", "saturno", "skull",
             "orange", "apple", "banana", "nude", "monk", "lance", "key of heaven", "banner", "chalice", "palm", "sword", "rooster",
             "knight", "scroll", "lily", "horn", "prayer", "tree", "arrow", "crozier", "deer", "devil", "dove", "eagle", "hands",
             "head", "lion", "serpent", "stole", "trumpet", "judith", "halo", "helmet", "shield", "jug", "holy shroud", "god the father",
             "swan", "butterfly", "bear", "centaur", "pegasus", "donkey", "mouse", "monkey", "cow", "unicron"]

#######################################
# environment setup - taking variables
# env local / production
#######################################
import os
print(".environment - Before - TF_SERVER_URL: {}".format(os.getenv('TF_SERVER_URL')))
print(".environment - Before - SGOAB_API_ENRICHMENTS_OUTPUT_DIR: {}".format(os.getenv('SGOAB_API_ENRICHMENTS_OUTPUT_DIR')))

if os.getenv('TF_SERVER_URL') is None:
    TF_SERVER_URL = 'http://localhost:8501/v1/models/1:predict'
else:
    TF_SERVER_URL =os.getenv('TF_SERVER_URL')

if os.getenv('SGOAB_API_ENRICHMENTS_OUTPUT_DIR') is None:
    SGOAB_API_ENRICHMENTS_OUTPUT_DIR = "/home/smendoza/local/sgoab/search-engine-api/src/static/object-detection/output/"
else:
    SGOAB_API_ENRICHMENTS_OUTPUT_DIR = os.getenv('SGOAB_API_ENRICHMENTS_OUTPUT_DIR')

print(".environment - After - TF_SERVER_URL: {}".format(TF_SERVER_URL))
print(".environment - After - SGOAB_API_ENRICHMENTS_OUTPUT_DIR: {}".format(SGOAB_API_ENRICHMENTS_OUTPUT_DIR))

# env production
"""
TF_SERVER_URL = 'http://growsmarter.bsc.es:8501/v1/models/1:predict'
SGOAB_API_ENRICHMENTS_OUTPUT_DIR = "/tmp/object-detection/output/"
"""


def mapping_label(class_index: list, CLASS_LABELS: list = label_map) -> str:
    """ For a given class ID, returns its label.

    Args:
        class_index (list): [identifier of the class]
        CLASS_LABELS (list, optional): [list of classes. The class identifier is the position on the list]. Defaults to label_map.

    Returns:
        str: label of the class identifier. None if the identifier doesn't exist.
    """
    class_index = int(class_index)
    if class_index < len(CLASS_LABELS) and 0 < class_index:
        return CLASS_LABELS[class_index]
    else:
        # do nothing
        return None


def mapping_id(class_name: str, CLASS_LABELS: list = label_map) -> int:
    """For a given label (str), returns the identifer of the label.
    The identifier is the index in the list of classes.

    Args:
        class_name (str): [label of the class]
        CLASS_LABELS (list, optional): [list of classes.]. Defaults to label_map.

    Returns:
        int: [identifier of the class label]
    """
    return CLASS_LABELS.index(class_name)


def object_detection_labeller(class_ids: list) -> list:
    """[For a given list of class identifiers, returns a list with the respective labels. It keeps the same order.]

    Args:
        output (list): [list of class identifiers]

    Returns:
        [list]: [list with class labels of the input]
    """

    # add "key" with classes names
    classes_labels = []
    for class_id in class_ids:
        label = mapping_label(class_id)
        classes_labels.append(label)
    return classes_labels


def class_ids_to_int(class_ids: list) -> list:
    """For a given list of 'class ids' as floats , returns the list with ints.

    Args:
        class_ids (list): list with class ids as floats

    Returns:
        list: list with class ids as int
    """
    output = []
    for cid in class_ids:
        if isinstance(cid, (float, complex)):
            output.append(int(cid))

    return output


def load_image_into_numpy_array(image: Image) -> np.array:
    """[Reshape a list into a numpy-array without changing the data]

    Args:
        image (Image): [Image object]

    Returns:
        np.array: [np.array with the Image information]
    """

    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


def TF_serving_query(img_path: str, img_is_URL: bool = False, TF_SERVER_URL: str = TF_SERVER_URL) -> dict:
    """[Function for querying TF-server for the object-detection enrichment.
    Currently it queries the TensorFlow-server with a trained model for the SGoaB project.]

    Args:
        img_path (str): [URL or localpath of an image file. Accepted Image formats are [png|jpg|jpeg] ]
        img_is_URL (bool, optional): [True if the image is an URL. Otherwise, False.]. Defaults to False.
        TF_SERVER_URL (str, optional): [URI of the TF-server.]. Defaults to 'http://localhost:8501/v1/models/1:predict'.

    Returns:
        dict: [response with the object-detection results. Results contains bounding-boxes, scores and classes. ]
    """
    # get the image for querying
    image = None
    if img_is_URL == True:
        # URL image
        # default solution
        resp = requests.get(img_path, stream=True)
        #image = Image.open(resp.raw)

        # solution Gemäldegalerie Alte Meister (terrible dataset!) 
        import io
        image = Image.open(io.BytesIO(resp.content))
    else:
        # localpath image
        image = Image.open(img_path)
    im_width, im_height = image.size
    image_np = load_image_into_numpy_array(image)
    image_np_expanded = np.expand_dims(image_np, axis=0)

    # query TF-serving
    payload = {"instances": image_np_expanded.tolist()}
    start = time.perf_counter()
    response = requests.post(TF_SERVER_URL, data=json.dumps(payload))
    print(f"Took {time.perf_counter()-start:.2f}s")
    start = time.perf_counter()
    json_data = json.loads(response.text)
    bbx = json_data['predictions'][0]["detection_boxes"]
    scores = json_data['predictions'][0]["detection_scores"]
    classes = json_data['predictions'][0]["detection_classes"]

    print(f"Took {time.perf_counter()-start:.2f}s")
    return {
        "bbx": bbx,
        "scores": scores,
        "classes": classes,
    }


def TF_serving_object_detection(img_path: str, img_is_URL: bool = True, save_bbx_img: bool = True, score_threshold:float=0.4) -> dict:
    """[Function for querying the object-detection enrichment, label the classes, and save the image with the bounding boxes.]

    Args:
        img_path (str): [URL or localpath of an image file. Accepted Image formats are [png|jpg|jpeg] ]
        img_is_URL (bool, optional): [True if the image is an URL. Otherwise, False.]. Defaults to True.
        save_bbx_img (bool, optional): [True if an image with bounding boxes is requested. Otherwise, False.]. Defaults to True.

    Returns:
        dict: [object-detection results (i.e. bounding boxes, score and classes), class' labels (optional), and image-path with bounding boxes (optional)]
    """
    # query TF-serving
    output = TF_serving_query(
        img_path, img_is_URL=img_is_URL, TF_SERVER_URL=TF_SERVER_URL)

    # get class' labels
    output["classes-labels"] = object_detection_labeller(output["classes"])

    # classes ids to int (incoming floats)
    output["classes"] = class_ids_to_int(output["classes"])

    # clean the objects with scores > score_threshold (default: > 0.4)
    from enrichment import enrichment as enr
    output = enr.clean_object_detection_html(output, score_threshold=score_threshold)

    # save the image with bounding boxes
    if save_bbx_img:
        fig = output_plt(img_path, output, img_is_URL=img_is_URL, score_threshold=score_threshold)
        img_bbx_path = output_plt_save(fig, img_path)
        output["img-bbx-path"] = img_bbx_path

    return output


def output_plt(img_path: str, TF_serving_query_output: dict, img_is_URL: bool = False, score_threshold:float = 0.4):
    """[Creates an image (matplotlib.pyplot) with the bounding boxes from the object-detection.]

    Args:
        img_path (str): [image path. It can be URL or local path.]
        TF_serving_query_output (dict): [output from TF_serving_object_detection()]
        img_is_URL (bool, optional): [True if img_path is a URL. Otherwise, False.]. Defaults to False.

    Returns:
        [None]: [Returns nothing.]
    """

    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    fig = plt.figure(figsize=(30, 40), dpi=180)
    ax = fig.subplots()

    # load the image
    image = None
    if img_is_URL:
        # URL image
        image = Image.open(requests.get(img_path, stream=True).raw)
    else:
        # localpath image
        image = Image.open(img_path)

    im_width, im_height = image.size
    ax.imshow(image)

    # TF_serving_query_output = TF_serving_call(img_path)
    bbx = TF_serving_query_output["bbx"]
    scores = TF_serving_query_output["scores"]
    classes = TF_serving_query_output["classes"]

    for i in range(0, len(bbx)):
        if scores[i] > score_threshold:
            rect = patches.Rectangle((int(bbx[i][1]*im_width), int(bbx[i][0]*im_height)), int(bbx[i][3]*im_width)-int(
                bbx[i][1]*im_width), int(bbx[i][2]*im_height-int(bbx[i][0]*im_height)), linewidth=3, edgecolor="g", facecolor='none')
            ax.annotate(label_map[int(classes[i])], (int(
                bbx[i][1]*im_width), int(bbx[i][0]*im_height)), color="w", size=20)
            ax.add_patch(rect)
    return plt  # figure.subplots()


def output_plt_show(figure_plt: plt):
    """[Shows locally the image (matplotlib.pyplot). It has the image with the bounding boxes from the object-detection.]

    Args:
        figure_plt ([plt]): [Figure matplotlib.pyplot]
    """
    figure_plt.show()


def output_plt_save(figure_plt: plt, img_path: str, outdir: str = SGOAB_API_ENRICHMENTS_OUTPUT_DIR) -> str:
    """[Saves locally the image (matplotlib.pyplot) with the bounding boxes.]

    Args:
        figure_plt (plt): [description]
        img_path (str): [description]
        outdir (str, optional): [description]. Defaults to SGOAB_API_ENRICHMENTS_OUTPUT_DIR.

    Returns:
        str: [public URL of the saved image with the bounding boxes. Accessible URL in the web-services server.]
    """
    # savefig(filename, dpi=None, format='png', bbox_inches='tight', pad_inches=0.2, bbox=None, pad=None, dashes=None, loc='upper left', rot=0, vmax='I', vmin='I', hmax='I', hmin='I')
    import base64
    filename = base64.b64encode(img_path.encode('utf-8'))
    filepath_local = "{dir}{img_path}.jpg".format(
        dir=outdir, img_path=filename)
    figure_plt.savefig(filepath_local, dpi=60, pil_kwargs={'quality': 40})

    # image-url for sgoab-ui
    filepath_url = url_for(
        'static', filename="/object-detection/output/{}.jpg".format(filename))
    print("FILE-URL : "+filepath_url)

    return filepath_url


if __name__ == "__main__":
    img_path = "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg"
    # TF-serving call for object detection
    output = TF_serving_object_detection(img_path, img_is_URL=True)

    # Local matplot example for showing the bounding boxes in the image (rectangles)
    figure = output_plt(img_path, output, img_is_URL=True)
    # output_plt_show(figure)
    output_plt_save(figure, "/tmp/tf_serving_object_detection-latest.jpg")

    # print(json.dumps(output))
