#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

from queue import Queue
import queue
import shutil
import json
from matplotlib import image
from numpy import array
import requests
import logging
import hashlib
from enrichment import object_detection as od

# sgoab-modules
from enrichment import enrichmentEF

# debugging
import traceback

# import local modules
import libaux


class EnrichmentQuery():
    id: int = None
    query_id: str = None
    #input: str = None
    img_path: str = None
    object_detection: bool = False
    visual_relations: bool = False
    output: dict = None
    status: int = None
    ENRICHMENTS_OUTPUT_DIR = "/home/smendoza/tmp/sgoab/enrichments"

    all_status = {
        100: "pending",  # not started yet
        200: "execution",
        201: "execution (object-detection)",
        202: "execution (visual-relations)",
        300: "paused",
        400: "fail",
        500: "success (visual-relations)"
    }

    def __init__(self, item: str, object_detection: bool, visual_relations: bool) -> None:
        """_summary_

        Args:
            item (str): [URL | Path] of the Painting
            object_detection (bool): object-detection enrichment
            visual_relations (bool): visual-relations enrichment
        """
        self.queryID = str(hashlib.md5(item.encode('utf-8')).hexdigest())
        #self.input:dict = item
        self.img_path: str = item
        self.object_detection = object_detection
        self.visual_relations = visual_relations
        self.output = None
        self.status = 0
        self.outdir = "{}/{}".format(self.ENRICHMENTS_OUTPUT_DIR,
                                     self.query_id)

    def dumps(self,) -> dict:
        return self.__dict__

    def get_id(self) -> int:
        return self.id

    def get_queryID(self) -> str:
        return self.queryID

    def get_item(self) -> str:
        """returns the image [PATH|URL] to be queried for enrichment

        Returns:
            _type_: _description_
        """
        return self.img_path

    def get_img_path(self,):
        return self.img_path

    def get_output(self) -> dict:
        return self.output

    def get_status(self) -> str:
        return self.all_status[self.status]

    def has_object_detection(self,) -> bool:
        return self.object_detection
        """
        if self.input["object_detection"]:
            return True
        else:
            return False
        """

    def has_visual_relations(self,) -> bool:
        return self.visual_relations
        """
        if self.input["visual_relations"]:
            return True
        else:
            return False
        """

    def set_id(self, value: int):
        self.id = value

    def set_img_path(self, value: str):
        self.img_path = value

    def set_queryID(self, value: str):
        self.queryID = value

    def set_object_detection(self, value: bool):
        self.object_detection = value

    def set_visual_relations(self, value: bool):
        self.visual_relations = value

    def set_input(self, value: dict):
        self.input = value

    def set_output(self, value: dict):
        self.output = value

    def set_status(self, value: int):
        self.label = value

    def run(self,):
        self.set_status(200)

    def pause(self,):
        self.set_status(300)

    def fail(self,):
        self.set_status(400)

    def finish(self,):
        self.set_status(500)


class EnrichmentQueryQueue():
    MAXSIZE_QUEUE_ENRICHMENTS = 50000
    get_timeout_default = 200  # 200 seconds
    get_block_default = False  # 200 seconds

    from queue import Queue
    queue_query_enrichments: Queue() = None
    query_dictionary: dict() = None

    def __init__(self, maxsize=MAXSIZE_QUEUE_ENRICHMENTS):
        self.queue_query_enrichments = Queue(maxsize=maxsize)
        self.query_dictionary = dict()

    def add(self, qe: EnrichmentQuery):
        if not self.queue_query_enrichments.full():
            self.queue_query_enrichments.put(qe)
            self.query_dictionary[qe.get_queryID()]
            return True
        else:
            return False

    def next(self, qe: EnrichmentQuery) -> EnrichmentQuery:
        next_qe = self.queue_query_enrichments.get()

    def get(self, qe: EnrichmentQuery) -> EnrichmentQuery:
        return self.queue_query_enrichments.get()

    def put(self, qe: EnrichmentQuery) -> EnrichmentQuery:
        return self.queue_query_enrichments.put(qe)

    def full(self) -> bool:
        """[Return True if the queue is full, False otherwise. Because of multithreading/multiprocessing semantics, this is not reliable.]

        Returns:
            bool: [True if the queue is full, False otherwise.]
        """
        return self.queue_query_enrichments.full()

    def qsize(self):
        return self.queue_query_enrichments.qsize()

    def empty(self) -> bool:
        """[Return True if the queue is empty, False otherwise. Because of multithreading/multiprocessing semantics, this is not reliable.]

        Returns:
            bool: [True if the queue is empty, False otherwise]
        """
        return self.queue_query_enrichments.empty()


class EnrichmentQueryManager():

    from queue import Queue
    queue_query_enrichments: Queue() = None
    query_dictionary: dict() = None

    def __init__(self,):
        self.queue_query_enrichments = Queue()
        self.query_dictionary = dict()

    def add_query_json(self, input: dict) -> list:
        """add query, from dict, to the queue. 

        Args:
            input (dict): dict with images [path | URLs], and the enrichments
        """

        items = input["items"]
        object_detection = input["object_detection"]
        visual_relations = input["visual_relations"]
        query_ids = self.add_queries(items, object_detection, visual_relations)
        return query_ids

    def add_queries(self, items: list, object_detection: str, visual_relations: str) -> list:
        """returns a list with the identifiers of the queries

        Args:
            items (list): [URL | path] of the images enqueued for enrichment  o
            object_detection (str): true if object_detection enrichment is required
            visual_relations (str): true if visual-relations enrichment is required
        Returns:
            list: identifier of the query_ids in the queue
        """
        # one query per image (although they come from the same input/request)
        query_ids = []
        for item in items:
            queryID = self.add_query(item, object_detection, visual_relations)
            query_ids.append(queryID)
        return query_ids

    def add_query(self, item: str, object_detection: bool, visual_relations: bool) -> str:
        """Enquement of the item. Not executed (See exec()/run()). Returns identifier of the query for the enrichment. With the identifier, it'll be possible to request execution, get results, etc.

        Args:
            items (str): [URL | path] of the image enqueued for enrichment
            object_detection (str): true if object_detection enrichment is required
            visual_relations (str): true if visual-relations enrichment is required
        Returns:
            list: identifier of the query_ids in the queue
        """
        # Query just enqueued. Not executed.
        eq = EnrichmentQuery(item, object_detection, visual_relations)
        # add the query in the queue (it will be used for keep order)
        self.queue_query_enrichments.put(eq)
        # add the query to dictionary
        self.query_dictionary[eq.get_queryID()] = eq
        return eq.get_queryID()

    def exec_query(self, query_id: str) -> EnrichmentQuery:
        # TBD: With queue it's impossible
        # maybe from collections import OrderedDict
        pass

    def exec_next(self,) -> EnrichmentQuery:
        # get next Query
        query: EnrichmentQuery = self.queue_query_enrichments.get()
        return self.exec(query)

    def exec(self, query: EnrichmentQuery) -> EnrichmentQuery:
        imagepath: str = query.get_img_path()

        query_output = {}
        if query.has_object_detection():
            results_od = od.TF_serving_object_detection(
                imagepath, save_bbx_img=False)
            query_output["object_detection"] = results_od
        if query.has_visual_relations():
            # updated February 2023
            from service_interface import ServiceInterface
            #results_visrel = ServiceInterface().visual_relation(imagepath, query_output["object_detection"])
            output_object_detection = query_output['object_detection'].copy()
            results_visrel = ServiceInterface().visrel(imagepath, output_object_detection)
            query_output["results_visrel"] = results_visrel

        query.set_output(query_output)  # save output
        query.finish()  # set status 'finished'
        return query

    def run_query(self, query_id: str) -> dict:
        eq = self.get_query(query_id)
        if isinstance(eq, EnrichmentQuery):
            self.exec(eq)
            return eq.dumps()
        else:
            # do nothing
            return None

    def get_query(self, query_id: str) -> EnrichmentQuery:
        if query_id in self.query_dictionary.keys():
            return self.query_dictionary[query_id]
        else:
            # The query doesn't exist
            return None

    def isfinished(self, query_id: str) -> bool:
        query = self.get_query(query_id)
        if query.get_status == 500:
            return True
        else:
            return False

    def arefinished(self, queries_id: list) -> bool:
        for query_id in queries_id:
            if not self.isfinished(query_id):
                return False
        return True

    def enrichment_query_service_interface(self, imagepath: str, object_detection: bool = True, visual_relations: bool = False) -> dict:
        """[Returns the enrichments for the given images.
        Available enrichments are:
        1. object-detection
        2. visual-relations
        ]

        Args:
            imagepaths (list): [list with the [URL|localpath] of the images to be queried for enrichment]
            object_detection (bool, optional): [True if querying for object-detection enrichments. False otherwise.]. Defaults to True.
            visual_relations (bool, optional): [True if querying for visual-relations enrichments. False otherwise.]. Defaults to False.

        Returns:
            dict: [has the specified enrichments for the given images]
        """

        # results[imagepath] = self.enrichment_query_process_image(
        # imagepath, outdir=ENRICHMENTS_OUTPUT_DIR, object_detection=object_detection, visual_relations=visual_relations)
        #results[imagepath] = object_detection.TF_serving_object_detection(imagepath, outdir=outdir, object_detection=object_detection, visual_relations=visual_relations)
        results = od.TF_serving_object_detection(imagepath)
        return results


class EnrichmentQueryExecution(EnrichmentQuery):

    query: EnrichmentQuery = None

    def __init__(self, input) -> None:
        super().__init__(input)

    def set_query(self, query: EnrichmentQuery):
        self.query = query

    def get_query(self, query: EnrichmentQuery):
        return self.query

    def run(self,):
        imagepath = self.query.get_imagepath()
        object_detection = self.query.has_object_detection()
        visual_relations = self.query.has_visual_relations()
        self.query.run()
        output = {}
        if self.query.has_object_detection():
            output['object_detection'] = self.run_object_detection(
                imagepath, outdir=self.query.outdir, object_detection=True, visual_relations=False)
            if self.query.has_visual_relations():
                output['visual_relations'] = self.run_visual_relations(
                    imagepath, output['object_detection'])

        self.query.set_output = output
        self.query.finish()

    def run_object_detection(self, imagepath: str, outdir: str, object_detection: bool = True, visual_relations: bool = False) -> dict:
        import object_detection
        return object_detection.TF_serving_object_detection(imagepath, outdir=outdir, object_detection=object_detection, visual_relations=visual_relations)

    def run_visual_relations(imagepath: str, object_detection_output: dict):
        from PIL import Image
        import vis_rel
        image = Image.open(imagepath)
        im_width, im_height = image.size

        img_dict, vra = vis_rel.img_data_model(
            object_detection_output, im_width, im_height)
        pass

    def set_outdir(self, outdir: str, rmdir: bool = True) -> str:
        """[Creates the output directories for saving generated enrichments:
        1. Directory for images
        1. Directory for image-metadata
        ]

        Args:
            outdir (str): [creates directory with the path `outdir`.]
            rmdir (bool): [If True, removes the existing directory with the same path. False otherwise. ]

        Returns:
            str: [path of directory for storing images]
            str: [path of directory for storing images-metadata]
        """
        outdir_metadata = "{}/metadata/".format(outdir)
        outdir_imgs = "{}/data/".format(outdir)

        # prepare output
        import os
        if os.path.exists(outdir_metadata) and rmdir:
            shutil.rmtree(outdir_metadata)
            os.makedirs(outdir_metadata)
        else:
            os.makedirs(outdir_metadata)
        if os.path.exists(outdir_imgs) and rmdir:
            shutil.rmtree(outdir_imgs)
            os.makedirs(outdir_imgs)
        else:
            os.makedirs(outdir_imgs)

        return outdir_imgs, outdir_metadata


if __name__ == "__main__":

    query1 = """{
        "items": [
            "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
            "http://data.europeana.eu/item/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj20822887"
        ],
        "object_detection": "True",
        "visual_relations": "True"
    }"""
    eqm = EnrichmentQueryManager()
    q = json.loads(query1)
    eqm.add_query_json(q)
    eqm.exec_next()
