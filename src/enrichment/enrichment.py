#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module enrichment
    * You have to also use ``sphinx.ext.todo`` extension

.. _Google Python Style Guide:
   https://google.github.io/styleguide/pyguide.html

"""


# production
import shutil
import logging
import json
import requests

# debugging
import traceback


OBJECT_DETECTION = "RCNN"
VISUAL_RELATION = "VISUAL_RELATIONS"

from enrichment import enrichmentEF


def TheEnrichment(entities, enrichment_req):
    '''
    input: 
    - list of entities to enrich(URI , metadata)
    - list of enrichments: ["RCNN", "VIS-REL"]
    output: 
    - list of enrichment (e.g. JSON, dictionary: URI -> enrichment)
    '''

    for i in entities:
        if OBJECT_DETECTION in enrichment_req:
            # call methos OBJECT-DETECTION
            res_obj_det = the_enrichment_object_detection()
            pass
        if VISUAL_RELATION in enrichment_req:
            # call methos VISUAL-RELATION
            if not OBJECT_DETECTION in enrichment_req:
                # can't execute vis-rel without object detection
                logging.error(
                    "Cannot execute vis-rel without object detection")
                pass
            else:
                res_vis_rel = the_enrichment_visual_relations()
            pass
        res = merge_enrichments(res_obj_det, res_vis_rel)


def merge_enrichments():

    pass


def get_img_from_URL(img_url: str, out_path: str) -> bool:
    """[Saves an image from URL on the specified output_path.]

    Args:
        img_url (str): [URL of the image to be saved. ]
        out_path (str): [path where the image will be saved.]

    Returns:
        bool: [True if successful. False otherwise.]
    """
    try:
        # 2. download the image from URL
        res = requests.get(img_url, stream=True, timeout=10)
        if res.status_code == 200:
            # download the image
            with open(out_path, 'wb') as f:
                shutil.copyfileobj(res.raw, f)
                logging.info('Image sucessfully Downloaded: ', out_path)
            return True
        else:
            logging.info(
                "WARNING: {img_url} couldn't be downloaded".format(img_url=img_url))
            return False
    except requests.exceptions.ReadTimeout:
        traceback.print_exc()
        logging.info("requests.exceptions.ReadTimeout")
        logging.info("BROKEN IMAGE LINK from EF : {}".format(out_path))
        return False
        # except NewConnectionError
    except Exception as e:
        traceback.print_exc()
        logging.info("BROKEN IMAGE LINK from EF-ID : {}".format(img_url))
        return False
    else:
        return False
        pass
    return False


def get_download_image_URL(img_url: str, outdir: str, EF_input: bool) -> dict:
    """[Saves an image from a URL. The URLs can be linked to:
    1. image on the internet [png|jpeg|jpg]
    2. Europeana items URL
    ]

    Args:
        img_url (str): [URL of the image]
        outdir (str): [Local path where image will be saved]
        EF_input (bool): [True, if img_url is an Europeana item. False otherwise].

    Returns:
        dict: [dictionary with information of the download (e.g. filepath, filename, successful download)]
    """

    output = None
    img_uri = None
    if EF_input:
        # get image URL from EF API
        img_uri = enrichmentEF.get_europeana_id(img_uri)
        img_url = enrichmentEF.get_image_EF_URL(img_uri)
    else:
        img_uri = img_url

    # name cannot have forward slash '/'. (Not allowed in ext3, ext4, etc.)
    out_fname = enrichmentEF.gen_readable_img_id(img_url)
    # generate output path
    out_path = "{dir}{filename}.jpeg".format(
        dir=outdir, filename=out_fname)

    download_success = get_img_from_URL(img_url, out_path)

    # preparing the output
    output = {
        "img-uri": img_uri,
        "img-url": img_url,
        "filepath": out_path,
        "filename": out_fname,
        "success": download_success
    }

    return output

def is_URL(input: str) -> bool:
    if input[0:4] == "http":
        # TO-improve identification of URI / localpath
        return True
    else:
        return False


def get_enrichment_object_detection(imagepath: str) -> dict:
    """[Calls the Object-Detection WS for image enrichment.]

    Args:
        imagepath (str): [URI of the image to be enriched with object-detection]

    Returns:
        dict: [object-detection enrichment]
    """

    # TODO : visrel call object detection
    object_detection_output = '''{"annotation":{"folder":"images","filename":"00000571.jpg","path":"/home/bscuser/BSC/datasets/final_data_coco_2/images/00000571.jpg","source":{"database":"Unknown"},"size":{"width":"193","height":"198","depth":"3"},"segmented":"0","object":[{"name":"bird","pose":"Unspecified","truncated":"0","difficult":"0","bndbox":{"xmin":"51","ymin":"28","xmax":"121","ymax":"85"}},{"name":"jug","pose":"Unspecified","truncated":"0","difficult":"0","bndbox":{"xmin":"62","ymin":"99","xmax":"124","ymax":"163"}}]}}'''

    # identify URL | localpath
    img_is_URL = is_URL(imagepath)

    # query the object_detection TF-server
    from object_detection import service_interface
    output = service_interface.TF_serving_object_detection(
        imagepath, img_is_URL=img_is_URL)

    return output


def get_enrichment_visual_relations(imagepath: str, object_detection_output: dict) -> dict:
    """[ This function gives visual-relations enrichment for the given image [ URL | localpath]
    - imagepath we're talking about
    - object-detection input. Necessary for running visual relations]

    Args:
        imagepath (str): [URL | localpath] of the image to be enriched with visual-relations
        object_detection_output (dict): [Output from object-detection WS. It's requirement for running visual relations]

    Returns:
        dict: [results from the visual-relations WS]
    """

    # TODO : visrel call

    visrel_output = '''{"filename":"00001659.jpg","size":{"width":1000,"height":705},"bbx_basic":{"sheep_1":{"xmin":932,"ymin":482,"xmax":988,"ymax":595,"cpt":{"xc":960,"yc":538.5},"oloc":"br","rsa":0.9,"off":0.0809},"sheep_2":{"xmin":858,"ymin":484,"xmax":922,"ymax":578,"cpt":{"xc":890,"yc":531},"oloc":"br","rsa":0.85,"off":0.0426},"cow_1":{"xmin":736,"ymin":382,"xmax":842,"ymax":511,"cpt":{"xc":789,"yc":446.5},"oloc":"cr","rsa":1.94,"off":0.0326},"sheep_3":{"xmin":430,"ymin":463,"xmax":567,"ymax":631,"cpt":{"xc":498.5,"yc":547},"oloc":"bc","rsa":3.26,"off":0.044},"cow_2":{"xmin":528,"ymin":467,"xmax":750,"ymax":619,"cpt":{"xc":639,"yc":543},"oloc":"bc","rsa":4.79,"off":-0.07},"tree_1":{"xmin":474,"ymin":268,"xmax":686,"ymax":453,"cpt":{"xc":580,"yc":360.5},"oloc":"cc","rsa":5.56,"off":-0.027},"person_1":{"xmin":384,"ymin":378,"xmax":423,"ymax":459,"cpt":{"xc":403.5,"yc":418.5},"oloc":"cc","rsa":0.45,"off":0.0596},"person_2":{"xmin":205,"ymin":480,"xmax":269,"ymax":557,"cpt":{"xc":237,"yc":518.5},"oloc":"bl","rsa":0.7,"off":0.0184},"shepherd_1":{"xmin":368,"ymin":371,"xmax":443,"ymax":475,"cpt":{"xc":405.5,"yc":423},"oloc":"cc","rsa":1.11,"off":0.0411},"boat_1":{"xmin":28,"ymin":502,"xmax":363,"ymax":603,"cpt":{"xc":195.5,"yc":552.5},"oloc":"bl","rsa":4.8,"off":-0.234}},"bbx_added":{},"bbx_cnt":10}'''

    # return visrel_output
    return '{"TODO": True}'





def clean_object_detection_html(out_obj_det: dict, score_threshold: float = 0.4) -> dict:
    """[This function "cleans" the object-detection results. It means
    1. Remove detected objects with scores under `score`]

    Args:
        out_obj_det (dict): [output results from object-detection WS]
        score_threshold (float, optional): [removes detected-objects with scores below `score`]. Defaults to 0.4.

    Returns:
        dict: [the output from object-detection, without the classes with a score below `score`]
    """ 
    indexes_to_keep = []
    for ind, val in enumerate(out_obj_det["scores"]):
         # print("INDEX: {} - VAL {}".format(ind, val))
        if float(val) > float(score_threshold):
            indexes_to_keep.append(ind)
            #print("INDEX: {}".format(ind))

    tmp_scores = []
    tmp_bbx = []
    tmp_classes = []
    tmp_classes_labels = []
    for i in indexes_to_keep:
        tmp_scores.append(out_obj_det["scores"][i])
        tmp_bbx.append(out_obj_det["bbx"][i])
        tmp_classes.append(out_obj_det["classes"][i])
        tmp_classes_labels.append(out_obj_det["classes-labels"][i])

    out_obj_det["scores"] = tmp_scores
    out_obj_det["bbx"] = tmp_bbx
    out_obj_det["classes"] = tmp_classes
    out_obj_det["classes-labels"] = tmp_classes_labels

    return out_obj_det


def is_europeana_uri(image_uri: str) -> bool:
    """[Returns True if `image_uri` is the an Europeana item. Otherwise, returns False.]

    Args:
        image_uri (str): [URL of Europeana item or image ]

    Returns:
        bool: [True if `image_uri` is an Europeana item URL. Otherwise, False.]
    """

    # identify if URL is Europeana's ITEM
    EF_ITEM_URL = "http://data.europeana.eu/item"
    if EF_ITEM_URL in image_uri:
        return True
    else:
        return False




if __name__ == "__main__":
    input = {
        "items": [
            "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
            "http://data.europeana.eu/item/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj20822887"
        ],
        "object_detection": "True",
        "visual_relations": "True"
    }
    for imagepath in input["items"]:
        enrichment_query_service_interface(
            imagepath, object_detection=True, visual_relations=False)
    pass
