#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


'''
This file offers the low-layer code for generating the enrichments wherever it cames from (EF, Alejandría's Library, M.del Prado).
'''


# production
import requests
import shutil
import json
import enrichment

# debugging
import traceback
import logging

OBJECT_DETECTION = "RCNN"
VISUAL_RELATION = "VISUAL_RELATIONS"

#######################################
# environment setup - taking variables
# env local / production
#######################################
import os
print(".environment - Before EF - EF_API_URL: {}".format(os.getenv('EF_API_URL')))
print(".environment - Before EF - EF_API_KEY: {}".format(os.getenv('EF_API_KEY')))

if os.getenv('EF_API_URL') is None:
    EF_API_URL = 'https://api.europeana.eu/record/v2'
else:
    EF_API_URL =os.getenv('EF_API_URL')

if os.getenv('EF_API_KEY') is None:
    EF_API_KEY = "api2demo"
else:
    EF_API_KEY =os.getenv('EF_API_KEY')
print(".environment - After EF - EF_API_URL: {}".format('EF_API_URL'))
print(".environment - After EF - EF_API_KEY: {}".format('EF_API_KEY'))


def enrichment_EF():

    pass


def TheEnrichment(entities, enrichment_req):
    '''
    input: 
    - list of entities to enrich(URI , metadata)
    - list of enrichments: ["RCNN", "VIS-REL"]
    output: 
    - list of enrichment (e.g. JSON, dictionary: URI -> enrichment)
    '''

    for i in entities:
        if OBJECT_DETECTION in enrichment_req:
            # call methos OBJECT-DETECTION
            res_obj_det = the_enrichment_object_detection()
            pass
        if VISUAL_RELATION in enrichment_req:
            # call methos VISUAL-RELATION
            if not OBJECT_DETECTION in enrichment_req:
                # can't execute vis-rel without object detection
                logging.error(
                    "Cannot execute vis-rel without object detection")
                pass
            else:
                res_vis_rel = the_enrichment_visual_relations()
            pass
        res = merge_enrichments(res_obj_det, res_vis_rel)


def merge_enrichments():

    pass


def the_enrichment_object_detection():
    """
    Calls the Object Detection
    """
    pass
    return None


def the_enrichment_visual_relations():
    """
    Calls the Visual-Relations
    """
    pass
    return None


def gen_readable_img_id(ef_id):
    '''
    returns writable alphanumeric. Avoids misunderstandings with directorys with slash '/'
    '''

    # 1. EF_ID : URIs without slash '/'
    splitted = ef_id.split('/')
    new_ef_id = '-'.join(splitted[1:])

    # 2. MD5 of EF_ID
    '''
    import hashlib
    hash_md5 = hashlib.md5(ef_id)
    '''

    return new_ef_id


"""
@ deprecated?
def get_metadata_EF():
    # for multiple files
    #get_metadata_EF(ef_id = "/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj00077541", outdir="/tmp")
    #out_path = "{dir}{filename}.metadata".format(dir=outdir, filename=ef_id)
    pass
"""


def get_metadata_EF(ef_id="/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj00077541", outdir="/tmp"):
    EF_API_URI_1 = EF_API_URL+"/record" 
    EF_API_URI_2 = ".json?wskey={EF_API_KEY}".format(EF_API_KEY=EF_API_KEY)

    url = "{EF_API_URI_1}{EF_ID}{EF_API_URI_2}".format(
        EF_API_URI_1=EF_API_URI_1, EF_ID=ef_id, EF_API_URI_2=EF_API_URI_2)

    example = "https://api.europeana.eu/api/v2/record/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj00077541.json?wskey=api2demo"

    # get metadata from EF
    # JSON format
    resp = requests.get(url)

    try:
        #resp = resp.json()
        resp_json = json.loads(str(resp.text))
        #write in file
        out_path = "{dir}{filename}.metadata".format(
            dir=outdir, filename=gen_readable_img_id(ef_id))
        with open(out_path, "w") as fileout:
            json.dump(resp_json, fileout)

        return True, resp_json, out_path

    except ValueError:
        logging.error("JSON loads error. RESPONSE:{}".format(resp.content))
        return False, None, None
    else:
        if resp_json["success"] != (True or "true"):
            logging.error(
                "EF response without success. EF metadata RESPONSE:{}".format(resp.content))
        pass
        return False, None, None

    return resp_json


def get_europeana_id(europeana_url: str) -> str:
    """For a given europeana item URL, return the Europeana identifier. The item URL must have this format https://www.europeana.eu/item/<europeana-id> 

    Args:
        europeana_url (str): europeana item. Non-relative URL with format https://www.europeana.eu/item/<europeana-id> 

    Returns:
        str: Europeana item identifier
    """
    #europeana_baseurl = "https://data.europeana.eu/item"
    europeana_baseurl = "https://www.europeana.eu/item"
    # check if the input URL is an europeana item URL
    if europeana_baseurl in europeana_url:
        return europeana_url.split(europeana_baseurl)[1]
    else:
        return europeana_url


def get_image_EF_URL(ef_id):
    # EF_LINK_IMAGE = "https://www.europeana.eu/api/v2/search.json?wskey=api2demo&query=europeana_id:%22/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj20428548%22&profile=facets&facet=provider_aggregation_edm_isShownBy&rows=0&f.provider_aggregation_edm_isShownBy.facet.limit=20"
    #EF_LINK_IMAGE_1 = "https://www.europeana.eu/api/v2/search.json?wskey=" # @deprecated 21-10-2022
    EF_LINK_IMAGE_1 = EF_API_URL+"/search.json?wskey="
    EF_LINK_IMAGE_2 = "&query=europeana_id:%22"
    EF_LINK_IMAGE_3 = "%22&profile=facets&facet=provider_aggregation_edm_isShownBy&rows=0&f.provider_aggregation_edm_isShownBy.facet.limit=20"
    EF_LINK_IMAGE = "{EF_LINK_IMAGE_1}{EF_API_KEY}{EF_LINK_IMAGE_2}{EF_ID}{EF_LINK_IMAGE_3}".format(
        EF_LINK_IMAGE_1=EF_LINK_IMAGE_1, EF_API_KEY=EF_API_KEY, EF_LINK_IMAGE_2=EF_LINK_IMAGE_2, EF_ID=ef_id, EF_LINK_IMAGE_3=EF_LINK_IMAGE_3)

    logging.info("EF Search API query: {}".format(EF_LINK_IMAGE))

    # 1. get the image URL
    resp = requests.get(EF_LINK_IMAGE, stream=True, timeout=5)
    resp_json = resp.json()
    EF_img_url = None
    if "facets" in resp_json.keys():
        EF_img_url = resp_json["facets"][0]["fields"][0]["label"]
        print(EF_img_url)
    else:
        logging.error("BROKEN LINK?: "+ef_id)
        pass

    return EF_img_url


def debug_EF_csv():
    '''
    method for testing EF metadata & images(Eleftheria's)
    input:
    - metadata: come from EF API
    - images: come from URI that referring any other place
    '''

    import pandas as pd

    # making dataframe
    df = pd.read_csv(EF_CSV_FILE)
    with open(EF_CSV_FILE, 'r') as file:
        for ef_id in df['Europeana ID']:
            resp = get_metadata_EF(ef_id, outdir=OUTPUT_METADATA)
            logging.info(str(resp)[0:150])
            #resp = get_image_EF(value, outdir=OUTPUT_IMGS)
            enrichment.get_download_image_URL(
                ef_id, None, OUTPUT_IMGS, from_EF=True)
            # logging.info(str(resp)[0:150])

    #resp = get_metadata_EF()
    #resp = get_image_EF()


if __name__ == "__main__":
    # "https://api.europeana.eu/api/v2/record/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj00077541.json?wskey=api2demo"

    # logging STDOUT
    logging.basicConfig(level=logging.DEBUG)

    #resp = get_metadata_EF()
    #resp = get_image_EF()
    # print(resp)

    # debugging EF csv-file
    debug_EF_csv()
