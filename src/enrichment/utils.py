#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

def output_plt_bbx(img_path: str, bbxes: list, classname: str, img_is_URL: bool = False):
    """[Creates an image (matplotlib.pyplot) with the bounding boxes from the object-detection.]

    Args:
        img_path (str): [image path. It can be URL or local path.]
        TF_serving_query_output (dict): [output from TF_serving_object_detection()]
        img_is_URL (bool, optional): [True if img_path is a URL. Otherwise, False.]. Defaults to False.

    Returns:
        [None]: [Returns nothing.]
    """
    from PIL import Image
    #from enrichment.object_detection import label_map
    import requests
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches


    fig = plt.figure(figsize=(30, 40), dpi=180)
    ax = fig.subplots()

    # load the image
    image = None
    if img_is_URL:
        # URL image
        
        # @ deprecated : open images deprecated. Seems not to work with all images.
        # #image = Image.open(requests.get(img_path, stream=True).raw)

        resp = requests.get(img_path, stream=True)
        import io
        image = Image.open(io.BytesIO(resp.content))

    else:
        # localpath image
        image = Image.open(img_path)

    im_width, im_height = image.size
    ax.imshow(image)

    # TF_serving_query_output = TF_serving_call(img_path)
    for i in range(0, len(bbxes)):
        bbx = bbxes[i]
        rect = patches.Rectangle((int(bbx[1]), int(bbx[0])), int(bbx[3])-int(
            bbx[1]), int(bbx[2])-int(bbx[0]), linewidth=3, edgecolor="g", facecolor='none')
        ax.annotate(classname, (int(
            bbx[1]), int(bbx[0])), color="w", size=20)
        ax.add_patch(rect)
    return plt  # figure.subplots()


def output_plt_bbx_ratio(img_path: str, bbxes: list, classname: str, img_is_URL: bool = False):
    """[Creates an image (matplotlib.pyplot) with the bounding boxes from the object-detection.]

    Args:
        img_path (str): [image path. It can be URL or local path.]
        TF_serving_query_output (dict): [output from TF_serving_object_detection()]
        img_is_URL (bool, optional): [True if img_path is a URL. Otherwise, False.]. Defaults to False.

    Returns:
        [None]: [Returns nothing.]
    """
    from PIL import Image
    #from enrichment.object_detection import label_map
    import requests
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches


    fig = plt.figure(figsize=(30, 40), dpi=180)
    ax = fig.subplots()

    # load the image
    image = None
    if img_is_URL:
        # URL image
        image = Image.open(requests.get(img_path, stream=True).raw)
    else:
        # localpath image
        image = Image.open(img_path)

    im_width, im_height = image.size
    ax.imshow(image)

    # TF_serving_query_output = TF_serving_call(img_path)
    for i in range(0, len(bbxes)):
        bbx = bbxes[i]
        rect = patches.Rectangle((int(bbx[1]*im_width), int(bbx[0]*im_height)), int(bbx[3]*im_width)-int(
            bbx[1]*im_width), int(bbx[2]*im_height-int(bbx[0]*im_height)), linewidth=3, edgecolor="g", facecolor='none')
        ax.annotate(classname, (int(
            bbx[1]*im_width), int(bbx[0]*im_height)), color="w", size=20)
        ax.add_patch(rect)
    return plt  # figure.subplots()
    
import matplotlib.pyplot as plt
import matplotlib.patches as patches
def output_plt_save(figure_plt: plt, img_path: str, fname:str = '',outdir: str = '/tmp/') -> str:
    """[Saves locally the image (matplotlib.pyplot) with the bounding boxes.]

    Args:
        figure_plt (plt): [description]
        img_path (str): [description]
        outdir (str, optional): [description]. Defaults to SGOAB_API_ENRICHMENTS_OUTPUT_DIR.

    Returns:
        str: [public URL of the saved image with the bounding boxes. Accessible URL in the web-services server.]
    """
    # savefig(filename, dpi=None, format='png', bbox_inches='tight', pad_inches=0.2, bbox=None, pad=None, dashes=None, loc='upper left', rot=0, vmax='I', vmin='I', hmax='I', hmin='I')
    import base64
    #filename = base64.b64encode(img_path.encode('utf-8'))
    if fname == '':
        filepath_local = "{dir}{fname}.jpg".format(
        dir=outdir, fname=fname)
    else:
        filepath_local = "{dir}{fname}.jpg".format(
        dir=outdir, fname=fname)


    figure_plt.savefig(filepath_local, dpi=60, pil_kwargs={'quality': 40})
    figure_plt.close()

    return filepath_local

"""
if __name__ == "__main__":
    filepath = [
        "/home/smendoza/local/sgoab/enrichments_europeana",
    ]
    for fp in filepath:
        with open(fp,'r') as openf:
"""