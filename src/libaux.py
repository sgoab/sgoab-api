#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################



from datetime import datetime
from email import message
from typing import Tuple
from PIL import Image
import json
import requests
import logging


def tuples_to_list(input):
    """Converts tuples in the input to lists

    Args:
        input (_type_): _description_

    Returns:
        _type_: _description_
    """

    if type(input) in [int, float, str, complex, type(None)]:
        return input
    elif isinstance(input, tuple):
        # convert the tuple to a list
        input = list(input)
        return tuples_to_list(input)
    elif isinstance(input, list):
        # iterate through list
        for ind, val in enumerate(input):
            input[ind] = tuples_to_list(val)
        return input
    elif isinstance(input, set):
        # iterate through list
        input = list(input)
        for ind, val in enumerate(input):
            input[ind] = tuples_to_list(val)
        return input
    elif isinstance(input, dict):
        # iterate through dictionary
        for key in input:
            val = input[key]
            input[key] = tuples_to_list(input[key])
        return input
    else:
        logging.warn("unmanaged type: {}".format(type(input)))


def link_checker(link):
    try:
        # GET request
        req = requests.get(link, timeout=50)
        message = None
        # check status-code
        if req.status_code in [400, 404, 403, 408, 409, 501, 502, 503]:
            message = "{} => Broken status-code:{}".format(
                link, req.status_code,)
            logging.error(message)
            return False
        elif req.status_code in [504]:
            message = "{} => Time-out link; status-code:{}".format(
                link, req.status_code,)
            logging.error(message)
            return False
        elif req.status_code in [200]:
            txt = "{} => Good Link".format(link)
            logging.debug(message)
            return True
        else:
            txt = "{} => Unknown status of the Link".format(link)
            logging.debug(message)
            return False

    # Exception
    except requests.exceptions.RequestException as e:
        # print link with Errs
        raise SystemExit(f"{link}: Somthing wrong \nErr: {e}")


def get_datetime_iso8601(time: datetime = None):
    """_summary_

    Returns:
        _type_: _description_
    """

    """
    Date and time in UTC	2022-07-20T07:20:43+00:00
    2022-07-20T07:20:43Z
    20220720T072043Z
    """

    from datetime import datetime

    if time is None or not isinstance(time, datetime):
        return datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
    elif isinstance(time, datetime):
        return time.strftime("%Y-%m-%dT%H:%M:%SZ")


def str_to_bool(input: str) -> bool:

    if isinstance(input, bool):
        # do nothing
        return input
    elif isinstance(input, str):
        true_values = ['true', '1', 't', 'y', 'yes', 'yeah', 'yup']
        false_values = ['false', '0', 'f', 'n', 'no', 'nope']
        if input.lower() in true_values:
            return True
        elif false_values:
            return False
        else:
            return None
    else:
        return None


def filter_dict(input: dict, keys: list) -> dict:
    """Returns a dictionary with the specified keys.

    Args:
        input (dict): dictionary to be filtered
        keys (list): keys that will be kept from the input

    Returns:
        dict: filtered dictionary with the keys and its correspondent values 
    """
    return {k: v for (k, v) in input.items() if k in keys}


def ratio_to_xywh(bbx: list, im_width: int, im_height: int) -> list:

    box_width = (bbx[2]-bbx[0])*im_width  # width
    box_height = (bbx[3]-bbx[1])*im_height  # heidth

    # ratio to px conversion
    bbx[0] = int(bbx[0]*im_width)  # 'xmin'
    bbx[1] = int(bbx[3]*im_height)  # 'ymax'
    bbx[2] = int(box_width)  # 'width'
    bbx[3] = int(box_height)  # heidth

    return bbx


def ratio_to_px_simple(bbx: list, im_width: int, im_height: int) -> list:
    # convert ratio:float to px:int (bounding boxes)
    # conversion round down

    for i, val in enumerate(bbx):
        # check all bbx are ratio, not px
        if not (isinstance(bbx[i], float) and bbx[i] >= 0.0 and bbx[i] <= 1.0):
            return bbx

    # ratio to px conversion
    bbx[0] = int(bbx[0]*im_height)  # ymax ; @deprecated: 'xmin'
    bbx[1] = int(bbx[1]*im_width)  # xmin ; @deprecated: 'ymin'
    bbx[2] = int(bbx[2]*im_height)  # ymin ; @deprecated: 'xmax'
    bbx[3] = int(bbx[3]*im_width)  # xmax ; @deprecated: 'ymax'
    return bbx


def ratio_to_px(input_bbxs: list, im_width: int, im_height: int) -> list:
    """
    Converts list of bounding boxes from ratio to px
    """
    # convert ratio:float to px:int (bounding boxes)
    # conversion round down
    for bbx in input_bbxs:
        bbx = ratio_to_px_simple(bbx, im_width, im_height)
    return input_bbxs


def image_width_height(img_path: str) -> Tuple[int, int]:
    """Gets the image width and height

    Args:
        img_path (str): image localpath

    Returns:
        Tuple[int,int]: width and height of the input image
    """

    import validators
    import requests

    if validators.url(img_path):
        resp = requests.get(img_path, stream=True)
        import io
        image = Image.open(io.BytesIO(resp.content))
    else:
        image = Image.open(img_path)

    im_width, im_height = image.size

    return im_width, im_height


def jsonify_for_sets(obj):
    """For any given object (possibly set, list, dict, etc.), converts the sets into lists. Used for json.dumps() containing sets (e.g. {a,b,c}), which are not valid for JSON format.

    Args:
        obj (_type_): python simple object that can contain sets

    Returns:
        object: python object with sets converted to lists
    """
    if isinstance(obj, set):
        return list(obj)

    return obj


def jsonify_visrel(visrel_output: dict) -> str:
    """For the dictionary with the vis-rel results, it returns a string with a valid JSON. 
    It converts sets into lists (also json.dumps() default: None into null, etc.)

    Args:
        visrel_output (dict): vis-rel output

    Returns:
        str: string with JSON valid format, containing the information in the vis-rel output.
    """

    return json.dumps(visrel_output, default=jsonify_for_sets)
