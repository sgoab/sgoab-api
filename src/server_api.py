#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


from marshmallow import fields
from apiflask import APIFlask
from apiflask import HTTPTokenAuth
import http
from http.client import HTTPS_PORT
from apiflask import APIFlask
from flask import Flask
# from flask_httpauth import HTTPTokenAuth
from flask import request, jsonify  # , abort, redirect, url_for, jsonify
from werkzeug.utils import secure_filename
import json
import logging

# service interface (with marshmallow models)
from service_interface import EnrichmentQueryMultipleSchema, EnrichmentQueryOutputMultipleSchema
from service_interface import EnrichmentQuerySchema, EnrichmentQueryOutputSchema, EnrichmentQueryOutputJSONSchema, EnrichmentQueryOutputAnnotationSchema
from service_interface import EnrichmentQueryEuropeanaSchema, EnrichmentQueryEuropeanaOutputSchema

# services modules
from enrichment import enrichment
import service_interface as si
import models_services as ms
# local modules
import libaux


# Environment variables
EF_ITEM_URL = "http://data.europeana.eu/item"

"""
app = Flask(__name__)
"""
# generating documentation openAPI
app = APIFlask(__name__, spec_path='/openapi.yaml')
app.config['SPEC_FORMAT'] = 'yaml'

# set app token authentication
auth = HTTPTokenAuth(scheme='Bearer')

# ServiceInterface instance
service_interface = si.ServiceInterface()


class UserManagement:
    roles = ['admin', 'sgoab_user', 'external_user']

    def get_roles(self):
        return self.roles


tokens = {
    "Ep8paH7GKHz9K75G5knles3nTZ0a5J": "Leonardo",
    "omq2qGDVxRAyqULdGIy7fEFkwTSidt": "Rafael",
    "dea127a971ce9bd8bf439025d752d32d": "Donatello",
    "dde66c0fa6fa1cde1536f67550efc169": "Michellangelo",
    "b32d35c1aae7919c22ec4dffc7b000f1": "Don Juan",
}


@auth.get_user_roles
def get_user_roles(user):
    # return user.get_roles()
    return ['admin', 'sgoab_user', 'external_user']


@auth.verify_token
def verify_token(token):
    if token in tokens:
        return tokens[token]


@app.route('/')
@auth.login_required
def index():
    return "Hello, {}!".format(auth.current_user())


@app.route('/api/enrichment', methods=['POST'])
@auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
# @app.input(EnrichmentInSchema, location='headers', example=json.dumps(example_enrichment_input))
@app.input(EnrichmentQuerySchema, location='json', example=json.dumps(ms.example_enrichment_input))
# @app.output(EnrichmentQueryOutputSchema, example=json.dumps(ms.example_enrichment_urls_output))
def enrichments(data):

    output = service_interface.unique_endpoint_enrichment(
        data["item"], data["enrichments"], output_format=data["output_profile"], output_format_bbx=data["output_bbx_format"])

    # jsonify visual-relations tuples
    output_json = libaux.tuples_to_list(output)

    print(output)
    print(json.dumps(output_json))

    return output_json


@ app.route('/api/enrichment/multiple', methods=['POST'])
@ auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
# @app.input(EnrichmentInSchema, location='headers', example=json.dumps(example_enrichment_input))
@ app.input(EnrichmentQueryMultipleSchema, location='json', example=json.dumps(ms.example_enrichment_input))
@ app.output(EnrichmentQueryOutputMultipleSchema, example=json.dumps(ms.example_enrichment_urls_output))
def enrichments_multiple(data):

    output = service_interface.enrichments(json.loads(request.form))
    output = service_interface.enrichments(json.loads(request.get_json()))


@ app.route('/api/enrichment/urls', methods=['POST'])
@ auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
@ app.input(EnrichmentQuerySchema, location='json', example=json.dumps(ms.example_enrichment_urls_input))
# @app.output(EnrichmentQueryOutputSchema, example=json.dumps(ms.example_enrichment_urls_output))
@ app.output(EnrichmentQueryOutputAnnotationSchema)
# @app.output(EnrichmentQueryOutputJSONSchema, example=json.dumps(ms.example_enrichment_urls_output))
def enrichments_urls(data):
    """Generates enrichment from image URLs as input
    """
    # output = service_interface.enrichments(data)

    output = service_interface.unique_endpoint_enrichment(
        data["item"], data["enrichments"], output_format=data["output_profile"])

    return output


@ app.route('/api/enrichment/europeana', methods=['POST'])
@ auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
@ app.input(EnrichmentQueryEuropeanaSchema, location='json', example=json.dumps(ms.example_enrichment_europeanaid_input))
@ app.output(EnrichmentQueryEuropeanaOutputSchema, example=json.dumps(ms.example_enrichment_europeanaid_output))
def enrichments_europeana(data):
    """Generates enrichment from Europeana IDs as input
    """

    output = service_interface.enrichments_europeana(data)

    # keys from the output that I want
    keys = ["europeana_id", "img_path", "output"]
    output_for_schema = libaux.filter_dict(output, keys)

    return output_for_schema


@ app.route('/api/enrichment/europeana/object-detection-annotation', methods=['POST'])
@ auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
def enrichments_europeana_annotation():
    """Generates enrichments with EF annotations format from Europeana IDs as input
    """

    data = request.get_json()
    # data = json.loads(data)
    output = service_interface.generate_europeana_object_detection_annotations(
        item=data["europeana_id"])

    return json.dumps(output)


@ app.route('/upload', methods=['POST'])
@ auth.login_required(role=['admin', 'sgoab_user', 'external_user'])
def upload():

    # manage the form
    data = request.form
    # input-textarea Europeana
    if "EF_URIs" in data.keys():
        print(data["EF_URIs"])
    # input-checkbox Europeana
    '''
    if "EF-input" in data.keys():
        EF_input = True
    else:
        EF_input = False
    '''
    # input-checkbox object-detection
    if "object_detection" in data.keys():
        object_detection = True
    else:
        object_detection = False
    # input-checkbox visual relations
    if "visrel" in data.keys():
        visrel = True
    else:
        visrel = False

    # "EF-URIs"
    image_to_process = data["EF_URIs"].replace(' ', '')
    from enrichment import enrichment
    beta_metadata = []
    # remove end of line characters
    # image_to_process = image_to_process.split("\r\n")
    for image in image_to_process.splitlines():
        # metadata = enrichmentEF.get_metadata_EF()
        # "img-id": "/2064137/Museu_ProvidedCHO_Bildarchiv_Foto_Marburg_obj20637897",  # image ID

        # identify if URL is Europeana's ITEM
        # EF-URLs contain "http://data.europeana.eu/item"
        if EF_ITEM_URL in image:
            EF_input = True
        else:
            EF_input = False

        # execute the enrichment
        img_url = enrichment.enrichment_query_processor(
            image, EF_input=EF_input, object_detection=object_detection, visual_relations=visrel)
        logging.info(img_url)

        beta_metadata.append(img_url)


if __name__ == '__main__':
    # app.run()
    # print(app.spec) # swagger openapi
    print(globals())
    app.run(host="0.0.0.0", debug=True, port=5050)
    # print_swagger_file()
