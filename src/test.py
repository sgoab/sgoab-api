#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


import libaux
import json

a = {
    "img_path": "https://realonline.imareal.sbg.ac.at/imageservice/kupo/DA005093",
    "output": {
        "object_detection": {
            "bbx": [
                [
                    70,
                    527,
                    229,
                    800
                ],
                [
                    167,
                    279,
                    500,
                    573
                ],
                [
                    198,
                    547,
                    541,
                    792
                ],
                [
                    108,
                    28,
                    348,
                    319
                ],
                [
                    4,
                    303,
                    242,
                    597
                ],
                [
                    280,
                    5,
                    536,
                    337
                ]
            ],
            "scores": [
                0.907691479,
                0.82788676,
                0.791020513,
                0.72861284,
                0.598623395,
                0.587797761
            ],
            "classes": [
                3,
                3,
                3,
                3,
                3,
                26
            ],
            "classes-labels": [
                "person",
                "person",
                "person",
                "person",
                "person",
                "nude"
            ]
        },
        "results_visrel": {
            "artist": {
                "name": {
                    "firstname": None,
                    "lastname": None
                },
                "life_dates": {
                    "birth_year": None,
                    "death_year": None
                }
            },
            "artwork": {
                "title": {
                    "en": None
                },
                "work_dates": {
                    "year": [],
                    "century": None
                },
                "dims_cm": {
                    "width": None,
                    "height": None
                },
                "dims_pixel": {
                    "width": 570,
                    "height": 800
                }
            },
            "main_topics": {
                "notable_objs": [("nude_1", (408.0,
                                             171.0),
                                  "tr",
                                  18.64), ("person_4", (228.0,
                                                        173.5),
                                           "tc",
                                           15.32), ("person_2", (333.5,
                                                                 426.0),
                                                    "cc",
                                                    21.47), ("person_3", (369.5,
                                                                          669.5),
                                                             "bc",
                                                             18.43)
                                 ],
                "bigger_objs": [("person_2", (333.5,
                                              426.0),
                                 "cc",
                                 21.47), ("nude_1", (408.0,
                                                     171.0),
                                          "tr",
                                          18.64), ("person_3", (369.5,
                                                   669.5),
                                                   "bc",
                                                   18.43), ("person_5", (123.0,
                                                            450.0),
                                                            "cl",
                                                            15.34), ("person_4", (228.0,
                                                                     173.5),
                                                                     "tc",
                                                                     15.32)
                                ],
                "ref_objs": [("nude_1", (408.0,
                                         171.0),
                              "tr",
                              18.64), ("person_2", (333.5,
                                                    426.0),
                             "cc",
                                       21.47), ("person_4", (228.0,
                                                             173.5),
                             "tc",
                                                15.32), ("person_3", (369.5,
                                                                      669.5),
                             "bc",
                                                         18.43)
                             ]
            },
            "composition": {
                "clusters": {
                    "person_4": {
                        "person_2",
                        "nude_1",
                        "person_4"
                    }
                },
                "foreback": {
                    "person_4": [("person_1",
                                  0.6,
                                  -0.06), ("person_2",
                                 0.32,
                                 0.06), ("person_3",
                                         0.59,
                                         0.03), ("person_5",
                                                 0.35,
                                                 0.0), ("nude_1",
                                                        0.02,
                                                        0.03)
                                 ]
                },
                "omt": {},
                "amt": {},
                "bmt": {
                    "person_4": [
                        "person_1",
                        "person_2",
                        "person_3",
                        "person_5"
                    ]
                },
                "mylomt": {},
                "myromt": {
                    "person_4": [
                        "nude_1"
                    ]
                }
            },
            "annot_txt": {
                "person_4++nude_1": [
                    "person_4 is_behind nude_1"
                ],
                "person_4++attr": [
                    "person_4 sits/kneels/bows/bends/crouches/squats/is_partial"
                ],
                "nude_1++attr": [
                    "nude_1 sits/kneels/bows/bends/crouches/squats/is_partial"
                ]
            }
        }
    },
    "europeana_id": "http://data.europeana.eu/item/15501/003841"
}

print(a)
b = libaux.tuples_to_list(a.copy())
print(json.dumps(b))
