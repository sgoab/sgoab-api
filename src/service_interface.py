#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


from doctest import OutputChecker
from wsgiref.validate import InputWrapper
from marshmallow import Schema, fields, validate
from marshmallow import ValidationError
from marshmallow import fields
from marshmallow import validate

import json
from numpy import imag
import requests
from europeanifier.europeana_item import EuropeanaItem
import logging

import libaux

# my modules
from enrichment import enrichment_query_manager as EQM
from europeanifier import europeanify

# global variables
from europeanifier.europeanify import ANNOTATION_SIMPLE, ANNOTATION_COMPLETE


class EnrichmentQueryMultipleSchema(Schema):
    # items = fields.List(fields.URL(required=True, relative=True, validate=validate.URL(
    #        relative=True, schemes=["http", "https"])), required=True)
    item = fields.List(fields.String(), required=True)
    enrichments = fields.List(fields.String(), required=True)
    output_profile = fields.String(required=False)


class EnrichmentQuerySchema(Schema):
    item = fields.URL(required=True, relative=True, validate=validate.URL(
        relative=True, schemes=["http", "https"]))
    enrichments = fields.List(fields.String(), required=True)
    output_bbx_format = fields.String(
        required=False, default='px', missing='px', validate=validate.OneOf(["ratio", "px"]))
    output_profile = fields.String(required=False)


class EnrichmentQueryEuropeanaSchema(Schema):
    item = fields.URL(required=True, relative=True)
    object_detection = fields.Boolean(required=True)
    visual_relations = fields.Boolean(required=True)


class ObjectDetectionOutputSchema(Schema):
    bounding_boxes = fields.List(fields.List(fields.Float()))
    scores = fields.List(fields.Float())
    classes = fields.List(fields.Float())
    classes_labels = fields.List(fields.String())
    img_bounding_boxes = fields.List(fields.String())


class EnrichmentQueryOutputMultipleSchema(Schema):
    fields.List(fields.Dict(), required=True)


class EnrichmentQueryOutputSchema(Schema):
    img_path = fields.String(required=False)
    output = fields.Dict(required=False)
    europeana_id = fields.String(required=False)
    annotations = fields.List(fields.Dict(), required=False)


class EnrichmentQueryOutputAnnotationSchema(Schema):
    europeana_id = fields.String()
    annotations = fields.List(fields.Dict())
    img_path = fields.String(required=False)


class EnrichmentQueryOutputJSONSchema(Schema):
    img_path = fields.String(required=False)
    output = fields.Dict(required=False)
    europeana_id = fields.String(required=False)
    #object_detection = ObjectDetectionOutputSchema()
    """
    {
            "queryID": "e91110d3f12b515152fabad660c66af7",
            "img_path": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
            "object_detection": true,
            "visual_relations": false,
            "output": {
                "object_detection": {
                    "bbx": [
                        [
                            0.601267517,
                            0.392477065,
                            0.68912,
                            0.806616366
                        ],
                        [
                            0.0504028499,
                            0.704973638,
                            0.323536694,
                            0.947903037
                        ],
                        [
                            0.289663911,
                            0.011395189,
                            0.665526867,
                            0.738713
                        ],
                        [
                            0.24941963,
                            0.0630826429,
                            0.369912684,
                            0.394429296
                        ]
                    ],
                    "scores": [
                        0.995772302,
                        0.994411647,
                        0.934053242,
                        0.736239
                    ],
                    "classes": [
                        11.0,
                        1.0,
                        3.0,
                        54.0
                    ],
                    "classes-labels": [
                        "book",
                        "crucifixion",
                        "person",
                        "halo"
                    ],
                    "img-bbx-path": "/static//object-detection/output/b%27aHR0cHM6Ly93d3cubXVzZW9kZWxwcmFkby5lcy9pbWFnZW5lcy9Eb2N1bWVudG9zL2ltZ3NlbS9jNC9jNGNhL2M0Y2FlM2I3LTA1MTgtNGNiYy05ODVhLTViNDNlZDNmYzYwMS85NTBmYzY2Yy1kZDVhLTRjODItYjIxYi1iM2Y1MTg2YmMxMDAuanBn%27.jpg"
                }
            },
            "status": 0,
            "outdir": "/home/smendoza/tmp/sgoab/enrichments/None",
            "label": 500
        },
    """


class EnrichmentQueryEuropeanaOutputSchema(Schema):

    europeana_id = fields.URL(relative=True)
    img_path = fields.URL(relative=False)
    output = fields.Dict()
    #object_detection = ObjectDetectionOutputSchema()


class ServiceInterface():
    # attributes
    eqm: EQM.EnrichmentQueryManager = None

    def __init__(self) -> None:
        self.eqm = EQM.EnrichmentQueryManager()
        pass

    def enrichments_list_images(self, input: dict) -> dict:
        """Generate enrichments for a given painting. The enrichments can include: object detectio, visual relations
        """
        imagepaths = []
        object_detection = True
        visual_relations = True

        if len(request.form.keys()) > 0:
            # 1. INPUT: HTTP web form
            data = request.form
            data_json = json.loads(data)
            if "EF_URIs" in data_json.keys():
                imagepaths = data_json["EF_URIs"]
                # 0. clean input text
                imagepaths = data_json["EF_URIs"].replace(' ', '')
                # 1. remove endline characters ["\r", "\n"]
                # 2. convert to list
                imagepaths = imagepaths.splitlines()
            object_detection = object_detection in data.keys()
            visual_relations = visual_relations in data.keys()
        elif "items" in request.get_json().keys():
            # 2. INPUT: JSON
            data = json.loads(request.data)
            #data = request.get_json()
            imagepaths = data["items"]
            object_detection = libaux.str_to_bool(data["object_detection"])
            visual_relations = libaux.str_to_bool(data["visual_relations"])
        else:
            # X. Unrecognized format
            return json.dumps({"status": "fail"})

        # object-detection service
        output = {}
        output["user"] = auth.current_user()

        from enrichment import enrichment_query_manager as EQM

        try:
            output = []
            # output["results"] = enrichment.enrichment_query_service_interface(imagepaths, object_detection=object_detection, visual_relations=visual_relations)
            eqm = EQM.EnrichmentQueryManager()
            # add queries to the task-list
            queries_ids = eqm.add_query_json(data)
            # run queries
            for qid in queries_ids:
                query_output = eqm.run_query(qid)
                output.append(query_output)
            """
            # wait for queries
            while not eqm.arefinished(queries_ids):
                pass
            """
            return json.dumps(output)
        except Exception:
            import traceback
            print(traceback.print_exc())
            output["status"] = "fail"
            return json.dumps(output)

    def error_msg(self, status, imagepath):
        return {
            "status": "fail",
            "image-failed": imagepath
        }

    def enrichments(self, imagepath: str, enrichments: list) -> dict:

        try:
            # add queries to the task-list
            object_detection: bool = 'object-detection' in enrichments
            visual_relations: bool = 'visual-relations' in enrichments

            # check image URL
            if imagepath != None and libaux.link_checker(imagepath):
                query_id = self.eqm.add_query(
                    item=imagepath, object_detection=object_detection, visual_relations=visual_relations)
                # run queries
                output = self.eqm.run_query(query_id)
                return output
            else:
                return self.error_msg('broken link', imagepath)
            return output
        except Exception:
            import traceback
            print(traceback.print_exc())
            return self.error_msg('fail', imagepath)
        pass

    def enrichments_dict(self, input: dict) -> dict:
        """Generate enrichments for a given painting. The enrichments can include: object detectio, visual relations
        """

        imagepath = input["item"]
        enrichments = input["enrichments"]

        return self.enrichments(imagepath, enrichments)

    def enrichments_urls(self, input: dict) -> dict:
        """Generate enrichments according, for the given image URLs. The enrichments can include: object detectio, visual relations.
        Args:
            input (dict): _description_

        Returns:
            dict: _description_
        """
        return self.enrichments(input)

    def enrichments_europeana(self, imagepath: str, enrichments: list) -> dict:
        from europeanifier.europeana_item import EuropeanaItem

        ef_item = EuropeanaItem(imagepath)

        # get image URL of the europeana item (from the Europeana API)
        input["item"] = ef_item.get_image_EF_URL(imagepath)

        # get image enrichemnts
        output = self.enrichments(imagepath, enrichments)

        # output with europeana_id
        output["europeana_id"] = ef_item.get_absolute_europeana_id(imagepath)

        pass

    def enrichments_europeana_dict(self, input: dict) -> dict:
        """Generate enrichments according, for the image in the given Europeana ID. The enrichments can include: object detectio, visual relations.

        Args:
            input (dict): _description_

        Returns:
            dict: _description_
        """

        imagepath = input["item"]
        enrichments = input["enrichments"]

        return self.enrichments(imagepath, enrichments)

        return output

    def object_detection(self, input: dict) -> dict:
        pass

    def visual_relation(self, imagepath: str, output_object_detection: dict) -> dict:
        # def bbx_vis_rel(width: int, height: int, img_dict: dict, vra: np.ndarray) -> dict:
        '''
        bbx_vis_rel():
        Computes caption seeds and compositional data.

        Input:
        @param      width: image width in pixels
        @type       int

        @param      height: image height in pixels
        @type       int

        @param      img_dict: processed image metadata and computed metrics
        @type:      json formatted dict

        @param      vra: visual relationship array
        @type       np.ndarray

        Output:
        @param      vrd: visual relationships dictionary
        @type       dict formated as json
        '''

        import json

        # tested modules
        from vis_rel import img_data_model as vr_idm
        from vis_rel import bbx_visrel_prod as bbx_visrel  # last version
        # from vis_rel import bbx_visrel as bbx_visrel # stable version
        # from vis_rel import bbx_vra as bbx_visrel  # stable version

        # get image width and height
        im_width, im_height = libaux.image_width_height(imagepath)

        # convert ratio to px for vis-rel service
        output_object_detection['bbx'] = libaux.ratio_to_px(
            output_object_detection['bbx'], im_width, im_height)

        # get img_dict: image metadata and computed metrics
        # get vra, visual relationship array
        img_dict, vra = vr_idm.img_data_model(
            output_object_detection, im_width, im_height)

        # get vis-rel generated
        # @deprecated
        # output_bbx_vis_rel = bbx_visrel.bbx_vis_rel(im_width, im_height, img_dict, vra)

        output_bbx_vis_rel = bbx_visrel.bbx_visrel(img_dict, vra)

        assert (isinstance(output_bbx_vis_rel, dict))

        return output_bbx_vis_rel

    def visual_relation_dict(self, input: dict) -> dict:

        imagepath = input["item"]
        output_object_detection = input["output_object_detection"]
        return self.visual_relation(imagepath, output_object_detection)

    def visrel(self, img_path: str, output_object_detection: dict) -> dict:
        """Version for sharing the visual relations.
        @date: updated February 2023

        Args:
            img_path (str): image patl [URL|absolute-path|relative-path]
            output_object_detection (dict): Output from object detection (artem's Tensorflow inference)

        Returns:
            dict: output with the visual relations
        """
        from vis_rel import img_data_model as vr_idm
        from vis_rel import bbx_visrel as bbx_visrel

        # get width & height of image
        im_width, im_height = libaux.image_width_height(img_path)

        # convert ratio to px of the bounding boxes
        output_object_detection['bbx'] = libaux.ratio_to_px(
            output_object_detection['bbx'], im_width, im_height)

        # get img_dict: image metadata and computed metrics
        # get vra, visual relationship array
        img_dict, vra = vr_idm.img_data_model(
            output_object_detection, im_width, im_height)

        output_bbx_vis_rel = bbx_visrel.bbx_vis_rel(
            im_width, im_height, img_dict, vra)

        assert (isinstance(output_bbx_vis_rel, dict))
        print(type(output_bbx_vis_rel))
        print(output_bbx_vis_rel)

        # jsonify the output, because it doesn't follow standards (e.g. tuples, etc.)
        out: str = libaux.jsonify_visrel(output_bbx_vis_rel)

        print("bbx_visrel.bbx_vis_rel() >> {}".format(out))

        return json.loads(out)

    def unique_endpoint_enrichment(self, image_uri: str, enrichment_methods: list, output_format: str = 'JSON', output_format_bbx: str = 'px'):
        """Generates enrichment for an image URI. The image URI con be a file URI, or an Europeana identifier (absolute or relative URI).
        The enrichments methods accept ['object-detection', 'visual-relations']

        Args:
            image_uri (str): image URI with [.jpeg|.jpg|.png] format  ) or Euroeana ID URI [absolute|relative]
            enrichment_method (list): list of the requested enrichments ['object-detection', 'visual-relations']
            output_format (str, optional): definition of the output format ['JSON', 'EF-ANNOTATION-SIMPLE', 'EF-ANNOTATION-COMPLETE'] . Defaults to 'JSON'.
        """

        ENRICHMENTS = ['object-detection', 'visual-relations']

        # check the input data
        # (1) identify if is EF or image URI
        if EuropeanaItem.is_europeana_id(image_uri):
            # requested object-detection must be true
            if 'object-detection' not in enrichment_methods:
                # ask for object detection
                logging.error(
                    'Europeana ID without object-detection requested')
                pass
            else:
                # get image URL of the europeana item (from the Europeana API)
                imagepath = EuropeanaItem(
                    image_uri).get_image_EF_URL(image_uri)
                # get image enrichemnts
                output = self.enrichments(imagepath, enrichment_methods)
        else:
            if output_format in [ANNOTATION_SIMPLE, ANNOTATION_COMPLETE]:
                # not europeana ID, just can have JSON format
                output_format = "JSON"  # default output format
            else:
                logging.error("ERROR: Output format incorrect.")

            # expected an image URI
            output = self.enrichments(
                image_uri, enrichments=enrichment_methods)

        # OUTPUT FORMATTING:

        if output_format in [ANNOTATION_SIMPLE, ANNOTATION_COMPLETE]:
            # check its an europeana item
            if EuropeanaItem.is_europeana_id(image_uri):
                annotations_EF = self.output_to_annotations(
                    output, europeana_id=image_uri, profile=output_format)

                ann_collection = self.output_to_annotations_collection(
                    annotations_EF)
                output = ann_collection
                return output
            else:
                # imposible ANNOTATION is just for Europeana ID
                logging.error(
                    'ANNOTATION output format is just for Europeana ID')
        elif output_format == 'JSON':
            # otherwise, .jpeg | .jpg | .png
            keys = []
            # convert ratio to px
            if output_format_bbx in ['px']:
                # get all the bounding boxes of objects detected in the image
                list_bbxes = output['output']['object_detection']['bbx']
                # convert the bounding boxes values from ratio to px
                im_width, im_height = libaux.image_width_height(
                    output['img_path'])
                output['output']['object_detection']['bbx'] = libaux.ratio_to_px(
                    list_bbxes, im_width, im_height)
            else:
                # do nothing: ratio will be returned
                pass

            if EuropeanaItem.is_europeana_id(image_uri):
                # add europeana_id to the output
                output["europeana_id"] = EuropeanaItem(
                    image_uri).get_absolute_europeana_id(image_uri)
                # keys from the output that I want
                keys = ["img_path", "output", "europeana_id"]
            else:
                # keys from the output that I want
                keys = ["img_path", "output"]

            # filtering keys from the dict
            output_filtered = libaux.filter_dict(output, keys)

            return output_filtered
        else:
            logging.error("output_profile incorrect: {}".format(output_format))

    def generate_europeana_object_detection_annotations(self, item: str = "/07101/O_156") -> list:

        # create an input for request
        input = {
            "item": item,
            "object_detection": True,
            "visual_relations": False
        }

        # request de enrichment to the service
        output = self.enrichments_europeana(input)

        # convert results to EF enrichment JSON-LD

        # for each bbx, generate annotation

        return output

    def output_to_annotations(self, output: dict, europeana_id: str = "CHO_ID", profile: str = ANNOTATION_SIMPLE) -> list:
        """Converts to JSON-LD with EF format the output of the object-detection service.
        This requirements is used to insert enrichments to the Europeana Foundation.

        Args:
            output (dict): output of the object-detection service

        Returns:
            dict: annotation (JSON-LD)
        """

        """
        output = {
            "europeana_id": output["europeana_id"],
            "imagepath": output["img_path"],
            # list of bounding boxes
            "bbx": output["output"]["object_detection"]["bbx"],
            "class_label": output["output"]["object_detection"]["classes-labels"],
            "lang": 'en',
            "datetime": libaux.get_datetime_iso8601(),
            "bbx_score": output["output"]["object_detection"]["scores"],
        }
        """
        output_bbxs: list = []
        from europeanifier import annotation
        from europeanifier.annotation import Annotation, AnnotationCollection
        output_bbxs = annotation.object_detection_2_annotation(
            europeana_id, output)

        output_annotations: list = []
        for val in output_bbxs:
            ann = None

            # convert ratio to px
            # get image width and height
            im_width, im_height = libaux.image_width_height(val['imagepath'])
            # convert ratio to px for vis-rel service
            val['bbx'] = libaux.ratio_to_xywh(val['bbx'], im_width, im_height)

            # generate Annotation in Web-Annotation format
            if profile == ANNOTATION_SIMPLE:
                ann = Annotation(val).generate_europeana_annotation_simple()
            elif profile == ANNOTATION_COMPLETE:
                ann = Annotation(val).generate_europeana_annotation_complete()
            output_annotations.append(ann)
        return output_annotations

    def output_to_annotations_collection(self, input: list) -> dict:
        from europeanifier.annotation import AnnotationCollection
        if isinstance(input, list):
            output = AnnotationCollection.generate_europeana_annotation_collection(
                input)
            return output
        else:
            # do nothing
            logging.warn(
                "error generating annotations collections: incorrect input")
            return None

        """ OUTPUT OF ENRICHMENT QUERY
            {
                "europeana_id": "/15501/011206",
                "img_path": "https://realonline.imareal.sbg.ac.at/imageservice/kupo/7011573",
                "output": {
                    "object_detection": {
                        "bbx": [
                            [
                                293,
                                159,
                                703,
                                449
                            ],
                            [
                                253,
                                269,
                                581,
                                366
                            ]
                        ],
                        "classes": [
                            5,
                            3
                        ],
                        "classes-labels": [
                            "horse",
                            "person"
                        ],
                        "scores": [
                            0.992810905,
                            0.749926507
                        ]
                    },
                }
            }
        """
