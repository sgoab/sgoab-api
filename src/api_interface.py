#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


class APIInterface ():

    # ENRICHMENTS INTERFACE
    def enrichment_query_service_interface(self, imagepath: str, object_detection: bool = True, visual_relations: bool = False) -> dict:
        """[Returns the enrichments for the given images.
        Available enrichments are:
        1. object-detection
        2. visual-relations
        ]

        Args:
            imagepaths (list): [list with the [URL|localpath] of the images to be queried for enrichment]
            object_detection (bool, optional): [True if querying for object-detection enrichments. False otherwise.]. Defaults to True.
            visual_relations (bool, optional): [True if querying for visual-relations enrichments. False otherwise.]. Defaults to False.

        Returns:
            dict: [has the specified enrichments for the given images]
        """
        pass


    # ENRICHMENTS INTERFACE
    def enrichment_query_json(self, query_json: str) -> dict:
        """[Returns the enrichments for the given images.
        Available enrichments are:
        1. object-detection
        2. visual-relations
        ]

        Args:
            imagepaths (list): [list with the [URL|localpath] of the images to be queried for enrichment]
            object_detection (bool, optional): [True if querying for object-detection enrichments. False otherwise.]. Defaults to True.
            visual_relations (bool, optional): [True if querying for visual-relations enrichments. False otherwise.]. Defaults to False.

        Returns:
            dict: [has the specified enrichments for the given images]
        """
        pass



    # SEARCH INTERFACE

