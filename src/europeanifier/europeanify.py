#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################



"""
This module manages everything associated with the format enrichment formats.
It includes converintg euopeanaEF json-ld format, etc.

See: https://docs.google.com/document/d/1Xgg98brF0rV0EQhDd-sfy9YL8M9l58Ui1Ms_RG7Ioqg/edit#heading=h.kvc3lqgl7ay9

Example:
{
  "@context": "http://www.w3.org/ns/anno.jsonld",
  "id": "http://saintgeorgeonabike.eu/annotations/1",
  "type": "Annotation",
  "created": "2022-07-10T14:08:07Z",
  "creator": { 
    "type": "Software",
    "name": "SGoaB Enrichment Service",
    "homepage": "https://saintgeorgeonabike.eu/"
  },
  "motivation": "tagging",
  "body": {
    "type": "TextualBody",
    "value": "book",
    "language": "en"
  },
  "target": {
    "scope": "http://data.europeana.eu/item/Europeana_ID",
    "source": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg"
  }
}
{
        "id": "http://saintgeorgeonabike.eu/annotations/1",
        "type": "Annotation",
        "created": "2022-07-10T14:08:07Z",
        "creator": {
          "type": "Software",
          "name": "SGoaB",
          "homepage": "https://saintgeorgeonabike.eu/"
        },
        "motivation": "tagging",
        "body": {
          "type": "TextualBody",
          "value": "crucifixion",
          "language": "en"
        },
        "target": {
          "scope": "http://data.europeana.eu/item/Europeana_ID",
          "source": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg"
        }
}

"""


import logging
from typing import Tuple
from matplotlib import image
from matplotlib.text import Annotation
from regex import F, Regex


# global variables
ANNOTATION_SIMPLE = 'EF-ANNOTATION-SIMPLE'
ANNOTATION_COMPLETE = 'EF-ANNOTATION-COMPLETE'


class europeana_annotation():
    def __init__(self) -> None:
        self.europeana_id = None
        self.source = None
        self.europeana_id = None
        self.europeana_id = None
        self.europeana_id = None
        self.europeana_id = None
        self.europeana_id = None
        self.europeana_id = None
        pass


def generate_annotation(data: dict, profile: str = ANNOTATION_SIMPLE,) -> dict:

    # required information for annotation profiles
    simple_keys = ['europeana_id', 'imagepath',
                   'class_label', 'lang', 'datetime', ]
    complete_keys = ['europeana_id', 'imagepath',
                     'class_label', 'lang', 'datetime', 'bbx', 'bbx_score']

    # check profile correct
    if not profile in [ANNOTATION_SIMPLE, ANNOTATION_COMPLETE]:
        logging.error('annotation profile incorrect: {}'.format(profile))

    from annotation import Annotation as Annotation

    # check data & execute
    if profile == ANNOTATION_SIMPLE and simple_keys in data.keys():
        annotation = europeanify_annotation_simple()
    elif profile == ANNOTATION_COMPLETE and complete_keys in data.keys():
        annotation = europeanify_annotation_complete()
    else:
        logging.warning(
            "generate annotation cannot be execute: incorrect profile")

    if profile == ANNOTATION_SIMPLE:
        annotation = europeanify_annotation_simple()

    annotation = {}

    return annotation


def europeanify_image_bbx(imageURI: str, bbx: list) -> str:
    # "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg#xywh=240,156,276,324"

    output = '{imageURI}#xywh={bbx}'.format(
        imageURI=imageURI, bbx=','.join(map(str, bbx)))

    return output


class annotation():

    def __init__(self) -> None:
        self.id = "http://saintgeorgeonabike.eu/annotations/1"
        self.type = "Annotation"
        self.created = "2022-07-10T14:08:07Z"
        self.creator = {
            "type": "Software",
            "name": "SGoaB",
            "homepage": "https://saintgeorgeonabike.eu/"
        }
        self.motivation = "tagging"
        self.body = {
            "type": "TextualBody",
            "value": "{class_label}",
            "language": "{class_label_lang}"
        },
        self.target = {
            "scope": "http://data.europeana.eu/item{eid}",
            "source": "{imagepath}"
        }
