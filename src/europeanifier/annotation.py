#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


# python libraries
import logging
from datetime import datetime
# my modules
from europeanifier.europeana_item import EuropeanaItem
import libaux


def object_detection_2_annotation(europeana_id: str, data: dict) -> list:
    """Converts the output of the object detection to a dictionary that will be readable by the Annotation.loads() class

    Args:
        data (dict): _description_

    Returns:
        list: _description_
    """
    output_annotations = []
    currenttime = datetime.utcnow()

    for i, val in enumerate(data["output"]["object_detection"]["bbx"]):
        output_i = {
            "europeana_id": europeana_id,
            "imagepath": data["img_path"],
            "bbx": data["output"]["object_detection"]["bbx"][i],
            "bbx_score": data["output"]["object_detection"]["scores"][i],
            "class_label": data["output"]["object_detection"]["classes-labels"][i],
            "lang": 'en',
            "datetime": currenttime,
        }
        output_annotations.append(output_i)
    return output_annotations


class AnnotationCollection():

    current_time = None

    def get_annotation_collection_id() -> str:
        # set a unique annotation collection id
        current_time = datetime.utcnow().strftime(
            "%Y%m%d%H%M%S%f")  # time with microseconds
        annotation_id = "http://saintgeorgeonabike.eu/collection/{}/1".format(
            current_time)

        return annotation_id

    def generate_europeana_annotation_collection(annotations: list) -> dict:

        if isinstance(annotations, list):
            annotation = {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "id": AnnotationCollection.get_annotation_collection_id(),
                "type": "AnnotationCollection",
                "total": len(annotations),
                "first": {
                    "id": "http://saintgeorgeonabike.eu/page/1",
                    "type": "AnnotationPage",
                    "startIndex": 0,
                    "items": annotations
                }
            }
            return annotation
        else:
            # do nothing
            logging.warn(
                "generate_europeana_annotation_collection(): annotations is wrong")
            return None


class Annotation():
    annotation_id: str = None
    europeana_id: str = None
    imagepath: str = None
    class_label: str = None
    lang: str = None
    datetime: str = None
    bbx: list = None
    bbx_score: float = None

    def __init__(self, annotation_id: str, europeana_id: str, imagepath: str, bbx: list, bbx_score: float, class_label: str,  lang: str = 'en', datetime: str = None) -> None:
        self.set_annotation_id(datetime)
        self.set_europeana_id(europeana_id)
        self.set_imagepath(imagepath)
        self.set_class_label(class_label)
        self.set_lang(lang)
        self.set_datetime(datetime)
        self.set_bbx(bbx)
        self.set_bbx_score(bbx_score)

    def __init__(self, input: dict) -> None:
        self.set_annotation_id(input["datetime"])
        self.set_europeana_id(input["europeana_id"])
        self.set_imagepath(input["imagepath"])
        self.set_class_label(input["class_label"])
        self.set_lang(input["lang"])
        self.set_datetime(input["datetime"])
        self.set_bbx(input["bbx"])
        self.set_bbx_score(input["bbx_score"])

    def get_annotation_id(self,):
        return self.annotation_id

    def get_europeana_id(self,) -> str:
        return self.europeana_id

    def get_europeana_id_absolute(self) -> str:
        absolute_europeana_id: str = EuropeanaItem(
            self.get_europeana_id()).get_absolute_europeana_id(self.get_europeana_id())
        return absolute_europeana_id

    def get_imagepath(self,) -> str:
        return self.imagepath

    def get_imagepath_and_bbx(self,) -> str:
        output = '{imageURI}#xywh={bbx}'.format(
            imageURI=self.get_imagepath(), bbx=','.join(map(str, self.get_bbx())))
        return output

    def get_class_label(self,) -> str:
        return self.class_label

    def get_lang(self,) -> str:
        return self.lang

    def get_datetime(self,) -> str:
        return self.datetime

    def get_bbx(self,) -> list:
        return self.bbx

    def get_bbx_score(self,) -> float:
        return self.bbx_score

    def set_annotation_id(self, data: str) -> None:
        # set a unique annotation id
        current_time = datetime.utcnow().strftime(
            "%Y%m%d%H%M%S%f")  # time with microseconds
        self.annotation_id = "http://saintgeorgeonabike.eu/annotations/{}/1".format(
            current_time)

    def set_europeana_id(self, data: str) -> None:
        europeana_item = EuropeanaItem(data)
        self.europeana_id: str = europeana_item.get_absolute_europeana_id(data)

        self.europeana_id = data

    def set_imagepath(self, data: str) -> None:
        self.imagepath = data

    def set_class_label(self, data: str) -> None:
        self.class_label = data

    def set_lang(self, data: str) -> None:
        self.lang = data

    def set_datetime(self, data: str) -> None:
        self.datetime = libaux.get_datetime_iso8601(data)

    def set_bbx(self, data: list) -> None:
        self.bbx = data

    def set_bbx_score(self, data: float) -> None:
        self.bbx_score = data

    def loads(self, data: dict) -> dict:

        self.set_europeana_id(data["europeana_id"])
        self.set_imagepath(data["imagepath"])
        self.set_class_label(data["class_label"])
        self.set_lang(data["lang"])
        self.set_datetime(data["datetime"])
        self.set_bbx(data["bbx"])
        self.set_bbx_score(data["bbx_score"])

    def dumping_data(self,) -> dict:
        return self.__dict__

    def generate_europeana_annotation_simple(self,) -> dict:
        simple_keys = ['europeana_id', 'imagepath',
                       'class_label', 'lang', 'datetime', ]

        # check required keys
        if not all([item in self.__dict__.keys() for item in simple_keys]):
            # required information for annotation profiles:
            logging.error(
                "Not all the data necessary attributes for simple annotation")
            pass
        else:

            annotation = {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "id": self.get_annotation_id(),
                "type": "Annotation",
                "created": self.get_datetime(),
                "creator": {
                    "type": "Software",
                    "name": "SGoaB Enrichment Service",
                    "homepage": "https://saintgeorgeonabike.eu/"
                },
                "motivation": "tagging",
                "body": {
                    "type": "TextualBody",
                    "value": self.get_class_label(),
                    "language": self.get_lang(),
                },
                "target": {
                    "scope": self.get_europeana_id_absolute(),
                    "source": self.get_imagepath()
                }
            }
        return annotation

    def generate_europeana_annotation_complete(self,) -> dict:

        complete_keys = ['europeana_id', 'imagepath',
                         'class_label', 'lang', 'datetime', 'bbx', 'bbx_score']

        # check required keys
        if not all([item in self.__dict__.keys() for item in complete_keys]):
            # required information for annotation profiles:
            logging.error(
                "Not all the data necessary attributes for complete annotation")
            pass
        else:

            annotation = {
                "@context": {
                    "@base": "http://www.w3.org/ns/anno.jsonld",
                    "confidenceLevel": "http://www.europeana.eu/schemas/edm/confidenceLevel"
                },
                "id": self.get_annotation_id(),
                "type": "Annotation",
                "created": "2022-07-10T14:08:07Z",
                "creator": {
                    "type": "Software",
                    "name": "SGoaB",
                    "homepage": "https://saintgeorgeonabike.eu/"
                },
                "motivation": "tagging",
                "body": {
                    "type": "TextualBody",
                    "value": self.get_class_label(),
                    "language": self.get_lang(),
                    "confidenceLevel": str(self.get_bbx_score())
                },
                "target": {
                    "scope": self.get_europeana_id_absolute(),
                    "source": self.get_imagepath_and_bbx()
                }
            }
            return annotation
