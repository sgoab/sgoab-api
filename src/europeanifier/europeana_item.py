#!/usr/bin/env python3
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################


import logging
import requests

#######################################
# environment setup - taking variables
# env local / production
#######################################
import os
print(".environment - Before EI - EF_API_URL: {}".format(os.getenv('EF_API_URL')))
print(".environment - Before EI - EF_API_KEY: {}".format(os.getenv('EF_API_KEY')))

EF_API_URL = None
if os.getenv('EF_API_URL') is None:
    EF_API_URL = 'https://api.europeana.eu/record/v2'
else:
    EF_API_URL =os.getenv('EF_API_URL')

EF_API_KEY = None
if os.getenv('EF_API_KEY') is None:
    EF_API_KEY = "api2demo"
else:
    EF_API_KEY =os.getenv('EF_API_KEY')

print(".environment - After EI - EF_API_URL: {}".format(EF_API_URL))
print(".environment - After EI - EF_API_KEY: {}".format(EF_API_KEY))

class EuropeanaItem():

    europeana_id:str = None # set at init

    def __init__(self, eid: str) -> None:
        """Expected a relative or absolute euroepana ID. E.g. 
        -- relative: /15501/000045
        -- absolute: http://data.europeana.eu/item/15501/000045

        Args:
            eid (str): europeana identifier. Expected relative or absolute URI.
        """
        if self.is_europeana_id(eid):
            self.europeana_id = self.get_absolute_europeana_id(eid)
        else:
            logging.warn("Cannot Instantiate EuropeanaItem. Not an europeana item URI:{}".format(eid))
        pass
    @staticmethod
    def is_europeana_id_relative(eid: str) -> bool:
        import re
        # regular expression for europeana ID, relative URI
        pattern_europeana_id_relative = "^(/[a-zA-Z0-9-_.&%]+/[a-zA-Z0-9-_.&+%]+)$"
        regex_pattern_relative = re.compile(
            pattern=pattern_europeana_id_relative)

        if regex_pattern_relative.match(eid):
            # check if path is europeana ID relative (e.g. /15501/004946)
            return True
        else:
            return False

    @staticmethod
    def is_europeana_id_absolute(eid: str) -> bool:
        import re
        # regular expression for europeana ID, absolute URI
        pattern_europeana_id = "^(http(s?)://data.europeana.eu/item{1})(/[a-zA-Z0-9-_.&%]+/[a-zA-Z0-9-_.&+%]+)$"
        regex_pattern = re.compile(pattern=pattern_europeana_id)

        if regex_pattern.match(eid):
            # check if path is europeana ID relative (e.g. http://data.europeana.eu/item/15501/004946)
            return True
        else:
            return False

    @staticmethod
    def is_europeana_id(eid: str) -> bool:
        return EuropeanaItem.is_europeana_id_absolute(eid) or EuropeanaItem.is_europeana_id_relative(eid)

    def get_relative_europeana_id(self, europeana_id: str) -> str:
        if EuropeanaItem.is_europeana_id(europeana_id):
            if EuropeanaItem.is_europeana_id_relative(europeana_id):
                return europeana_id
            else:
                return self.europeana_id.split('europeana.eu/item')[1]
        else:
            # ERROR: "Invalid input, not relative nor absolute Europeana ID"
            logging.info(
                "get_absolute_europeana_id(), not Europeana ID relative URI {}".format(europeana_id))
            return None

    def get_absolute_europeana_id(self, europeana_id: str) -> str:

        if EuropeanaItem.is_europeana_id(europeana_id):
            if EuropeanaItem.is_europeana_id_absolute(europeana_id):
                return europeana_id
            else:
                return "http://data.europeana.eu/item{eid}".format(eid=europeana_id)
        else:
            # ERROR: "Invalid input, not relative nor absolute Europeana ID"
            logging.error(
                "get_absolute_europeana_id(), not Europeana ID absolute URI {}".format(europeana_id))
            return None

    def get_image_EF_URL(self, ef_id: str) -> str:

        relative_EF_ID = self.get_relative_europeana_id(ef_id)
        """
        EF_API_URL = 'https://api.europeana.eu/record/v2'
        EF_API_KEY = "api2demo"
        """
        print("get_image_EF_URL >> "+ EF_API_URL)
        print("get_image_EF_URL >> "+ EF_API_KEY)
        
        
        # generate API GET query URL
        EF_LINK_IMAGE_1 = "/search.json?wskey="
        EF_LINK_IMAGE_2 = "&query=europeana_id:%22"
        EF_LINK_IMAGE_3 = "%22&profile=facets&facet=provider_aggregation_edm_isShownBy&rows=0&f.provider_aggregation_edm_isShownBy.facet.limit=20"
        EF_LINK_IMAGE = "{EF_API_URL}{EF_LINK_IMAGE_1}{EF_API_KEY}{EF_LINK_IMAGE_2}{EF_ID}{EF_LINK_IMAGE_3}".format(
            EF_API_URL=EF_API_URL,EF_LINK_IMAGE_1=EF_LINK_IMAGE_1, EF_API_KEY=EF_API_KEY, EF_LINK_IMAGE_2=EF_LINK_IMAGE_2, EF_ID=relative_EF_ID, EF_LINK_IMAGE_3=EF_LINK_IMAGE_3)

        print("EUROPEANA IMAGE >> "+EF_LINK_IMAGE)

        logging.info("EF Search API query: {}".format(EF_LINK_IMAGE))

        # 1. get the image URL
        resp = requests.get(EF_LINK_IMAGE, stream=True, timeout=5)
        resp_json = resp.json()
        EF_img_url = None
        if "facets" in resp_json.keys():
            EF_img_url = resp_json["facets"][0]["fields"][0]["label"]
            print(EF_img_url)
        else:
            logging.error("BROKEN LINK?: Euroepana ID={}".format(relative_EF_ID))
            pass

        return EF_img_url
