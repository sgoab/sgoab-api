enrichment package
==================

Submodules
----------

enrichment.enrichment module
----------------------------

.. automodule:: enrichment.enrichment
   :members:
   :undoc-members:
   :show-inheritance:

enrichment.enrichmentEF module
------------------------------

.. automodule:: enrichment.enrichmentEF
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: enrichment
   :members:
   :undoc-members:
   :show-inheritance:
