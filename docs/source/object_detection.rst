object\_detection package
=========================

Submodules
----------

object\_detection.service\_interface module
-------------------------------------------

.. automodule:: object_detection.service_interface
   :members:
   :undoc-members:
   :show-inheritance:

object\_detection.terminal module
---------------------------------

.. automodule:: object_detection.terminal
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: object_detection
   :members:
   :undoc-members:
   :show-inheritance:
