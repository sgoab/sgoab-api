#!/bin/bash
#
# Licensing terms and copyright
# This code is protected by the terms and conditions of the CC BY-NC-ND 4.0 license
#
# This module was developed while the author was working at Barcelona Supercomputing Center (BSC-CNS)
#
# Author:  Sergio Mendoza
# Contact: sergio.mendoza@bsc.es
#
# This program is under the license Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
# This license allows reusers to copy and distribute the material in any
# medium or format in unadapted form only, for noncommercial purposes only,
# and only so long as attribution is given to the creator.
#
# You should have received a copy of the CC BY-NC-ND 4.0 License
# along with this program.  If not, see <https://creativecommons.org/about/cclicenses/>.
#
################################################################################

# auto-generate rst files
sphinx-apidoc -f -o ./doc-sphinx src

# auto-generate generate HTML
make html
