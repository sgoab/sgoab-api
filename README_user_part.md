# SGoaB API (BSC)

**This README explains how to use to the SGoaB API:**

The SGoaB is intended to generate metadata enrichments for paintings between s.XII and s.XVIII.
The offered enrichments that can be generated are:
- object detection
- visual relations

## Table of Contents
- [SGoaB API (BSC)](#sgoab-api-bsc)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Objectives](#objectives)
  - [API](#api)
    - [Authentication](#authentication)
    - [API HTTP requests](#api-http-requests)
    - [/api/enrichments endpoint](#apienrichments-endpoint)
      - [Input parameters](#input-parameters)
      - [Request contraints](#request-contraints)
      - [HTTP Request](#http-request)
      - [Curl examples](#curl-examples)
      - [Output](#output)
        - [JSON profile](#json-profile)
        - [EF-ANNOTATION-SIMPLE profile](#ef-annotation-simple-profile)
        - [EF-ANNOTATION-COMPLETE profile](#ef-annotation-complete-profile)
  - [Maintainers](#maintainers)
  - [License](#license)

## Introduction
The objective of the tool is generate enrichment for paintings between s.XII and s.XVIII.
These enrichments include:
- object detection in the painting
- visual relations between the objects detected in the painting

## Objectives
The tool objetives, for a given  painting, are generating metadata enrichments contaning:
- depicted objects in the paintings
- visual relations between the objects
- description generation (TBD)

## API

### Authentication

The client must send this token in the Authorization header when making requests to the endpoints.

If the client doesn't have a token, ask the [mantainers](#maintainers) .


### API HTTP requests

* Server URL: http://growsmarter.bsc.es:8050/api/chosen/service/
  * Remote endpoint: http://growsmarter.bsc.es:8050/api/chosen/service/
  * Local endpoint: http://localhost:5050/api/chosen/service/

| Parameter                        | Required | Description                                                                |
| -------------------------------- | -------- | -------------------------------------------------------------------------- |
| Authorization:Bearer<br>*header* | Required | Authentication token that the server manager will give you.                |
| Content-Type<br>*header*         | Required | Defines the format of the request body.<br> Valid values: application/json |
| Body<br>*body*                   | Required | Defines  Valid values: application/json                                    |

### /api/enrichments endpoint

For a painting, the service returns the enrichments requested.
The enrichments include object-detetion and visual relations.

#### Input parameters

- **Image URI**:
  - type: string
  - Accepted URI:
    - Public image URI
      - Image Formats: [JPG | JPEG | PNG]
      - Accepted protocols: http , https
    - Europeana item absolute URI (europeana ID)
      - e.g. http://data.europeana.eu/item/90402/RP_P_OB_915
    - Europeana item  relative URI (europeana ID)
      - e.g. /90402/RP_P_OB_915

- **Enrichments**: 
  - type: list
  - Accepted enrichemnts:
    - “object-detection”
- **Output Profile**: 
  - type: string
  - Accepted profiles:
    - “JSON”
    - “EF-ANNOTATION-SIMPLE”
    - “EF-ANNOTATION-COMPLETE”
- **Output BBX format** (optional): 
  - type: string
  - Accepted formats:
    - "px" (default)
    - "ratio"


#### Request contraints
- Valid authentication Token in header
- Enrichments cannot be an empty list
- Visual-relations enrichment requires the “JSON” output profile.
- Visual-relations metadata is not compatible with [“EF-ANNOTATIONS-SIMPLE” , “EF-ANNOTATIONS-COMPLETE”] output profiles (yet).
- EF-ANNOTATIONS-SIMPLE output profile requires Europeana ID input item
- EF-ANNOTATIONS-COMPLETE output profile requires Europeana ID inputs item

#### HTTP Request 

```shell
POST /api/enrichment HTTP/1.1
Authorization: Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J
Content-Length: 333
Content-Type: application/json
Host: growsmarter.bsc.es:8050
{
  "item": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
  "enrichments":["object-detection"],
  "output_profile":"JSON"
}
```

#### Curl examples

Internet public image
```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
  '{
    "item": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
    "enrichments":["object-detection"],
    "output_profile":"JSON"
  }' \
 'http://localhost:5050/api/enrichment'
```

Request with bounding boxes with ratio. (The default is bounding boxes by px)
```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
  '{
    "item": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
    "enrichments":["object-detection"],
    "output_profile":"JSON",
    "output_bbx_format":"ratio"
  }' \
 'http://localhost:5050/api/enrichment'
```

Europeana item  (absolute URI)
```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
  '{
  "item": "http://data.europeana.eu/item/15501/003841",
  "enrichments":["object-detection"],
  "output_profile":"EF-ANNOTATION-SIMPLE"
  }' \
 'http://localhost:5050/api/enrichment'
```

Europeana item  (relative URI)
```bash
curl -i -X POST \
   -H "Authorization:Bearer Ep8paH7GKHz9K75G5knles3nTZ0a5J" \
   -H "Content-Type:application/json" \
   -d \
  '{
  "item": "/90402/RP_P_OB_915",
  "enrichments":["object-detection"],
  "output_profile":"EF-ANNOTATION-SIMPLE"
  }' \
 'http://localhost:5050/api/enrichment'
```

#### Output

##### JSON profile

The JSON profile contains a list with the following results for each image in the list: 
- object-detection results:
  - bbx: list of bounding boxes of the objects detected
  - scores: list of scores for each objects detected
  - classes: identifier of the objects detected
  - classes-labels: labels of the objects detected

```json
{
  "queryID": "e91110d3f12b515152fabad660c66af7",
  "img_path": "https://www.museodelprado.es/imagenes/Documentos/imgsem/c4/c4ca/c4cae3b7-0518-4cbc-985a-5b43ed3fc601/950fc66c-dd5a-4c82-b21b-b3f5186bc100.jpg",
  "object_detection": true,
  "visual_relations": false,
  "output": {
      "object_detection": {
          "bbx": [
              [
                  0.601267517,
                  0.392477065,
                  0.68912,
                  0.806616366
              ],
              [
                  0.0504028499,
                  0.704973638,
                  0.323536694,
                  0.947903037
              ]
          ],
          "scores": [
              0.995772302,
              0.994411647
          ],
          "classes": [
              11.0,
              1.0
          ],
          "classes-labels": [
              "book",
              "crucifixion"
          ],
          "img-bbx-path": "/static//object-detection/output/b%27aHR0cHM6Ly93d3cubXVzZW9kZWxwcmFkby5lcy9pbWFnZW5lcy9Eb2N1bWVudG9zL2ltZ3NlbS9jNC9jNGNhL2M0Y2FlM2I3LTA1MTgtNGNiYy05ODVhLTViNDNlZDNmYzYwMS85NTBmYzY2Yy1kZDVhLTRjODItYjIxYi1iM2Y1MTg2YmMxMDAuanBn%27.jpg"
      }
  }
}

```

##### EF-ANNOTATION-SIMPLE profile
```json
{
    "@context": "http://www.w3.org/ns/anno.jsonld",
    "id": "http://saintgeorgeonabike.eu/collection/20221108184823248381/1",
    "total": 8,
    "type": "AnnotationCollection",
    "first": {
        "id": "http://saintgeorgeonabike.eu/page/1",
        "startIndex": 0,
        "type": "AnnotationPage",
        "items": [
            {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "body": {
                    "language": "en",
                    "type": "TextualBody",
                    "value": "person"
                },
                "created": "2022-11-08T18:48:23Z",
                "creator": {
                    "homepage": "https://saintgeorgeonabike.eu/",
                    "name": "SGoaB Enrichment Service",
                    "type": "Software"
                },
                "id": "http://saintgeorgeonabike.eu/annotations/20221108184823247902/1",
                "motivation": "tagging",
                "target": {
                    "scope": "http://data.europeana.eu/item/15501/003841",
                    "source": "https://realonline.imareal.sbg.ac.at/imageservice/kupo/DA005093"
                },
                "type": "Annotation"
            },
            ...
            ...
            ...
        ],
    },
}

```

##### EF-ANNOTATION-COMPLETE profile
```json
{
    "@context": "http://www.w3.org/ns/anno.jsonld",
    "id": "http://saintgeorgeonabike.eu/collection/20221108181026006609/1",
    "total": 8,
    "type": "AnnotationCollection",
    "first": {
        "id": "http://saintgeorgeonabike.eu/page/1",
        "startIndex": 0,
        "type": "AnnotationPage",
        "items": [
            {
                "@context": {
                    "@base": "http://www.w3.org/ns/anno.jsonld",
                    "confidenceLevel": "http://www.europeana.eu/schemas/edm/confidenceLevel"
                },
                "body": {
                    "confidenceLevel": "0.99714762",
                    "language": "en",
                    "type": "TextualBody",
                    "value": "person"
                },
                "created": "2022-07-10T14:08:07Z",
                "creator": {
                    "homepage": "https://saintgeorgeonabike.eu/",
                    "name": "SGoaB",
                    "type": "Software"
                },
                "id": "http://saintgeorgeonabike.eu/annotations/20221108181026005758/1",
                "motivation": "tagging",
                "target": {
                    "scope": "http://data.europeana.eu/item/90402/RP_P_OB_915",
                    "source": "https://lh3.googleusercontent.com/vxPRjGcsAJX9_36hwJL7zxyE3PQsZsVyTudvikaM7Msj76Cc4K-ITmTxZFG8osxtHvmiKRiORzT32WIZZ6itU1TwfZM=s0#xywh=203,723,395,514"
                },
                "type": "Annotation"
            },
            ...
            ...
            ...
        ]
    }
}
```




## Maintainers

| Email address                                    |
| ------------------------------------------------ |
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es) |
| [SGOAB project] (mailto:sgoab-contact@bsc.es)    |


## License

[SGoaB License](LICENSE.md)